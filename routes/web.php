<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Honeypot;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// ----------- migration vers le CMS -------------
//Si on est sur un auto hébergé on affiche la page locale sinon c'est le CMS
if (config('app.env') != 'prod') {
  Route::get('/',                 'Auth\LoginController@showIndex')->name('webHome');
} else {
  Route::get('/',                 'HomeController@redirectPage')->name('webHome');
}
Route::get('/tarifs',             'HomeController@redirectPage')->name('webTarifs');
Route::get('/application',        'HomeController@redirectPage')->name('webApplication');
Route::get('/application-details', 'HomeController@redirectPage')->name('webApplicationDetails');
Route::get('/documentation',      'HomeController@redirectPage')->name('webDocumentation');
Route::get('/serveur',            'HomeController@redirectPage')->name('webServeur');
Route::get('/webservices',        'HomeController@redirectPage')->name('webWebServices');
Route::get('/revendeurs',         'HomeController@redirectPage')->name('webPartenairesRevendeurs');
Route::get('/experts-comptables', 'HomeController@redirectPage')->name('webPartenairesExpertsComptables');
Route::get('/entreprises',        'HomeController@redirectPage')->name('webPartenairesEntreprises');
Route::get('/independants',       'HomeController@redirectPage')->name('webPartenairesIndependants');
Route::get('/a-propos',           'HomeController@redirectPage')->name('webApropos');
Route::get('/mentions-legales',   'HomeController@redirectPage')->name('webMentionsLegales');
Route::get('/rgpd',               'HomeController@redirectPage')->name('webRGPD');
Route::get('/cgu',                'HomeController@redirectPage')->name('webCGU');
Route::get('/contact',            'HomeController@redirectPage')->name('webContact');
// -------- end migration vers le CMS -------------
//Redirect vers les factures
Route::get('/webInvoices',        'HomeController@redirectInvoices')->name('webLinkToInvoices');

//Un header utilisable à distance pour avoir le même entre CMS et le reste
Route::get('/header',        'HomeController@showPage')->name('webHeader');
Route::get('/footer',        'HomeController@showPage')->name('webFooter');

// Route::get('/application',        'HomeController@showPage')->name('webApplication');
// Route::get('/a-propos',           'HomeController@showPage')->name('webApropos');

Route::get('/webLogin/{email?}',  'Auth\LoginController@showLoginForm')->name("webLogin");
Route::post('/webLogin',          'Auth\LoginController@webLogin');
Route::get('/admindoli/login',    'Auth\LoginController@showLoginForm')->name('code16.sharp.login');
Route::get('/webRegister',        'Auth\RegisterController@showRegistrationForm')->name('webRegister');
Route::post('/webRegister',       'Auth\RegisterController@webRegister')->name('webRegisterPost');
Route::post('/webLeave',          'Auth\LoginController@webLeaveImpersonate')->name("webLeaveImpersonate");

//mot de passe oublié
Route::get('password/reset/{token}',  'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::get('password/reset',          'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/reset',         'Auth\ResetPasswordController@reset')->name('password.update');
Route::post('password/email',         'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');


//La mise à jour automatique via git
Route::post('/gitUpdate',   'DeployController@deploy');

//Le robots.txt
Route::get('robots.txt', 'RobotsController');

//Tout ce qui suit est "protégé"
Route::group(['middleware' => ['auth']], function () {
  Route::get('/home',       'HomeController@index')->name('home');
  Route::get('/webConfig',  'ConfigurationController@webShowConfigForm')->name('webConfig');
  Route::post('/webConfig', 'ConfigurationController@webUpdateConfig')->name('webConfigPost');

  // Route de génération de code d'authentification
  Route::get('/redirect', 'PassportController@redirect')->name('passport.redirect');

  // Routes de génération de tokens
  Route::get('/token', 'PassportController@tokenForm')->name('passport.token.form');
  Route::get('/callback', 'PassportController@callbackForm')->name('passport.callback');
  Route::post('/callback', 'PassportController@callbackStore');

  // Routes de rafraichissement de tokens
  Route::get('/refresh', 'PassportController@refreshForm')->name('passport.refresh');
  Route::post('/refresh', 'PassportController@refreshStore');

  // Routes de création de tokens d'accès personnel
  // Route::get('/personal-access', 'PassportController@personalAccessForm');
  // Route::post('/personal-access', 'PassportController@personalAccessStore');

  // Route::resource('configuration', 'ConfigurationController');
  Route::get('/user/qrCodeForApp/{lenom}',  'UserController@userQrCodeForApp')->name("userQrCodeForApp");

  Route::get('/customizingApp/logo/{id}/{name}',  'CustomizingAppController@webLogo')->name("customizingApp.logo");

  Route::get('/webIK',            'WebIKController@index')->name("webIK.insert");
  Route::post('/webIK',           'WebIKController@store')->name("webIK.save");
  Route::get('/webIKparts/{nb}/{slug?}/{ville?}/{uid?}',  'WebIKController@part')->name("webIK.part");


  //résolution adresses et routage
  Route::get('/webGeo/{d}',                              'WebIKController@ville');
  Route::get('/webGeoDistance/{d}/{a}',                  'WebIKController@distance');
});

//Bien qu'on soit en dehors du middleware auth, voir la doc et le __construct :)
//Solution inspiree de https://stackoverflow.com/questions/28562908/how-to-deal-with-private-images-in-laravel-5
Route::get('/ldfImages/{lenom}/{image}',  'LdeFraisImagesController')->where(['file_name' => '*.jpeg'])->name("ldfImages");


//Accès direct via un deep-link pour faciliter la récupération dans les mails
//TODO ajouter une date de péremption ? + message "vous essayez de télécharger une ressource ancienne, connectez vous à l'interface web pour ça" ?
Route::get('/cdf/{hash}',                 'CdeFraisController')->name("cdefraisDeepDownload");
Route::get('/ndf/{hash}',                 'NdeFraisController')->name("ndefraisDeepDownload");
Route::get('/Backups/{hash}',             'UserController')->name("userDeepDownload");

Route::impersonate();

//Le pot de miel
Route::get('{path}', 'Honeypot')->where('path', '.*');
Route::post('{path}', 'Honeypot')->where('path', '.*');
