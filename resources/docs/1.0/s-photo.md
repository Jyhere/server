# DoliSCAN : l'Application sur Smartphone

---

## Bien découper la photo ...

Lorsque vous prennez une photo de votre justificatif l'application vous permet de faire un découpage propre pour ne pas avoir ce qui entoure le justificatif.

Par exemple voici une photo "brute" qu'il faudrait éviter d'envoyer sur nos serveurs :

![Pas de découpage](images/app/crop-ko.jpg)

Et voici ce qu'il vaudrait mieux envoyer :

![Découpage correct](images/app/crop-ok.jpg)

Pour quelle raison ? Par souci écologique en particulier : dans le permier cas quasiment la moitié de la photo ne sert à rien, vous allez donc envoyer sur le réseau des Mo de données inutiles, elles seront ensuite stockées pour rien… Et si vous avez pris l'option "archive probante" c'est stocké pour dix ans… On parle d'un ratio de 1 pour 20 pour les cas les plus extrêmes !

## Prendre un justificatif en photo

L'application DoliSCAN vous permet de photographier vos justificatifs.

> {info} Cette étape est *IMPORTANTE* et vous devez donc prendre un soin tout particulier à la prise de vue de votre document.

Exemple ci-dessous avec un téléphone Android:

![Parking](images/app/parking-02.jpg)

Une fois la photo prise, DoliSCAN bascule en mode "édition de photo" pour vous permettre de découper la partie importante de celle-ci (uniquement le ticket) et éventuellement de la pivoter pour qu'elle soit directement lisible.

![Parking](images/app/parking-03.jpg) ![Parking](images/app/parking-04.jpg)

Une fois le découpage effectué (et pivotement éventuel), appuyez sur le bouton "redimensionner" pour revenir dans l'interface principale du logiciel.

![Parking](images/app/parking-05.jpg)

> {success} La photo est maintenant intégrée à la création en cours, vous pouvez continuer la saisie.

