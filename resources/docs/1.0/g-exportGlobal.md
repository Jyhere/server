# DoliSCAN : Export global des notes de frais


## Généralités

Lorsque vous avez plusieurs salarié⋅es qui utilisent DoliSCAN vous pouvez avoir envie de gagner encore plus de temps en fin de période pour importer les écritures comptables d'un seul coup. Cette option sert à ça.

Au moment de l'export des notes de frais, chaque salarié reçoit ses fichiers personnels et le responsable de l'entreprise recevra un regroupement de toutes les notes de frais.

> {info} L'export global est intéressant si vous avez plusieurs salariés.
