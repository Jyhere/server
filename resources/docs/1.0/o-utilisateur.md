# DoliSCAN : le backend utilisable sur un ordinateur

---

- [DoliSCAN sur ordinateur](#section-1)

<a name="section-1"></a>
## Connexion

Connectez-vous sur votre serveur DoliSCAN, pour connaitre son adresse "web" regardez dans l'application sur votre smartphone :

![Menu droite](images/app/menu-droite-01.jpg)


<a name="section-2"></a>
## L'interface


<a name="section-3"></a>
## Vos notes de frais


<a name="section-4"></a>
## Export des notes de frais au format PDF


<a name="section-6"></a>
## Envoi automatique au comptable !=)


<a name="section-7"></a>
## Opérations spéciales

<a name="section-5"></a>
## Téléchargement de la totalité de vos données
