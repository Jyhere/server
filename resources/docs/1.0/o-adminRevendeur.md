# DoliSCAN : le rôle de l'administrateur revendeur

---

- [Profil utilisateur adminRevendeur](#section-1)

<a name="section-1"></a>
## Connexion

Le revendeur de DoliSCAN dispose des outils de gestion qui lui permettent de créer des entreprises (clients) et des utilisateurs affectés à ces entreprises.

Le revendeur peut être un cabinet d'expertise comptable (qui crée alors les comptes de ses clients) ou un professionnel qui souhaite renvendre la solution DoliSCAN.

L'adresse de connexion est <a href="{{ Config::get('app.url')}}">{{ Config::get('app.url')}}</a>

![Page d'accueil](images/web/doliscan-accueil-01.jpg) ![Authentification](images/web/doliscan-auth-01.jpg) ![Accueil une fois authentifié](images/web/doliscan-accueil-auth_ok.jpg)

<a name="section-2"></a>
## L'interface

Une fois connecté vous pourrez aller sur le "backend" réservé aux revendeurs. Via cette interface vous pourrez gérer vos clients (entreprises) et leurs utilisateurs pour qu'ils puissent utiliser l'application sur leur smartphone. Cliquez sur le lien "Accéder aux outils pour les Administrateurs":


![Backend](images/web/doliscan-backend-01.jpg)


### Description de l'interface d'administration

Le menu de gauche vous permet de naviguer entre les différents modules d'administration (cette liste évoluera probablement en fonction des développements à venir) :
- Tableau de bord vous permet de voir en un coup d'œil vos principaux indicateurs
- Logs (réservé au super administrateur) : pour accéder aux journaux systèmes de l'application
- Mon compte : vous permet de modifier quelques informations de votre compte
- Préconfiguration : vous permet de choisir quels seront les paramètres par défaut à appliquer à tous les utilisateurs que vous aller créer par la suite
- Entreprises : gestion de vos entreprises
- Utilisateurs : gestion de vos utilisateurs
- Mes notes de frais : normalement désactivé pour les administrateurs, ce menu permet d'aller consulter / modifier vos notes de frais
- Retour : retour sur la page d'accueil de DoliSCAN
- Aide : affiche cette aide

## Création d'entreprises et d'utilisateurs

Il est conseillé de créer l'entreprise cliente puis l'administrateur de cette entreprise, lui affecter les droits d'administration et ensuite réouvrir la session avec ce compte administrateur restreint, configurer ses paramètres par défaut et ensuite créer tous les autres comptes utilisateurs de l'entreprise.

Cette démarche permet de "cloisonner" les créations de comptes et de gérer proprement les droits d'accès et les paramètres par défaut à appliquer à chaque utilisateur.

## Création d'entreprises

Lors de la création d'une entreprise, si vous saisissez son numéro SIREN la base nationale des entreprises est alors questionnée pour compléter automatiquement les informations.

![Liste des entreprises](images/web/doliscan-backend-entreprises-listing-01.jpg) ![Ajout d'une entreprise - étape 1](images/web/doliscan-backend-entreprises-add-01.jpg) ![Ajout d'une entreprise - étape 2](images/web/doliscan-backend-entreprises-add-02.jpg) ![Ajout d'une entreprise - étape 3](images/web/doliscan-backend-entreprises-add-03.jpg)



## Création d'utilisateurs

La création d'un utilisateur permet de paramétrer un certain nombre d'éléments :
- Son rôle principal [voir ici pour les détails](/{{route}}/{{version}}/g-roles)
- Son mot de passe, que vous pouvez laisser vide, un lien spécial lui permettra par la suite de choisir lui-même son mot de passe lorsque vous lui enverrez une invitation
- Activer le ["mode simplifié"](/{{route}}/{{version}}/g-modeSimple) (attention aux conséquences, nous vous conseillons de ne pas activer ce mode)
- Configurer la comptabilité, c'est là que vous allez pouvoir :
  - Choisir à qui envoyer en copie tous les courriels concernant cet utilisateur (son secrétariat, par exemple, dans le cadre d'un dirigeant d'entreprise)
  - Renseigner l'adresse du service comptable à qui il faudra envoyer automatiquement les informations comptables en fin de mois (contact au sein du cabinet d'expertise comptable par exemple)
- Les différents codes comptables correspondant au plan comptable de l'entreprise
- Enfin vous pouvez associer cet utilisateur à une entreprise et lui affecter un rôle.

Note : Les paramètres par défauts renseignés dans le menu "Préconfiguration" sont appliqués lorsque vous ajoutez un nouvel utilisateur.

![Liste des utilisateurs](images/web/doliscan-backend-users-listing-01.jpg) ![Ajout d'un utilisateur - 1](images/web/doliscan-backend-users-add-01.jpg) ![Ajout d'un utilisateur - 2](images/web/doliscan-backend-users-add-02.jpg) ![Ajout d'un utilisateur - 3](images/web/doliscan-backend-users-add-03.jpg)


### Envoyer un lien d'invitation à un utilisateur


Lorsque le compte utilisateur est créé, au lieu de lui envoyer "en clair" le mot de passe, utilisez plutôt le lien "Envoyer une invitation" qui aura pour conséquence d'envoyer un courriel à cet utilisateur avec un lien lui permettant de définir lui-même son mot de passe.

![Envoyer une invitation](images/web/doliscan-backend-users-invitation-01.jpg)
