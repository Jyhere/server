# DoliSCAN : l'Application sur Smartphone

---

<a name="section-1"></a>
## Export du tableau récap en fin de mois

En fin de mois, vous recevrez par courrier électronique des notifications concernant la proche clôture de votre note de frais. À tout moment vous pouvez exporter votre note de frais au format PDF en suivant la procédure suivante :

Ouvrez le menu de votre application en cliquant sur l'icone suivante en haut à droite puis choisissez "Historique":

![Ouverture du menu](images/app/menu-evidence_menu.jpg) ![Menu ouvert](images/app/menu-sous_menu_ouvert.jpg)

Une fois le menu Historique ouvert, vous pourrez consulter toutes vos anciennes notes de frais.

![Historique](images/app/historique-01.jpg)

Choisissez celle que vous voulez, puis déplacez-vous tout en bas et appuyez sur le bouton "Télécharger le PDF"…

![Historique](images/app/historique-02.jpg) ![Historique](images/app/historique-03.jpg) ![Historique](images/app/historique-04.jpg)


<a name="section-6"></a>
## Export de tous les justificatifs

Pour télécharger tous vos justificatifs, vous devez ouvrir le menu de votre application en cliquant sur l'icone en haut à droite depuis la page principale, puis choisissez "Historique".

Sélectionnez la note de frais que vous voulez et choisissez ensuite "Justificatif PDF" tout en bas de la liste des frais du mois.


<a name="section-7"></a>
## Envoi automatique au comptable !=)

Si vous voulez que votre comptable, ou le cabinet d'experise comptable, reçoive automatiquement les documents en question tous les mois, vous pouvez configurer l'adresse courriel de destination via le menu "Mon Compte" et compléter l'adresse "Envoyer les notes de frais en copie à :"

![Configuration, Mon Compte](images/app/moncompte-01.jpg)

Note : Cette configuration n'est pas nécessaire si DoliSCAN vous a été pré-configuré par votre expert comptable.

<a name="section-8"></a>
## Opérations spéciales

D'autres opérations sont possible en passant directement par le "backoffice" web de l'application. Cette interface est détaillée dans la rubrique "Sur Ordinateur" de la documentation.
