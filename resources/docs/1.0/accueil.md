# Présentation générale

- [DoliSCAN](#section-1)

<a name="section-1"></a>
## DoliSCAN : Une application pour dématérialiser vos notes de frais

L'objectif principal est de vous éviter d'avoir à passer des heures tous les mois à scotcher vos facturettes sur des feuilles A4 et calculer le total de vos frais professionnels en se demandant toujours où vous avez pu ranger le justificatif 38 que vous avez eu entre les mains "la semaine dernière"… 🦊

Une contrainte : avoir un smartphone et prendre une photo de qualité du justificatif le plus tôt possible (le jour même de l'établissement du ticket).

<a name="section-2"></a>
## Une méthode pour faciliter le calcul des Indemnités Kilométriques

Des algorithmes de calculs sont mis en œuvre au travers de DoliSCAN pour prendre en compte les spécificités de la règlementation fiscale en vigueur (différents taux de remboursement selon le véhicule utilisé et les éventuels effets rétroactifs des changements de barème, et aussi la gestion des multivéhicules).

Notre serveur de calcul d'itinéraire vous propose une aide lorsque vous devez entrer la distance parcourue. Sans être bloquant, il vous indiquera par exemple qu'entre Bordeaux et Toulouse il y a environ 242k m (mais vous seul⋅e connaissez réellement la distance parcourue, en particulier s'il y a eu des déviations, des travaux, des accidents, des étapes ou des déplacements complémentaires).

<a name="section-3"></a>
## Un export de données pour votre service comptabilité ou expert-comptable

Votre service comptabilité ou expert-comptable peut recevoir votre note de frais récapitulative mensuelle automatiquement par courriel avec les justificatifs, accompagnée d'un fichier FEC (Fichier des Écritures Comptables) que l'on peut importer directement dans un logiciel métier.

<!-- <a name="section-4"></a>
## Et tout ceci propulsé par du code source libre de A à Z

La licence appliquée est la GNU/GPL ou Affero GPL pour les services webs mis en jeu. -->
