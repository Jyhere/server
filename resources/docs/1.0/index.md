- ## Généralités
    - [Accueil](/{{route}}/{{version}}/accueil)
    - [Utilisateurs et rôles](/{{route}}/{{version}}/g-roles)
    - [Le mode simplifié](/{{route}}/{{version}}/g-modeSimple)

- ## Sur smartphone
    - [Installation](/{{route}}/{{version}}/s-installation)
    - [Configuration](/{{route}}/{{version}}/s-premierlancement)
    - [Utilisation](/{{route}}/{{version}}/s-utilisation)
    - [Photo d'un justificatif](/{{route}}/{{version}}/s-photo)
    - [Validation mensuelle](/{{route}}/{{version}}/s-validation)
    - [Nouvelles versions](/{{route}}/{{version}}/s-updates)
    <!-- - [Exports de fin de mois](/{{route}}/{{version}}/s-exportsnotedefrais) -->
    <!-- - [Problèmes](/{{route}}/{{version}}/s-problemes) -->

- ## Sur Ordinateur
    - [Accueil](/{{route}}/{{version}}/o-accueil)

- ## Utilisation avancée
    - [Utilisateur normal](/{{route}}/{{version}}/o-utilisateur)
    - [Correcteur / Validateur](/{{route}}/{{version}}/o-correcteur)
    - [Service RH ou Comptabilité](/{{route}}/{{version}}/o-serviceComptabilite)
    - [Responsable d'entreprise](/{{route}}/{{version}}/o-responsableEntreprise)
    - [L'administrateur de l'entreprise (le DSI)](/{{route}}/{{version}}/o-adminEntreprise)
    - [Revendeur et Administrateurs](/{{route}}/{{version}}/o-adminRevendeur)


- ## Côté serveur
