@extends('layouts.app')

@section('content')
<div class="container">
  <div class="justify-content-center">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">Détail de la note de frais</div>

        <div class="card-body">
          @if (session('status'))
          <div class="alert alert-success" role="alert">
            {{ session('status') }}
          </div>
          @endif

          @include('webdetailssubpart', ['recap' => 'pour remboursement', 'persopro' => 'personnel', 'typeFrais' => $typeFrais, 'lignes' => $lignes ])

          <hr />
          <hr />

          @include('webdetailssubpart', ['recap' => '', 'persopro' => 'professionnel', 'typeFrais' => $typeFraispro, 'lignes' => $lignespro ])

          <hr>
          <h4>Opérations possibles:</h4>

          <ul>
            @if ($status == 1)
            <li><a href="{{ route('webCloseNdF', ['id' => $id]) }}">Clôturer cette note de frais pour pouvoir l'exporter</a></li>
            @else
            <li>Exporter les écritures pour Dolibarr</li>
            <li>Exporter les écritures pour Quadratus</li>
            <li><a href="{{ route('webBuildPDF', ['id' => $id]) }}">Imprimer cette note de frais</a></li>
            <li><a href="{{ route('webBuildPDFJustificatifs', ['id' => $id]) }}">Télécharger les justificatifs</a></li>
            @endif
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>

@endsection
