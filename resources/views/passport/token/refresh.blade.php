@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __("Rafraichissement de jeton d'accès") }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('passport.refresh') }}" class="px-3">
                        @csrf

                        <div class="form-group row">
                            <label for="refresh_token">{{ __('Jeton de rafraichissement') }}</label>
                            <textarea class="form-control" name="refresh_token" rows="8">{{ old('refresh_token') }}</textarea>
                            @if ($errors->has('refresh_token'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('refresh_token') }}</strong>
                            </span>
                            @endif
                        </div>

                        <div class="form-group row">
                            <label for="client_id">{{ __('Application') }}</label>

                            <select id="client_id" type="" class="form-control{{ $errors->has('client_id') ? ' is-invalid' : '' }}" name="client_id" required autofocus>
                                <option value=""></option>
                                @foreach ($clients as $client)
                                    <option value="{{ $client->id }}" @if(old('client_id') == $client->id) selected @endif>{{ $client->name }}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('client_id'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('client_id') }}</strong>
                            </span>
                            @endif
                        </div>

                        <div class="form-group row">
                            <label for="client_secret">{{ __('Mot de passe') }}</label>
                            <textarea class="form-control" name="client_secret" rows="2">{{ old('client_secret') }}</textarea>
                            @if ($errors->has('client_secret'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('client_secret') }}</strong>
                            </span>
                            @endif
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Valider') }}
                                </button>

                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
