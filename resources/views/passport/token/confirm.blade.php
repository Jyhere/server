@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __("Gérénation d'un jeton : valider") }}</div>

                <div class="card-body">
                    <form method="POST" action="">
                        @csrf

                        <div class="form-group row">
                            <label for="state" class="col-md-4 col-form-label text-md-right">{!! __('Code de session') !!}</label>

                            <div class="col-md-6">
                                <textarea class="form-control" disabled rows="2">{{ $state }}</textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="client_id" class="col-md-4 col-form-label text-md-right">{{ __('Application') }}</label>

                            <div class="col-md-6">
                                <input type="text" class="form-control" disabled value="{{ $clientName }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="client_secret" class="col-md-4 col-form-label text-md-right">{{ __('Mot de passe') }}</label>

                            <div class="col-md-6">
                                <textarea class="form-control{{ $errors->has('client_secret') ? ' is-invalid' : '' }}" name="client_secret" rows="2"></textarea>
                                @if ($errors->has('client_secret'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('client_secret') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        {{-- Code d'authentification généré par OAuth2, code de session et id du client pour lequel on demande un jeton --}}
                        <input type="hidden" name="code" value="{{ $code }}">
                        <input type="hidden" name="state" value="{{ $state }}">
                        <input type="hidden" name="client_id" value="{{ $clientId }}">

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Valider') }}
                                </button>

                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
