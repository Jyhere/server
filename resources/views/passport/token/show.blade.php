@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">{{ __('Vos jetons') }}</div>

        <div class="card-body">
            <div class="form-group row px-3">
                <label for="access_token">{!! __("Jeton d'accès (<span class='text-danger'>expire dans $expiresAccessToken jours</span>)") !!}</label>
                <textarea class="form-control" disabled rows="10">{{ $accessToken }}</textarea>
            </div>
            <div class="form-group row px-3">
                <label for="refresh_token">{!! __("Jeton de rechargement (<span class='text-danger'>expire dans $expiresRefreshToken jours</span>)") !!}</label>
                <textarea class="form-control" disabled rows="5">{{ $refreshToken }}</textarea>
            </div>
        </div>
    </div>
</div>
@endsection
