<h1>Frais payés par un moyen de paiement {{ $persopro }}</h1>

@php($totalHT = 0)
@php($totalTVA = 0)
@php($totalTTC = 0)
<table>
    <thead>
        <tr>
            <th class="th10nb" class="date">Date</th>
            @if($persopro == "professionnel")
            <th class="th80">Résumé</th>
            <th class="th10nb">Moyen</th>
            @else
            <th class="th80" colspan="2">Résumé</th>
            @endif

            <th class="th10nb">HT<br /><span style="font-size:6pt; font-style: italic; font-weight: normal;">si TVA récupérable</span></th>
            <th class="th10nb">TVA<br /><span style="font-size:6pt; font-style: italic; font-weight: normal;">récupérable</span></th>
            <th class="th10nb">TTC</th>
        </tr>
    </thead>

    <tbody>
        @foreach($typeFrais as $type)

        @if($persopro == "professionnel")
        <tr>
            <td colspan="2" class="tdh2">
                <h2>{{ $type->label }}</h2>
            </td>
            <td class="tdh2">
                &nbsp;
            </td>
            <td class="tdh2">
                &nbsp;
            </td>
            <td class="tdh2">
                &nbsp;
            </td>
            <td class="tdh2">
                &nbsp;
            </td>
        </tr>
        @else
        <tr>
            <td colspan="3" class="tdh2">
                <h2>{{ $type->label }}</h2>
            </td>
            <td class="tdh2">
                &nbsp;
            </td>
            <td class="tdh2">
                &nbsp;
            </td>
            <td class="tdh2">
                &nbsp;
            </td>
        </tr>
        @endif

        @foreach($lignes[$type->id] as $ligne)
        @php($totalHT += $ligne->ht)
        @php($totalTVA += $ligne->totalTVA)
        @php($totalTTC += $ligne->ttc)
        <tr>
            <td class="td10nb date">{{ datefr($ligne->ladate) }}</td>

            @if($persopro == "professionnel")
            <td class="td80">  {!! wordwrap($ligne->getResume(),110,"<br />") !!} </td>
            <td class="td10nb moyen"> {{ $ligne->moyenPaiement()->first()->label }} </td>
            @else
            <td class="td80" colspan="2">{!! wordwrap($ligne->getResume(),110,"<br />") !!}</td>
            @endif

            @if($ligne->ht > 0)
            <td class="td10nb">{{ nbFR($ligne->ht) }}&nbsp;€</td>
            <td class="td10nb">{{ nbFR($ligne->totalTVA) }}&nbsp;€</td>
            @else
            <td class="tdvide"> </td>
            <td class="tdvide"> </td>
            @endif

            <td class="td10nb">{{ nbFR($ligne->ttc) }}&nbsp;€</td>
        </tr>
        @endforeach

        <tr>
            @if($persopro == "personnel")
            <td class="tdtotalleft" colspan="3">Total {{ $type->label }}</td>
            @else
            <td class="tdtotalleft" colspan="2">Total {{ $type->label }}</td>
            <td class="tdtotalnb"></td>
            @endif
            @if($totalHT > 0)
            <td class="tdtotalnb">{{ nbFR($totalHT) }}&nbsp;€</td>
            <td class="tdtotalnb">{{ nbFR($totalTVA) }}&nbsp;€</td>
            @else
            <td class="tdtotalvide"> </td>
            <td class="tdtotalvide"> </td>
            @endif
            <td class="tdtotalnb">{{ nbFR($totalTTC) }}&nbsp;€</td>
        </tr>
        @endforeach        
    </tbody>
</table>

@if($persopro == "personnel")
<table>
    <thead>
        <tr>
            <th class="th10nb" class="date">Date</th>
            <th class="th80" colspan="3">Résumé</th>
            <th class="th10nb">Distance</th>
            <th class="th10nb">Montant</th>
        </tr>
    </thead>

    <tbody>
    @include('webprintsubpartIK', ['recap' => 'pour remboursement', 'persopro' => 'personnel', 'typeFrais' => $typeFrais, 'lignes' => $lignes, 'utilisationDoliScanAnneeComplete' => $utilisationDoliScanAnneeComplete ])
    </tbody>
</table>
@endif


<br />
<table style="page-break-inside: avoid;">
    <thead>
        <tr>
            <th colspan="4">
                <h2>Récapitulatif {{ $recap }}</h2>
            </th>
        </tr>
        <tr>
            <td class="td80"> </td>
            <th class="th10nb">HT</th>
            <th class="th10nb">TVA<br /><span style="font-size:8pt; font-style: italic; font-weight: normal;">récupérable</span></th>
            <th class="th10nb">TTC</th>
        </tr>
    </thead>
    <tbody>

        @php($grandTotalHT = 0)
        @php($grandTotalTVA = 0)
        @php($grandTotalTTC = 0)

        @foreach($typeFrais as $type)
        @php($totalHT = 0)
        @php($totalTVA = 0)
        @php($totalTTC = 0)
        @foreach($lignes[$type->id] as $ligne)
        @php($totalHT += $ligne->ht)
        @php($totalTVA += $ligne->totalTVA)
        @php($totalTTC += $ligne->ttc)
        @endforeach

        <tr>
            <td class="td80"><i>Total {{ $type->label }}</i> </td>
            @if($totalHT > 0)
            <td class="td10nb">{{ nbFR($totalHT) }} &nbsp;€</td>
            <td class="td10nb">{{ nbFR($totalTVA) }} &nbsp;€</td>
            @else
            <td class="td10nb"></td>
            <td class="td10nb"></td>
            @endif
            <td class="td10nb">{{ nbFR($totalTTC) }}&nbsp;€</td>
        </tr>

        @php($grandTotalHT += $totalHT)
        @php($grandTotalTVA += $totalTVA)
        @php($grandTotalTTC += $totalTTC)
        @endforeach

        {{-- Les IK on a que du "ttc" --}}
        @if($persopro == "personnel")
        @foreach($vehicules as $vehicule)

        @php($totalTTC = 0)
        @foreach($lignesIK[$vehicule]["listeIKMonth"] as $ligne)
        @php($totalTTC += $ligne->ttc)
        @endforeach
        @if($lignesIK[$vehicule]["changeTranche"] && ($lignesIK[$vehicule]["KMavantDoli"] == 0))
        @php($totalTTC += $lignesIK[$vehicule]["MontantTrancheRegule"])
        @endif

        <tr>
            <td class="td80"><i>Total IK pour {{ $lignesIK[$vehicule]["nom"] }} - {{ $lignesIK[$vehicule]["cv"] }}</i> </td>
            <td class="td10nb"> </td>
            <td class="td10nb"> </td>
            <td class="td10nb">{{ nbFR($totalTTC) }}&nbsp;€</td>
        </tr>

        @php($grandTotalTTC += $totalTTC)
        @endforeach
        @endif

        <tr>
            <td class="tdtotalleft">Total global
                ({{ $persopro }})
            </td>
            <td class="tdtotalnb">
                {{-- {{ nbFR($grandTotalHT) }}&nbsp;€ --}}
            </td>
            <td class="tdtotalnb">
                {{ nbFR($grandTotalTVA) }}&nbsp;€
            </td>
            <td class="tdtotalnb">
                {{ nbFR($grandTotalTTC) }}&nbsp;€
            </td>
        </tr>
    </tbody>
</table>
