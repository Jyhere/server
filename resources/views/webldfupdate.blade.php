@extends('layouts.app')


@section('content')
<script language="JavaScript">
    function gotoPrev(){
  window.location.href='';
}
function gotoNext(){
  window.location.href='';
}
</script>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Modification d'une ligne de la note de frais</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    <div class="col-md-12">
                        <form class="form-horizontal" method="post" action="{{ route('webLdFUpdate', ['id' => $ldf->sid]) }}">
                            @csrf
                            <div class="form-row">
                                <div class="col-md-6">
                                    @if($ldf->fileName != "")
                                    <div id="justificatif">
                                        <div id="justificatifheader">Justificatif</div>
                                        <a href="{{ route('ldfImages', ['email' => $userEmail, 'id' => $ldf->fileName]) }}" onclick="window.open(this.href,'targetWindow','toolbar=no,location=no,status=no,menubar=no,scrollbars=yes,resizable=yes,width=400,height=600'); return false;">
                                            <img style="max-width: 260px;" src="{{ route('ldfImages', ['email' => $userEmail, 'id' => $ldf->fileName]) }}">
                                        </a>
                                    </div>
                                    @else
                                    Il n'y a pas de justificatif pour cette dépense.
                                    @endif
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    <label for="label">Objet</label>
                                    <input type="text" name="label" value="{{ $ldf->label }}" class="form-control">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="ladate">Date</label>
                                    <input type="text" name="ladate" value="{{ $ldf->ladate }}" class="form-control">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="ht">Montant HT</label>
                                    <input type="text" name="ht" value="{{ $ldf->ht }}" class="form-control">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="ht">Montant TTC</label>
                                    <input type="text" name="ttc" value="{{ $ldf->ttc }}" class="form-control">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    <label for="tvaTx1">TVA {{ $ldf->tvaTx1 }}%</label>
                                    <input type="text" name="tva1" value="{{ $ldf->tvaVal1 }}" class="form-control">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="tvaTx2">TVA {{ $ldf->tvaTx2 }}%</label>
                                    <input type="text" name="tva2" value="{{ $ldf->tvaVal2 }}" class="form-control">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="tvaTx3">TVA {{ $ldf->tvaTx3 }}%</label>
                                    <input type="text" name="tva3" value="{{ $ldf->tvaVal3 }}" class="form-control">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="tvaVal4">TVA {{ $ldf->tvaTx4 }}%</label>
                                    <input type="text" name="tva4" value="{{ $ldf->tvaVal4 }}" class="form-control">
                                </div>
                            </div>


                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    <label for="depart">Ville de départ</label>
                                    <input type="text" name="depart" value="{{ $ldf->depart }}" class="form-control">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="arrivee">Ville d'arrivée</label>
                                    <input type="text" name="arrivee" value="{{ $ldf->arrivee }}" class="form-control">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="distance">Distance</label>
                                    <input type="text" name="distance" value="{{ $ldf->distance }}" class="form-control">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="vehicule">Véhicule</label>
                                    <input type="text" name="vehicule" value="{{ $ldf->vehicule }}" class="form-control">
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-md-3">
                                    <label for="invites">Invités / remarques</label>
                                    <input type="text" name="invites" value="{{ $ldf->invites }}" class="form-control">
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="col-md-4">
                                    @if ($previous == 0)
                                    <a class="btn btn-primary">{{ __('<<') }}</a>
                                    @else
                                    <a href="{{URL::to('/webLdFUpdate/')}}/{{$previous}}" class="btn btn-primary">{{ __('<<') }}</a>
                                    @endif
                                    @if ($next == 0)
                                    <a class="btn btn-primary" @if ($previous===0) disabled @endif>{{ __('>>') }}</a>
                                    @else
                                    <a href="{{URL::to('/webLdFUpdate/')}}/{{$next}}" class="btn btn-primary" @if ($previous===0) disabled @endif>{{ __('>>') }}</a>
                                    @endif
                                </div>
                                <div class="offset-md-6">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Sauvegarder') }}
                                    </button>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
