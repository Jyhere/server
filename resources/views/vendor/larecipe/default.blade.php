<!doctype html>
<html>

<head>
    <!-- META Tags -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>{{ isset($title) ? $title . ' | ' : null }}{{ config('app.name') }}</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- SEO -->
    <meta name="author" content="{{ config('larecipe.seo.author') }}">
    <meta name="description" content="{{ config('larecipe.seo.description') }}">
    <meta name="keywords" content="{{ config('larecipe.seo.keywords') }}">
    <meta name="twitter:card" value="summary">
    @if (isset($canonical) && $canonical)
    <link rel="canonical" href="{{ url($canonical) }}" />
    @endif
    @if($openGraph = config('larecipe.seo.og'))
    @foreach($openGraph as $key => $value)
    @if($value)
    <meta property="og:{{ $key }}" content="{{ $value }}" />
    @endif
    @endforeach
    @endif

    <!-- CSS -->
    <link rel="stylesheet" href="{{ larecipe_assets('css/app.css') }}">

    @if (config('larecipe.ui.fav'))
    <!-- Favicon -->
    <link rel="apple-touch-icon" href="{{ asset(config('larecipe.ui.fav')) }}">
    <link rel="shortcut icon" type="image/png" href="{{ asset(config('larecipe.ui.fav')) }}" />
    @endif

    <!-- FontAwesome -->
    <link rel="stylesheet" href="{{ larecipe_assets('css/font-awesome.css') }}">
    @if (config('larecipe.ui.fa_v4_shims', true))
    <link rel="stylesheet" href="{{ larecipe_assets('css/font-awesome-v4-shims.css') }}">
    @endif

    <!-- Dynamic Colors -->
    @include('larecipe::style')

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @foreach(LaRecipe::allStyles() as $name => $path)
    @if (preg_match('/^https?:\/\//', $path))
    <link rel="stylesheet" href="{{ $path }}">
    @else
    <link rel="stylesheet" href="{{ route('larecipe.styles', $name) }}">
    @endif
    @endforeach
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

<body>
    <div id="app" v-cloak>
        <header>
            <nav class="navbar navbar-expand-md navbar-light fixed-top navbar-laravel">
                <div class="container">
                    @guest
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'DoliScan') }}
                    </a>
                    @else
                    <a class="navbar-brand" href="{{ url('/home') }}">
                        {{ config('app.name', 'DoliScan') }}
                    </a>
                    @endguest
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Ouvrir le menu') }}">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="navbar-collapse collapse justify-content-end">
                        <ul class="navbar-nav">
                            <li class="nav">
                                <a class="nav-link" href="{{ route('home') }}">{{ __('Accueil') }}</a>
                            </li>
                            <li class="nav">
                                <a class="nav-link" href="/docs" target="_blank">{{ __('Documentation') }}</a>
                            </li>
                            <li class="nav">
                                <a class="nav-link" href="https://www.doliscan.fr/tarifs">{{ __('Tarifs') }}</a>
                            </li>
                            <li class="nav">
                                <a class="nav-link" href="https://www.doliscan.fr/lapplication">{{ __('L\'application') }}</a>
                            </li>

                            @guest
                            <li class="nav">
                                <a class="nav-link" href="{{ route('webLogin') }}">{{ __('Mon compte') }}</a>
                            </li>
                            @else
                            <li class="nav dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->firstname }} {{ Auth::user()->name }} <span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                                    <li><a href="{{ Config::get('sharp.custom_url_segment') }}/show/account">{{ __('Mon Compte') }}</a></li>
                                    <li><a href="{{ Config::get('sharp.custom_url_segment') }}/">{{ __('Administration') }}</a></li>
                                    @if(session('impersonated_by') )
                                    <li><a href="{{ route('webLeaveImpersonate') }}" onclick="event.preventDefault();document.getElementById('leave-form').submit();">
                                            {{ __('Retour SU') }}</a>
                                        <form id="leave-form" action="{{ route('webLeaveImpersonate') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </li>
                                    @endif
                                    <li><a href="{{ Config::get('sharp.custom_url_segment') }}/logout">
                                            {{ __('Quitter') }}</a>
                                    </li>
                                </ul>
                            </li>
                            @endguest
                        </ul>
                    </div>
                </div>
            </nav>
        </header>

        <main>
            @include('larecipe::partials.nav')

            @include('larecipe::plugins.search')

            @yield('content')

            <larecipe-back-to-top></larecipe-back-to-top>
        </main>


        <script>
            window.config = @json([]);
        </script>

        <script type="text/javascript">
            if(localStorage.getItem('larecipeSidebar') == null) {
                localStorage.setItem('larecipeSidebar', !! {{ config('larecipe.ui.show_side_bar') ?: 0 }});
            }
        </script>

        <script src="{{ larecipe_assets('js/app.js') }}"></script>

        <script>
            window.LaRecipe = new CreateLarecipe(config)
        </script>

        @foreach (LaRecipe::allScripts() as $name => $path)
        @if (preg_match('/^https?:\/\//', $path))
        <script src="{{ $path }}"></script>
        @else
        <script src="{{ route('larecipe.scripts', $name) }}"></script>
        @endif
        @endforeach

        <script>
            LaRecipe.run()
        </script>

    </div>
    <footer class="footer mt-auto">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <h5 class="font-weight-bold text-uppercase mt-3 mb-4">L'application</h5>
                    <ul>
                        <li><a href="https://apps.apple.com/fr/app/id1455241946">Installer sur iPhone/iPad</a></li>
                        <li><a href="https://play.google.com/store/apps/details?id=fr.caprel.doliscan">Installer sur Android</a></li>
                        <li><a href="https://www.doliscan.fr/documentation">Documentation utilisateur</a></li>
                        <li><a href="https://www.doliscan.fr/serveur">Le serveur</a></li>
                        <li><a href="https://www.doliscan.fr/webservices">Les webservices</a></li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <h5 class="font-weight-bold text-uppercase mt-3 mb-4">Partenaires</h5>
                    <ul>
                        <li><a href="https://www.doliscan.fr/revendeurs">Revendeurs</a></li>
                        <li><a href="https://www.doliscan.fr/experts-comptables">Experts-Comptables</a></li>
                        <li><a href="https://www.doliscan.fr/entreprises">Entreprises</a></li>
                        <li><a href="https://www.doliscan.fr/independants">Indépendants</a></li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <h5 class="font-weight-bold text-uppercase mt-3 mb-4">L'entreprise</h5>
                    <ul>
                        <li><a href="https://www.doliscan.fr/a-propos">À propos</a></li>
                        <li><a href="https://www.doliscan.fr/mentions-legales">Mentions légales</a></li>
                        <li><a href="https://www.doliscan.fr/rgpd">RGPD</a></li>
                        <li><a href="https://www.doliscan.fr/cgu">CGU</a></li>
                        <li><a href="https://www.doliscan.fr/contact">Contact</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
</body>

</html>
