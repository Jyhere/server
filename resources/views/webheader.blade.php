<header>
  <nav class="navbar navbar-expand-md navbar-light fixed-top navbar-laravel">
    <div class="container">
      @guest
      <a class="navbar-brand" href="{{ url('https://www.doliscan.fr/') }}">
        {{ config('app.name', 'DoliScan') }}
      </a>
      @else
      <a class="navbar-brand" href="{{ url('/home') }}">
        {{ config('app.name', 'DoliScan') }}
      </a>
      @endguest
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Ouvrir le menu') }}">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="navbar-collapse collapse justify-content-end">
        <ul class="navbar-nav">
          <li class="nav">
            <a class="nav-link" href="{{ route('home') }}">{{ __('Accueil') }}</a>
          </li>
          <li class="nav">
            <a class="nav-link" href="/docs">{{ __('Documentation') }}</a>
          </li>
          <li class="nav">
            <a class="nav-link" href="https://www.doliscan.fr/tarifs">{{ __('Tarifs') }}</a>
          </li>
          <li class="nav">
            <a class="nav-link" href="https://www.doliscan.fr/lapplication">{{ __('L\'application') }}</a>
          </li>

          <li class="nav dropdown ">
            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Extensions</a>
            <div class="dropdown-menu">
              <a href="https://www.doliscan.fr/plugins" class="dropdown-item ">Extensions</a>
              <div class="dropdown-divider"></div>
              <a href="https://www.doliscan.fr/plugins/dolibarr" class="dropdown-item ">Dolibarr</a>
              <a href="https://www.doliscan.fr/plugins/quadratus" class="dropdown-item ">Quadratus</a>
            </div>
          </li>
          <li class="nav">
            <a class="nav-link" href="https://www.doliscan.fr/tester">{{ __('Tester') }}</a>
          </li>

          @guest
          <li class="nav">
            <a class="nav-link" href="{{ route('webLogin') }}">{{ __('Mon compte') }}</a>
          </li>
          @else
          <li class="nav dropdown">
            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
              {{ Auth::user()->firstname }} {{ Auth::user()->name }} <span class="caret"></span>
            </a>
            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
              <li><a href="{{ Config::get('sharp.custom_url_segment') }}/show/account">{{ __('Mon Compte') }}</a></li>
              <li><a href="{{ Config::get('sharp.custom_url_segment') }}/">{{ __('Administration') }}</a></li>
              @if(session('impersonated_by') )
              <li><a href="{{ route('webLeaveImpersonate') }}" onclick="event.preventDefault();document.getElementById('leave-form').submit();">
                  {{ __('Retour SU') }}</a>
                <form id="leave-form" action="{{ route('webLeaveImpersonate') }}" method="POST" style="display: none;">
                  @csrf
                </form>
              </li>
              @endif
              <li><a href="{{ Config::get('sharp.custom_url_segment') }}/logout">
                  {{ __('Quitter') }}</a>
              </li>
            </ul>
          </li>
          @endguest
        </ul>
      </div>
    </div>
  </nav>
</header>
