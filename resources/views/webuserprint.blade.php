<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ config('app.name', 'DoliScan') }} - fiche utilisateur - {{ $name }}</title>

    <style>
        @page {
            margin: 0px 0px 0px 0px !important;
            padding: 0px 0px 0px 0px !important;
        }
        @font-face {
            font-family: 'Ubuntu';
            font-style: normal;
            font-weight: 400;
            src: url( {{ public_path('fonts/ubuntu/ubuntu-v14-latin-regular.ttf') }} ) format("truetype");
        }

        html {
            width: 720px;
        }

        body {
            font-family: Ubuntu, sans-serif; /*, Helvetica, Verdana, Geneva, Tahoma, sans-serif;*/
            font-size: 9pt;
            width: 720px;
            margin: 0.5cm 1cm 1cm 1cm;
        }

        table {
            border-collapse: collapse;
            /* border-left: solid black 1px; */
            /* border-right: solid black 1px; */
            /* page-break-inside: avoid; */
            margin-bottom: 10px;
            width: 720px;
        }
        tbody {
            /* border: solid black 1px; */
            width: 720px;
        }

        th {
            border: solid black 1px;
            text-align: center;
            vertical-align: top;
        }

        td {
            padding-left: 5px;
            border-right: solid #000 1px;
            border-left: solid #000 1px;
        }

        h1 {
            text-align: center;
            font-weight: normal;
            font-size: 12pt;
            margin: 0;
            margin-bottom: 10px;
            padding: 5px;
            border: solid black 1px;
        }

        h2 {
            padding-left: 10px;
            background-color: gray;
            color: #fff;
            width: 90%;
        }

        h3 {
            font-weight: normal;
            font-size: 10pt;
            margin: 0 0 0 5px;
        }

        p {
            width: 90%;
            text-align: justify;
        }

        .page-break {
            page-break-after: always;
        }

        #header,
        #footer {
            position: fixed;
            left: 0;
            right: 0;
            color: #aaa;
            font-size: 8pt;
        }

        header {
            position: fixed;
            top: 0cm;
            left: 0cm;
            right: 0cm;
            height: 0cm;
        }

        footer {
            position: fixed;
            bottom: 0cm;
            left: 1cm;
            right: 1cm;
            height: 1.4cm;
            border-top: 0.5px solid #444;
        }

        .page-number:before {
            content: "Page "counter(page);
        }

        .tdh1 {
            background-color: #aaa;
        }

        .tdh2 {
            background-color: #fff;
            padding-top: 10px;
        }

        .tdh3 {
            background-color: #eee;
        }

        .td80nb {
            /* width: 50px; */
            text-align: right;
            padding-right: 5px;
            vertical-align: top;
            white-space: nowrap;
            font-size: 0.9em;
        }

        .td80 {
            /* width: 50px; */
            vertical-align: top;
            white-space: nowrap;
            font-size: 0.9em;
        }

        .td10nb {
            text-align: right;
            padding-right: 5px;
            min-width: 5em;
            max-width: 5em;
            width: 5em;
            vertical-align: top;
            white-space: nowrap;
            font-size: 0.8em;
        }
        .date{
            text-align: left;
            padding-left: 10px;
        }
        .moyen{
            text-align: left;
        }
        .th10nb {
            text-align: center;
            min-width: 5em;
            max-width: 5em;
            width: 5em;
            vertical-align: top;
            white-space: nowrap;
        }
        .th80 {
            text-align: center;
            vertical-align: top;
            white-space: nowrap;
        }
        .tdvide {
            padding-right: 0px;
            width: 0px;
            max-width: 0px;
            font-size: 0.9em;
        }

        .tdtotalleft {
            border-bottom: solid #000 1px;
            background: #eee;
            padding-left: 10px;
            font-style: italic;
        }

        .tdtotalnb {
            border-bottom: solid #000 1px;
            background: #eee;
            text-align: right;
            padding-right: 5px;
            width: 60px;
            white-space: nowrap;
            font-style: italic;
        }

        .tdtotalvide {
            border-bottom: solid #000 1px;
            background: #eee;
            text-align: right;
            padding-right: 0px;
            width: 0px;
            max-width: 0px;
        }

        .detailCalculIK {
            font-size: 0.9em;
            margin-top: -5px;
            padding-left: 5px;
        }

        .tdDetailCalcul {
            border-bottom: solid #000 1px;
            background: #fff;
        }

        ul[title]::before {
            content: attr(title);
            /* then add some nice styling as needed, eg: */
            display: block;
            padding-left: -45px;
        }

        li {
            padding-left: -25px;
            width: 90%;
            text-align: justify;
        }

        li li {
            padding-left: -40px;
            width: 90%;
            text-align: justify;
        }

        #cartoucheHD {
            padding: 5px;
            width: 240px;
            float: right;
            text-align: right;
            margin-top: 0px;
            height: 100px;
        }

        #cartoucheHG {
            padding: 5px;
            width: 200px;
            float: left;
            height: 100px;
        }

        #salarie {
            border: solid black 1px;
            padding: 5px;
            width: 280px;
            float: right;
            height: 100px;
        }

        #societe {
            border: solid black 1px;
            padding: 5px;
            width: 240px;
            float: left;
            height: 100px;
        }

        .notedefrais {
            font-size: 2em;
        }
    </style>
</head>

<body>
    <header>

    </header>
    <footer>

    </footer>
    <main>
            <div id="cartoucheHG">
                <p>
                    <img src="../public/doliscan_web-logo_400.png" style="width: 240px;" />
                </p>
            </div>
            <div id="cartoucheHD">
                <p>
                    <b class="notedefrais">Fiche utilisateur</b><br />
                    Réf. : {{ $name  }}<br />
                </p>
            </div>
            <div style="clear: both;"></div>
            <div id="societe">
                <b>{{ $societe->name }}</b><br />
                {{ $societe->adresse }}<br />
                {{ $societe->cp }} {{ $societe->ville }}<br />
                Tel. {{ $societe->tel }}<br />
                Mail. {{ $societe->email }}<br />
                Web. {{ $societe->web }}<br />
            </div>
            <div id="salarie">
                Utilisateur : {{ $name }}<br />
                Mail : {{ $email }}<br />
                Date création : {{ date('d/m/Y') }}<br /><br />
            </div>
            <div style="clear: both;"></div>

            <p style="margin-top: 1em;"><i>DoliSCAN est un assistant numérique pour la gestion courante de vos notes de frais. Cette fiche contient les informations essentielles pour bien démarrer avec ce système.</i></p>

            @include('emails.account.new_html_content', [ 'name' => $name, 'email' => $email, 'role' => $role, 'passwd' => $passwd, 'relance' => '' ] )

            @yield('content')

            <h2>Besoin d'un petit coup de pouce ?</h2>

            <p>Consultez la documentation en ligne disponible sur le site : <a href="https://doliscan.fr/docs/">doliscan.fr/docs/</a>.</p>

            <p>N'hésitez pas à nous demander de l'aide par courriel: <a href="mailto:aide@doliscan.fr">aide@doliscan.fr</a> précisez votre demande le plus possible pour qu'on puisse vous apporter une aide rapide et efficace.</p>

    <script type="text/php">
    if (isset($pdf)) {
        $text = "page {PAGE_NUM} / {PAGE_COUNT}";
        $size = 8;
        $font = $fontMetrics->getFont("Verdana");
        $width = $fontMetrics->get_text_width($text, $font, $size) / 2;
        //$x = ($pdf->get_width() - $width) / 2;
        $x = $pdf->get_width() - 80;
        $y = $pdf->get_height() - 35;
        $pdf->page_text($x, $y, $text, $font, $size);
        $pdf->page_text(40, $y, "doliscan.fr", $font, $size);
    }
    </script>
    </main>
</body>

</html>
