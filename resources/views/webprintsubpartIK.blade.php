@if($vehicules->count() > 0)

<tr>
    <td colspan="4" class="tdh2">
        <h2>Indemnités kilométriques</h2>
    </td>
    <td class="tdh2">
        &nbsp;
    </td>
    <td class="tdh2">
        &nbsp;
    </td>
</tr>

@foreach($vehicules as $vehicule)
@php($totalHT = 0)
@php($totalTTC = 0)
@php($totalKM = 0)
@foreach($lignesIK[$vehicule]["listeIKMonth"] as $ligne)
@php($totalHT += $ligne->ht)
@php($totalTTC += $ligne->ttc)
@if($ligne->distance > 0)
@php($totalKM += $ligne->distance)
@endif
@endforeach

<tr>
    <td colspan="4" class="tdh3">
        <h3>Véhicule : <i>{{ $lignesIK[$vehicule]["nom"] }} - {{ $lignesIK[$vehicule]["cv"] }}  [{{ $lignesIK[$vehicule]["immat"] }}]</i></h3>
    </td>
    <td class="tdh3">
        &nbsp;
    </td>
    <td class="tdh3">
        &nbsp;
    </td>
</tr>

@foreach($lignesIK[$vehicule]["listeIKMonth"] as $ligne)
<tr>
    <td class="td10nb date">{{ $ligne->ladate }}</td>
    <td class="td80" colspan="3" style="word-wrap: break-word">{!! wordwrap($ligne->getResume(false),147,"<br />") !!}</td>

    <td class="td10nb">{{ $ligne->distance }}&nbsp;km</td>
    <td class="td10nb">{{ nbFR($ligne->ttc) }}&nbsp;€</td>
</tr>
@endforeach

{{-- si on a une regule pour changement de bareme --}}
@if($lignesIK[$vehicule]["changeBaremeImpact"])
<tr>
    <td class="td10nb"></td>
    <td class="td80" colspan="3">Régularisation pour changement de barème</td>
    <td class="tdvide">&nbsp;</td>
    <td class="td10nb">
        {{ nbFR($lignesIK[$vehicule]["MontantAnnuelBaremeCorrige"]) }}&nbsp;€
    </td>
</tr>
@php($totalTTC += $lignesIK[$vehicule]["MontantAnnuelBaremeCorrige"])
@endif


{{-- si on a une regule pour changement de tranche --}}
@if($lignesIK[$vehicule]["changeTranche"] && (($utilisationDoliScanAnneeComplete == true) || ($lignesIK[$vehicule]["KMavantDoli"] == 0)))
<tr>
    <td class="td10nb"></td>
    <td class="td80" colspan="3">Régularisation pour changement de tranche</td>
    <td class="tdvide">&nbsp;</td>
    <td class="td10nb">
        {{ nbFR($lignesIK[$vehicule]["MontantTrancheRegule"]) }}&nbsp;€
    </td>
</tr>
@php($totalTTC += ($lignesIK[$vehicule]["MontantTrancheRegule"]))
@endif

@php($totalKMs = $lignesIK[$vehicule]["nom"] . " - " . $lignesIK[$vehicule]["cv"])

<tr>
    <td colspan="4" class="tdtotalleft">Total {{ $totalKMs }}</td>
    <td class="tdtotalnb">{{ $totalKM }} km</td>
    <td class="tdtotalnb">{{ nbFR($totalTTC) }}&nbsp;€</td>
</tr>

<tr>
    <td colspan="6" class="tdDetailCalcul">
        <div class="detailCalculIK">
            <ul title="Détail du calcul des indemnités kilométriques pour {{ $lignesIK[$vehicule]["nom"] }} - {{ $lignesIK[$vehicule]["cv"] }} :">
                @if(($utilisationDoliScanAnneeComplete == true) || ($lignesIK[$vehicule]["KMavantDoli"] == 0))
                <li>Total de la distance parcourue cette année avec ce véhicule : {{ $lignesIK[$vehicule]["totalKM"] }} km
                </li>
                @else
                <li>Total de la distance parcourue cette année avec ce véhicule (y compris la distance initiale parcourue avant l'utilisation de DoliScan) : {{ $lignesIK[$vehicule]["totalKM"] }} km</li>
                <li>Distance déclarée comme ayant été déjà fait l'objet d'IK avant l'utilisation de DoliSCAN cette année : {{ $lignesIK[$vehicule]["KMavantDoli"] }} km, 
                    Montant théorique correspondant payé avant l'utilisation de DoliSCAN cette année : {{ $lignesIK[$vehicule]["detailCalculAvantDoli"] }} =  {{ $lignesIK[$vehicule]["MontantAvantDoli"] }} €</li>
                @endif
                <li>Distance parcourue ce mois-ci : {{ $lignesIK[$vehicule]["kmPeriode"] }} km</li>
                <li>Puissance du véhicule : {{ $lignesIK[$vehicule]["cv"] }}</li>
                <li>Opération : {{ $lignesIK[$vehicule]["detailCalcul"] }} = {{ $lignesIK[$vehicule]["Montant"] }}&nbsp;€ {{ $lignesIK[$vehicule]["bareme"] }}</li>
                @if($lignesIK[$vehicule]["changeBareme"])
                <li>Attention, changement de barème: le nouveau barème de l'année est maintenant connu, configuré et paramétré dans DoliSCAN.
                    <ul>
                    @if($lignesIK[$vehicule]["changeBaremeImpact"] != 0)
                        <li>Le total des IK depuis le début de l'année selon barème A ({{ $lignesIK[$vehicule]["DetailAnnuelBaremeA"] }}) = {{ nbFR($lignesIK[$vehicule]["MontantAnnuelBaremeA"]) }}&nbsp;€</li>
                        <li>Le total des IK depuis le début de l'année selon barème B ({{ $lignesIK[$vehicule]["DetailAnnuelBaremeB"] }}) = {{ nbFR($lignesIK[$vehicule]["MontantAnnuelBaremeB"]) }}&nbsp;€</li>
                        <li>Total des IK versées cette année pour ce véhicule (mois en cours inclus): {{ $lignesIK[$vehicule]["MontantAnnuel"] }}&nbsp;€</li>
                        <li>Donc régularisation de {{ nbFR($lignesIK[$vehicule]["MontantAnnuelBaremeCorrige"]) }}&nbsp;€ portée sur cette note de frais.</li>
                    @else
                        <li>Le changement de barème n'a aucun impact dans votre cas.</li>
                    @endif
                    </ul>
                </li>
                @endif
                @if($lignesIK[$vehicule]["changeTranche"])
                <li>Attention, changement de tranche !
                    <ul>
                    @if(($utilisationDoliScanAnneeComplete == true) || ($lignesIK[$vehicule]["KMavantDoli"] == 0))
                        <li>Total des IK versées cette année pour ce véhicule (mois en cours inclus): {{ $lignesIK[$vehicule]["MontantAnnuel"] }}&nbsp;€</li>
                        <li>Le total des IK depuis le début de l'année devrait être de {{ $lignesIK[$vehicule]["detailCalculComplet"] }} = {{ nbFR($lignesIK[$vehicule]["MontantAnnuelCorrige"]) }}&nbsp;€</li>
                        <li>Donc régularisation de {{ nbFR($lignesIK[$vehicule]["MontantTrancheRegule"]) }}&nbsp;€ portée sur cette note de frais.</li>
                        @if($utilisationDoliScanAnneeComplete != true)
                            <li>Note: vous n'utilisez pas doliscan depuis le début de l'année mais vous avez déclarés ne pas avoir utilisé ce véhicule avant donc ce calcul est juste.</li>
                        @endif
                    @else
                        <li>Le total des IK depuis le début de l'année devrait être de {{ $lignesIK[$vehicule]["detailCalculComplet"] }} = {{ nbFR($lignesIK[$vehicule]["MontantAnnuelCorrige"]) }}&nbsp;€ mais comme vous n'utilisez pas DoliScan depuis le début de l'année ce total ne peut-être certifié.</li>
                    @endif
                    </ul>
                </li>
                @endif
            </ul>
            <p>Référence du texte de loi sur le calcul des IK et les changements de tranches: <a href="https://www.impots.gouv.fr/portail/particulier/questions/jutilise-plusieurs-vehicules-comment-dois-je-appliquer-le-bareme-kilometrique">https://www.impots.gouv.fr</a></p>
        </div>
    </td>
</tr>

@endforeach
@endif
