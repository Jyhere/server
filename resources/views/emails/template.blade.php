<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ config('app.name', 'DoliScan') }}</title>
    <style type="text/css">
        @viewport {
            width: device-width;
            zoom: 1.0;
        }

        @media screen and (max-width: 600px) {
            body {}
        }

        @media screen and (min-width: 610px) {

            h2,
            p,
            ul,
            li {
                width: 800px;
            }
        }

        body {
            font-family: sans-serif;
            font-size: 11pt;
        }

        h1 {
            font-family: 'Nunito', sans-serif;
            font-size: +300%;
            margin-bottom: 0px;
            margin-top: 0px;
        }

        h2 {
            font-family: 'Nunito', sans-serif;
            font-weight: 400;
            padding-left: 10px;
            padding-bottom: 5px;
            padding-top: 5px;
            background-image: linear-gradient(to right, gray, gray, rgba(255, 0, 0, 0));
            color: #fff;
        }

        p {
            margin: 0;
            padding: 0;
        }

        ul,
        li {
            font-size: 11pt;
        }

        .soustitre {
            margin-left: 40px;
            margin-top: -10px;
            font-family: Georgia, serif;
            font-size: 13pt;
            line-height: 13pt;
            color: #000000;
            font-style: italic
        }

        .ladate {
            margin-left: 200px;
            margin-top: 10px;
            font-family: Georgia, serif;
            font-size: 13pt;
            line-height: 13pt;
            color: #000000;
        }

    </style>
</head>

<body>
    <img src="file://{{ resource_path('assets/images/mail-logo.png') }}" alt="Logo DoliSCAN" border="0" />

    {{-- <h1>DoliScan</h1>
    <span class="soustitre">Votre assistant "notes de frais"</span> --}}

    @yield('content')

    <h2>Actualité ...</h2>

    <p>Pensez à mettre à jour votre application sur Smartphone, vérifiez bien que vous disposez de la dernière version :</p>
    <ul>
        <li>Version {{ config('constants.app.versionIos') }} pour iPhone</li>
        <li>Version {{ config('constants.app.versionAndroid') }} pour Android</li>
    </ul>

    <p>Pour plus de détails sur l'historique des évolutions de l'application consultez la <a href="{{ config('constants.app.versionUri') }}">page suivante du site web</a>.</p>

    <h2>Besoin d'un petit coup de pouce ?</h2>

    <p>Consultez la documentation en ligne disponible sur le site : <a href="https://doliscan.fr/docs/">doliscan.fr/docs/</a>.</p>

    <p>N'hésitez pas à nous demander de l'aide par courriel: <a href="mailto:aide@doliscan.fr">aide@doliscan.fr</a> précisez votre demande le plus possible pour qu'on puisse vous apporter une aide rapide et efficace.</p>

    {{-- <h2>Résumé du mois</h2>
    <p>
        <ul>
            <li>Ce mois-ci vous avez parcouru ... km</li>
            <li>Resto</li>
            <li>Hotel</li>
            <li>.../...</li>
        </ul>
    </p>

    <h2>Votre compte</h2>
    <p>
        <ul>
            <li>Vous utilisez DoliScan depuis le dateouverturecompte</li>
            <li>Votre profil utilisateur est complet</li>
            <li>Vous pouvez le mettre à jour depuis l'application</li>
        </ul>
    </p>


    <h2>Mais aussi ... quelques informations</h2>
    <p>
        Saviez-vous qu'en cas d'utilisation de plusieurs véhicules les indemnités kilométriques devaient être
        individualisées ?
        Pour plus de détails consultez <a href="https://www.impots.gouv.fr/portail/particulier/questions/jutilise-plusieurs-vehicules-comment-dois-je-appliquer-le-bareme-kilometrique">le
            site du ministère</a>.
    </p> --}}


    <pre>
--
Mail envoyé depuis la plate-forme {{ config('app.name') }}
{{ config('app.name') }} :: {{ config('app.version') }}
{{ \Illuminate\Support\Str::limit($currentURI, 40, $end = '...') }}
</pre>
</body>

</html>
