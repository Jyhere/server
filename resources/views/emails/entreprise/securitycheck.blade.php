@extends('emails.template')

@section('content')

<h2>Droits administrateur sur votre société</h2>

<p>Une demande de modification de compte administrateur est en cours. Merci d'utiliser le code suivant <b>{{ $security_code }}</b> pour confirmer cette procédure.</p>

<p>Si jamais vous n'êtes pas à l'origine de cette demande, ignorez ce message ou contactez votre administrateur.</p>

<p>En cas de doute (et de demandes répétées) n'hésitez pas à prendre contact avec le support technique : {{ config('constants.mail.sav') }} </p>

@endsection('content')
