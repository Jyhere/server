Note de frais...
----------------

Vous recevez cette note de frais car l'utilisateur {{ $userName }} ({{ $userEmail }}) vous a désigné(e) comme destinataire de ses documents comptables.


Fichier à importer dans Quadratus
---------------------------------

Le fichier quadratus.zip vous permet d'importer les pièces justificatives automatiquement.

Pour ce faire vous devrez extraire le contenu du fichier ZIP et importer ensuite le fichier .TXT, tous les documents PDF présents dans le même répertoire que le fichier TXT seront importés dans quadratus.


Documents à télécharger
-----------------------

Veuillez cliquer sur les liens ci-dessous pour télécharger vos documents :

@foreach($attachFiles as $ficURI)
  * {{ $ficURI['label'] }} : {{ $ficURI['uri'] }}
@endforeach
