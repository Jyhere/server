@extends('emails.template_text')

@section('content')

Votre note de frais est passée à l'état "gelée"
-----------------------------------------------

La note de frais ci-jointe est passée maintenant à l'état "gelée", vous pouvez néanmoins toujours ajouter / modifier des frais.

Une nouvelle note pour le mois qui commence a automatiquement été ouverte, continuez donc à utiliser DoliSCAN normalement sur votre smartphone.

D'ici quelques jours la note de frais du mois passé sera définitivement archivée et vous sera envoyée par mail (ainsi qu'à votre contact "comptabilité", assurez vous d'avoir renseigné la bonne adresse dans la configuration du logiciel).

@endsection('content')
