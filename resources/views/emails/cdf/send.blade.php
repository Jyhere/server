@extends('emails.template')

@section('content')
<h2>Classeur de Notes de frais...</h2>

<p>Le classeur des notes de frais de la société regroupe l'ensemble des notes de frais des salariés de la société.</p>

<p>Ce classeur vous permet d'importer en une seule passe la totalité des notes de frais et des justificatifs associés.</p>

<h2>État des remboursements de frais à faire</h2>

@isset($cdFraisUsers)
<p>Pour information vous trouverez ci-dessous la liste synthétique des remboursements de frais à effectuer:</p>
@endisset

<ul>
    @forelse($cdFraisUsers as $u)
    <li>{{ $u['nom'] }} {{ nbFR($u['ttc']) }} €</li>
    @empty
    <li>Aucun remboursement à faire ce mois-ci</li>
    @endforelse
</ul>

<h2>Fichier à importer dans Quadratus</h2>

<p>Le fichier quadratus.zip proposé en téléchargement ci-dessous vous permet d'importer les pièces justificatives automatiquement.</p>

<p>Pour ce faire vous devrez extraire le contenu du fichier ZIP et importer ensuite le fichier .TXT, tous les documents PDF présents dans le même répertoire que le fichier TXT seront importés dans quadratus.</p>

<h2>Documents à télécharger</h2>

<p>Veuillez cliquer sur les liens ci-dessous pour télécharger vos documents :</p>

<ul>
    @foreach($attachFiles as $ficURI)
    <li><a href="{{ $ficURI['uri'] }}">{{ $ficURI['label'] }}</a></li>
    @endforeach
</ul>

@endsection('content')
