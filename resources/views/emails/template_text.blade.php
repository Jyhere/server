@yield('content')


Actualité ...
-------------

Pensez à mettre à jour votre application sur Smartphone, vérifiez bien que vous disposez de la dernière version :

* Version {{ config('constants.app.versionIos') }} pour iPhone
* Version {{ config('constants.app.versionAndroid') }} pour Android

Pour plus de détails sur l'historique des évolutions de l'application consultez la page suivante du site web: 

{{ config('constants.app.versionUri') }}

Besoin d'un petit coup de pouce ?
---------------------------------

Consultez la documentation en ligne disponible sur le site : https://doliscan.fr/docs/

N'hésitez pas à nous demander de l'aide par courriel: aide@doliscan.fr, précisez votre demande le plus possible pour qu'on puisse vous apporter une aide rapide et efficace.

--
Mail envoyé depuis la plate-forme {{ config('app.name') }}
{{ config('app.name') }} :: {{ config('app.version') }}
{{ \Illuminate\Support\Str::limit($currentURI, 40, $end='...') }}
