@extends('emails.template')

@section('content')

<h2>Vérification sécurité.</h2>

<p>Merci d'utiliser le code suivant <b>{{ $security_code }}</b> pour confirmer votre identité.</p>

<p>Si jamais vous n'êtes pas à l'origine de cette demande, ignorez ce message ou contactez votre administrateur.</p>

<p>En cas de doute (et de demandes répétées) n'hésitez pas à prendre contact avec le support technique : {{ config('constants.mail.sav') }} </p>

@endsection('content')
