@section('content')

<p>Bienvenue dans DoliSCAN.</p>

@if( $role == '')

@endif

@if( config('app.env') != 'prod')

<h2>Vous êtes sur un serveur de démonstration</h2>

<p>Le compte qui est détaillé ci-dessous a été créé sur un serveur de démonstration.</p>

<p>Pour que votre test soit le plus proche possible de la réalité nous avons fabriqué de fausses notes de frais aléatoires, seuls les photos des justificatifs sont manquantes.</p>

@endif

@if( $relance == 1)

<h2>Relance</h2>

<p>Ce mail est une relance car votre compte semble ne pas avoir été activé dans les temps vu que le lien d'activation n'est valable que 24h.</p>

<p>Un nouveau lien d'activation est présent dans ce courriel, vous pouvez donc supprimer l'ancienne invitation qui n'est plus valable.</p>

@endif

<h2>Pour vous connecter:</h2>

<p>Vous pouvez maintenant vous authentifier à l'aide des informations suivantes :
    <ul>
        <li>Votre Nom: {{ $name }}</li>
        <li>Identifiant de connexion: {{ $email }}</li>
        <li><a href="{{ $passwd }}">Cliquez sur ce lien pour activer votre compte</a> (lien valable {{ (config('auth.passwords.users.expire')/60) }} heures)</li>
        <li>Type de compte: {{ $role }}</li>
    </ul>
</p>

<h2>Installez l'application sur votre smartphone:</h2>

<p>Pour installer l'application:
    <ul>
        <li><a href="https://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=1455241946&mt=8">Cliquez ici si vous utilisez un iPhone</a></li>
        <li><a href="https://play.google.com/store/apps/details?id=fr.caprel.doliscan">Cliquez ici si vous utilisez un téléphone équipé d'Android</a></li>
    </ul>
    <p>Si vous utilisez un téléphone équipé d'Android et que vous n'avez pas accès à Google Play (réservé aux utilisateurs avancés)</p>
    <ul>
        <li><a href="https://download.doliscan.fr/">Cliquez ici</a> et installez directement le fichier APK</li>
    </ul>
</p>

<h2>Vos premiers pas:</h2>

<p>Après avoir initialisé votre mot de passe, lancez l'application, saisissez votre identifiant et mot de passe et laissez vous guider par l'assistant de première utilisation...</p>

<h2>Votre agenda:</h2>

<p>Voici les principales étapes de l'agenda de doliscan:
    <ul>
        <li>Le 1er jour du mois: ouverture automatique de la note de frais du mois en cours</li>
        <li>Jour après jour vous utilisez l'application pour envoyer vos justificatifs de frais</li>
        <li>5 jours avant la fin du mois un mail récapitulatif vous est envoyé pour vous avertir de la clôture proche de votre note de frais en cours</li>
        <li>Le dernier jour du mois la note de frais passe en mode "pré-cloture" (vous avez encore 4 jours pour faire des modifications)</li>
        <li>Le 5 du mois suivant votre note de frais est archivée, envoyée par mail à votre contact comptable/RH avec le fichier des écritures comptables</li>
    </ul>
</p>

@if( $role == 'adminRevendeur')
<h2>Ce compte est associé à un rôle de revendeur</h2>

<p>Vous pouvez donc créer des comptes clients qui vous seront ensuite facturés selon les termes du contrat. Vous pouvez également mettre un compte utilisateur "inactif" pour stopper sa prise en compte dans la facturation</p>

<h2>Pour créer les comptes de vos clients :</h2>

<ul>
    <li>Activez votre compte en suivant les indications du paragraphe "Pour vous connecter"</li>
    <li>Puis lorsque vous serez sur l'interface web de doliscan vous aurez alors accès au lien "Outils pour les revendeurs", cliquez dessus pour aller sur l'interface d'administration des revendeurs</li>
    <li>Créez l'entreprise de votre client.</li>
    <li>Créez ensuite le ou les comptes utilisateurs de votre client, différents profils de comptes sont possibles:
        <ul>
            <li><b>responsableEntreprise</b>: pour les dirigeants d'entreprise, ils pourront gérer leur équipe</li>
            <li><b>serviceComptabilite</b>: si l'entreprise dispose d'un comptable (ou secrétaire comptable) qui aura alors la possibilité de valider/corriger les notes de frais (du point de vue comptable)</li>
            <li><b>utilisateur</b>: le profil de compte standard qui permet donc au salarié d'utiliser l'application DoliSCAN et de gérer ses notes de frais</li>
        </ul>
    </li>
</ul>
@endif

@if( $role == 'adminEntreprise')
<h2>Ce compte est associé à un rôle de responsable informatique type DSI</h2>

<p>Vous pouvez donc créer des comptes utilisateurs de votre entreprise et leur attribuer un rôle. Attention chaque compte actif vous sera ensuite facturés selon les termes du contrat. Vous pouvez également mettre un compte utilisateur "inactif" pour stopper sa prise en compte dans la facturation</p>

<h2>Pour créer les comptes de vos utilisateurs :</h2>

<ul>
    <li>Activez votre compte en suivant les indications du paragraphe "Pour vous connecter"</li>
    <li>Puis lorsque vous serez sur l'interface web de doliscan vous aurez alors accès au lien "Outils pour les Administrateurs", cliquez dessus pour aller sur l'interface d'administration</li>
    <li>Cliquez sur utilisateurs.</li>
    <li>Créez ensuite le ou les comptes utilisateurs, différents profils de comptes sont possibles:
        <ul>
            <li><b>responsableEntreprise</b>: pour les dirigeants, ils pourront gérer leur équipe</li>
            <li><b>serviceComptabilite</b>: si votre structure dispose d'un comptable (ou secrétaire comptable) qui aura alors la possibilité de valider/corriger les notes de frais (du point de vue comptable)</li>
            <li><b>utilisateur</b>: le profil de compte standard qui permet donc à l'utilisateur d'utiliser l'application DoliSCAN et de gérer ses notes de frais</li>
        </ul>
    </li>
</ul>
@endif

@if( $role == 'responsableEntreprise')
<h2>Ce compte est associé à un rôle de responsable d'entreprise</h2>

<p>Vous pouvez donc créer des comptes utilisateurs de votre entreprise et leur attribuer un rôle. Attention chaque compte actif vous sera ensuite facturés selon les termes du contrat. Vous pouvez également mettre un compte utilisateur "inactif" pour stopper sa prise en compte dans la facturation</p>

<h2>Pour créer les comptes de vos utilisateurs :</h2>

<ul>
    <li>Activez votre compte en suivant les indications du paragraphe "Pour vous connecter"</li>
    <li>Puis lorsque vous serez sur l'interface web de doliscan vous aurez alors accès au lien "outils des Responsables d'Entreprise", cliquez dessus pour aller sur l'interface d'administration</li>
    <li>Cliquez sur utilisateurs.</li>
    <li>Créez ensuite le ou les comptes utilisateurs, différents profils de comptes sont possibles:
        <ul>
            <li><b>responsableEntreprise</b>: pour les dirigeants, ils pourront gérer leur équipe</li>
            <li><b>serviceComptabilite</b>: si votre structure dispose d'un comptable (ou secrétaire comptable) qui aura alors la possibilité de valider/corriger les notes de frais (du point de vue comptable)</li>
            <li><b>utilisateur</b>: le profil de compte standard qui permet donc à l'utilisateur d'utiliser l'application DoliSCAN et de gérer ses notes de frais</li>
        </ul>
    </li>
</ul>
@endif

@if( $role == 'serviceComptabilite')
<h2>Ce compte est associé à un rôle de responsable du service comptabilité de l'entreprise</h2>

<p>Vous pouvez donc avoir accès aux notes de frais des salariés de la société</p>
@endif

@if( $role == 'correcteur')
<h2>Ce compte est associé à un rôle de "correcteur"</h2>

<p>Ce rôle est un peu particulier: il vous permet de "corriger" ou "compléter" des frais saisis par votre patron par exemple...</p>
@endif

@endsection('content')
