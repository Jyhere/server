@extends('errors::illustrated-layout')

@section('code', '500')
@section('title', __('Error'))

@section('image')
    <div style="background-image: url({{ asset('/svg/500.svg') }});" class="absolute pin bg-cover bg-no-repeat md:bg-left lg:bg-center">
    </div>
@endsection

@section('message', __('Erreur 500, veuillez re-essayer plus tard ou prenez contact avec notre équipe technique : sav@doliscan.fr'))

@if (config('app.env')!='prod')

@section('message', __('Ooooooooooops, quelquechose a mal tourné'))

@section('details', $exception->getMessage())

@endif
