<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ config('app.name', 'DoliScan') }} - justificatif {{ $ldf->label }}</title>

    <style>
        @page {
            margin: 0px 0px 0px 0px !important;
            padding: 0px 0px 0px 0px !important;
        }

        @font-face {
            font-family: 'Ubuntu';
            font-style: normal;
            font-weight: 400;
            src: url({{ public_path('fonts/ubuntu/ubuntu-v14-latin-regular.ttf') }}) format("truetype");
        }

        html {
            width: 720px;
        }

        body {
            font-family: Ubuntu, sans-serif;
            /*, Helvetica, Verdana, Geneva, Tahoma, sans-serif;*/
            font-size: 9pt;
            width: 720px;
            margin: 3cm 1cm 1cm 1cm;
        }

        .page-break {
            page-break-after: always;
        }

        #header,
        #footer {
            position: fixed;
            left: 0;
            right: 0;
            color: #aaa;
            font-size: 8pt;
        }

        header {
            position: fixed;
            top: 0cm;
            left: 0cm;
            right: 0cm;
            height: 0cm;
        }

        footer {
            position: fixed;
            bottom: 0cm;
            left: 1cm;
            right: 1cm;
            height: 1.4cm;
            border-top: 0.5px solid #444;
        }

        .page-number:before {
            content: "Page "counter(page);
        }
    </style>
</head>

<body>
    <header>

    </header>
    <main>
        <div class="column">
            <img style="max-width: 100%;max-height: 80%;" src="{{ $image }}">
        </div>
        <p>
            {{ $label }} <br />
        </p>
        {!! $montant !!}

        <script type="text/php">
            if (isset($pdf)) {
                $text = "page {PAGE_NUM} / {PAGE_COUNT}";
                $size = 8;
                $font = $fontMetrics->getFont("Verdana");
                $width = $fontMetrics->get_text_width($text, $font, $size) / 2;
                $x = $pdf->get_width() - 80;
                $y = $pdf->get_height() - 35;
                $pdf->page_text($x, $y, $text, $font, $size);
                $pdf->page_text(40, $y, "{{ $username }} | {{ $ldf->ladate }} | {{ $ldf->label }}", $font, $size);
                $pdf->page_text(40, ($y+8), "https://doliscan.fr", $font, 6);
            }
            </script>

    </main>
    <footer>

    </footer>
</body>

</html>
