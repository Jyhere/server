<div class="form-row">
    <div class="form-group col-md-8">
        <label for="etape{{ $nb }}">Étape {{ $nb }}</label>
        <input type="text" name="etape{{ $nb }}" id="etape{{ $nb }}" value="{{ $ville }}" class="form-control">
        <input type="hidden" name="etape{{ $nb }}Slug" id="etape{{ $nb }}Slug" value="{{ $slug }}" class="form-control">
        <input type="hidden" name="etape{{ $nb }}UID" id="etape{{ $nb }}UID" value="{{ $uid }}" class="form-control">
    </div>
    <div class="form-group col-md-4">
        <label for="etape{{ $nb }}Distance">Distance</label>
        <div style="display: flex;  align-items:baseline;">
            <button type="button" class="form-control" id="btnCalculDistanceEtape{{ $nb }}"><i class="fas fa-cloud" style="color: #339AF0;"> </i></button>
            <input class="form-control" float name="etape{{ $nb }}Distance" id="etape{{ $nb }}Distance" type="number" placeholder="Distance (en km)" value="{{ $distance }}">
        </div>
    </div>
</div>
<div class="insertEtape-{{ $nb }}">
</div>

<script language="JavaScript">
    $(document).ready(function() {
        $('#etape{{ $nb }}').focus();

        $('#btnCalculDistanceEtape{{ $nb }}').click(function() {
            //
            searchVille('leformIK', 'etape{{ $nb }}', 'etape{{ $nb }}Slug', 'etape{{ $nb }}UID')
                .then((value) => distanceAutomatiqueEtape('leformIK', 'etape{{ $nb }}', {{ $nb }}));
        });
    });

</script>
