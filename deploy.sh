#!/bin/bash
#voir source https://medium.com/@gmaumoh/laravel-how-to-automate-deployment-using-git-and-webhooks-9ae6cd8dffae

source .env

#Affichage de la date de début / date de fin pour avoir une idée de la durée totale, surtout pour la version
#demo où on fait un gros jeu d'essai ...
echo -n "================================ "
TIMESTART=`date +%s`
date

php artisan down --message="Mise à jour en cours ..." --retry=20

#dev ou prod ?
#ou dans le .env APP_ENV="dev" ?
isDev="0"
if [ "${APP_ENV}" = "dev" ]; then
    echo "On est en mode dev ..."
    isDev="1"
fi

# update source code
git reset --hard
git pull

# update PHP dependencies
#composer install --no-interaction --no-dev --prefer-dist
# --no-interaction Do not ask any interactive question
# --no-dev  Disables installation of require-dev packages.
# --prefer-dist  Forces installation from package dist even for dev versions.

# update database

# pour eviter le pb qui fait que la version en prod retourne "local" ...
#php artisan config:cache
APP_ENV=prod php artisan config:clear
#php artisan vendor:publish --provider="Code16\Sharp\SharpServiceProvider" --tag=assets

if [ "${isDev}" = "1" ]; then
    echo "On est en mode dev on lance composer pour du dev..."
    composer install --no-interaction
    composer update
    #copie du vendor hack a la place du vendor deploye par composer (font awesome 5 dans sharp)
    #cp -a public/vendor.hack/sharp public/vendor
    php artisan down --message="Installation du jeu d'essai pour la version de démonstration ..." --retry=10
    if [ -f /tmp/doliscan_force_new_data ]; then
        #avec option -seed -> injection du jeu d'essai
        php artisan migrate:fresh --seed
    else
        php artisan migrate --force
    fi
    php artisan down --message="Compilation des sources javascript ..." --retry=10
    npm install
    #si on est sur le pc de dev ...
    npm run dev
    #mais sur un serveur de demo on passe en prod pour le javascript
    npm run prod
else
    composer install --no-interaction --no-dev --prefer-dist
    # composer update
    #copie du vendor hack a la place du vendor deploye par composer (font awesome 5 dans sharp)
    #cp -a public/vendor.hack/sharp public/vendor
    # --force  Required to run when in production.
    php artisan down --message="Compilation des sources javascript ..." --retry=30
    npm run prod
    php artisan migrate --force
fi

#on optimize le tout mais provoque le pb suivant
php artisan optimize

#20200521 serveur prod affiche des messages de dev (blade) correctif du optimize
# php artisan config:clear

# stop maintenance mode
php artisan up

# et on relance le superviseur qui gere les queues
echo "please run 'sudo systemctl restart supervisor' command to restart supervisor ..."

TIMESTOP=`date +%s`
DELTATIME=$((${TIMESTOP}-${TIMESTART}))
date
echo -n "################################ takes "
echo -n $DELTATIME
echo " seconds to run"
