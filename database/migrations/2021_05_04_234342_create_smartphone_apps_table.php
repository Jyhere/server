<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSmartphoneAppsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('smartphone_apps', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('os', 20); //ios|android
            $table->string('version', 20); //1.6.2
            $table->dateTime('last_mail')->nullable(); //to prevent sending 20 mails per day, only one is enought
            $table->dateTime('last_seen')->nullable(); //updated each time this user is logged in

            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')
                ->references('id')->on('users')
                ->onDelete('cascade');
            $table->timestamps();

            $table->unique(['version', 'os', 'user_id']);

            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('smartphone_apps');
    }
}
