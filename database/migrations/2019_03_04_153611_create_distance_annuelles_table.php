<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDistanceAnnuellesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('distance_annuelles', function (Blueprint $table) {
      $table->increments('id');
      $table->integer('distanceTotale')->nullable();
      $table->year('annee')->nullable();
      $table->unsignedBigInteger('user_id');
      $table->timestamps();

      $table->foreign('user_id')
        ->references('id')->on('users')
        ->onDelete('cascade');

      $table->index(['user_id', 'annee']);
      $table->unique(['user_id', 'annee']);
    });

    /*
    //utilisateur 1 a fait 4500km en 2018
    DB::table('distance_annuelles')->insert(
      array(
        'distanceTotale' => 4500,
        'annee' => '2018',
        'user_id' => 1
      )
    );
*/
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('distance_annuelles');
  }
}
