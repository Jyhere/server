<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBillingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('billings', function (Blueprint $table) {
            $table->bigIncrements('id');
            //le prix unitaire
            $table->float('price')->nullable()->default(null);
            //la date à laquelle on commence
            $table->date('start')->nullable()->default(null);
            //json, historique du tarif, par exemple au début 6€ puis passé à 5 le ... et +5 pour archive probante le ...
            $table->json('history')->nullable()->default(null);
            //commentaires lisibles
            $table->text('comments')->nullable()->default(null);
            //l'entreprise à laquelle on s'adresse
            $table->unsignedBigInteger('entreprise_id');
            //l'entreprise à qui ont facture (peut-être null)
            $table->unsignedBigInteger('billingto_id')->nullable();

            $table->foreign('entreprise_id')->references('id')->on('entreprises');
            $table->foreign('billingto_id')->references('id')->on('entreprises');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('billings');
    }
}
