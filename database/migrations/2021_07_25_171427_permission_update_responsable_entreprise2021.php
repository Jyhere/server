<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class PermissionUpdateResponsableEntreprise2021 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        //Le chef d'entreprise a quand même le droit de modifier les informations de son entreprise ...
        $role = Role::findByName('responsableEntreprise');
        $role->givePermissionTo('show others NdeFrais');
        $role->givePermissionTo('show others LdeFrais');
        $role->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
