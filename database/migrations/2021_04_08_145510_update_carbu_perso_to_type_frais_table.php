<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class UpdateCarbuPersoToTypeFraisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Le carburant peut-être payé par un moyen perso !
        DB::statement("UPDATE `type_frais` SET `perso` = '1' WHERE (`slug` = 'carburant')");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('type_frais', function (Blueprint $table) {
            //
        });
    }
}
