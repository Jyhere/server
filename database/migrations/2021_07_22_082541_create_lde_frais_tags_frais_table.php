<?php
/**
 * 2021_07_22_082541_create_lde_frais_tags_frais_table.php
 *
 * Copyright (c) 2021 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLdeFraisTagsFraisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lde_frais_tags_frais', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->unsignedBigInteger('lde_frais_id');
			$table->unsignedBigInteger('tags_frais_id');
			$table->foreign('lde_frais_id')->references('id')->on('lde_frais')
						->onDelete('restrict')
						->onUpdate('restrict');


			$table->foreign('tags_frais_id')->references('id')->on('tags_frais')
						->onDelete('restrict')
						->onUpdate('restrict');

            $table->unique(['lde_frais_id', 'tags_frais_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lde_frais_tags_frais');
    }
}
