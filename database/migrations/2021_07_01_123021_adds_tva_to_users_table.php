<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddsTvaToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->float('compta_tva_tx1', 4, 2)->nullable()->default('2.1')->after('compta_tvadeductible');
            $table->float('compta_tva_tx2', 4, 2)->nullable()->default('5.5')->after('compta_tva_tx1');
            $table->float('compta_tva_tx3', 4, 2)->nullable()->default('10')->after('compta_tva_tx2');
            $table->float('compta_tva_tx4', 4, 2)->nullable()->default('20')->after('compta_tva_tx3');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
      });
    }
}
