<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomizingAppsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customizing_apps', function (Blueprint $table) {
            $table->bigIncrements('id');
            //le css qu'on veut pousser sur l'app
            $table->text('css')->nullable()->default(null);
            //le texte à mettre sur la boite apropos
            $table->text('apropos')->nullable()->default(null);
            //le message à afficher
            $table->text('message')->nullable()->default(null);
            //et plus si nécessaire
            $table->json('more')->nullable()->default(null);
            //l'id du compte admin revendeur
            $table->unsignedBigInteger('user_id');
            //l'état de la customization (0 = dev / 10 = prod / autre à inventer)
            $table->tinyInteger('status')->default('0');
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customizing_apps');
    }
}
