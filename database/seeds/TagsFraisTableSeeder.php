<?php
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TagsFraisTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    // Let's clear the users table first
    DB::statement('SET FOREIGN_KEY_CHECKS=0;');
    DB::table('lde_frais_tags_frais')->truncate();
    DB::table('tags_frais')->truncate();
    DB::statement('SET FOREIGN_KEY_CHECKS=1;');
  }
}
