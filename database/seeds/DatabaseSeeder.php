<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\App;
use Faker\Generator as Faker;
use Illuminate\Support\Carbon;
use App\LdeFrais;
use App\User;
use App\BaseCalculIks;
use App\MoyenPaiement;
use App\TypeFrais;
use App\NdeFrais;
use App\TagsFrais;
use Illuminate\Support\Facades\Log;

class DatabaseSeeder extends Seeder
{
  /**
   * Seed the application's database.
   *
   * @return void
   */
  public function run()
  {
    //Comme un fait appel aussi a seed depuis l'objet user pour injecter des donnees il faut ruser un peu ...
    if (App::isDownForMaintenance()) {

      // $this->call(ArticlesTableSeeder::class);
      $this->call(VehiculesTableSeeder::class);
      $this->call(EntreprisesTableSeeder::class);
      $this->call(TagsFraisTableSeeder::class);
      $this->call(UsersTableSeeder::class);
      $this->call(NdeFraisTableSeeder::class);
      $this->call(LdeFraisTableSeeder::class);
      $this->call(JobsTableSeeder::class);

      /*
       * Note: on ne peut pas trop réfléchir à un seeder "table par table" pour les notes de frais et les lignes de frais
       *   il faut pouvoir créer une note pour un mois, ajouter des frais et ensuite clôturer la note et passer à la suivante
       *   -> donc je fais cet algo de "seed" ici et non dans chaque table
       */

      //On génère des notes de frais
      //Liste des utilisateurs qui utilisent l'application
      //A accès aux moyens de paiements pro
      $dirigeants   = array(
        'patron@' . config('app.domain'),
        'patron.nautique@' . config('app.domain'),
        'patron.campagne@' . config('app.domain')
      );

      //N'ont que des paiements perso
      $utilisateurs = array(
        'com1@' . config('app.domain'),
        'com1.nautique@' . config('app.domain'),
        'com1.campagne@' . config('app.domain'),
        'com2@' . config('app.domain')
      );
      $toutlemonde  = array_merge($dirigeants, $utilisateurs);

      if (config('app.env') != 'prod') {
        $faker = \Faker\Factory::create('fr_FR');
        $faker->seed(142);
        $faker->addProvider(new \App\Faker\DoliSCAN($faker));
        $faker->addProvider(new \Faker\Provider\fr_FR\Person($faker));
        $faker->addProvider(new \Faker\Provider\fr_FR\Company($faker));

        foreach ($toutlemonde as $u) {
          $this->seedDataForUser($u, $faker, $dirigeants, 12);
        }
      }
    } else {
      echo "No db seed : please put app in maintenance mode before !";
    }
  }

  public function addTags($ldf, $list)
  {
    $tagsTab = array();
    for ($nbTags = 0; $nbTags < rand(0, 4); $nbTags++) {
      $tagsTab[] = $list[rand(0, count($list) - 1)];
    }
    $ldf->tagsFrais()->sync($tagsTab);
  }

  public function seedDataForUser($u, $faker, $dirigeants = array(), $nbmois)
  {
    Log::debug("DatabaseSeeder::seedDataForUser : $u");
    if (config('app.env') != 'prod') {

      $user = User::where('email', $u)->first();
      if (!$user) {
        return -1;
      }
      $calculetteIK = new BaseCalculIks();
      $ldeFrais     = new LdeFrais();
      $tabTagsFrais = array();

      //Moyens de paiements pour dirigeants : perso et pro
      $paiementsDirigeantsIDs = MoyenPaiement::where('id', '>', 1)->pluck('id');
      //Pour les utilisateurs normaux : que perso
      $paiementsPersoIDs = MoyenPaiement::where('id', '>', 1)->where('is_pro', '0')->pluck('id');
      $paiementsPersoPseudoIK = MoyenPaiement::where('slug', 'pseudo-perso-ik')->pluck('id')->first();

      //On garde le même couple vehicule pro/perso pour chaque utilisateur sur l'année ...
      $vehiculePro = $faker->vehiculePro;
      $vehiculePro->user_id = $user->id;
      $vehiculePro->save();

      $vehiculePerso = $faker->vehiculePerso;
      $vehiculePerso->user_id = $user->id;
      $vehiculePerso->save();

      Log::debug("cet utilisateur a un vehicule pro : " . $vehiculePro->id . " et " . json_encode($vehiculePro));
      Log::debug("et aussi un vehicule perso : " . $vehiculePerso->id . " et " . json_encode($vehiculePerso));
      // return;

      //Moyens de paiements : selon dirigeant ou pas
      $paiementsIDs = $paiementsPersoIDs;
      $is_patron = 0;
      if (in_array($u, $dirigeants)) {
        $paiementsIDs = $paiementsDirigeantsIDs;
        $is_patron = 1;
      }

      //On créé des tags pour chaque utilisateur
      for ($t = 1; $t < 10; $t++) {
        $tag = TagsFrais::create(['code' => 'Mission ' . $t, 'user_id' => $user->id, 'label' => 'Mission ' . $t, 'user_id' => $user->id]);
        $tabTagsFrais[] = $tag->id;
      }
      for ($t = 1; $t < 50; $t++) {
        $tag = TagsFrais::create(['code' => 'CLIENT-' . $t, 'user_id' => $user->id, 'label' => 'Client ' . $faker->unique()->company, 'user_id' => $user->id]);
        $tabTagsFrais[] = $tag->id;
      }

      //On créé 1 an de notes de frais pour chaque utilisateur
      for ($m = $nbmois; $m >= 0; $m--) {
        $debut  = Carbon::now()->subMonths($m)->startOfMonth();
        $fin  = Carbon::now()->subMonths($m)->endOfMonth();
        print "Début du mois :" . $debut . " et fin " . $fin . "\n";

        //On créé la note "ouverte"
        $ndf = NdeFrais::create([
          'label' => $debut->monthName . " " . $debut->year,
          'debut' => $debut,
          'fin' => $fin,
          'status' => NdeFrais::STATUS_OPEN,
          'montant' => 0,
          'user_id' => $user->id,
          'created_at' => $debut,
        ]);


        //Et on ajoute des frais ----------------------------------------------------------------------------------------------------------

        //Frais de restauration
        Log::debug('=================== restauration multi TVA start');
        $rID = TypeFrais::where('slug', 'restauration')->first()->id;
        for ($j = 0; $j < $faker->numberBetween(6, 26); $j++) {
          // $directory    = storage_path() . "/LdeFrais/$u/";
          // if (!is_dir($directory)) mkdir($directory);
          // $image = $faker->image($directory, 600, 400, 'technics', true, true, 'DoliScan');
          // $image = str_replace($directory . "/", "", $image);
          //Le HT total
          $ht = $faker->randomFloat(2, 50, 450);
          //La base de calcul de la part à 5.5% comprise entre 0 et le total ht
          $ht5 = $faker->randomFloat(2, 0, $ht);
          $reste = $ht - $ht5;
          $ht10 = $faker->randomFloat(2, 0, $reste);
          $ht20 = $reste - $ht10;
          $tva5 = round((float) ($ht5 * 0.055), 2);
          $tva10 = round((float) ($ht10 * 0.10), 2);
          $tva20 = round((float) ($ht20 * 0.20), 2);
          $ttc = $ht + $tva5 + $tva10 + $tva20;
          $ladate = $faker->dateTimeBetween($debut, $fin)->format("Y-m-d");
          $invites = $faker->name;


          //On a un nombre variable d'invités au resto ... entre 1 et 6
          for ($inv = 1; $inv < $faker->numberBetween(0, 6); $inv++) {
            $invites .= ", " . $faker->name;
          }
          // DB::enableQueryLog();
          $tab = [
            'label' => $faker->nameRestauration,
            // 'fileName' => $image,
            'ladate' => $ladate,
            'created_at' => $ladate,
            'ttc' => $ttc,
            'ht' => $ht,
            'tvaTx1' => "5.5%",
            'tvaVal1' => $tva5,
            'tvaTx2' => "10%",
            'tvaVal2' => $tva10,
            'tvaTx3' => "20%",
            'tvaVal3' => $tva20,
            'invites' => $invites,
            'user_id' => $user->id,
            'type_frais_id' => $rID,
            'moyen_paiement_id' => $faker->randomElement($paiementsIDs),
            'nde_frais_id' => $ndf->id,
          ];
          $ldf = LdeFrais::create($tab);
          echo "  Resto du $ladate pour " . $faker->nameRestauration . " tva $tva5 :: $tva10 :: $tva20 :: total ttc=$ttc \n";

          Log::debug('Creation de frais de restauration avec multi TVA: ' . implode(',', $tab));
          //affectation de tags
          $this->addTags($ldf, $tabTagsFrais);
          // Log::debug(DB::getQueryLog());
          // DB::disableQueryLog();
        }
        Log::debug('=================== restauration multi TVA end');


        //Frais de carburant (Pour un véhicule utilitaire)
        $rID = TypeFrais::where('slug', 'carburant')->first()->id;

        for ($i = 0; $i < $faker->numberBetween(1, 4); $i++) {
          // $directory    = storage_path() . "/LdeFrais/$u/";
          // if (!is_dir($directory)) mkdir($directory);
          // $image = $faker->image($directory, 600, 400, 'technics', true, true, 'DoliScan');
          // $image = str_replace($directory . "/", "", $image);
          $ladate = $faker->dateTimeBetween($debut, $fin)->format("Y-m-d");
          $ldf = LdeFrais::create([
            'label' => $faker->nameCarburant,
            // 'fileName' => $image,
            'ladate' => $ladate,
            'created_at' => $ladate,
            'vehicule_id' => $vehiculePro->id,
            'ttc' => $faker->randomFloat(2, 70, 120),
            'user_id' => $user->id,
            'type_frais_id' => $rID,
            'moyen_paiement_id' => $faker->randomElement($paiementsIDs),
            'nde_frais_id' => $ndf->id,
          ]);
          //affectation de tags
          $this->addTags($ldf, $tabTagsFrais);
        }
        // //Si on est le patron on a peut-être un véhicule "particulier" pour lequel on fait des IK
        // if ($is_patron) {
        //   for ($i = 0; $i < $faker->numberBetween(1, 4); $i++) {
        //     $ladate = $faker->dateTimeBetween($debut, $fin)->format("Y-m-d");
        //     // $directory    = storage_path() . "/LdeFrais/$u/";
        //     // if (!is_dir($directory)) mkdir($directory);
        //     // $image = $faker->image($directory, 600, 400, 'technics', true, true, 'DoliScan');
        //     // $image = str_replace($directory . "/", "", $image);
        //     $ldf = LdeFrais::create([
        //       'label' => $faker->nameCarburant,
        //       // 'fileName' => $image,
        //       'ladate' => $ladate,
        //       'created_at' => $ladate,
        //       'vehicule_id' => $vehiculePerso->id,
        //       'ttc' => $faker->randomFloat(2, 70, 120),
        //       'user_id' => $user->id,
        //       'type_frais_id' => $rID,
        //       'moyen_paiement_id' => $faker->randomElement($paiementsIDs),
        //       'nde_frais_id' => $ndf->id,
        //     ]);
        //     //affectation de tags
        //     $this->addTags($ldf, $tabTagsFrais);
        //   }
        // }

        //Des IK si on est le patron avec véhicule perso ou le salarié avec véhicule perso
        //donc paiement par moyens perso
        $rID = TypeFrais::where('slug', 'ik')->first()->id;
        for ($i = 0; $i < $faker->numberBetween(1, 40); $i++) {
          $depart = $faker->nameVille;
          $arrivee = $faker->nameVille;
          $distance = $calculetteIK->distance($depart, $arrivee);

          //Penser à faire une petite pause pour eviter d'exploser les limites de req/API/sec
          sleep(1);

          $cv = $vehiculePerso->power;
          $ladate = $faker->dateTimeBetween($debut, $fin)->format("Y-m-d");

          echo "  IK pour $depart -> $arrivee : $distance \n";
          // $directory    = storage_path() . "/LdeFrais/$u/";
          // if (!is_dir($directory)) mkdir($directory);
          // $image = $faker->image($directory, 600, 400, 'technics', true, true, 'DoliScan');
          // $image = str_replace($directory . "/", "", $image);
          if ($distance > 0 && $distance < 1500) {
            $ldf = LdeFrais::create([
              'label' => "Rdv client " . $faker->name(),
              // 'fileName' => $image,
              'depart' => $depart,
              'arrivee' => $arrivee,
              'distance' => $distance,
              'vehicule_id' => $vehiculePerso->id,
              // 'ttc' => $calculetteIK->calcul($cv, $distance, 0), c'est justement un pb on a pas le montant ttc dans la table ldf pour les ik
              'ladate' => $ladate,
              'created_at' => $ladate,
              'user_id' => $user->id,
              'type_frais_id' => $rID,
              'moyen_paiement_id' => $paiementsPersoPseudoIK,
              'nde_frais_id' => $ndf->id,
            ]);
            //affectation de tags
            $this->addTags($ldf, $tabTagsFrais);
          } else {
            echo "    error: $distance nulle ou >= 1500km pour $depart -> $arrivee on ne fait pas l'insert !\n";
          }
        }


        //Frais de péage et parking
        $rID = TypeFrais::where('slug', 'peage')->first()->id;
        for ($i = 0; $i < $faker->numberBetween(1, 20); $i++) {
          // $directory    = storage_path() . "/LdeFrais/$u/";
          // if (!is_dir($directory)) mkdir($directory);
          // $image = $faker->image($directory, 600, 400, 'technics', true, true, 'DoliScan');
          // $image = str_replace($directory . "/", "", $image);
          //Peage
          $ht = $faker->randomFloat(2, 4, 80);
          //Le HT ... on a de la TVA à 20%
          $tva20 = (float) $ht * 0.20;
          $ttc = $ht + $tva20;
          $ladate = $faker->dateTimeBetween($debut, $fin)->format("Y-m-d");

          $ldf = LdeFrais::create([
            'label' => $faker->namePeage,
            // 'fileName' => $image,
            'ladate' => $ladate,
            'created_at' => $ladate,
            'ttc' => $ttc,
            'ht' => $ht,
            'tvaTx1' => "20%",
            'tvaVal1' => $tva20,
            'user_id' => $user->id,
            'type_frais_id' => $rID,
            'moyen_paiement_id' => $faker->randomElement($paiementsIDs),
            'nde_frais_id' => $ndf->id,
          ]);
          //affectation de tags
          $this->addTags($ldf, $tabTagsFrais);

          //Parking - il n'y a quasi jamais de tva indiquée sur les tickets
          $ladate = $faker->dateTimeBetween($debut, $fin)->format("Y-m-d");
          $ldf = LdeFrais::create([
            'label' => $faker->nameParking,
            // 'fileName' => $image,
            'ladate' => $ladate,
            'created_at' => $ladate,
            'ttc' => $faker->randomFloat(2, 4, 80),
            'user_id' => $user->id,
            'type_frais_id' => $rID,
            'moyen_paiement_id' => $faker->randomElement($paiementsIDs),
            'nde_frais_id' => $ndf->id,
          ]);
          //affectation de tags
          $this->addTags($ldf, $tabTagsFrais);
        }

        //Frais d'hotel
        $rID = TypeFrais::where('slug', 'hotel')->first()->id;
        for ($i = 0; $i < $faker->numberBetween(1, 6); $i++) {
          $ladate = $faker->dateTimeBetween($debut, $fin)->format("Y-m-d");

          // $directory    = storage_path() . "/LdeFrais/$u/";
          // if (!is_dir($directory)) mkdir($directory);
          // $image = $faker->image($directory, 600, 400, 'technics', true, true, 'DoliScan');
          // $image = str_replace($directory . "/", "", $image);
          $ldf = LdeFrais::create([
            'label' => $faker->nameHotel,
            // 'fileName' => $image,
            'ladate' => $ladate,
            'created_at' => $ladate,
            'ttc' => $faker->randomFloat(2, 50, 160),
            'user_id' => $user->id,
            'type_frais_id' => $rID,
            'moyen_paiement_id' => $faker->randomElement($paiementsIDs),
            'nde_frais_id' => $ndf->id,
          ]);
          //affectation de tags
          $this->addTags($ldf, $tabTagsFrais);
        }

        //Taxi
        $rID = TypeFrais::where('slug', 'taxi')->first()->id;
        for ($i = 0; $i < $faker->numberBetween(1, 10); $i++) {
          // $directory    = storage_path() . "/LdeFrais/$u/";
          // if (!is_dir($directory)) mkdir($directory);
          // $image = $faker->image($directory, 600, 400, 'technics', true, true, 'DoliScan');
          // $image = str_replace($directory . "/", "", $image);
          $ladate = $faker->dateTimeBetween($debut, $fin)->format("Y-m-d");

          $ldf = LdeFrais::create([
            'label' => $faker->nameTaxi,
            // 'fileName' => $image,
            'ladate' => $ladate,
            'created_at' => $ladate,
            'ttc' => $faker->randomFloat(2, 10, 120),
            'user_id' => $user->id,
            'type_frais_id' => $rID,
            'moyen_paiement_id' => $faker->randomElement($paiementsIDs),
            'nde_frais_id' => $ndf->id,
          ]);
          //affectation de tags
          $this->addTags($ldf, $tabTagsFrais);
        }

        //Frais Divers
        $rID = TypeFrais::where('slug', 'divers')->first()->id;
        for ($i = 0; $i < $faker->numberBetween(1, 10); $i++) {
          $ht = $faker->randomFloat(2, 5, 100);
          //Le HT ... on a de la TVA à 20%
          $tva20 = (float) $ht * 0.20;
          $ttc = $ht + $tva20;
          // $directory    = storage_path() . "/LdeFrais/$u/";
          // if (!is_dir($directory)) mkdir($directory);
          // $image = $faker->image($directory, 600, 400, 'technics', true, true, 'DoliScan');
          // $image = str_replace($directory . "/", "", $image);
          $ladate = $faker->dateTimeBetween($debut, $fin)->format("Y-m-d");

          $ldf = LdeFrais::create([
            'label' => $faker->nameDivers,
            // 'fileName' => $image,
            'ladate' => $ladate,
            'created_at' => $ladate,
            'ttc' => $ttc,
            'ht' => $ht,
            'tvaTx1' => "20%",
            'tvaVal1' => $tva20,
            'user_id' => $user->id,
            'type_frais_id' => $rID,
            'moyen_paiement_id' => $faker->randomElement($paiementsIDs),
            'nde_frais_id' => $ndf->id,
          ]);
          //affectation de tags
          $this->addTags($ldf, $tabTagsFrais);
        }

        //Frais de train ou d'avion
        $rID = TypeFrais::where('slug', 'train')->first()->id;
        for ($i = 0; $i < $faker->numberBetween(1, 8); $i++) {
          // $directory    = storage_path() . "/LdeFrais/$u/";
          // if (!is_dir($directory)) mkdir($directory);
          // $image = $faker->image($directory, 600, 400, 'technics', true, true, 'DoliScan');
          // $image = str_replace($directory . "/", "", $image);
          $ladate = $faker->dateTimeBetween($debut, $fin)->format("Y-m-d");

          $ldf = LdeFrais::create([
            'label' => $faker->nameTrainOuAvion,
            // 'fileName' => $image,
            'ladate' => $ladate,
            'created_at' => $ladate,
            'ttc' => $faker->randomFloat(2, 35, 240),
            'user_id' => $user->id,
            'type_frais_id' => $rID,
            'moyen_paiement_id' => $faker->randomElement($paiementsIDs),
            'nde_frais_id' => $ndf->id,
          ]);
          //affectation de tags
          $this->addTags($ldf, $tabTagsFrais);
        }

        //Et on ferme la NDF si c'est pas la dernière du mois en cours
        if ($m > 0) {
          $ndf->close();
        }
        // else {
        //   $ndf->status = NdeFrais::STATUS_OPEN;
        // }
      }

      //Empty jobs - to avoid mail self-spams
      $this->call(JobsTableSeeder::class);
    }
  }
}
