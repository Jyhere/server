<?php
use Illuminate\Database\Seeder;
use App\NdeFrais;
use App\LdeFrais;
use Illuminate\Support\Carbon;

class NdeFraisTableSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    //
    DB::statement('SET FOREIGN_KEY_CHECKS=0;');
    LdeFrais::truncate();
    NdeFrais::truncate();
    DB::statement('SET FOREIGN_KEY_CHECKS=1;');

    // NdeFrais::create([
    //   'label' => 'Décembre 2018',
    //   'debut' => '2018-12-01',
    //   'fin' => '2018-12-31',
    //   'status' => NdeFrais::STATUS_CLOSED,
    //   'montant' => 1586.15,
    //   'user_id' => 2
    // ]);

    // NdeFrais::create([
    //   'label' => 'Janvier 2019',
    //   'debut' => '2019-01-01',
    //   'fin' => '2019-01-31',
    //   'status' => NdeFrais::STATUS_OPEN,
    //   'montant' => 0,
    //   'user_id' => 2
    // ]);

    // NdeFrais::create([
    //   'label' => 'Décembre 2018',
    //   'debut' => '2018-12-01',
    //   'fin' => '2018-12-31',
    //   'status' => NdeFrais::STATUS_CLOSED,
    //   'montant' => 682.16,
    //   'user_id' => 3
    // ]);

    // NdeFrais::create([
    //   'label' => 'Janvier 2019',
    //   'debut' => '2019-01-01',
    //   'fin' => '2019-01-31',
    //   'status' => NdeFrais::STATUS_CLOSED,
    //   'montant' => 0,
    //   'user_id' => 3
    // ]);

    // NdeFrais::create([
    //   'label' => 'Février 2019',
    //   'debut' => '2019-02-01',
    //   'fin' => '2019-02-28',
    //   'status' => NdeFrais::STATUS_CLOSED,
    //   'montant' => 0,
    //   'user_id' => 3
    // ]);

    // NdeFrais::create([
    //   'label' => 'Mars 2019',
    //   'debut' => '2019-03-01',
    //   'fin' => '2019-03-31',
    //   'status' => NdeFrais::STATUS_CLOSED,
    //   'montant' => 0,
    //   'user_id' => 3
    // ]);

    // NdeFrais::create([
    //   'label' => 'Avril 2019',
    //   'debut' => '2019-04-01',
    //   'fin' => '2019-04-30',
    //   'status' => NdeFrais::STATUS_CLOSED,
    //   'montant' => 0,
    //   'user_id' => 3
    // ]);

    // NdeFrais::create([
    //   'label' => 'Mai 2019',
    //   'debut' => '2019-05-01',
    //   'fin' => '2019-05-31',
    //   'status' => NdeFrais::STATUS_CLOSED,
    //   'montant' => 0,
    //   'user_id' => 3
    // ]);

    // NdeFrais::create([
    //   'label' => 'Juin 2019',
    //   'debut' => '2019-06-01',
    //   'fin' => '2019-06-30',
    //   'status' => NdeFrais::STATUS_CLOSED,
    //   'montant' => 0,
    //   'user_id' => 3
    // ]);

    // NdeFrais::create([
    //   'label' => 'Juillet 2019',
    //   'debut' => '2019-07-01',
    //   'fin' => '2019-07-31',
    //   'status' => NdeFrais::STATUS_CLOSED,
    //   'montant' => 0,
    //   'user_id' => 3
    // ]);

    // NdeFrais::create([
    //   'label' => 'Août 2019',
    //   'debut' => '2019-08-01',
    //   'fin' => '2019-08-31',
    //   'status' => NdeFrais::STATUS_CLOSED,
    //   'montant' => 0,
    //   'user_id' => 3
    // ]);

    // NdeFrais::create([
    //   'label' => 'Septembre 2019',
    //   'debut' => '2019-09-01',
    //   'fin' => '2019-09-30',
    //   'status' => NdeFrais::STATUS_CLOSED,
    //   'montant' => 0,
    //   'user_id' => 3
    // ]);


    // NdeFrais::create([
    //   'label' => 'Octobre 2019',
    //   'debut' => '2019-10-01',
    //   'fin' => '2019-10-31',
    //   'status' => NdeFrais::STATUS_CLOSED,
    //   'montant' => 0,
    //   'user_id' => 3
    // ]);


    // NdeFrais::create([
    //   'label' => 'Novembre 2019',
    //   'debut' => '2019-11-01',
    //   'fin' => '2019-11-30',
    //   'status' => NdeFrais::STATUS_CLOSED,
    //   'montant' => 0,
    //   'user_id' => 3
    // ]);


    // NdeFrais::create([
    //   'label' => 'Décembre 2019',
    //   'debut' => '2019-12-01',
    //   'fin' => '2019-12-31',
    //   'status' => NdeFrais::STATUS_OPEN,
    //   'montant' => 0,
    //   'user_id' => 3
    // ]);

  }
}
