# DOLISCAN Web - branche "prod"

Résumé synthétique des évolutions du code pour ne pas avoir à aller chercher dans les
messages de commit ...

## 2021/10/20
- amélioration de la recuperation des données de facturation (ne pas charger des comptes inactifs)
- fix deprecated const on trustproxy
- corrige la mention "provisoire" sur les ndf en fin de période
- nouvelles commandess cli (rebuild ndf, cdf, send mail)
- envoi automatique d'un mail d'invitation lors de la création d'un utilisateur
- améliorations dans le coeur du système pour élargir le jeu d'essai
- avancées pour le support de dolibarr + multicompany
- échanges de mails pour authentifier un utilisateur qui se reconnecterait via dolibarr
- modification d'une réponse json/jsonResponse pour forcer le passage en utf-8
- amélioration du déploiement d'un serveur de dev/tests
- relecture optimisation et factorisation de code pour l'authentification via l'api
- changement de maniere de numeroter la version du serveur : prod/2021.10.20

## 2021/10/10

- nouvelle cle de configuration dans le .env : MAIL_SECURITY

## 2021/07/07
- Ajout d'un export de logs pour fail2ban sous la forme de l'implémentation d'un pot de miel
  vu les milliers de lignes qui trainent dans nos logs et qui essayent de récupérer le fichier
  .env ou des failles connues de vendor/* -> direction fail2ban

## 2021/05/11
- Ajout d'une clé SRV_SIRET et SRV_SIRET_KEY pour l'accès au webservice de résolution des numéros
  SIREN/SIRET (normalement pas nécessaire si vous êtes en auto-hébergement: vous n'allez pas créer
  des centaines d'entreprises sur votre serveur, vous auto-hébergez votre structure)
  Si nécessaire -> https://siret.cap-rel.fr/ propose un pack d'accès à l'API pour une somme modique
  et ça sponsorise le dev !

## 2021/05/09
- Ajout d'une clé SRV_MONITORING dans le fichier .env pour exclure des logs les "ping" provenant du
  serveur de supervision

## 2021/05/03

- Attention, changement de la clé DEVMODE_MAIL_TO en MAIL_DEVMODE_TO dans le fichier .env pour plus de
  cohérence (prefixe MAIL_ unifié)
- Ajout de deux clés de configuration : MAIL_PING et MAIL_SAV pour eviter d'exfiltrer des données vers
  doliscan.fr si vous auto-hébergez votre serveur (c'est un comble) !
- Modification du délais de pause après envoi de mail par défaut de 30 à 1 seconde pour éviter un bug
  en cas d'auto-hébergement si redis n'est pas disponible

## 2021/03/25

- Gestion des véhicules sur le backend web
- Amelioration des accès pour le comptable entreprise : accès aux NDF des salariés
- Customization de l'app cliente pour les revendeurs (option marque blanche)
- Fin de la gestion du taux de 60% de récupération de la TVA pour le carburant des véhicules de société

## 2021/03/10

- Utilisateur : affichage du qrcode d'autoconnexion [ok]
- Utilisateur : fermer son compte [ok]
- Utilisateur : télécharger toutes ses données [ok]
- Utilisateur : changer d'adresse email [ok]
- Grosses modifications dans la présentation des logs sur le backend web
- Mise à jour de sharp pour le backend de l'application serveur
- Modification des graphes affichés sur la page d'accueil du backend web

## 2021/03/05

- Changement de structure de la base de données : utilisation du code SIREN et non plus
  SIRET pour ne pas cloisonner des utilisateurs dont l'entreprise aurait plusieurs
  établissements
- Ajout du barème de remboursement des IK 2021 + ajout de la majoration spéciale pour les
  véhicules électriques (nouvelle règle française, une de plus)
- Amélioration des procedures automatiques (cron), la temporisation n'est plus dans le
  code principal mais uniquement dans le worker/job d'envoi des mails

## 2021/02/25

- Mise à jour du champ email_verified_at lorsqu'un usager clique sur le lien d'invitation
- Ajout d'une entrée dans le cron qui renvoie le mail d'invitation lorsque le délais de
  validité du jeton est dépassé et que l'utilisateur n'a pas encore validé son compte ...
- Amélioration de l'envoi des mails (factorisation de code)

## 2021/02/11

- Création d'une API pour permettre la récupération automatique de données de factuation
- Améliore des codes erreurs retournés en cas de problème
- Un revendeur ou administrateur limité peut maintenant consulter le journal des évènements
  restreint à son périmètre (Logs)
- Améliore les logs pour avoir les infos de connexion via l'API
- Ajoute le revendeur en copie des mails envoyés lors de la création d'un compte utilisateur
- Ajout d'un filigrane "Brouillon" sur les documents PDF générés tant que la note de frais
  n'est pas clôturée
- Ajout d'un filtre possible sur la liste des utilisateurs pour n'afficher que les membres
  d'une entreprise

## 2021/02/04

- Autorise certains frais en doublons (exemple péage quand on fait un A/R dans la journée)
- Gestion du QRCode utilisateur pour pouvoir le flasher via l'application et autoconfigurer
  le login & le serveur de connexion (pratique pour les auto-hébergements)
- remplace les appels à env par config pour pouvoir bénéficier du cache/compilation Laravel
- .../...

## Avant

Il n'y avait pas de ChangeLog tout simplement !
