<?php
/*
 * ExportNDF.php
 *
 * Copyright (c) 2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use App\NdeFrais;

class ExportNDF
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $ndf;
    public $ndfOrJustificatifs;
    public $lignesExportCompta;
    public $lignesFraisPerso;
    public $lignesFraisPro;
    public $lignesIK;
    public $utilisationDoliScanAnneeComplete;
    public $contexte;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    //        event(new ExportNDF($this->_ndf, "ndf", $typeFrais, $lignesperso, $keepVehicules, $lignesIK, $typeFraispro, $lignespro));
    public function __construct(NdeFrais $ndf, $ndfOrJustificatifs, $lignesExportCompta, $lignesFraisPerso, $lignesFraisPro, $lignesIK, $utilisationDoliScanAnneeComplete, $contexte)
    {
        //
        Log::debug("ExportNDF :: construct");
        $this->ndf                  = $ndf;
        $this->ndfOrJustificatifs   = $ndfOrJustificatifs;
        $this->lignesExportCompta   = $lignesExportCompta;
        $this->lignesFraisPerso     = $lignesFraisPerso;
        $this->lignesFraisPro       = $lignesFraisPro;
        $this->lignesIK             = $lignesIK;
        $this->utilisationDoliScanAnneeComplete = $utilisationDoliScanAnneeComplete;
        $this->contexte             = $contexte;
        // Log::debug(json_encode($this->lignesIK));
        Log::debug("ExportNDF :: construct end");
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        Log::debug("ExportNDF :: broadcastOn");
        return new PrivateChannel('exports.exportNDF');
    }
}
