<?php
/*
 * LdeFraisImage.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class LdeFraisImage extends Model
{
    //
    private $fileName = "";
    private $userName = "";
    private $fullFileName = "";

    public function __construct($filename = "", $username = "")
    {
        Log::debug("LdeFraisImage::__construct: $filename / $username");

        $this->fileName = $filename;
        $this->userName = $username;
        $this->fullFileName = $this->getFullFileName($filename, $username);
    }

    public function delete()
    {
        if ($this->getFullFileName()) {
            unlink($this->getFullFileName());
        }
    }

    public function getFullFileName()
    {
        if ($this->fileName == "" || $this->userName == "") {
            return null;
        }
        if ($this->fullFileName == "") {
            $ledossier   = substr($this->fileName, 0, 6);
            $fic         = storage_path() . "/LdeFrais/" . $this->userName . "/" . $ledossier . "/" . $this->fileName;
            $this->fullFileName = $fic;

            if (file_exists($fic) && is_file($fic)) {
                Log::debug("LdeFrais::getFullFileName : $fic existe");
            } else {
                Log::debug("LdeFraisImage::getFullFileName : $fic n'existe pas ...");
            }
        }
        Log::debug("LdeFraisImage getFullFileName : " . $this->fullFileName);
        return $this->fullFileName;
    }
}
