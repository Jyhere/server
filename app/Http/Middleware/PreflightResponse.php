<?php
        // Pas necessaire on a un .htaccess qui fait le boulot

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Log;

class PreflightResponse
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Log::debug(" PreflightResponse Handler ... : " . $request->getMethod());
        if ($request->getMethod() === "OPTIONS") {
            Log::debug(" PreflightResponse " . json_encode($request->headers->all()));
            return response('', 204)
                ->header('Access-Control-Allow-Origin', '*') // Allow requests from any origin
                ->header('Access-Control-Allow-Headers', 'X-Auth, Origin, Content-Type, Accept, X-Auth-Token, Authorization, X-Requested-With, Session, Host, Accept-Encoding, Connection, User-Agent, Content-Length, Accept-Language, resumableChunkNumber, resumableChunkSize, resumableCurrentChunkSize, resumableTotalSize, resumableType, resumableIdentifier, resumableFilename, resumableRelativePath, resumableTotalChunks, ldffilename')
                ->header('Access-Control-Expose-Headers', 'Upload-Key, Upload-Checksum, Upload-Length, Upload-Offset, Upload-Metadata, Location, resumableChunkNumber, resumableChunkSize, resumableCurrentChunkSize, resumableTotalSize, resumableType, resumableIdentifier, resumableFilename, resumableRelativePath, resumableTotalChunk, ldffilename')
                ->header('Access-Control-Allow-Credentials', 'true')
                ->header('Access-Control-Allow-Methods', 'GET, POST, PATCH, PUT, DELETE, OPTIONS') // Allow requests with these methods
                ->header('Access-Control-Max-Age', '10000');
        } else {
            return $next($request);
        }
    }
}
