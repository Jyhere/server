<?php
/*
 * RedirectIfAuthenticated.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        Log::debug('======= RedirectIfAuthenticated / handle ============');
        // Log::debug(request()->route()->uri());
        // Log::debug(request()->route()->getActionName());

        //shortcut
        if ($request->is('api/password/email')) {
            Log::debug('RedirectIfAuthenticated:: request password/email');
            return $next($request);
        }
        if ($request->is('api/*')) {
            Log::debug('RedirectIfAuthenticated::api -> next');
            return $next($request);
        }
        Log::debug('RedirectIfAuthenticated::guard ' . $guard);

        if (Auth::guard($guard)->check()) {
            Log::debug('RedirectIfAuthenticated:: auth guard checked');
            if ($request->is('api/*')) {
                Log::debug('  RedirectIfAuthenticated : API ============');

                //nothing to do (go to next step)
            } else {
                //Si on est en interface "web" on fait un redirect vers la page /home
                Log::debug('  RedirectIfAuthenticated  -> /home');
                return redirect('/home');
            }
        }
        return $next($request);
    }
}
