<?php
/*
 * LdeFraisController.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Http\File;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use DB;

//Pour les tests de rotation de l'image
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Spatie\LaravelImageOptimizer\Facades\ImageOptimizer;

use App\LdeFrais;
use App\TypeFrais;
use App\BaseCalculIks;
use App\MoyenPaiement;
use App\NdeFrais;
use Auth;
use Mail;
use App\Jobs\ProcessSendEmail;
use App\Vehicule;

class LdeFraisController extends Controller
{
    /**
     * Get / Convert val as float
     *
     * @param   [type]  $val  [$val description]
     *
     * @return  [type]        [return description]
     */
    protected function myFloatValue($val)
    {
        if ($val != "") {
            $v = $val;
            if ($val == "NaN" || $val == "null" || empty($val)) {
                $val = 0;
            }
            $val = str_replace(",", ".", $val);
            $val = preg_replace('/\.(?=.*\.)/', '', $val);
            Log::debug("  LdeFraisController myFloatValue from $v converted to $val");
            if (is_numeric($val)) {
                return floatval($val);
            }
            return floatval(0);
        }
        return floatval(0);
    }

    /**
     * pour simplifier la vie on ne pousse pas forcément le détail de la tva
     * exemple parking on "pousse" HT / TTC et tvatx4=20 et rien de plus ...
     *
     * @param   LdeFrais  $l  La ligne de frais en cours
     *
     * @return  LdeFrais
     */
    public function recolleTVA(&$l)
    {
        Log::debug('======= Recolle la TVA ============');
        $tabTVAUtilisee = array();
        for ($i = 1; $i < 5; $i++) {
            //Liste des taux de tva déjà utilisés
            $tx = "tvaTx$i";
            $val = "tvaVal$i";
            //Si ce taux est déjà dans la liste on reset les données
            // Log::debug("  Recolle la TVA :: recherche " . $l->{$tx} . " dans " . json_encode($tabTVAUtilisee));
            if (in_array($l->{$tx}, $tabTVAUtilisee)) {
                // Log::debug("  Recolle la TVA :: taux déjà utilisé, reset data");
                $l->{$tx} = null;
                $l->{$val} = null;
            } else {
                array_push($tabTVAUtilisee, $l->{$tx});
                Log::debug("  Recolle la TVA pour $tx :: $val => " . $l->{$tx});
                if ($l->{$tx} > 0 && $l->{$val} == 0) {
                    $tmpVal = round($l->ttc - $l->ht, 2);
                    $tmpVerif = round($l->ht * $l->{$tx} / 100, 2);
                    Log::debug("  Recolle la TVA confirmé pour $i: " . $l->{$tx} . " - tva = $tmpVal et verif = $tmpVerif");
                    //Si on est ok à 2 centimes près c'est qu'on a eu affaire a des calculs d'arrondis différents en js / php
                    if (abs($tmpVal - $tmpVerif) <= 0.02) {
                        $l->{$val} = $tmpVal;
                        Log::debug("  Recolle la TVA appliquée pour $tx :: $val = $tmpVal");
                    } else {
                        Log::debug("  Recolle la TVA non appliquée pour $tx :: $val != $tmpVal");
                    }
                }
            }
        }
        Log::debug(json_encode($l));
        return;
    }

    //
    public function index()
    {
        Log::debug("LdeFraisController index");
        Auth::user()->LdeFrais;
    }

    /**
     * Show LdeFrais
     *
     * @param   [type]  $id  [$id description]
     *
     * @return  [type]       [return description]
     */
    public function show($id)
    {
        Log::debug("==========LdeFraisController : show $id =========");
        //TODO dispositif pour eviter d'accéder à une ldf dont on a pas le droit ...
        $ldf = LdeFrais::findOrFail($id);

        //On ajoute deux champs composés: montant et texte pour simplifier l'affichage sur smartphone
        // if ($ldf->ht > 0 && ($ldf->ht != $ldf->ttc))
        //     $ldf->montant = "HT: " . $ldf->ht . "€";
        if ($ldf->ttc > 0) {
            if ($ldf->montant != "") {
                $ldf->montant .= " / ";
            }
            $ldf->montant .= "TTC: " . nbFR($ldf->ttc) . "€";
        }
        if ($ldf->montant == "") {
            $ldf->montant = "Ce document n'a pas encore été analysé";
        }


        if ($ldf->label != "") {
            $tf = new TypeFrais();
            $ldf->texte = $tf->getLabelFromID($ldf->type_frais_id) . " : " . $ldf->label;
        }
        if ($ldf->depart != "" && $ldf->arrivee != "" && $ldf->distance != "" && $ldf->distance > 0) {
            $ldf->texte .= " " . $ldf->depart . " -> " . $ldf->arrivee . "(" . $ldf->distance . " km)";
        }
        if ($ldf->invites != "") {
            $ldf->texte .= " " . $ldf->invites;
        }
        if ($ldf->texte == "") {
            $ldf->texte = "Ce document n'a pas encore été analysé";
        }

        //Le type de frais en toutes lettres
        $ldf->frais_slug = $ldf->typeFrais->slug;
        //Et le moyen de paiement
        $ldf->paiement_slug = $ldf->moyenPaiement->slug;

        //Est-ce qu'on a le droit de modifier cette facturette ? -> si la note n'est pas cloturée
        Log::debug("========== status : " . $ldf->ndeFrais->status . " a comparer avec " . NdeFrais::STATUS_CLOSED);
        if ($ldf->ndeFrais->status != NdeFrais::STATUS_CLOSED) {
            $ldf->isUpdatable = true;
        } else {
            $ldf->isUpdatable = false;
        }
        // Log::debug("LdeFraisController::show return " . json_encode($ldf));

        return $ldf;
    }

    public function store(Request $request)
    {
        Log::debug("========== LdeFraisController : store =========");
        // Log::debug(json_encode($request->all()));

        //debug complet de la requete entrante ...
        $tmpfname = storage_path() . "/logs/" . time() . "-upload.debug";
        Log::debug('Pour le contenu complet de la requete, voir le fichier ' . $tmpfname);
        if ($fp = fopen($tmpfname, 'w')) {
            Log::debug("  ouveture ok fichier $tmpfname");
            fwrite($fp, $request);
            fwrite($fp, "\n\n");
            fwrite($fp, \json_encode($request->all()));
            fwrite($fp, "\n\n");
            fclose($fp);
        } else {
            Log::debug("  impossible d'ouvrir le fichier $tmpfname");
        }

        //Il faudrait esayer d'éviter les doublons: même date, même montant, même auteur, même label ... beep
        //-> on update la ligne ? (ça arrive sur les resync)
        //Si on a affaire à des IK il faut faire une recherche un peu differente, quoi-qu'on puisse faire 2 fois le meme trajet dans la mm journée non ?
        //idem pour les tickets de péage d'autoroute on a parfois l'aller et le retour dans la même journée :)
        $tf = new TypeFrais();
        $LdeFrais = null;

        $vehiculeFromPOST    = $request->input('vehicule', "");
        $vehicule = Vehicule::searchFromPOST($vehiculeFromPOST);

        if ($request->input('type_frais_id') == $tf->getIDfromSlug("ik")) {
            $LdeFrais = LdeFrais::where('user_id', Auth::id())->where('ladate', $request->input('ladate'))
                ->where('depart', $request->input('depart'))
                ->where('arrivee', $request->input('arrivee'))
                ->where('distance', $request->input('distance'))
                ->where('vehicule_id', $vehicule->id)
                ->where('type_frais_id', $request->input('type_frais_id'))
                ->where('label', $request->input('label'))->first();
        } else {
            //On laisse les tickets de péage en dehors de ça
            if ($request->input('type_frais_id') != $tf->getIDfromSlug("peage")) {
                $LdeFrais = LdeFrais::where('user_id', Auth::id())->where('ladate', $request->input('ladate'))
                    ->where('ttc', $request->input('ttc'))
                    ->where('label', $request->input('label'))->first();
            }
        }
        if (null !== $LdeFrais) {
            Log::debug("==========LdeFraisController : doublon =========");
            // Log::debug(json_encode($LdeFrais));

            //On fait quoi des doublons ? si on veut autoriser l'écrasage de la photo ...
            // $LdeFrais->fileName  = "waitForUpload-" . Str::random(28) . ".jpeg";

            Log::debug("Retour code 201 avec la ligne de frais OK pour éviter le doublon...");
            return response()->json($LdeFrais, 201);
        }

        $LdeFrais = new LdeFrais();
        //Ajout d'une nouvelle note de frais
        $LdeFrais->label       = $request->input('label', "");
        $LdeFrais->ladate      = $request->input('ladate', "");
        $LdeFrais->ht          = $this->myFloatValue($request->input('ht', 0));
        $LdeFrais->ttc         = $this->myFloatValue($request->input('ttc', 0));
        $LdeFrais->tvaTx1      = $this->myFloatValue($request->input('tvaTx1', null));
        $LdeFrais->tvaTx2      = $this->myFloatValue($request->input('tvaTx2', null));
        $LdeFrais->tvaTx3      = $this->myFloatValue($request->input('tvaTx3', null));
        $LdeFrais->tvaTx4      = $this->myFloatValue($request->input('tvaTx4', null));
        $LdeFrais->tvaVal1     = $this->myFloatValue($request->input('tvaVal1', null));
        $LdeFrais->tvaVal2     = $this->myFloatValue($request->input('tvaVal2', null));
        $LdeFrais->tvaVal3     = $this->myFloatValue($request->input('tvaVal3', null));
        $LdeFrais->tvaVal4     = $this->myFloatValue($request->input('tvaVal4', null));
        $LdeFrais->depart      = $request->input('depart', "");
        $LdeFrais->arrivee     = $request->input('arrivee', "");
        $LdeFrais->distance    = $this->myFloatValue($request->input('distance', 0));
        $LdeFrais->invites     = $request->input('invites', "");
        $vehiculeFromPOST      = $request->input('vehicule', "");
        // $LdeFrais->vehiculecv  = $request->input('vehiculecv', "");
        $LdeFrais->user_id     = Auth::id();

        //On stoppe si on a pas le type de Frais
        Log::debug('On cherche TypeDeFrais : ' . $request->input('typeFrais'));
        if ($request->input('typeFrais') == "") {
            Log::debug('TypeDeFrais vide, early break');
            return;
        }

        Log::debug('======= Avant le recollage HT/TTC ============');
        Log::debug(json_encode($LdeFrais));
        $this->recolleTVA($LdeFrais);

        Log::debug('======= Avant le calcul HT/TTC ============');
        Log::debug(json_encode($LdeFrais));

        //Calcul du HT ou du TTC en fonction des données fournies
        if (!isset($LdeFrais->ht) || empty($LdeFrais->ht)) {
            $LdeFrais->ht = $LdeFrais->ttc - ($LdeFrais->tvaVal1
                + $LdeFrais->tvaVal2 + $LdeFrais->tvaVal3
                + $LdeFrais->tvaVal4);
            if ($LdeFrais->ht > $LdeFrais->ttc) {
                //TODO problème à gérer
                Log::debug("erreur : HT > TTC");
            }
        }

        //Gestion de l'upload d'un fichier
        $filename  = "";
        $filecheck = "";

        Log::debug('======= STORE (request) ============');
        Log::debug($request);
        Log::debug('======= STORE (LdeFrais) ============');
        Log::debug(json_encode($LdeFrais));
        Log::debug('===================');

        //Note:
        //A une époque on expédiait le fichier dans le même paquet, voir plus bas pour la nouvelle gestion a
        //base de resumable.js ...
        $ladate          = Carbon::createFromFormat('Y-m-d', $LdeFrais->ladate);
        $ladateSub       = $ladate->format('Ym');
        $directory       = storage_path() . "/LdeFrais/" . Auth::user()->email . "/" . $ladateSub . "/";
        if (!is_dir($directory)) {
            mkdir($directory, 0770, true);
        }

        if (!empty($request->input('afile'))) {
            Log::debug('======= STORE - UPLOAD FICHIER ============');
            $image = $request->input('afile');  // your base64 encoded
            //list($type, $image) = explode(';', $image);
            //list(, $image)      = explode(',', $image);
            //$imageType          = explode('/', $type)[1];
            //20190614 on a un seul fichier jpeg qui arrive

            $imageType = "jpeg";

            $filename        = $ladate->format('Ymd') . '-' . Str::limit(Str::slug($request->input('typeFrais'), 10)) . "-" . Str::limit(Str::slug($LdeFrais->label), 20) . '-' . Str::random(10) . '.' . $imageType;
            $filenameTmp     = $ladate->format('Ymd') . '-' . Str::limit(Str::slug($request->input('typeFrais'), 10)) . "-" . Str::limit(Str::slug($LdeFrais->label), 20) . '-' . Str::random(10) . '-TEMPORARY.' . $imageType;
            $fullFilename    = $directory . $filename;
            $fullFilenameTmp = $directory . $filenameTmp;
            Log::debug("Stockage du fichier dans $directory et nom de fichier aleatoire $filename donc nom complet $fullFilename");

            //\File::put( $fullFilename, base64_decode($image));
            //Il faut decoder l'image (bytearray from javascript Uint8Array)
            $imgOut = "";
            $tab = explode(",", $image);
            for ($n = 0; $n < count($tab); $n++) {
                $b = pack("C*", $tab[$n]);
                $imgOut .= $b;
            }
            \File::put($fullFilenameTmp, $imgOut);

            //On optimize et le résultat est envoyé dans le nom final
            ImageOptimizer::optimize($fullFilenameTmp, $fullFilename);

            //On verifie s'il faut faire une rotation de l'image à l'aide de tesseract
            //fausse bonne idée, on espère que l'utilisateur a fait le job !
            // $cmd = ["/usr/bin/tesseract", "$fullFilenameTmp", "stdout", "--psm", "0", "-l", "osd", "--oem", "0"];
            // Log::debug("======= Onlance : $cmd");

            // $process = new Process($cmd);
            // $process->run();

            // executes after the command finishes
            // if (!$process->isSuccessful()) {
            //erreur, commande n'existe pas ou autre, on sauvegarde ce fichier
            // rename($fullFilenameTmp, $fullFilename);
            // Log::debug("======= Erreur de lancement de tesseract");
            // } else {
            //tesseract fonctionne on essaye de prendre la valeur de rotation de l'image pour la tourner comme il faut
            /*
          Warning. Invalid resolution 0 dpi. Using 70 instead.
          Estimating resolution as 422
          Page number: 0
          Orientation in degrees: 270
          Rotate: 90
          Orientation confidence: 5.00
          Script: Latin
          Script confidence: 5.64
        */
            //     $retour = $process->getOutput();
            //     Log::debug("======= Retour de tesseract : $retour");
            //     preg_match_all("/Orientation in degrees:(.*)/", $retour, $tabOut);
            //     $rotation = trim($tabOut[1][0]);
            //     Log::debug("======= Rotation : $rotation");
            //     if ($rotation > 0) {
            //         Log::debug("======= Lancement de la rotation de : $rotation");
            //         // Load
            //         $source = imagecreatefromjpeg($fullFilenameTmp);
            //         // Rotate
            //         $rotate = imagerotate($source, $rotation, 0);
            //         // Output
            //         imagejpeg($rotate, $fullFilename);
            //         // Free the memory
            //         imagedestroy($source);
            //         imagedestroy($rotate);
            //         // Remove temp file
            //         unlink($fullFilenameTmp);
            //         Log::debug("======= rotation terminée");
            //     } else {
            //         Log::debug("======= rotation 0");
            //         rename($fullFilenameTmp, $fullFilename);
            //     }
            // }

            $filecheck = sha1_file($fullFilename);
            $LdeFrais->fileName  = $filename;
            $LdeFrais->fileCheck = $filecheck;

            Log::debug("Nom du fichier : $filename");
        } else {
            //On est peut-être en mode "resume.js" ... alors on créé un nom de fichier aléatoire imprévisible et on le retourne
            //pour que le resume.js nous l'envoie par la suite ...
            $LdeFrais->fileName  = "waitForUpload-" . Str::random(28) . ".jpeg";
        }

        //TODO entre le 1 et le 5 on accepte encore des frais du mois précédent
        //On essaye de faire que si on est entre le 1 et le 5 du mois et qu'on a un frais qui concerne la note de frais du mois précédent
        //on l'y rajoute
        $now = Carbon::now();
        $jour = $now->day;
        $mois = $now->month;
        $ndf = null;
        if (($jour <= 5) && ($ladate->addMonth()->month <= $mois)) {
            Log::debug("recherche de la note de frais freezed...");
            $ndf = Auth::user()->NdeFrais()->orderBy('id', 'DESC')->where('status', NdeFrais::STATUS_FREEZED)->first();
        }
        if (empty($ndf)) {
            Log::debug("recherche de la note de frais en cours...");
            //La note de frais en cours: la dernière "ouverte" et si pas de ndf ouverte on en créé une pour le mois en cours ...
            $ndf = Auth::user()->NdeFrais()->orderBy('id', 'DESC')->where('status', NdeFrais::STATUS_OPEN)->first();
            if (empty($ndf)) {
                $n = new NdeFrais;
                $ndf = $n->makeNew(Auth::user()->id);
                Log::debug("Nouvelle note de frais disponible : id " . $ndf->id);
            }
            // $ndf = Auth::user()->NdeFrais()->orderBy('id', 'DESC')->where('status', NdeFrais::STATUS_OPEN)->first();
        }

        $LdeFrais->nde_frais_id = $ndf->id;

        //Si on a donné un véhicule on cherche si il existe (aucune raison qu'il n'existe pas)
        $vehicule = "";
        if ($vehiculeFromPOST != "") {
            $vehicule = Vehicule::searchFromPOST($vehiculeFromPOST);
            $LdeFrais->vehicule_id = $vehicule->id;
        }

        //Si la cylindrée n'est pas vide et qu'on a du km on essaye de calculer les IK
        if (($vehicule != "") && !empty($LdeFrais->distance)) {
            $b = new BaseCalculIks();
            $totalKMthisYear = $LdeFrais->getKMforVehiculeThisYear(Auth::user()->id, $vehicule, date('Y'), date('Y-m-d'), $LdeFrais->nde_frais_id);
            $LdeFrais->ttc   = $b->calcul($vehicule->power, $LdeFrais->distance, $totalKMthisYear, false, null, $vehicule->type);
        }

        $tf = new TypeFrais();
        $LdeFrais->type_frais_id = $tf->getIDfromSlug($request->input('typeFrais'));

        $mp = new MoyenPaiement();
        if ($request->input('typeFrais') == "ik") {
            Log::debug("frais type IK");
            $LdeFrais->moyen_paiement_id = $mp->getIDfromSlug("pseudo-perso-ik");
            //Au passage on pourrait mettre le détail de l'itinéraire dans le champ "invites"
            $LdeFrais->invites = $this->getIKitineraire($request);
            //C'est le seul type de frais pour lequel on ne veut pas de justificatif, donc pas la peine de polluer la base avec
            //des données en trop
            $LdeFrais->fileName = "";
            Log::debug("on ajoute les étapes !");
        } else {
            //Rechercher dans la base ce qui correspond a cette etiquette
            $LdeFrais->moyen_paiement_id = $mp->getIDfromSlug($request->input('moyenPaiement'));
        }
        if (empty($LdeFrais->moyen_paiement_id)) {
            $LdeFrais->moyen_paiement_id = 1;
        }

        Log::debug("  ================ LdeFrais save ================ ");
        Log::debug($LdeFrais);
        $LdeFrais->save();
        // Log::debug($LdeFrais);
        //debug sql ?

        // $LdeFrais = LdeFrais::create($request->all());

        //Petite notification pour aller vérifier que tout se passe bien (phase de prelancement)
        if (config('mail.ping') != "") {
                $details = array(
                'to' => config('mail.ping'),
                'subject' => "[" . config('app.name') . "] Nouvelle entrée",
                'message' => "A verifier, une nouvelle facturette viens d'arriver: \n\n" . $LdeFrais->toText() . "\n\n--\n" . config('app.url')
            );
            ProcessSendEmail::dispatch($details);
        }

        Log::debug("Retour code 201 avec la ligne de frais OK...");
        return response()->json($LdeFrais, 201);
    }

    public function update(Request $request, int $id)
    {
        //TODO dispositif pour eviter de modifier une Ldf différente de celle qui est demandée
        //par exemple corruption de données anciennes, écrasement de ldf d'un autre utilisateur
        Log::debug(" ============= update LdeFrais $id ============== ");

        $l = LdeFrais::findOrFail($id);
        $l->update($request->all());

        return response()->json($l, 200);
    }

    public function delete(LdeFrais $LdeFrais, int $id)
    {
        Log::debug(" ============= delete LdeFrais ============== " . $LdeFrais->id);
        //TODO dispositif pour eviter de modifier une Ldf différente de celle qui est demandée
        //par exemple corruption de données anciennes, écrasement de ldf d'un autre utilisateur
        if (empty($LdeFrais->id)) {
            Log::debug("  LdeFrais null ");
            $l = LdeFrais::findOrFail($id);
            $l->delete();
        } else {
            Log::debug("  LdeFrais not null ");
            $LdeFrais->delete();
        }
        // $l = LdeFrais::find($id);
        // $l->delete();
        Log::debug(" ============= delete LdeFrais end ============== ");
        return response()->json(array("message" => "DELETE OK"), 200);
    }

    public function webLdFDeleteGet(int $id)
    {
        //TODO dispositif pour eviter de modifier une Ldf différente de celle qui est demandée
        //par exemple corruption de données anciennes, écrasement de ldf d'un autre utilisateur
        Log::debug("===================webLdFDeleteGet $id");
        $l = LdeFrais::findOrFail($id);
        // Log::debug($l);
        $l->delete();

        return back();
    }

    public function webLdFUpdateGet(int $id)
    {
        Log::debug("===================webLdFUpdateGet $id");
        // Log::debug($tabID);
        // Log::debug('===================');
        $l = LdeFrais::findOrFail($id);
        //        Log::debug($l);

        $previous = LdeFrais::where('nde_frais_id', '=', $l->nde_frais_id)->where('id', '<', $id)->orderBy('id', 'desc')->first();
        $next     = LdeFrais::where('nde_frais_id', '=', $l->nde_frais_id)->where('id', '>', $id)->orderBy('id', 'asc')->first();
        $previousID = 0;
        $nextID     = 0;
        if ($previous) {
            $previousID = $previous->sid;
        }
        if ($next) {
            $nextID = $next->sid;
        }
        return view('webldfupdate', [
            'ldf'       => $l,
            'previous'  => $previousID,
            'next'      => $nextID,
            'userEmail' => $l->user->email,
        ]);
    }
    public function webLdFUpdatePost(Request $request, int $id)
    {
        //TODO dispositif pour eviter de modifier une Ldf différente de celle qui est demandée
        //par exemple corruption de données anciennes, écrasement de ldf d'un autre utilisateur
        Log::debug("===================webLdFUpdatePost $id");
        // Log::debug($tabID);
        // Log::debug('===================');
        $l = LdeFrais::findOrFail($id);
        if ($l) {
            $l->label       = $request->input('label', "");
            $l->ladate      = $request->input('ladate', "");
            $l->ht          = $this->myFloatValue($request->input('ht', 0));
            $l->ttc         = $this->myFloatValue($request->input('ttc', 0));
            //TODO a verifier - attention aux taux personnalisés
            // $l->tvaTx1      = $request->input('tauxtva1', 0);
            // $l->tvaTx2      = $request->input('tauxtva2', 5.5);
            // $l->tvaTx3      = $request->input('tauxtva3', 10);
            // $l->tvaTx4      = $request->input('tauxtva4', 20);
            $l->tvaVal1     = $this->myFloatValue($request->input('tva1', 0));
            $l->tvaVal2     = $this->myFloatValue($request->input('tva2', 0));
            $l->tvaVal3     = $this->myFloatValue($request->input('tva3', 0));
            $l->tvaVal4     = $this->myFloatValue($request->input('tva4', 0));
            $l->depart      = $request->input('depart', "");
            $l->arrivee     = $request->input('arrivee', "");
            $l->distance    = $this->myFloatValue($request->input('distance', 0));
            $l->invites     = $request->input('invites', "");
            $vehiculeFromPOST    = $request->input('vehicule', "");

            $vehicule = Vehicule::searchFromPOST($vehiculeFromPOST);
            $l->vehicule_id = $vehicule->id;

            $l->save();
            Log::debug($l);
        }
        return $this->webLdFUpdateGet($id);
    }

    //Ajoute une nouvelle regule d'ik... sauf si déjà présente
    //date, raison (bareme ou tranche), montant ttc, vehicule
    public function newReguleIK($ladate, $label, $ttc, $vehicule, $vehiculecv, $ndfid, $userid)
    {
        Log::debug("===================newReguleIK");
        $l = new LdeFrais();
        $mp = new MoyenPaiement();

        //L'ID des types de frais speciaux "regule"
        $idTypeFraisIKRegule = TypeFrais::where('slug', 'ik-regule')->pluck('id')->first();
        $idMoyenPaiement     = $mp->getIDfromSlug("pseudo-perso-ik");

        //->where('label', $label)
        $testDoublon = LdeFrais::where('nde_frais_id', $ndfid)->where('user_id', $userid)->where('type_frais_id', $idTypeFraisIKRegule)->where('moyen_paiement_id', $idMoyenPaiement)->first();

        if ($testDoublon) {
            $l = $testDoublon;
        }
        //Dans l'ideal il faudrait eviter de faire un doublon si on a déjà inséré cette regule
        $l->label               = $label;
        $l->ladate              = $ladate;
        $l->ttc                 = $ttc;
        $l->vehicule_id         = $vehicule->id;
        $l->nde_frais_id        = $ndfid;
        $l->user_id             = $userid;
        $l->moyen_paiement_id   = $idMoyenPaiement;
        $l->type_frais_id       = $idTypeFraisIKRegule;
        $l->save();
    }

    //Ajoute une nouvelle regule d'ik... sauf si déjà présente
    //date, raison (bareme ou tranche), montant ttc, vehicule
    public function geoDist(Request $request)
    {
        Log::debug("===================geoDist");
        $depart  = $request->depart;
        $arrivee = $request->arrivee;
        $l = LdeFrais::where('user_id', Auth::id())
            ->where('depart', '=', $depart)
            ->where('arrivee', '=', $arrivee)
            ->orderBy('ladate', 'desc')
            ->first();
        if ($l) {
            $r = array();
            $r["depart"] = $depart;
            $r["arrivee"] = $arrivee;
            $r["distance"] = $l->distance;
            $r["ladate"] = $l->ladate;
            return response()->json($r, 201);
        } else {
            //Retour 204
            return response()->json("null", 204);
        }
    }

    /**
     * Transforme une liste d'étapes en une chaine de texte à stocker dans le champs invites
     *  (utilisation détournée du champ invite)
     *
     * @return  [type]  [return description]
     */
    public function getIKitineraire($request)
    {
        Log::debug("getIKitineraire");
        $r = "";
        for ($i = 1; $i < 100; $i++) {
            $step = $request->input("etape" . $i, "");
            if ($step != "") {
                $value = $request->input("etape" . $i);
                //Cas particulier de la dernière étape qui peut être la ville d'arrivée
                if ($request->input("etape" . ($i+1), "") == "") {
                    //Il n'y a pas d'élément après, c'est donc le dernier
                    if ($value == $request->input("arrivee")) {
                        //Exit du for
                        break;
                    }
                }

                //Si on est sur le 1er élément on ajoute Étapes : au début
                //et -> pour les autres
                if ($r != "") {
                    $r .= " -> ";
                } else {
                    $r = "Étapes : ";
                }

                //On réduit le code postal au numéro de département
                if (preg_match('/(.*)(\([\d-]*\))/', $value, $parts)) {
                    $r .= $parts[1] . "(" . substr($parts[2], 1, 2) . ")";
                } else {
                    $r .= $value;
                }
            }
        }

        Log::debug("getIKitineraire -> $r");
        return $r;
    }

    /**
     * L'inverse: transforme la chaine de texte "invites" d'un itinéraire en liste d'étapes
     *
     * @return  array  tableau des etapes
     */
    public function getEtapesFromIKitineraire($str, $last)
    {
        Log::debug("getEtapesFromIKitineraire");
        $t = [];
        //début = Étapes :
        //séparateur = ->
        $start = "Étapes :";
        if (Str::contains($str, $start)) {
            $clean = str_replace($start, "", $str);
            $collection = Str::of($clean)->explode(' -> ');
            foreach ($collection as $v) {
                $t[] = trim($v);
            }
        }
        //Si la derniere etape = arrivee ce n'est pas une étape
        if (last($t) == $last || last($t) == "") {
            array_pop($t);
        }
        Log::debug("getEtapesFromIKitineraire -> " . json_encode($t));
        return $t;
    }


    /**
     * Get list of missed pictures to make a sort of "try to sync"
     *
     * @return  [type]  [return description]
     */
    public function indexOfMissedPictures(Request $request)
    {
        Log::debug("=================== indexOfMissedPictures pour " . Auth::id());
        //Remove all very old missed pictures from this list (pictures for closed ndf) (too late to put pictures)
        $debutDuMois = Carbon::parse("first day of previous month")->format("Y-m-d");

        $l = LdeFrais::select(['id', 'type_frais_id', 'ladate', 'label', 'ttc', 'created_at', 'fileName'])
            ->where('user_id', Auth::id())
            ->where('fileCheck', '=', null)
            ->where('fileName', 'LIKE', "waitForUpload%")
            ->where('ladate', '>=', $debutDuMois)
            ->orderBy('ladate', 'desc')
            ->get();

        // ->transform(function ($madate, $key) {
        // return $created_at;
        // return date("Ymd_Hi", strtotime($madate));
        // });
        if ($l) {
            foreach ($l as $lde) {
                //Sur le smartphone on a YYYYMMDD_Hm
                $lde->searchFile = date("Ymd_G", strtotime($lde->created_at));
                $lde->searchFile .= ltrim(date("i", strtotime($lde->created_at)), '0');
            }
            Log::debug("  indexOfMissedPictures retour de nbres = " . count($l));
            Log::debug(\json_encode($l));
            return response()->json($l, 201);
        } else {
            //Retour 204
            Log::debug("  retour null");
            return response()->json("null", 204);
        }
    }


    /**
     * uploadOfMissedPicture
     *
     * @param   Request  $request  [$request description]
     * @param   int      $id       [$id description]
     *
     * @return  [type]             [return description]
     */
    public function uploadOfMissedPicture(Request $request, int $id)
    {
        Log::debug("========== LdeFraisController : uploadOfMissedPicture $id =========");
        // Log::debug(json_encode($request->all()));

        //debug complet de la requete entrante ...
        $tmpfname = storage_path() . "/logs/" . time() . "-upload.debug";
        Log::debug('Pour le contenu complet de la requete, voir le fichier ' . $tmpfname);
        if ($fp = fopen($tmpfname, 'w')) {
            Log::debug("  ouveture ok fichier $tmpfname");
            fwrite($fp, $request);
            fwrite($fp, "\n\n");
            fwrite($fp, \json_encode($request->all()));
            fwrite($fp, "\n\n");
            fclose($fp);
        } else {
            Log::debug("  impossible d'ouvrir le fichier $tmpfname");
        }

        $LdeFrais = LdeFrais::findOrFail($id);

        //On vérifie que cet utilisateur est bien le propriétaire de cette lde
        if ($LdeFrais->user_id != Auth::id()) {
            Log::debug("  Erreur, cette LdeFrais est propriété de " . $LdeFrais->user_id . " et celui qui est authentifié est " . Auth::id());
            $erreur = "You don't have the right to do that !";
            return response()->json($erreur, 403);
        }

        //Gestion de l'upload d'un fichier
        $filename  = "";
        $filecheck = "";

        if (!empty($request->input('afile'))) {
            Log::debug('======= uploadOfMissedPicture STORE - UPLOAD FICHIER ============');
            $image = $request->input('afile');  // your base64 encoded
            $imageType = "jpeg";

            $filename        = $ladate->format('Ymd') . '-' . Str::limit(Str::slug($request->input('typeFrais'), 10)) . "-" . Str::limit(Str::slug($LdeFrais->label), 20) . '-' . Str::random(10) . '.' . $imageType;
            $filenameTmp     = $ladate->format('Ymd') . '-' . Str::limit(Str::slug($request->input('typeFrais'), 10)) . "-" . Str::limit(Str::slug($LdeFrais->label), 20) . '-' . Str::random(10) . '-TEMPORARY.' . $imageType;
            $fullFilename    = $directory . $filename;
            $fullFilenameTmp = $directory . $filenameTmp;
            Log::debug("Stockage du fichier dans $directory et nom de fichier aleatoire $filename donc nom complet $fullFilename");

            //\File::put( $fullFilename, base64_decode($image));
            //Il faut decoder l'image (bytearray from javascript Uint8Array)
            $imgOut = "";
            $tab = explode(",", $image);
            for ($n = 0; $n < count($tab); $n++) {
                $b = pack("C*", $tab[$n]);
                $imgOut .= $b;
            }
            \File::put($fullFilenameTmp, $imgOut);

            //On optimize et le résultat est envoyé dans le nom final
            ImageOptimizer::optimize($fullFilenameTmp, $fullFilename);

            $filecheck = sha1_file($fullFilename);
            $LdeFrais->fileName  = $filename;
            $LdeFrais->fileCheck = $filecheck;

            Log::debug("Nom du fichier : $filename");
        } else {
            //On est peut-être en mode "resume.js" ... alors on créé un nom de fichier aléatoire imprévisible et on le retourne
            //pour que le resume.js nous l'envoie par la suite ...
            $LdeFrais->fileName  = "waitForUpload-" . Str::random(28) . ".jpeg";
        }

        Log::debug("  ================ LdeFrais save ================ ");
        Log::debug($LdeFrais);
        $LdeFrais->save();

        //Petite notification pour aller vérifier que tout se passe bien (phase de prelancement)
        if (config('mail.ping') != "") {
            $details = array(
                'to' => config('mail.ping'),
                'subject' => "[" . config('app.name') . "] Synchronisation de fichier manquant ",
                'message' => "A verifier, un nouveau fichier manquant a été synchronisé: \n\n" . $LdeFrais->toText() . "\n\n--\n" . config('app.url')
            );
            ProcessSendEmail::dispatch($details);
        }

        Log::debug("Retour code 201 avec la ligne de frais OK...");
        return response()->json($LdeFrais, 201);
    }

    /**
     * Récupère la liste des itinéraires les plus fréquents pour les proposer à l'utilisateur
     * lors de l'ajout d'IK sur le smartphone
     *
     * @return  [type]  [return description]
     */
    public function getMostFrequentRoutes($asjson = 1)
    {
        Log::debug("LdeFraisController::getMostFrequentRoutes");
        $tf = new TypeFrais();
        $typeFrais = $tf->getIDfromSlug("ik");
        // $l = LdeFrais::select('depart', 'arrivee', 'distance', 'invites', 'label', DB::raw('count(*) as nbres'))
        //     ->where('user_id', Auth::id())
        //     ->where('type_frais_id', $typeFrais)
        //     ->groupBy('invites')
        //     ->havingRaw('nbres > ?', [2])
        //     ->orderBy('nbres')
        //     ->limit(10)->get();
        // Log::debug("LdeFraisController::getMostFrequentRoutes end");
        // return response()->json($l, 200);

        $ldfs = DB::table('lde_frais')
            ->selectRaw('depart,arrivee,distance,invites as etapes,label, count(label) as nbres')
            ->where('user_id', Auth::id())
            ->where('type_frais_id', $typeFrais)
            ->groupBy('label', 'distance')
            ->havingRaw('nbres > ?', [0])
            ->orderBy('nbres', 'DESC')
            ->latest()
            ->limit(10)->get();

        //Il faut transformer les "invites" en liste d'étapes ...
        foreach ($ldfs as $l) {
            $l->etapes = $this->getEtapesFromIKitineraire($l->etapes, $l->arrivee);
        }

        Log::debug("LdeFraisController::getMostFrequentRoutes end");
        Log::debug(json_encode($ldfs));
        if ($asjson == 1) {
            return response()->json($ldfs, 200);
        }
        else {
            return $ldfs;
        }

        /*
        SELECT   COUNT(*) AS nbres, depart,arrivee,distance,invites,label
        FROM     lde_frais
        WHERE user_id='90' AND type_frais_id='8'
        GROUP BY invites
        HAVING   COUNT(*) > 1
        ORDER BY nbres DESC
        LIMIT 10
        */
    }
}
