<?php
/*
 * DependencyUploadController.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Http\Controllers;

use Pion\Laravel\ChunkUpload\Exceptions\UploadMissingFileException;
use Pion\Laravel\ChunkUpload\Handler\AbstractHandler;
use Pion\Laravel\ChunkUpload\Receiver\FileReceiver;

use Illuminate\Support\Facades\Log;
use App\Http\Controllers\UploadController;

class DependencyUploadController extends UploadController
{
    /**
     * Handles the file upload
     *
     * @param FileReceiver $receiver
     *
     * @return \Illuminate\Http\JsonResponse
     *
     * @throws UploadMissingFileException
     *
     */
    public function uploadFile(FileReceiver $receiver)
    {
        Log::debug("DependencyUploadController: uploadFile");
        try {
            // check if the upload is success, throw exception or return response you need
            if ($receiver->isUploaded() === false) {
                Log::debug("DependencyUploadController: UploadMissingFileException");
                throw new UploadMissingFileException();
            }
            // receive the file
            $save = $receiver->receive();

            // check if the upload has finished (in chunk mode it will send smaller files)
            if ($save->isFinished()) {
                Log::debug("DependencyUploadController: isFinished");
                // save the file and return any response you need
                return $this->saveFile($save->getFile());
            }

            Log::debug("DependencyUploadController: we are in chunk mode, lets send the current progress");
            // we are in chunk mode, lets send the current progress
            /** @var AbstractHandler $handler */
            $handler = $save->handler();
            return response()->json([
                "done" => $handler->getPercentageDone()
            ]);
        } catch (Exception $exception) {
            Log::debug("DependencyUploadController: exeption" . json_encode($exception));
        }
    }
}
