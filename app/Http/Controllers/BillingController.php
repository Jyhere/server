<?php

namespace App\Http\Controllers;

use App\User;
use App\Entreprise;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Log;

class BillingController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
    }


    /**
     * Retourne la liste des utilisateurs qu'il faut facturer pour cette entreprise
     *
     * @param  $entreprise : l'entreprise
     *         $rvd :        si on est un revendeur on essaye de lister tous les "sous clients"
     * @return
     */
    static function getBillableUsers($entrepriseID, $rvd = false)
    {
        //Liste des roles utilisateurs qui sont facturables
        $roleTxt = array("utilisateur", "responsableEntreprise");
        $rolesID = array();
        foreach ($roleTxt as $r) {
            $rolesID[] = Role::findByName($r, 'web')->id;
        }

        $l = User::join('entreprise_user', 'users.id', '=', 'entreprise_user.user_id')
            ->select('user_id AS uid')
            ->where('entreprise_id', $entrepriseID)
            ->whereIn('role_id', $rolesID)
            ->groupBy('user_id')
            ->orderBy('name')
            ->pluck('uid')
            ->toArray();

        // //Gestion d'un revendeur
        // if ($rvd) {
        //     $listeRvdID = array();
        //     $listeEntreID = array($entrepriseID); //Pour démarrer
        //     //On cherche son ou ses administrateurs qui peuvent créer des comptes
        //     $roleAdminRvd = Role::findByName('adminRevendeur', 'web')->id;
        //     Log::debug(" roleAdminRvd : $roleAdminRvd");

        //     $e = Entreprise::findOrFail($entrepriseID);
        //     $rvdAdmins = Entreprise::getRevendeurs($roleAdminRvd, $listeEntreID, $listeRvdID, $e->creator_id);
        //     foreach ($rvdAdmins as $r) {
        //         $tmp = array_merge($listeEntreID, Entreprise::getEntreprisesCreatedBy($r));
        //         $listeEntreID = $tmp;

        //         $tmp = array_merge($listeEntreID, Entreprise::getEntreprisesOfRvd($roleAdminRvd, $listeEntreID, $listeRvdID));
        //         $listeEntreID = $tmp;
        //     }
        //     Log::debug(" getBillableUsers + rvd, liste des admin revendeurs : " . json_encode($listeRvdID));

        //     Log::debug(" getBillableUsers + rvd, liste des entreprises clientes: " . json_encode($listeEntreID));

        //     //Maintenant on récupère les utilisateurs de ces entreprises ...
        //     $tmp = array_merge($listeEntreID, $l);
        //     $l = $tmp;
        // }
        Log::debug("Liste des comptes payants : " . \json_encode($l));
        return $l;
    }
}
