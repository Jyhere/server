<?php
/**
 * Honeypot.php
 *
 * Copyright (c) 2021 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

// Thanks to https://aureola.codes/en/blog/2021/how-stop-vulnaribility-scanners-laravel-fail2ban

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

class Honeypot extends Controller
{
    public function __invoke(Request $request, string $path = '')
    {
        // Load the array of honeypot paths from the configuration.
        $honeypot_paths_array = config('honeypot.paths', []);

        // Turn the path array into a regex pattern.
        $honeypot_paths = '/^(' . str_replace(['.', '/'], ['\.', '\/'], implode('|', $honeypot_paths_array)) . ')/i';

        // If the user tries to access a honeypot path, fail with the teapot code.
        if (preg_match($honeypot_paths, $path)) {
            $logLine = "IP:" . $request->ip() . " \""  . $request->method() . " " . $request->path() . "\" " . Response::HTTP_I_AM_A_TEAPOT;
            Log::channel('honeypot')->alert($logLine, []);
            abort(Response::HTTP_I_AM_A_TEAPOT);
        }

        // Otherwise just display our regular 404 page.
        abort(Response::HTTP_NOT_FOUND);
    }
}
