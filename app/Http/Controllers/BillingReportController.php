<?php
/*
 * BillingReportControllerReportController.php
 *
 * Copyright (c) 2019-2021 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
/*
 *
 * Permet de fournir les informations de facturation
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\Process\Process;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;

use App\Entreprise;
use App\User;
use Illuminate\Support\Str;

class BillingReportController extends Controller
{

    //Préparation pour la facturation ...
    public function index()
    {
        Log::debug("======BillingReportControllerReportController : index =============");
        // $e = Entreprise::getMyEntreprises();

        //Liste des roles utilisateurs qui sont facturables
        $roleTxt = array("utilisateur", "responsableEntreprise");
        $rolesID = array();
        foreach ($roleTxt as $r) {
            $rolesID[] = Role::findByName($r, 'web')->id;
        }

        $filterUser = ["id", "firstname", "name", "email", "created_at"];
        $filterEntreprise = ["id", "name", "email", "created_at"];
        $retour = array();
        foreach (Entreprise::getMyEntreprises()->where('creator_id', Auth::user()->id) as $e) {
            $nbaccount = 0;
            $eid = $e['id'];
            //Ensuite on recupere la liste des utilisateurs de cette entreprise pour savoir ce qu'il faut facturer
            // return $e->users->whereIn('pivot.role_id',$rolesID);
            foreach ($e->users->whereIn('pivot.role_id', $rolesID)->unique('id') as $u) {
                $uid = $u['id'];
                $nbaccount++;
                foreach ($filterUser as $fu) {
                    $retour[$eid][$uid][$fu] = $u[$fu];
                }
            }
            foreach ($filterEntreprise as $fe) {
                $retour[$eid][$fe] = $e[$fe];
            }
            $retour[$eid]["accounts"] = $nbaccount;
        }
        return $retour;

        // $filterUser = ["id", "firstname", "name", "email"];
        // $users = array();
        // foreach (User::getMyUsers() as $u) {
        //     $uid = $u['id'];
        //     foreach ($filter as $f) {
        //         $users[$uid][$f] = $u[$f];
        //     }
        // }
        // return $users;
    }


    //Préparation pour la facturation ...
    public function stats(Request $request)
    {
        Log::debug("======BillingReportControllerReportController : stats =============");
        Log::debug("  restriction de la recherche pour " . $request->account);

        if ($request->account == "") {
            return response("", 404);
        }
        $chUser = User::where('email', $request['account'])->first();
        if (!$chUser) {
            return response()->json([
                "nbUsers" => 0
            ]);
        }

        //Note: On vérifie qu'on est superuser et qu'on a le droit de changer d'identité ...
        // $e = Entreprise::getMyEntreprises();

        //Liste des roles utilisateurs qui sont facturables
        $roleTxt = array("utilisateur", "responsableEntreprise");
        $rolesID = array();
        foreach ($roleTxt as $r) {
            $rolesID[] = Role::findByName($r, 'web')->id;
        }

        $filterUser = ["id", "firstname", "name", "email", "created_at"];
        $filterEntreprise = ["id", "name", "email", "created_at"];
        $retour = array();
        $nbaccount = 0;
        foreach (Entreprise::getMyEntreprises('name', $chUser->id) as $e) {
            $eid = $e['id'];
            //Ensuite on recupere la liste des utilisateurs actifs (main_role > 1) de cette entreprise pour savoir ce qu'il faut facturer
            // return $e->users->whereIn('pivot.role_id',$rolesID);
            foreach ($e->users->where('main_role', '>', '1')->whereIn('pivot.role_id', $rolesID)->unique('id') as $u) {
                $uid = $u['id'];
                $nbaccount++;
                foreach ($filterUser as $fu) {
                    $retour[$eid][$uid][$fu] = $u[$fu];
                }
            }
            foreach ($filterEntreprise as $fe) {
                $retour[$eid][$fe] = $e[$fe];
            }
            $retour[$eid]["accounts"] = $nbaccount;
        }

        Log::debug("  return nbUsers = " . $nbaccount);
        return response()->json([
            "nbUsers" => $nbaccount
        ]);
    }
}
