<?php

/**
 * VehiculeController.php
 *
 * Copyright (c) 2021 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Http\Controllers;

use App\Vehicule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class VehiculeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vehicules = Vehicule::where("user_id", "=", auth()->id)->get();

        return response()->json([
            'vehicules' => $vehicules->toArray(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Log::debug('========VehiculeController : store ===========');

        Log::debug($request);

        //pour pouvoir supprimer les véhicules à virer
        $listeVehiculesUUIDs = [];

        //Le / Les véhicules perso
        if ($request['vehicules']) {
            foreach (json_decode($request['vehicules']) as $k => $v) {
                $t['name']     = $v->nom;
                $t['energy']   = $v->energie;
                $t['power']    = $v->puissance;
                $t['type']     = $v->type;
                $t['kmbefore'] = $v->kmbefore;
                $t['number']   = $v->immat;
                $t['user_id']  = auth()->id();
                $t['is_perso'] = true;

                //Si uuid existe -> c'est une mise à jour
                if (isset($v->uuid) && ($v->uuid != "")) {
                    Log::debug("  VehiculeController perso : update pour $v->uuid ...");
                    $vehicule = Vehicule::where('uuid', '=', $v->uuid)->first();
                    $vehicule->fill($t);
                } else {
                    //Il faudrait peut-être aller chercher dans les supprimés voir si c'est pas une sorte de résurection pour contourner l'info du
                    //km before
                    $vehicule = Vehicule::withTrashed()->where('number', '=', $v->immat)->first();
                    if ($vehicule) {
                        Log::debug("  VehiculeController perso : pas d'uuid mais un ancien véhicule supprimé existe avec cette immatriculation, on le réactive ...");
                        if ($vehicule->trashed())
                            $vehicule->restore();
                        $vehicule->ignore(['kmbefore'])->fill($t);
                    } else {
                        Log::debug("  VehiculeController perso : pas d'uuid c'est une création ...");
                        $vehicule = new Vehicule($t);
                    }
                }
                $vehicule->save();
                $listeVehiculesUUIDs[] = $vehicule->uuid;
            }
        }

        //Le / Les véhicules pro
        if ($request['vehiculesPro']) {
            foreach (json_decode($request['vehiculesPro']) as $k => $v) {
                $t['name']     = $v->nom;
                $t['energy']   = $v->energie;
                $t['power']    = $v->puissance;
                $t['type']     = $v->type;
                $t['kmbefore'] = $v->kmbefore;
                $t['number']   = $v->immat;
                $t['user_id']  = auth()->id();
                $t['is_perso'] = false;

                //Si uuid existe -> c'est une mise à jour
                if (isset($v->uuid) && ($v->uuid != "")) {
                    Log::debug("  VehiculeController pro : update pour $v->uuid ...");
                    $vehicule = Vehicule::where('uuid', '=', $v->uuid)->first();
                    $vehicule->fill($t);
                } else {
                    $vehicule = Vehicule::withTrashed()->where('number', '=', $v->immat)->first();
                    if ($vehicule) {
                        Log::debug("  VehiculeController pro : pas d'uuid mais un ancien véhicule supprimé existe avec cette immatriculation, on le réactive ...");
                        if ($vehicule->trashed())
                            $vehicule->restore();
                        $vehicule->ignore(['kmbefore'])->fill($t);
                    } else {
                        Log::debug("  VehiculeController pro : pas d'uuid c'est une création ...");
                        $vehicule = new Vehicule($t);
                    }
                }
                $vehicule->save();
                $listeVehiculesUUIDs[] = $vehicule->uuid;
            }
        }
        //et maintenant on peut supprimer tous les véhicules "de trop"
        $aSupprimer = Vehicule::where('user_id', '=', auth()->id())
            ->whereNotIn('uuid', $listeVehiculesUUIDs)->delete();


        //Et on retourne la liste complète pour que l'appli puisse se synchroniser (mettre à jour les uuid éventuellement)
        $vehicules = Vehicule::where("user_id", "=", auth()->id())->get();

        return response()->json([
            'vehicules' => $vehicules->toArray(),
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Vehicule  $vehicule
     * @return \Illuminate\Http\Response
     */
    public function show(Vehicule $vehicule)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Vehicule  $vehicule
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vehicule $vehicule)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Vehicule  $vehicule
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vehicule $vehicule)
    {
        //
    }
}
