<?php
/*
 * RegisterController.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Http\Controllers\Auth;

use App\Entreprise;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use App\Mail\NewAccount;
use Illuminate\Support\Facades\DB;
use App\Mail\UserMailInvitation;
use Swift_Encoding;
use App\Http\Controllers\NdeFrais;
use App\NdeFrais as AppNdeFrais;
use Spatie\Permission\Models\Role;

class RegisterController extends Controller
{
  /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

  use RegistersUsers;

  /**
   * Where to redirect users after registration.
   *
   * @var string
   */
  protected $redirectTo = '/home';

  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    Log::debug('========RegisterController register construct===========');
    $this->middleware('guest');
  }

  /**
   * Get a validator for an incoming registration request.
   *
   * @param  array  $data
   * @return \Illuminate\Contracts\Validation\Validator
   */
  protected function validator(array $data)
  {
    Log::debug('========RegisterController register validator===========');
    Log::debug($data);

    //Par defaut le creator c'est 1 quand on est en auto-register... mais si jamais on arrive d'une affiliation
    $creator_id = 1;
    if (isset($data['partner']) && $data['partner'] != "") {
      $creator = User::where('email', '=', $data['partner'])->pluck('id')->first();
      if (trim($creator) != "")
        $creator_id = $creator;
    }

    //le validator ne marche pas sur les mails des serveurs de dev car on fait une réécriture de
    //l'adresse mail en une adresse @alpha.devtemp.fr par ex.

    //Message personnalisé :)
    $messages = [
      'email.unique' => 'Cette adresse mail (:input) est déjà utilisée sur ce serveur DoliSCAN !',
      'cgu.accepted' => "Veuillez accepter les Conditions Générales d'Utilisation (CGU)",
    ];

    $v = Validator::make($data, [
      'firstname' => ['required', 'string', 'max:255'],
      'name' => ['required', 'string', 'max:255'],
      'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
      // 'password' => ['required', 'string', 'min:6'],
      'cgu' => 'accepted',
      'creator_id' => '$creator_id',
    ], $messages);

    Log::debug("======== RegisterController validator pour $creator_id ===========");
    if ($v->fails()) {
      Log::debug("======== RegisterController validator fail ===========");
      Log::debug($v->errors());
      response()->json(['data' => $v->errors()], 400);
      // response()->json($v->messages(), 200);
    } else {
      Log::debug("======== RegisterController validator ok ===========");
    }

    return $v;
  }

  /**
   * Create a new user instance after a valid registration.
   *
   * @param  array  $data
   * @return \App\User
   */
  protected function create(array $data)
  {
    // Si on est sur un serveur de dev ... alors on créé un compte avec une adresse
    // "locale" au serveur de dev ... pourquoi ? -> ça pose plus de pb qu'autre chose -> commentaires 20200529?
    // if (env('APP_ENV') != 'prod') {
    //   Log::debug("  -> Demande sur un serveur qui n'est pas en prod... on ajoute le domaine local à l'adresse mail");
    //   $domaine = config('app.domain');
    //   //On construit une adresse mail locale
    //   $email = \substr($data['email'], 0, strpos($data['email'], "@")) . "@" . $domaine;
    //   $data['emailOrig'] = $data['email'];
    //   $data['email']     = $email;
    //   Log::debug("Après modification de data pour avoir un mail local : ");
    //   Log::debug($data);
    // }
    $creator_id = null;
    //Par defaut le creator c'est 1 quand on est en auto-register... mais si jamais on arrive d'une affiliation
    if (isset($data['partner']) && $data['partner'] != "") {
      $creator_id = User::where('email', $data['partner'])->pluck('id')->first();
    }
    if (is_null($creator_id)) {
      $creator_id = 1;
    }

    $roleId = Role::findByName('responsableEntreprise', 'web')->id;
    Log::debug("Le role responsableEntreprise a pour id " . $roleId);

    $u = User::create([
      'firstname' => $data['firstname'],
      'name' => $data['name'],
      'email' => $data['email'],
      // 'password' => Hash::make($data['password']),
      'creator_id' => $creator_id,
      'main_role' => $roleId,
    ]);
    $u->setMainRole('responsableEntreprise');

    //On lui pré-créé une entreprise dont il est responsable ?
    if(isset($data['siret']) || isset($data['siren'])) {
      Log::debug("  Un num SIRET/SIREN a ete passé on ne créé pas d'entreprise 'vide' pour ce compte");
    }
    else {
      $e = new Entreprise();
      $e->name = "A compléter - " . $u->email;
      $e->adresse = "A compléter - " . $u->name . " " . $u->firstname;
      //Attention si créé par un partenaire c'est le partenaire le creator
      if ($creator_id > 1) {
          $e->creator_id = $creator_id;
      } else {
          //Sinon on rattache l'utilisateur fraichement créé
          $e->creator_id = $u->id;
      }
      $e->save();
      //Si on a un revendeur on le connecte avec l'entreprise
      if ($creator_id > 1) {
        $c = User::findOrFail($creator_id);
        //Un peu brutal mais on donne les droits de responsable d'entreprise direct
        $c->addRoleOnEntreprise(Role::findByName('adminRevendeur', 'web'), $e);
      }
      //Puis les droits de responsable au nouvel utilisateur sur son entreprise
      //Un peu brutal mais on donne les droits de responsable d'entreprise direct
      $u->addRoleOnEntreprise($roleId, $e);
    }

    //Et sa 1ere note de frais ?
    $ndf = new AppNdeFrais();
    $ndf->makeNew($u->id);

    return $u;
  }

  //API
  public function register(Request $request)
  {
    Log::debug('========RegisterController register (API)===========');

    //Cas particulier de l'email déjà existant ? on essaye d'envoyer un message avec demande de code secret pour reconnecter le compte ?
    Log::debug("  RegisterController register mail: " . $request->email . " ===========");
    $verifMail = User::where('email', '=', $request->email)->first();
    if ($verifMail) {
      //Le code a déjà été passé et la session est en cours, par exemple on est à l'étape suivante du code de la société donc on ne boucle pas :)
      Log::debug("  request->checkSecurityCodeEntreprise: " . $request->checkSecurityCodeEntreprise);
      if (isset($request->checkSecurityCodeEntreprise) && $request->checkSecurityCodeEntreprise != "") {
        return $this->registered($request, $verifMail);
      }
      //Un code de sécurité a été communiqué, vérification
      if (isset($request->checkSecurityCode)) {
        Log::debug("  RegisterController JSON " . $verifMail->security_code);
        $sec = json_decode($verifMail->security_code);
        //Vérification que le code est ok et qu'il a été généré il y a moins de 15 minutes
        //moins de 15 minutes ...
        $ts15min = time() - (60 * 15);
        if ($sec->code == $request->checkSecurityCode && $sec->timestamp > $ts15min) {
          Log::debug("  RegisterController checkSecurityCode valide");
          return $this->registered($request, $verifMail);
        } else {
          //Erreur de vérification de code
          $data = "CheckSecurityCodeError";
          Log::debug("  RegisterController register mail: error checkSecurity return 401 $ts15min <> " . $sec->timestamp . " ou " . $sec->code . " != " . $sec->timestamp);
          return response()->json(['data' => $data], 401);
        }
      } else {
        $data = "emailExistCheckSecurityCode";
        $verifMail->envoyerMailVerificationRegisterAPI();
        Log::debug("  RegisterController register mail: return 202 (send check code via mail)");
        return response()->json(['data' => $data], 202);
      }
    }

    // Here the request is validated. The validator method is located
    // inside the RegisterController, and makes sure the name, email
    // password
    $v = $this->validator($request->all())->validate();

    Log::debug("======== RegisterController register retour du validator ===========");

    // A Registered event is created and will trigger any relevant
    // observers, such as sending a confirmation email or any
    // code that needs to be run as soon as the user is created.
    Log::debug('========RegisterController register event 0===========');

    // And finally this is the hook that we want. If there is no
    // registered() method or it returns null, redirect him to
    // some other URL. In our case, we just need to implement
    // that method to return the correct response.
    $create = $user = $this->create($request->all());
    if ($create) {
      event(new Registered($create));
      Log::debug('========RegisterController register mail new account ===========');

      Mail::to($request->email)->bcc(config('mail.notifications'))->send(new UserMailInvitation($user), function ($message) {
        $message->getSwiftMessage()->setEncoder(Swift_Encoding::get8BitEncoding());
      });

      // After the user is created, he's logged in ... or not (please look at your mails to validate your account ?)
      Log::debug('========RegisterController register login ... on est en mode API ===========');
      // $this->guard()->login($user);

      // And finally this is the hook that we want. If there is no
      // registered() method or it returns null, redirect him to
      // some other URL. In our case, we just need to implement
      // that method to return the correct response.
      return $this->registered($request, $user);
    } else {
      return $create;
    }
  }

  /**
   * redirect to cap-rel sys
   *
   * @return  [type]             [return description]
   */
  public function showRegistrationForm()
  {
    if (config('app.srv_public')) {
      return redirect(config('app.srv_dolibarr') . '/register.php');
    } else {
      return view('auth.register');
    }
  }

  public function webRegister(Request $request)
  {
    Log::debug('========RegisterController register (WEB)===========');
    // Here the request is validated. The validator method is located
    // inside the RegisterController, and makes sure the name, email
    // password and password_confirmation fields are required.
    $this->validator($request->all())->validate();
    Log::debug('========RegisterController register retour du validator ===========');

    // A Registered event is created and will trigger any relevant
    // observers, such as sending a confirmation email or any
    // code that needs to be run as soon as the user is created.
    Log::debug('========RegisterController register event 1 ===========');
    $create = $user = $this->create($request->all());
    if ($create) {
      event(new Registered($create));
      Log::debug("========RegisterController register mail: " . $request->email . " ===========");

      Log::debug('========RegisterController register mail new account ===========');
      Mail::to($request->email)->bcc(config('mail.notifications'))->send(new UserMailInvitation($user), function ($message) {
        $message->getSwiftMessage()->setEncoder(Swift_Encoding::get8BitEncoding());
      });

      // After the user is created, he's logged in ... or not (please look at your mails to validate your account ?)
      Log::debug('========RegisterController register login ... on est en mode web ===========');
      $this->guard()->login($user);

      // And finally this is the hook that we want. If there is no
      // registered() method or it returns null, redirect him to
      // some other URL. In our case, we just need to implement
      // that method to return the correct response.
      return $this->registered($request, $user);
    } else {
      return $create;
    }
  }

  protected function registered(Request $request, $user)
  {
    $user->generateToken();
    Log::debug('=================== RegisterController registered');
    Log::debug(Route::currentRouteName());
    if (Route::currentRouteName() == "webRegister" || Route::currentRouteName() == "webRegisterPost") {
      Log::debug('=================== RegisterController web registered -> redirect ');
      return redirect($this->redirectPath());
    } else {
      Log::debug('=================== RegisterController api registered -> return object data: ' . json_encode($user));
      return response()->json(['data' => $user->toArray()], 201);
    }
  }
}
