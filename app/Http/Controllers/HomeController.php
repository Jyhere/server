<?php
/*
 * HomeController.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // pour que les rvd n'aient plus la page intermédiaire ... pb pour superadmin
        // qui ne peut plus revenir sur la page pour switcher d'utilisateur
        // if (
        //     sharp_user()->hasRole('adminRevendeur', 'web') ||
        //     sharp_user()->hasRole('superAdmin', 'web')
        // ) {
        //     return redirect(config('sharp.custom_url_segment'));
        // } else {
        return view('home');
        // }
    }

    public function showPage(Request $request)
    {
        $page = $request->path();
        Log::debug("=================== showPage " . $page . " ====================");
        if (view()->exists("web$page")) {
            return view("web$page");
        } else {
            abort(404);
        }
    }

    public function redirectPage(Request $request)
    {
        $page = $request->path();
        if (request()->ip() != config("app.srv_monitoring")) {
            Log::debug("=================== redirectPage " . $page . " ====================");
        }

        //Version prod : direction le CMS
        if (config('app.env') == 'prod') {
            if (request()->ip() != config("app.srv_monitoring")) {
                Log::debug(" -> redirect to " . config('app.cms'));
            }
            if ($page == "/") {
                return redirect(config('app.cms'));
            } else {
                return redirect(config('app.cms') . "/" . $page);
            }
        }
        return redirect(config('app.url') . "/" . $page);
    }

    public function redirectInvoices(Request $request)
    {
        $page = $request->path();
        Log::debug("=================== redirectInvoices " . $page . " ====================");
        //voir .env
        return redirect(config('app.srv_dolibarr'));
    }
}
