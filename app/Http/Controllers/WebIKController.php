<?php

/**
 * WebIKController.php
 *
 * Copyright (c) 2021 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Http\Controllers;

use App\BaseCalculIks;
use App\Http\Controllers\LdeFraisController;
use App\TypeFrais;
use App\WebIK;
use Illuminate\Http\Request;
use App\Vehicule;
use Illuminate\Support\Facades\Log;

class WebIKController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Log::debug("========== WebIKController : index =========");
        $vehicules = Vehicule::distinct()->where('user_id', auth()->id())->get();
        Log::debug($vehicules);

        $ladate = "";
        $message = "";
        return view('web_ik_insert', [
            'message'   => $message,
            'ladate'    => $ladate,
            'depart'    => "",
            'arrivee'   => "",
            'distance'  => "",
            'vehicules' => $vehicules,
            'label'     => "",
            'distanceTot'  => "",
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function part($nb, $slug = null, $ville = null, $uid = null)
    {
        Log::debug("========== WebIKController : part =========");
        //
        $nb++;
        $ladate = "";
        return view('web_ik_insert_part', [
            'nb'       => $nb,
            'slug'     => $slug,
            'ville'    => $ville,
            'uid'      => $uid,
            'distance' => 0
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Log::debug("========== WebIKController : store =========");
        //On passe à LdeFraisController Store
        $tf = new TypeFrais();
        $request->merge(['type_frais_id' => $tf->getIDfromSlug("ik")]);
        $request->merge(['typeFrais' => "ik"]);

        $l = new LdeFraisController();
        $ret = $l->store($request);
        if ($ret->status() == 201) {
            return redirect('webIK')->with('status', 'Sauvegarde effectuée, vous pouvez faire une nouvelle saisie (ou fermer cette page pour terminer).');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\WebIK  $webIK
     * @return \Illuminate\Http\Response
     */
    public function show(WebIK $webIK)
    {
        Log::debug("========== WebIKController : show =========");

        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\WebIK  $webIK
     * @return \Illuminate\Http\Response
     */
    public function edit(WebIK $webIK)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\WebIK  $webIK
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WebIK $webIK)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\WebIK  $webIK
     * @return \Illuminate\Http\Response
     */
    public function destroy(WebIK $webIK)
    {
        //
    }

    public function ville($ville)
    {
        Log::debug("========== WebIKController : ville $ville =========");
        $s = $ville;
        if(strpos($ville, '(')) {
            $s = substr($ville,0,strpos($ville,'(')-1);
        }
        $b = new BaseCalculIks();
        return $b->ville($s);
    }


    public function distance($depart, $arrivee)
    {
        Log::debug("========== WebIKController : distance =========");

        $b = new BaseCalculIks();
        return $b->distance($depart,$arrivee);
    }
}
