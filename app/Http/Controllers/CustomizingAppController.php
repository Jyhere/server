<?php

/**
 * CustomizingAppController.php
 *
 * Copyright (c) 2021 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Http\Controllers;

use App;
use App\User;
use App\Entreprise;
use App\CustomizingApp;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Log;
use Illuminate\Filesystem\FilesystemManager;
use Propaganistas\LaravelFakeId\RoutesWithFakeIds;

class CustomizingAppController extends Controller
{
    use RoutesWithFakeIds;

    protected $_c = null;
    protected $_uid = null;


    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        Log::debug("CustomizingAppController : invoque");
        //
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct($userid = null)
    {
        //Cas particulier pour les appels depuis la ligne de commande
        if (php_sapi_name() == 'cli') {
            return;
        }

        Log::debug("CustomizingAppController : construct");
        if (is_null($userid)) {
            $this->_uid = auth()->id();
        } else {
            $this->_uid = $userid;
        }

        if (is_null($this->_uid)) {
            Log::debug("CustomizingAppController : construct anonyme");
            return;
        }
        Log::debug("CustomizingAppController : construct pour uiserid " . $this->_uid);

        //On cherche le bon CustomObjc
        $this->_c = CustomizingApp::where('user_id', $this->_uid)->first();
        //Si vide, on remonte d'un cran à la recherche de la customization de mon créateur
        if (is_null($this->_c)) {
            $creator = User::findOrFail($this->_uid);

            // Log::debug("  CustomizingAppController : ce compte a été créé par " . $creator->creator_id);
            $this->_c = CustomizingApp::where('user_id', $creator->creator_id)->first();
            if (is_null($this->_c)) {
                Log::debug("CustomizingAppController : rien pour la 1ere etape");
                $this->_c = new CustomizingApp();
            } else {
                Log::debug("CustomizingAppController : le créateur de cet utilisateur (" . $creator->email . ") a une customization 'perso'");
            }
        } else {
            Log::debug("CustomizingAppController : cet utilisateur a une customization 'perso'");
        }

        //Si jamais le status = 0 c'est qu'on est en version de dev, donc on n'envoie que si l'utilisateur est membre de la meme
        //societe que le createur de l'objet ...
        if ($this->_c->status == 0) {
            Log::debug("  CustomizingAppController version dev de la customzation, on verifie si cet utilisateur fait partie de la meme entreprise");

            //TODO : comment trouver l'entreprise "principale" d'un utilisateur !!!!
            //l'astuce pas pérenne consiste à prendre celle dont l'id est le plus petit mais c'est super foireux
            // $e = Entreprise::with(['users' => function ($query) {
            //     $query->where('user_id', $this->_uid);
            // }])->select('entreprises.id as eid')->groupBy('eid')->first();
            $e = Entreprise::join('entreprise_user', 'entreprises.id', '=', 'entreprise_user.entreprise_id')
                ->select('entreprises.id as eid')
                ->where('user_id', $this->_uid)
                ->groupBy('eid')->first();
            Log::debug("    " . \json_encode($e));

            if ($e) {

                Log::debug("  Les autres membres de cette entreprise (#" . $e->eid . "): (uid:" . $this->_uid . ")");
                $ee = User::join('entreprise_user', 'users.id', '=', 'entreprise_user.user_id')
                    ->select("user_id")
                    ->where('entreprise_id', $e->eid)
                    ->groupBy('user_id')
                    ->pluck('user_id');

                Log::debug("    " . \json_encode($ee));

                // Log::debug("  on cherche " . $this->_c->user_id);
                if (\in_array($this->_uid, $ee->toArray())) {
                    Log::debug("  CustomizingAppController Ok cet utilisateur fait partie de la même entreprise (#" . $e->eid . ")");
                } else {
                    Log::debug("  CustomizingAppController Cet utilisateur ne fait pas partie de la même entreprise -> reset");
                    $this->_c = new CustomizingApp();
                }
            }
            else {
                Log::debug("  CustomizingAppController pas d'eid -> reset");
                $this->_c = new CustomizingApp();
            }
        }
        if ($this->_c->status == 1) {
            Log::debug("CustomizingAppController version prod");
        }
        Log::debug("CustomizingAppController : _c = " . json_encode($this->_c));
    }

    /**
     * retourne la css
     *
     * @param   Request  $request  [$request description]
     *
     * @return  [type]             [return description]
     */
    public function css(Request $request = null)
    {
        Log::debug("CustomizingAppController : css ");

        // Log::debug("  CustomizingAppController : " . json_encode($c));
        if ($this->_c->css) {
            $style = "<style>" . $this->_c->css . "</style>";
            return response($style, 200)->header('Content-Type', 'text/css');
        }
        if ($request) {
            return response("", 404);
        } else {
            return "";
        }
    }

    /**
     * retourne le texte de la boite a propos
     *
     * @param   Request  $request  [$request description]
     *
     * @return  [type]             [return description]
     */
    public function apropos(Request $request = null)
    {
        Log::debug("CustomizingAppController : apropos ");
        // Log::debug("  CustomizingAppController : " . json_encode($c));
        if ($this->_c->apropos) {
            return response($this->_c->apropos, 200)->header('Content-Type', 'text/html');
        }

        if ($request) {
            return response("", 404);
        } else {
            return "";
        }
    }


    /**
     * retourne le texte du message a afficher
     *
     * @param   Request  $request  [$request description]
     *
     * @return  [type]             [return description]
     */
    public function message(Request $request = null)
    {
        Log::debug("CustomizingAppController : message ");
        // Log::debug("  CustomizingAppController : " . json_encode($c));
        if ($this->_c->message) {
            return response($this->_c->message, 200)->header('Content-Type', 'text/html');
        }

        if ($request) {
            return response("", 404);
        } else {
            return "";
        }
    }

    /**
     * retourne le logo png
     *
     * @param   Request  $request  [$request description]
     *
     * @return  [type]             [return description]
     */
    public function logo(Request $request = null, $asBase64 = false)
    {
        Log::debug("CustomizingAppController::logo");
        //On cherche le logo qu'il faut pousser pour cet utilisateur ...
        //En priori "mon" logo
        $driver = new FilesystemManager(app());

        if ($this->_c->id) {
            $sourceRelativeFilePath = "data/CustomizingApp/" . $this->_c->id . "/logo.png";
            // Log::debug("  CustomizingAppController :  on cherche " . $sourceRelativeFilePath);
            $driver->disk('local')->get($sourceRelativeFilePath);
            $cc = $driver->path($sourceRelativeFilePath);
            if (\file_exists($cc)) {
                // Log::debug("  CustomizingAppController :  on cherche " . \json_encode($driver));
                Log::debug("  CustomizingAppController :  on a trouvé le fichier " . \json_encode($cc));
            }

            if ($asBase64 == true) {
                Log::debug("  CustomizingAppController on le retourne en base64");

                // create the object
                $obj = array(
                    "original" => base64_encode(file_get_contents($cc)),
                    "headers"  => ['Content-Type' => 'image/png']
                );
                return $obj;
            } else {
                Log::debug("  CustomizingAppController on le retourne en download");
                return response()->download($cc, "logo.png");
            }
        } else {
            Log::debug("  CustomizingAppController :  c->id est vide, exit ");
        }

        // Log::debug("  CustomizingAppController : " . json_encode($driver));
        if ($request) {
            return response("", 404);
        } else {
            return "";
        }
    }


    /**
     * TODO : download all customizing stuff in one request
     *
     * @param   Request  $request  [$request description]
     *
     * @return  [type]             [return description]
     */
    public function all(Request $request)
    {
    }

    /**
     * retourne le logo png
     *
     * @param   Request  $request  [$request description]
     *
     * @return  [type]             [return description]
     */
    public function webLogo(Request $request, $id, $name)
    {
        Log::debug("CustomizingAppController::webLogo $id :: $name");
        //On cherche le logo qu'il faut pousser pour cet utilisateur ...
        //En priori "mon" logo
        $driver = new FilesystemManager(app());

        if (!isset($this->_c)) {
            // Log::debug("CustomizingAppController::webLogo on cherche ");
            $idDec = App::make('fakeid')->decode($id);
            $this->_c = CustomizingApp::find($idDec);
        }

        if ($this->_c->id) {
            $sourceRelativeFilePath = "data/CustomizingApp/" . $this->_c->id . "/logo.png";
            Log::debug("  CustomizingAppController :  on cherche " . $sourceRelativeFilePath);
            $driver->disk('local')->get($sourceRelativeFilePath);
            $cc = $driver->path($sourceRelativeFilePath);
            if (\file_exists($cc)) {
                // Log::debug("  CustomizingAppController :  on cherche " . \json_encode($driver));
                Log::debug("  CustomizingAppController :  on a trouvé le fichier " . \json_encode($cc));
            }

            Log::debug("  CustomizingAppController on le retourne en download");
            return response()->download($cc, "logo.png");
        } else {
            Log::debug("  CustomizingAppController :  c->id est vide, exit ");
        }

        // Log::debug("  CustomizingAppController : " . json_encode($driver));
        if ($request) {
            return response("", 404);
        } else {
            return "";
        }
    }
}
