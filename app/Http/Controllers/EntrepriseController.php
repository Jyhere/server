<?php
/*
 * EntrepriseController.php
 *
 * Copyright (c) 2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\Process\Process;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

use App\Entreprise;
use App\User;
use Illuminate\Support\Str;
use App\Jobs\ProcessSendEmail;

class EntrepriseController extends Controller
{
    public function index()
    {
        Log::debug("======EntrepriseController : index =============");
        return Entreprise::getMyEntreprises();
    }

    public function update(Request $request)
    {
        Log::debug(" ============= EntrepriseController : update ============== ");
        $e = null;
        $code = 500;

        $entreprises_possibles = Entreprise::getMyEntreprises(); //->pluck('eid', 'siren'); //->toArray();
        Log::debug("    Liste des entreprises possibles (EntrepriseController) : " . json_encode($entreprises_possibles));
        $cleanSIREN = substr(preg_replace('/\D/', '', trim($request->siret)), 0, 9);
        if ($e = $entreprises_possibles->firstWhere('siren', $cleanSIREN)) {
            Log::debug("    Cet utilisateur a le droit d'accéder à cette entreprise");
            if (sharp_user()->hasPermissionTo('edit Entreprise', 'web')) {
                Log::debug("    Cet utilisateur a le droit de modifier cette entreprise");
                //On vire les espaces éventuels du SIREN
                $request->merge(['siren' => substr(preg_replace('/\D/', '', $request->siren), 0, 9)]);
                $validatedData = $request->validate([
                    'name' => 'required|max:255',
                    'adresse' => 'required',
                    'cp' => 'required|numeric',
                    'ville' => 'required',
                    'pays' => 'required',
                    'web' => 'required',
                    'tel' => 'required',
                    'siren' => 'required',
                    'email' => 'required|email',
                ]);
                Log::debug(" modification ok pour : ");
                Log::debug($request);
                $e = Entreprise::findOrFail($e->id);
                $e->update($request->all());
                $code = 200;
            } else {
                Log::debug("EntrepriseController: cet utilisateur n'a pas le droit de mettre à jour l'entreprise.");
                $code = 403;
            }
        } else {
            //Dans le cadre du client dolibarr cette situation peut se produire si l'admin a une adresse mail existante dans
            //doliscan et le compte de l'entreprise existe mais n'est pas encore "connecté" avec ce compte utilisateur
            //pour essayer d'éviter un pb de sécurité il faudrait envoyer un mail à l'adresse du compte doliscan qui correspond
            //a cette entreprise pour valider la demande ...
            $verifEntrep = Entreprise::where('siren', $cleanSIREN)->first();
            if ($verifEntrep) {
                //Un code de sécurité a été communiqué, vérification
                Log::debug("EntrepriseController verifEntrep =  " . json_encode($verifEntrep));

                if (isset($request->checkSecurityCodeEntreprise)) {
                    //TODO
                    Log::debug("EntrepriseController JSON " . $verifEntrep->security_code);
                    $sec = json_decode($verifEntrep->security_code);
                    //Vérification que le code est ok et qu'il a été généré il y a moins de 15 minutes
                    if ($sec->code == $request->checkSecurityCodeEntreprise && $sec->timestamp > (time() - (60 * 15))) {
                        Log::debug("EntrepriseController checkSecurityCodeEntreprise valide");
                        //Il faut maintenant associer ce compte utilisateur avec l'entreprise :)
                        $u = User::where('email',$request->emailAdmin)->first();
                        $u->addRoleOnEntreprise($u->mainRole(), $verifEntrep->id);
                        $code = 200;
                        return response()->json($verifEntrep, $code);
                    } else {
                        //Erreur de vérification de code
                        $data = "CheckSecurityCodeErrorEntrep";
                        Log::debug("EntrepriseController register mail: error checkSecurity return 401");
                        return response()->json(['data' => $data], 401);
                    }
                } else {
                    $data = "emailExistCheckSecurityCodeEntreprise";
                    $verifEntrep->envoyerMailVerificationGetAdministratorRights();
                    Log::debug("========EntrepriseController register mail: return 202 (send security code by mail)");
                    return response()->json(['data' => $data], 202);
                }
            } else {
                //Cette entreprise n'existe pas ... création d'une entreprise ?
                Log::debug("    Cette entreprise n'existe pas");
                if (sharp_user()->hasRole('adminEntreprise', 'web') || sharp_user()->hasRole('responsableEntreprise', 'web')) {
                    Log::debug("    Cet utilisateur est adminEntreprise ou responsableEntreprise donc autorisation de créer son entreprise");
                    //On vire les espaces éventuels du SIREN
                    $request->merge(['siren' => substr(preg_replace('/\D/', '', $request->siren), 0, 9)]);
                    $validatedData = $request->validate([
                        'name' => 'required|max:255',
                        'adresse' => 'required',
                        'cp' => 'required|numeric',
                        'ville' => 'required',
                        'pays' => 'required',
                        'web' => 'required',
                        'tel' => 'required',
                        'siren' => 'required',
                        'email' => 'required|email',
                    ]);
                    Log::debug(" creation d'une entreprise : ");
                    Log::debug($request);
                    $u = User::findOrFail(sharp_user()->id);
                    $e = new Entreprise($request->all());
                    $e->creator_id = $u->id;
                    Log::debug($e);
                    $e->save();

                    //Et on lui donne les droits sur cette entreprise
                    $u->addRoleOnEntreprise($u->mainRole(), $e);
                    $code = 201;    
                }
                else {
                    Log::debug(" erreur 404.");
                    $code = 404;
                }
            }
        }
        Log::debug("    return $code");
        return response()->json($e, $code);
    }


    public function store(Request $request)
    {
        //Si Auth::user() a le droit
        $authUser = Auth::user();
        if ($authUser->hasRole('superAdmin', 'web') || $authUser->hasRole('adminRevendeur', 'web')) {
            Log::debug("========== EntrepriseController : store =========");

            $duplicate = Entreprise::where('email', $request->email)->first();
            if ($duplicate) {
                Log::debug("  EntrepriseController duplicate");
                $code = 409;
                $msg = "Entreprise already exist";
            } else {
                $t['name'] = $request->name;
                $t['email'] = $request->email;
                $e = new Entreprise($t);
                $e->creator_id = Auth::user()->id;

                Log::debug("  ================ EntrepriseController save ================ ");
                Log::debug($e);
                $e->save();

                //Petite notification pour aller vérifier que tout se passe bien (phase de prelancement)
                $details = array(
                    'to' => config('mail.notifications'),
                    'subject' => "[" . config('app.name') . "] Nouvelle entreprise",
                    'message' => "A verifier, une nouvelle entreprise viens d'etre ajoutée: \n\n" . json_encode($e) . "\n\n--\n" . config('app.url')
                );
                ProcessSendEmail::dispatch($details);

                Log::debug("Retour code 201 avec l'entreprise OK...");
                $code = 201;
                $msg = "Entreprise created";
            }
        } else {
            $code = 401;
            $msg = "You are not admin !";
        }
        response()->json($msg, $code);
    }
}
