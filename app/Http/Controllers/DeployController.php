<?php
/*
 * DeployController.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Symfony\Component\Process\Process;
use Illuminate\Support\Facades\Log;

class DeployController extends Controller
{
  //
  public function deploy(Request $request)
  {
    Log::debug('========deploy===========');
    Log::debug($request);

    //$githubPayload = $request->getContent();
    //$githubHash = $request->header('X-Hub-Signature');

    //$localToken = config('app.deploy_secret');
    //$localHash = 'sha1=' . hash_hmac('sha1', $githubPayload, $localToken, false);

    //if (hash_equals($githubHash, $localHash)) {
    // if ($request) {
    $buffer = "";
    $root_path = base_path();
    //cd $root_path ;;
    $cmd = "cd $root_path ; /usr/bin/nohup ./deploy.sh 2>&1 >> storage/logs/deploy.log";
    Log::debug("  $cmd");
    $process = Process::fromShellCommandline($cmd, $root_path);
    $process->setWorkingDirectory($root_path);
    $process->run(function ($type, $buffer) {
      echo $buffer;
    });

    // }
    Log::debug($buffer);
    Log::debug('=========end deploy==========');
  }
}
