<?php
/*
 * SirenController.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class SirenController extends Controller
{
    public function search(Request $request)
    {
        Log::debug('========SirenController search===========');
        // Log::debug($request);

        $siren = '';
        if (isset($request['siren'])) {
            $siren = substr(preg_replace('/\D/', '', $request['siren']), 0, 9);
        } elseif (isset($request['siret'])) {
            $siren = substr(preg_replace('/\D/', '', $request['siret']), 0, 9);
        }
        Log::debug('======== on cherche '.$siren);

        if (strlen($siren) != 9) {
            return;
        }

        if (config('app.srv_siret_key')) {
            //On propage la question au webservice ad hoc -> http://192.168.16.126/api/search/520339938
            //2021-05-11 https://siret.cap-rel.fr/ s'ouvre pour les auto-hébergements tiers, voir le site
            $client = new \GuzzleHttp\Client();

            $headers = [
            'User-Agent' => 'DoliSCAN/'.config('app.domain'),
            'Authorization' => 'Bearer '.config('app.srv_siret_key'),
            'Accept' => 'application/json',
        ];
            $res = $client->request('GET', config('app.srv_siret').'/api/sirets/'.$siren, [
            'headers' => $headers,
        ]);

            Log::debug($res->getStatusCode());
            Log::debug($res->getBody());
            if ($res->getStatusCode() == 200) {
                //On encadre de [] pour rester compatible avec l'ancienne api
                Log::debug('=======SirenController search ok============');

                return '['.$res->getBody().']';
            } else {
                Log::debug('=======SirenController search error============');

                return;
            }
        }
        Log::debug('===================');
    }
}
