<?php
/*
 * CdeFraisController.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Http\Controllers;

use Auth;
use App\CdeFrais;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Exports\ExportsTools;
use App\LdeFrais;
use App\NdeFrais;
use App\Entreprise;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Crypt;
use App\Mail\MailSendCDF;
use Illuminate\Support\Facades\Mail;
use App\MoyenPaiement;


class CdeFraisController extends Controller
{
    protected CdeFrais $_cdeFrais;
    protected Entreprise $_entreprise;
    protected String $_bigZipFile;
    protected array $_users;

    public function __construct()
    {
        Log::debug('=================== CdeFraisController __construct');
        // $this->_entreprise = new Entreprise();
        // $this->_cdeFrais = new CdeFrais();;
        // $this->_users = null;
        // $this->_bigZipFile = null;
    }

    public function __invoke($hash)
    {
        //TODO verifier si ce user a le droit de télécharger ce fichier ?
        //                        The file "//srv/webs/doliscan.fr/ndf/storage/CdeFrais/excial-440119147/20210331//quadratus.zip" does not exist
        $fullFilename = realpath(Crypt::decryptString($hash));
        Log::debug("CdeFraisController::__invoque on demande " . Crypt::decryptString($hash));
        $filename = "";
        if (file_exists($fullFilename)) {
            $filename = \basename($fullFilename);
        } else {
            return abort(404);
        }

        //On recupere le path
        $directory = storage_path() . "/CdeFrais/";
        $reste = \str_replace($directory, "", $fullFilename);
        Log::debug("CdeFraisController:: on enleve $directory et il reste $reste");
        if (trim($reste) != "") {
            //On recupere la 1ere partie
            $tab = explode("/", $reste);
            $lenom  = $tab[0];
            $ladate = $tab[1];
            Log::debug("CdeFraisController::le nom de la société est $lenom pour $ladate");
            //On remplace le "@" de l'adresse mail par un "-"
            $nom = \str_replace("@", "-", $ladate . "-" . $lenom . "-" . $filename);

            activity('Download')->log("CdeFrais: $lenom / $ladate");

            return response()->download($fullFilename, $filename);
        } else {
            return abort(404);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CdeFrais  $cdeFrais
     * @return \Illuminate\Http\Response
     */
    public function show(CdeFrais $cdeFrais)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CdeFrais  $cdeFrais
     * @return \Illuminate\Http\Response
     */
    public function edit(CdeFrais $cdeFrais)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CdeFrais  $cdeFrais
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CdeFrais $cdeFrais)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CdeFrais  $cdeFrais
     * @return \Illuminate\Http\Response
     */
    public function destroy(CdeFrais $cdeFrais)
    {
        //
    }

    /**
     * Build Zip File with all NDF of all members of a company (entreprise)
     *
     * @param   [type]  $entrepriseID  id of company
     * @param   [type]  $ladate        date of NDF/CDF to build
     *
     * @return  null|int                 code (null=default, -1=error,0=no ndf for this month,1=ok)
     */
    public function BuildCdeFrais($entrepriseID, $ladate)
    {
        Log::debug("CdeFraisController::BuildCdeFrais ==================================================================");
        Log::debug("  CdeFraisController::BuildCdeFrais pour entreprise=$entrepriseID date=$ladate");
        $retour = null;
        $this->_users = array();
        $this->_cdeFrais = new CdeFrais($entrepriseID, $ladate);
        $exportTools = new ExportsTools();
        $zipSource = array(); //Liste des fichiers à zipper...
        $bigZipDir = $this->_cdeFrais->getDirectory();

        $exportTools->cleanupPath($bigZipDir);

        if ($this->_entreprise == null)
            $this->_entreprise = Entreprise::findOrFail($entrepriseID);
        //Tous les utilisateurs de l'entreprise

        //On refresh toutes les notes de frais
        $nbUser = 0;
        foreach ($this->_entreprise->users as $user) {
            $codeUser = "D" . sprintf("%'.02d", $nbUser);
            Log::debug("  CdeFraisController::BuildCdeFrais utilisateur : " . $user->email . " numéro $nbUser code $codeUser");

            $ldeFrais = new LdeFrais();
            $aDesFrais = $ldeFrais->where('user_id', $user->id)->orderby('ladate', 'asc')->first();
            if ($aDesFrais) {
                $ndf = $user->NdeFrais->where('fin', $ladate)->where('status', NdeFrais::STATUS_CLOSED)->last();
                if ($ndf) {
                    Log::debug("  CdeFraisController::BuildCdeFrais il a une NDF ... ");

                    $ndfC = new NdeFraisController;
                    $ndfC->webBuildPDF($ndf->id);

                    $montant = $ndf->montant;
                    if ($montant == 0) {
                        Log::debug("    CdeFraisController::BuildCdeFrais montant null de la note de frais !");
                    } else {
                        Log::debug("    CdeFraisController::BuildCdeFrais montant de la note de frais : $montant");
                        Log::debug("    CdeFraisController::BuildCdeFrais on devrait avoir les fichiers suivants : " . $ndfC->getfilePDFFullPath() . " et " . $ndfC->getfilePDFName());
                        //Le hic c'est que Sharp est propre et conforme au plan de stockage Laravel ... il faut donc transformer le $ndfC->getfilePDFFullPath()
                        //en un blabla acceptable ...
                        $p = Storage::disk('local')->getDriver()->getAdapter()->getPathPrefix();
                        $reste = str_replace($p, "", $ndfC->getfilePDFFullPath());
                        $dir = dirname($ndfC->getfilePDFFullPath());
                        $quadraDir = $dir . "/" . substr($ndfC->getfilePDFName(), 0, strpos($ndfC->getfilePDFName(), '-')) . "-quadratus";
                        Log::debug("    CdeFraisController::BuildCdeFrais Les fichiers quadra sont ici : $quadraDir");

                        $exportTools->copyFilesAndRenamePrefix($quadraDir, $bigZipDir, "D00", $codeUser);

                        //On devrait donc avoir
                        $test = Storage::disk('local')->exists($reste);
                        Log::debug("    CdeFraisController::BuildCdeFrais test pour reste: $reste -> $test");
                        if ($test == 1) {
                            $zipSource[] = $ndfC->getfilePDFFullPath();
                        } else {
                            Log::debug("  CdeFraisController::BuildCdeFrais attention le fichier ($reste) n'est pas présent sur le disque !!!");
                        }

                        unset($u);
                        $u['nom'] = $user->full_name;
                        $paiementPerso = MoyenPaiement::where('slug', 'perso')->first();
                        $totalPayePerso = $ldeFrais->where('user_id', $user->id)->where('nde_frais_id', $ndf->id)->where('moyen_paiement_id', $paiementPerso->id)->sum('ttc');
                        Log::debug("  CdeFraisController::BuildCdeFrais paiementPerso : " . json_encode($paiementPerso) . " rembourser " . $totalPayePerso);

                        //Les IK
                        $paiementPersoIK = MoyenPaiement::where('slug', 'pseudo-perso-ik')->first();
                        $totalPayePersoIK = $ldeFrais->where('user_id', $user->id)->where('nde_frais_id', $ndf->id)->where('moyen_paiement_id', $paiementPersoIK->id)->sum('ttc');
                        Log::debug("  CdeFraisController::BuildCdeFrais paiementPersoIK : " . json_encode($paiementPersoIK) . " rembourser " . $totalPayePersoIK);

                        $u['grandTotal'] = $montant;                        // pb c'est le total y compris les frais payés par un moyen "pro" donc ce n'est pas le montant à rembourser
                        $u['ttc'] = $totalPayePerso + $totalPayePersoIK;    // montant à rembourser

                        //Pour eviter d'afficher un "rembourser 0 à utilisateur x" dans le mail récap
                        if ($u['ttc'] > 0) {
                            $this->_users[$nbUser] = $u;
                        }
                        $nbUser++;
                    }
                } else {
                    Log::debug("  CdeFraisController::BuildCdeFrais :: cet utilisateur n'a pas de note de frais cloturee pour cette période");
                }
            } else {
                Log::debug("  CdeFraisController::BuildCdeFrais :: cet utilisateur n'a pas de note de frais (compte admin / autre)");
            }
        }

        if (count($zipSource) > 0) {
            //On empaquette tout ça dans un gros fichier
            Log::debug("  CdeFraisController::BuildCdeFrais on fabrique le gros zip avec : " . implode(', ', $zipSource) . "");

            //20210831-devtemp-sas-132456789-quadratus.zip
            $this->_bigZipFile = $this->_cdeFrais->getFileName();
            if ($exportTools->buildZipFile($bigZipDir, $this->_bigZipFile)) {
                Log::debug("  CdeFraisController::BuildCdeFrais création du fichier zip OK");
                $retour = 1;
            } else {
                Log::debug("  CdeFraisController::BuildCdeFrais Erreur de création du fichier zip ...");
                $retour = -1;
            }
        } else {
            Log::debug("  CdeFraisController::BuildCdeFrais Il n'y a pas de notes de frais > 0 pour cette période ...");
            $retour = 0;
        }
        //=======================================================================================================================================
        return $retour;
    }


    /**
     * Pour envoyer toutes les notes de frais de l'entreprise à un destinataire
     *
     * @param   [type]  $entrepriseID  [$entrepriseID description]
     * @param   [type]  $ladate        [$ladate description]
     * @param   [type]  $destinataire  [$destinataire description]
     *
     * @return  [type]                 [return description]
     */
    public function SendCdeFrais($entrepriseID, $ladate, $destinataire = "")
    {
        Log::debug("CdeFraisController::SendCdeFrais pour entreprise=$entrepriseID date=$ladate");

        if ($this->BuildCdeFrais($entrepriseID, $ladate) == 1) {
            if ($this->_entreprise == null)
                $this->_entreprise = Entreprise::findOrFail($entrepriseID);

            // =========== send mail
            if ($destinataire != "") {
                $f['uri']   = $this->_cdeFrais->getDownloadURI();
                $f['label'] = basename($this->_bigZipFile);
                $attachments[] = $f;

                Log::debug("  CdeFraisController::SendCdeFrais document joint: ");
                Log::debug($attachments);

                activity('Mail')->log("CdeFrais for " . $this->_entreprise->name . " to " . $destinataire);
                //TODO urgent
                Mail::to($destinataire)
                    ->bcc(config('mail.notifications'))
                    ->send(new MailSendCDF($this, $this->_entreprise->name, $ladate, $attachments, $this->_users));

                //TODO deplacer en queue
                // sleep(config('mail.sleep'));
                $retour = "Le classeur de frais est en cours d'envoi par mail.";
            }
        }
        return $retour;
    }
}
