<?php

/**
 * SmartphoneApp.php
 *
 * Copyright (c) 2021 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Jobs\ProcessSendEmail;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Carbon;
use App\Mail\MailNewVersion;


class SmartphoneApp extends Model
{
    use SoftDeletes;

    protected $guarded = ['id'];

    /**
     * os : 
     * version : 
     * last_mail : 
     * last_seen : 
     * user_id : 
     */
    protected $fillable = ['os', 'version', 'last_mail', 'last_seen', 'user_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get user agent
     *
     * @param   [type]  $request  http request
     *
     * @return  [type]            android|ios|other
     */
    static public function getUAFromRequest($request)
    {
        //La dernière version disponible de l'application ...
        //Note: on peut recupérer l'os utilisé dans la signature de l'application ...
        $ua = $request->header('User-Agent');

        if (Str::contains($ua, 'Android')) {
            return "android";
        } else if (Str::contains($ua, 'iOS')) {
            return "ios";
        }
        return "other";
    }

    /**
     * Get user agent version (for example app version on smartphone)
     * string is like "Mozilla/5.0 OpenNDF/1.4.2 Android"
     * 
     * @param   [type]  $request  http request
     *
     * @return  [string]            1.4.2
     */
    static public function getUAVersionFromRequest($request)
    {
        //La dernière version disponible de l'application ...
        //Note: on peut recupérer l'os utilisé dans la signature de l'application ...
        $ua = $request->header('User-Agent');
        if (preg_match('/.*OpenNDF\/(.*)\s.*/', $ua, $parts)) {
            return trim($parts[1]);
        }
        return "";
    }


    /**
     * Send mail to $user because of old app version on his/her smartphone
     *
     * @return  [type]            [return description]
     */
    public function envoyerMailAppUpgrade($user, $versionUsed, $versionAvailable)
    {
        Log::debug("User::envoyerMailUpgrade");

        //Dans tous les cas on update la date de dernier passage
        $this->update([
            'last_seen' => now()
        ]);

        //Uniquement si on ne l'a pas déjà envoyé aujourd'hui !
        $spamTest = SmartphoneApp::whereDate('last_mail', Carbon::today())->where('user_id', '=', $user->id)->first();
        if($spamTest) {
            Log::debug("User::envoyerMailUpgrade un rappel a déjà été envoyé aujourd'hui ...");
            return;
        }

        $msg = "<p>Bonjour,<br />\n";
        $msg .= "Vous venez de vous connecter au service DoliSCAN avec une version <b>ancienne</b> de l'application installée sur votre Smartphone.</p>\n\n";
        $msg .= "<p> </p>";
        $msg .= "<p>Cette version n'est pas complètement compatible avec la version installée sur le serveur vous devez donc procéder sans tarder à une mise à jour.</p>\n\n";
        $msg .= "<p>Version installée sur votre smartphone: $versionUsed<br />\n";
        $msg .= "Version disponible : $versionAvailable</p>\n\n";
        $msg .= "<p> </p>";
        $msg .= "<p>Mise à jour pour iPhone: <a href='https://apps.apple.com/fr/app/id1455241946'>https://apps.apple.com/fr/app/id1455241946</a></p>\n\n";
        $msg .= "<p> </p>";
        $msg .= "<p>Mise à jour pour Android: <a href='https://play.google.com/store/apps/details?id=fr.caprel.doliscan'>https://play.google.com/store/apps/details?id=fr.caprel.doliscan</a></p>\n\n";
        $msg .= "<p> </p>";
        $msg .= "<p>Pour plus de détails vous pouvez consulter le site internet <a href='https://www.doliscan.fr/fr/lapplication'>https://www.doliscan.fr/fr/lapplication</a></p>\n\n";

        $details = array(
            'to' => $user->email,
            'bcc' => config('mail.notifications'),
            'subject' => "[" . config('app.name') . "] ",
            'objectMail' => new MailNewVersion("Mise à jour requise", $msg)
        );
        ProcessSendEmail::dispatch($details);

        $this->update([
            'last_mail' => now()
        ]);

        //Il faudrait envoyer en copie le mail au revendeur associé...
        $message = "envoyerMailAppUpgrade to " . $this->email;

        activity('Mail')
            ->by(\sharp_user())
            ->performedOn($this)
            ->withProperty('email', $this->email)
            ->log($message);
        return "Mail envoyé";
    }
}
