<?php
/*
 * AppServiceProvider.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\DB;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Activitylog\Models\Activity;
use MadWeb\Robots\RobotsFacade;
use Illuminate\Support\Facades\Log;

class AppServiceProvider extends ServiceProvider
{
    // use LogsActivity;

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //la gestion du robots.txt
        RobotsFacade::setShouldIndexCallback(function () {
            if (config('app.env') == 'prod') {
                // Log::debug("setShouldIndexCallback: true");
                return true;
            } else {
                // Log::debug("setShouldIndexCallback: false : " . config('app.env') );
                return false;
            }
        });

        if (config('app.debug')) {
            DB::listen(function ($query) {

                // $query->bindings ne peut transformer les dates de type DateTimeImmutable et DateTime en string
                // On effectue donc la conversion avant l'écriture du log
                foreach ($query->bindings as $key => $value)
                    if (gettype($value) == 'object' && (get_class($value) == 'DateTimeImmutable' || get_class($value) == 'DateTime'))
                        $query->bindings[$key] = $query->bindings[$key]->format('Y-m-d h:i:s');

                File::append(
                    storage_path('/logs/sql.log'),
                    $query = $query->sql . ' [' . implode(', ', $query->bindings) . ']' . PHP_EOL
                );
            });
        }

        //On ajoute l'ip et le user agent dans les logs
        Activity::saving(function (Activity $activity) {
            $activity->properties = $activity->properties->put('ip', request()->ip())
                ->put('User-Agent', request()->header('User-Agent'));
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
