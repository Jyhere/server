<?php

/**
 * TagsFrais.php
 *
 * Copyright (c) 2021 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App;

use App\LdeFrais;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class TagsFrais extends Model
{
    //
    protected $guarded = ['user_id'];
    protected $fillable = ['code', 'label', 'start', 'end'];
    protected $table = 'tags_frais';

    //1 tag n frais
    public function ldeFrais(): BelongsToMany
    {
        return $this->belongsToMany(LdeFrais::class);
    }

    //Les tags sont créés par l'utilisateur
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    //Et eventuellement partagés au sein d'une entreprise
    public function entreprise()
    {
        return $this->belongsTo(Entreprise::class);
    }

    public function save(array $options = array())
    {
        Log::debug("Appel de TagsFrais :: save");

        //add user_id
        if (is_null($this->user_id))
            $this->user_id = Auth::user()->id;
        return parent::save();
    }
}
