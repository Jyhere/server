<?php

/**
 * CustomizingApp.php
 *
 * Copyright (c) 2021 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use DB;
use Code16\Sharp\Form\Eloquent\Uploads\Thumbnails\Thumbnail;
use Illuminate\Support\Facades\Log;
use Illuminate\Filesystem\FilesystemManager;
use Propaganistas\LaravelFakeId\RoutesWithFakeIds;


class CustomizingApp extends Model
{
    use RoutesWithFakeIds;

    //
    protected $fillable = ['css', 'apropos', 'message', 'more', 'user_id', 'status'];
    protected $dates = ['created_at', 'deleted_at'];
    protected $guarded = ['id'];

    //
    // public function users()
    // {
    //     return $this->belongsTo('App\User');
    // }


    public function __construct(array $attributes = array())
    {
        $this->setRawAttributes(array(
            'css'  => "",
            'apropos' => "",
            'message' => "",
            "more" => ""
        ), true);

        parent::__construct($attributes);
    }

    private function _defaultCSS()
    {
        return ".page, .page__background, .page__content, .page--material__background {

}

.container {

}

.toolbar {

}

.menubutton {

}

.ons-icon {

}

.menulabel {

}";
    }

    private function _defaultApropos()
    {
        return "<b>Société xxxx</b> <br />Hotline : <br />Téléphone : <br />Votre commercial :";
    }

    public function logo()
    {
        return "logo.png";
    }

    /**
     * @param int|null $width
     * @param int|null $height
     * @param array $filters
     * @return string
     */
    public function thumbnail()
    {
        $sourceRelativeFilePath = "data/CustomizingApp/" . $this->id . "/logo.png";
        $thumbnailDisk = new FilesystemManager(app());
        $thumbnailDisk->disk('local')->get($sourceRelativeFilePath);

        // $fullFileName = storage_path() . "/data/CustomizingApp/" . $this->id . "/logo.png";
        $retour = "." . $thumbnailDisk->url($sourceRelativeFilePath);
        Log::debug("CustomizingApp : disk " . $retour);
        //TODO ameliorer (un jour)
        // $ret = url(config('app.url') . '/' . config('sharp.custom_url_segment') . "/api/form/download/logo/customizingApp/" . $this->id . "?fileName=logo.png");
        $ret = route("customizingApp.logo", [ $this, "logo.png" ] );
        Log::debug("CustomizingApp : ret " . $ret);
        return $ret;
        //return $thumbnailDisk->url($sourceRelativeFilePath) . ($this->appendTimestamp ? "?" . filectime($thumbnailDisk->path($sourceRelativeFilePath)) : "");
    }


}
