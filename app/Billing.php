<?php
/**
 * Billing.php
 *
 * Copyright (c) 2021 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App;

use Illuminate\Database\Eloquent\Model;
use Log;
use DB;

class Billing extends Model
{
    protected $fillable = ['price', 'start', 'history', 'comments', 'entreprise_id' ,'billingto_id'];
    protected $dates = ['created_at', 'deleted_at'];
    protected $guarded = ['id'];

    //
    public function entreprise()
    {
        return $this->belongsTo('App\Entreprise');
    }

    public function billingto()
    {
        return $this->belongsTo('App\Entreprise');
    }
}
