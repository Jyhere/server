<?php

/**
 * Vehicule.php
 *
 * Copyright (c) 2021 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App;

use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Support\Str;
use App\LdeFrais;
use Illuminate\Support\Facades\Auth;


class Vehicule extends Model
{
    use SoftDeletes;
    use LogsActivity;

    protected $guarded = ['id', 'uuid'];

    protected static $logName = 'Vehicule';
    protected static $logAttributes = ['*'];
    protected static $logAttributesToIgnore = ['updated_at'];
    protected static $logOnlyDirty = true;
    protected static $submitEmptyLogs = false;

    /**
     * number : immatriculation / vehicule number
     * name : nom (exemple opel astra)
     * energy : essence/diesel/electrique
     * power : nombre de chevaux
     * type : vu, vp, n1 (pour les pickup etc ...), moto, cyclo
     * kmbefore : ik payés avant utilisation de doliscan
     * details : json libre
     * is_perso : true si véhicule personnel
     * user_id : ref de l'utilisateur (key)
     *
     * @var [type]
     */
    protected $fillable = ['number', 'name', 'energy', 'power', 'type', 'kmbefore', 'details', 'is_perso', 'user_id'];

    //
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function LdeFrais()
    {
        return $this->hasMany(LdeFrais::class);
    }


    protected static function boot()
    {
        parent::boot();

        static::creating(function ($vehicule) {
            $vehicule->uuid = (string) Str::uuid();
        });
    }

    /**
     * Accesseur qui "corrige" la mise en forme de l'immat
     *
     * @param   [type]  $value  [$value description]
     *
     * @return  [type]          [return description]
     */
    public function getNumberAttribute($value)
    {
        $clean = Str::upper(preg_replace('/[^a-zA-Z0-9]/', "", $value));
        if (preg_match('/(\w\w)(\d\d\d\d)(\w\w)/', $clean, $parts)) {
            $clean = $parts[1] . "-" . $parts[2] . "-" . $parts[3];
        }
        return ($clean);
    }

    //Retourne les données au format historique exemple
    //"Opel Vivaro;diesel;6cv;vu;50000;AA-123-ZZ;xxxx-xxxx-xxxx-xxxx-xxxxx"
    public function getPackedData()
    {
        return ($this->name . ";" . $this->energy . ";" . $this->power . ";" . $this->type . ";" . $this->kmbefore . ";" . $this->number . ";" . $this->uuid . ";");
    }

    /**
     * accesseur special qui permet de faire $vehicule->packed_data
     *
     * @return  [type]  [return description]
     */
    public function getPackedDataAttribute()
    {
        return $this->getPackedData();
    }

    /**
     * permet de faire un appel $vehicule->amc
     *
     * @return  [type]  auto / moto / cyclo
     */
    public function getAmcAttribute()
    {
        if ($this->type == "moto") {
            return "moto";
        } else if ($this->type == "cyclo") {
            return "cyclo";
        }
        //vp / vu / n1 etc.
        return "auto";
    }

    /**
     * permet de faire un appel $vehicule->is_utilitaire
     *
     * @return  [boolean]  true si vehicule utilitaire
     */
    public function getIsUtilitaireAttribute()
    {
        if ($this->type == "vu") {
            return true;
        } else {
            return false;
        }
    }

    /**
     * factorisation de code pour l'extraction de données depuis la chaine "csv"
     *
     * @param   [type]  $str      [$str description] "Opel vivaro;diesel;7cv;vu;1450;AA-123-ZZ"
     * @param   [type]  $fieldnb  [$fieldnb description]
     * @param   [type]  $default  [$default description]
     *
     * @return  [type]            [return description]
     */
    public static function extractGenericFromString($str, $fieldnb, $default)
    {
        $res = $default;
        $tab = explode(';', $str);
        if (isset($tab[$fieldnb]) && ($tab[$fieldnb] != "")) {
            $res = $tab[$fieldnb];
        }
        return $res;
    }

    //Extrait la partie "nom" de la chaine $v
    //"Opel vivaro;diesel;7cv;vu;1450"
    public static function extractVehiculeNameFromString($v = "")
    {
        return Vehicule::extractGenericFromString($v, 0, "");
    }

    //Extrait la partie "diesel" de la chaine $v
    //"Opel vivaro;diesel;7cv;vu;1450"
    public static function extractVehiculeCarburantFromString($v = "")
    {
        return Vehicule::extractGenericFromString($v, 1, "essence");
    }

    //Extrait la partie "cv" de la chaine $v
    //"Opel vivaro;diesel;7cv;vu;1450"
    public static function extractVehiculeCVFromString($v = "")
    {
        return Vehicule::extractGenericFromString($v, 2, 4);
    }

    //Extrait la partie "type" de la chaine $v
    //"Opel vivaro;diesel;7cv;vu;1450;AA-123-ZZ" -> vu
    public static function extractVehiculeTypeFromString($v = "")
    {
        return Vehicule::extractGenericFromString($v, 3, 0);
    }

    //Extrait la partie "km fait avant doliscan" de la chaine $v
    //"Opel vivaro;diesel;7cv;vu;1450" -> 1450
    public static function extractVehiculeKMBeforeFromString($v = "")
    {
        return Vehicule::extractGenericFromString($v, 4, 0);
    }

    //Extrait la partie "immatriculation" de la chaine $v
    //"Opel vivaro;diesel;7cv;vu;1450;AA-123-ZZ"
    public static function extractVehiculeImmatFromString($v = "")
    {
        return Vehicule::extractGenericFromString($v, 5, 0);
    }

    //Extrait la partie "uuid" de la chaine $v
    //"Opel vivaro;diesel;7cv;vu;1450;AA-123-ZZ;uuid"
    public static function extractVehiculeUUIDFromString($v = "")
    {
        return Vehicule::extractGenericFromString($v, 6, 0);
    }

    /**
     * recherche le vehicule correspondant à ce que l'application a envoyé via le POST du formulaire
     *
     * @param   [type]  $str  [$str description]
     *
     * @return  [type]        [return description]
     */
    public static function searchFromPOST($str)
    {
        Log::debug("Vehicule::searchFromPOST ($str)");
        //1er cas de figure cet utilisateur n'a qu'un vehicule perso
        $vehicules = Vehicule::withTrashed()->where("user_id", "=", Auth::user()->id)
            // ->where('is_perso', 1) //idée : ajouter un argument a la fonction pour la recherche ?
            ->get();
        if ($vehicules->count() == 0) {
            Log::debug("  aucun véhicule !!!!!!!!! erreur grave -> création d'un nouveau !");
            //On en créé un à partir des données du post
            $v = Vehicule::create(
                [
                    "number"      => Vehicule::extractVehiculeImmatFromString($str),
                    "name"        => Vehicule::extractVehiculeNameFromString($str),
                    "energy"      => Vehicule::extractVehiculeCarburantFromString($str),
                    "power"       => Vehicule::extractVehiculeCVFromString($str),
                    "type"        => Vehicule::extractVehiculeTypeFromString($str),
                    "kmbefore"    => Vehicule::extractVehiculeKMBeforeFromString($str),
                    "user_id"     => Auth::user()->id,
                ]
            );
            return $v;
        } else if ($vehicules->count() == 1) {
            Log::debug("  un seul vehicule pour cet utilisateur");
            return $vehicules->first();
        } else {
            Log::debug("  cet utilisateur a plusieurs vehicules ... on cherche pour voir s'il y en a un qui correspond.");
            //En priorité on cherche sur l'immat
            $v = $vehicules->where('number', Vehicule::extractVehiculeImmatFromString($str));
            if ($v->count() == 1) {
                Log::debug("  véhicule trouvé par son immat");
                $ret = $v->first();
                if ($ret->trashed())
                    $ret->restore();
                return $ret;
            }
            //Puis l'uuid
            $v = $vehicules->where('uuid', Vehicule::extractVehiculeUUIDFromString($str));
            if ($v->count() == 1) {
                Log::debug("  véhicule trouvé par son uuid");
                $ret = $v->first();
                if ($ret->trashed())
                    $ret->restore();
                return $ret;
            }
            //Et si toujours rien on ratisse plus large
            $v = $vehicules->where('power', Vehicule::extractVehiculeCVFromString($str))->where('energy', Vehicule::extractVehiculeCarburantFromString($str));
            if ($v->count() == 1) {
                Log::debug("  véhicule trouvé par le couple puissance/carburant");
                $ret = $v->first();
                if ($ret->trashed())
                    $ret->restore();
                return $ret;
            }
            Log::debug("  par défaut on passe le 1er");
            return $vehicules->first();
        }
    }
}
