<?php
/*
 * dashSuperAdmin.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Sharp;

use DB;
use Code16\Sharp\Dashboard\SharpDashboard;
use Code16\Sharp\Dashboard\DashboardQueryParams;
use Code16\Sharp\Dashboard\Widgets\SharpLineGraphWidget;
use Code16\Sharp\Dashboard\Widgets\SharpBarGraphWidget;
use Code16\Sharp\Dashboard\Widgets\SharpPanelWidget;
use Code16\Sharp\Dashboard\Widgets\SharpOrderedListWidget;
use Code16\Sharp\Dashboard\Widgets\SharpGraphWidgetDataSet;
use Code16\Sharp\Dashboard\Widgets\SharpPieGraphWidget;
use Code16\Sharp\Dashboard\Layout\DashboardLayoutRow;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;

use App\User;
use App\Entreprise;
use App\Console\Commands\CheckLastVersion;

class dashSuperAdmin extends SharpDashboard
{
    /**
     * Build dashboard's widget using ->addWidget.
     */
    protected function buildWidgets()
    {
        // Log::debug("******buildWidgets*************");
        $this->addWidget(
            SharpBarGraphWidget::make("usersEntreprises")
                ->setTitle("Création de comptes")
                ->setHeight(200)
                ->setShowLegend(false)
                ->setDisplayHorizontalAxisAsTimeline(true)
        )->addWidget(
            SharpBarGraphWidget::make("notesDeFrais")
                ->setHeight(200)
                ->setTitle("Notes de frais")
                ->setShowLegend(false)
                ->setDisplayHorizontalAxisAsTimeline(true)
        )->addWidget(
            SharpLineGraphWidget::make("facturettes")
                ->setHeight(200)
                ->setTitle("Facturettes")
                ->setShowLegend(false)
                ->setDisplayHorizontalAxisAsTimeline(true)
        )->addWidget(
            SharpOrderedListWidget::make("statistiquesChiffres")
                ->setTitle("Informations synthétiques")
        )->addWidget(
            SharpPanelWidget::make("upgradeMessage")
                ->setInlineTemplate("<h2>Mise à jour</h2><br /><p style=\"text-align: left\">Votre serveur DoliSCAN peut-être mis à jour. Vous utilisez actuellement la version <b>{{ localVersion }}</b> (révision git <b>{{ localGitVersion }}</b>), la dernière version stable proposée par la société CAP-REL est <b>{{ stableVersion }}</b>. Vous pouvez suivre le lien de <a href=\"https://inligit.fr/doliscan/server/-/blob/prod/\">Téléchargement</a> et consulter la <a href=\"https://inligit.fr/doliscan/server/-/blob/prod/INSTALLATION.md\">Documentation</a>.</p>")
        );
    }


    function buildDashboardConfig()
    {
        // Log::debug("*****************buildDashboardConfig**");
    }

    /**
     * Build dashboard's widgets layout.
     */
    protected function buildWidgetsLayout()
    {
        // Log::debug("************buildWidgetsLayout*******");
        $this
            ->addRow(function (DashboardLayoutRow $row) {
                $row->addWidget(12, "upgradeMessage");
            })
            ->addRow(function (DashboardLayoutRow $row) {
                $row->addWidget(6, "usersEntreprises")
                    ->addWidget(6, "notesDeFrais");
            })
            ->addRow(function (DashboardLayoutRow $row) {
                $row->addWidget(6, "statistiquesChiffres")
                    ->addWidget(6, "facturettes");
            });
    }

    /**
     * Build dashboard's widgets data, using ->addGraphDataSet and ->setPanelData
     *
     * @param DashboardQueryParams $params
     */
    protected function buildWidgetsData(DashboardQueryParams $params)
    {
        // Log::debug("**************buildWidgetsData*****");
        //SQLite ne sait pas faire des group by comme on le souhaite (ou je ne sais pas le faire)
        //Alors j'ai bricolé un truc qui génère des données (mais pas les bonnes) pour pouvoir avancer sachant que la
        //prod est sur un MariaDB :)
        if (config('database.default') === 'sqlite') {
            $queryE =   DB::table('entreprises')->select(DB::raw("strftime('%Y-%m',created_at) as label, count(*) as value"));
            $queryU =   DB::table('users')->select(DB::raw("strftime('%Y-%m',created_at) as label, count(*) as value"));
            $queryNDF = DB::table('nde_frais')->select(DB::raw("strftime('%Y-%m',created_at) as label, count(*) as value"));
            $queryLDF = DB::table('lde_frais')->select(DB::raw("strftime('%Y-%m',created_at) as label, count(*) as value"));
            $queryE->groupBy(DB::raw('label'));
            $queryU->groupBy(DB::raw('label'));
            $queryNDF->groupBy(DB::raw('label'));
            $queryLDF->groupBy(DB::raw('label'));
        } else {
            $queryE =   DB::table('entreprises')->select(DB::raw("DATE_FORMAT(created_at,'%Y-%m') as label, count(*) as value"));
            $queryU =   DB::table('users')->select(DB::raw("DATE_FORMAT(created_at,'%Y-%m') as label, count(*) as value"));
            $queryNDF = DB::table('nde_frais')->select(DB::raw("DATE_FORMAT(created_at,'%Y-%m') as label, count(*) as value"));
            $queryLDF = DB::table('lde_frais')->select(DB::raw("DATE_FORMAT(created_at,'%Y-%m') as label, count(*) as value"));

            $queryE->groupBy(DB::raw("DATE_FORMAT(created_at,'%Y-%m')"));
            $queryU->groupBy(DB::raw("DATE_FORMAT(created_at,'%Y-%m')"));
            $queryNDF->groupBy(DB::raw("DATE_FORMAT(created_at,'%Y-%m')"));
            $queryLDF->groupBy(DB::raw("DATE_FORMAT(created_at,'%Y-%m')"));
        }
        $dataE = $queryE
            ->where('created_at', '>=', Carbon::now()->submonth(12))
            ->take(12)
            ->get()
            ->pluck("value", "label");

        $dataU = $queryU
            ->where('created_at', '>=', Carbon::now()->submonth(12))
            ->take(12)
            ->get()
            ->pluck("value", "label");

        $dataNDF = $queryNDF
            ->where('created_at', '>=', Carbon::now()->submonth(12))
            ->take(12)
            ->orderBy("debut")
            ->get()
            ->pluck("value", "label");

        $dataLDF = $queryLDF
            ->where('created_at', '>=', Carbon::now()->submonth(12))
            ->take(12)
            ->orderBy("created_at")
            ->get()
            ->pluck("value", "label");

        // Log::debug($queryE->toSql());
        // Log::debug($queryU->toSql());
        // Log::debug($queryNDF->toSql());
        // Log::debug($queryLDF->toSql());
        Log::debug(json_encode($dataLDF));

        $this->addGraphDataSet(
            "usersEntreprises",
            SharpGraphWidgetDataSet::make($dataE)
                ->setLabel("entreprises")
                ->setColor("grey")
        );

        $this->addGraphDataSet(
            "usersEntreprises",
            SharpGraphWidgetDataSet::make($dataU)
                ->setLabel("utilisateurs")
                ->setColor("orange")
        );

        $this->addGraphDataSet(
            "notesDeFrais",
            SharpGraphWidgetDataSet::make($dataNDF)
                ->setLabel("")
                ->setColor("green")
        );

        $this->addGraphDataSet(
            "facturettes",
            SharpGraphWidgetDataSet::make($dataLDF)
                ->setLabel("")
                ->setColor("grey")
        );


        //Récupération des informations de version
        $check = new CheckLastVersion();
        $this->setPanelData(
            "upgradeMessage",
            [
                "localVersion" => config('app.version'),
                "localGitVersion" => $check->getLocalGitRevision(),
                "stableVersion" => $check->getPublicLastStableVersion(),
            ]
        );

        $u = new User();
        $nbUsers = count($u->getMyUsers());
        $e = new Entreprise();
        $nbEntrep = count($e->getMyEntreprises());
        $this->setOrderedListData(
            "statistiquesChiffres",
            [
                [
                    "label" => "Nombre d'entreprises",
                    "count" => $nbEntrep,
                ],
                [
                    "label" => "Nombre d'utilisateurs",
                    "count" => $nbUsers,
                ],
            ]
        );
    }
}
