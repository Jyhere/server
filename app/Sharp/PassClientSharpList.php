<?php
/*
 * PassClientSharpList.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Sharp;

use App\Passport\PassClient;
use Illuminate\Support\Facades\Auth;
use Code16\Sharp\EntityList\SharpEntityList;
use Code16\Sharp\EntityList\EntityListQueryParams;
use Code16\Sharp\EntityList\Containers\EntityListDataContainer;

class PassClientSharpList extends SharpEntityList
{
    /**
     * Build list containers using ->addDataContainer()
     *
     * @return void
     */
    public function buildListDataContainers()
    {
        $this->addDataContainer(
            EntityListDataContainer::make('id')
                ->setLabel('ID')
                ->setSortable()
        )->addDataContainer(
            EntityListDataContainer::make('name')
                ->setLabel('Nom')
                ->setSortable()
        )->addDataContainer(
            EntityListDataContainer::make('revoked')
                ->setLabel('Révoqué')
                ->setSortable()
        )->addDataContainer(
            EntityListDataContainer::make('created_at')
                ->setLabel('Créé le')
                ->setSortable()
        );
    }

    /**
     * Build list layout using ->addColumn()
     *
     * @return void
     */

    public function buildListLayout()
    {
        $this->addColumn('name', 4)
            // ->addColumn('provider', 1)
            ->addColumn('revoked', 4)
            ->addColumn('created_at', 4);
        // ->addColumn('updated_at', 3);
    }

    /**
     * Build list config
     *
     * @return void
     */
    public function buildListConfig()
    {
        $this->setInstanceIdAttribute('id')
            ->setSearchable()
            ->setDefaultSort('name', 'desc')
            ->setPaginated();
    }

    /**
     * Retrieve all rows data as array.
     *
     * @param EntityListQueryParams $params
     * @return array
     */
    public function getListData(EntityListQueryParams $params)
    {
        $clients = PassClient::whereUserId(Auth::id())->orderBy($params->sortedBy(), $params->sortedDir());

        // Recherche sur le nom
        collect($params->searchWords())
            ->each(function ($word) use ($clients) {
                $clients->where(function ($query) use ($word) {
                    $query->orWhere('name', 'like', $word);
                });
            });

        // collect($params->setDefaultSort('name', 'asc'));
        return $this->setCustomTransformer(
            "created_at",
            function ($label, $client) {
                return $client->created_at ? date_format($client->created_at, 'd/m/Y à H:i:s') : '';
            }
        )->setCustomTransformer(
            "revoked",
            function ($label, $client) {
                return $client->revoked ? 'Oui' : 'Non';
            }
        )->transform($clients->paginate(12));
    }
}
