<?php

namespace App\Sharp;

use App\Entreprise;
use App\Billing;
use App\Http\Controllers\BillingController;
use Code16\Sharp\EntityList\Containers\EntityListDataContainer;
use Code16\Sharp\EntityList\EntityListQueryParams;
use Code16\Sharp\EntityList\SharpEntityList;
use App\Sharp\Commands\UserShowNdfCommand;

class BillingSharpList extends SharpEntityList
{
    /**
     * Build list containers using ->addDataContainer()
     *
     * @return void
     */
    public function buildListDataContainers()
    {
        $this->addDataContainer(
            EntityListDataContainer::make("name")
                ->setLabel("Entreprise")
                ->setSortable()
        )->addDataContainer(
            EntityListDataContainer::make("price")
                ->setLabel("P.U.")
                ->setSortable()
        )->addDataContainer(
            EntityListDataContainer::make("nbu")
                ->setLabel("Nb.U.")
                ->setSortable()
        )->addDataContainer(
            EntityListDataContainer::make("billingto")
                ->setLabel("Payeur")
                ->setSortable()
        )->addDataContainer(
            EntityListDataContainer::make("start")
                ->setLabel("Depuis")
                ->setSortable()
        );
    }

    /**
     * Build list layout using ->addColumn()
     *
     * @return void
     */

    public function buildListLayout()
    {
        $this->addColumn('name', 3)
            ->addColumn("price", 1)
            ->addColumn("nbu", 1)
            ->addColumn("billingto", 2)
            ->addColumn("start", 1)
            ->addColumn("comments", 4);
    }

    /**
     * Build list config
     *
     * @return void
     */
    public function buildListConfig()
    {
        $this->setInstanceIdAttribute('id')
            ->setSearchable()
            ->setDefaultSort('name', 'asc')
            ->setPaginated();
        //            ->addInstanceCommand("show_users", UserShowNdfCommand::class);
    }

    /**
     * Retrieve all rows data as array.
     *
     * @param EntityListQueryParams $params
     * @return array
     */
    public function getListData(EntityListQueryParams $params)
    {
        $bills = Billing::all();

        return $this->setCustomTransformer("nbu", function ($name, $bill) {
            $l = BillingController::getBillableUsers($bill->entreprise_id);
            return count($l);
        })->setCustomTransformer("name", function ($name, $bill) {
            return Entreprise::findOrFail($bill->entreprise_id)->name;
        })->setCustomTransformer("billingto", function ($name, $bill) {
            if (isset($bill->billingto_id) && ($bill->billingto_id > 0)) {
                return Entreprise::findOrFail($bill->billingto_id)->name;
            }
        })->transform($bills);
    }
}
