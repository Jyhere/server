<?php
/*
 * DefaultAccountsSettingsSharpShow.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Sharp;

use App\Sharp\Commands\AccountUpdateName;
use App\Sharp\States\AccountStatusState;
use App\User;
use Code16\Sharp\Show\Fields\SharpShowTextField;
use Code16\Sharp\Show\Layout\ShowLayoutColumn;
use Code16\Sharp\Show\Layout\ShowLayoutSection;
use Code16\Sharp\Show\SharpSingleShow;
use Code16\Sharp\Form\Fields\SharpFormTextField;
use Illuminate\Support\Facades\Log;

class DefaultAccountsSettingsSharpShow extends SharpSingleShow
{
    function buildShowFields()
    {
        $this->addField(
            SharpFormTextField::make('compta_ik')
                ->setLabel('Indemnités kilométriques')
                ->setPlaceholder("625100")
        )->addField(
            SharpFormTextField::make('compta_peage')
                ->setLabel('Péages & Parking')
        )->addField(
            SharpFormTextField::make('compta_train')
                ->setLabel('Transports (Train/Avion...)')

        )->addField(
            SharpFormTextField::make('compta_hotel')
                ->setLabel('Hébergement (hôtel)')
        )->addField(
            SharpFormTextField::make('compta_taxi')
                ->setLabel('Taxi')
        )->addField(
            SharpFormTextField::make('compta_restauration')
                ->setLabel('Restauration')
        )->addField(
            SharpFormTextField::make('compta_divers')
                ->setLabel('Frais divers')

        )->addField(
            SharpFormTextField::make('compta_compteperso')
                ->setLabel('Compte personnel')
        )->addField(
            SharpFormTextField::make('compta_compteprocb')
                ->setLabel('Compte pro CB')
        )->addField(
            SharpFormTextField::make('compta_compteproesp')
                ->setLabel('Compte pro Especes')

        )->addField(
            SharpFormTextField::make('compta_carburant0recup')
                ->setLabel('Carburant zéro récup. tva')
        )->addField(
            SharpFormTextField::make('compta_carburant80recup')
                ->setLabel('Carburant 80% récup. tva')
        )->addField(
            SharpFormTextField::make('compta_carburant100recup')
                ->setLabel('Carburant 100% récup. tva')

        )->addField(
            SharpFormTextField::make('compta_tvadeductible')
                ->setLabel('Compte TVA déductible')
        )->addField(
            SharpFormTextField::make('send_copy_to')
                ->setLabel('Envoyer les mails en copie à')
        )->addField(
            SharpFormTextField::make('compta_email')
                ->setLabel('Adresse mail de votre service comptable')
        )->addField(
            SharpFormTextField::make('compta_code_dossier')
                ->setLabel('Code comptable de votre dossier')
        )->addField(
            SharpFormTextField::make('compta_tva_tx1')
                ->setLabel('Taux 1')
        )->addField(
            SharpFormTextField::make('compta_tva_tx2')
                ->setLabel('Taux 2')
        )->addField(
            SharpFormTextField::make('compta_tva_tx3')
                ->setLabel('Taux 3')
        )->addField(
            SharpFormTextField::make('compta_tva_tx4')
                ->setLabel('Taux 4')
        )->addField(
            SharpShowTextField::make("tvalabel")
                ->setLabel("Modification des taux de TVA")
        );
        // ->addField(
        //     SharpFormTextField::make('compta_carburant60recup')
        //         ->setLabel('Carburant 60% récup. tva')
        // )
    }

    function buildShowLayout()
    {
        $this->addSection('Paramétrage général', function (ShowLayoutSection $section) {
            $section->addColumn(12, function (ShowLayoutColumn $column) {
                $column->withSingleField("compta_code_dossier");
            });
        });
        $this->addSection('Configuration des comptes comptables à affecter par défaut à tout nouvel utilisateur', function (ShowLayoutSection $section) {
            $section
                ->addColumn(12, function (ShowLayoutColumn $column) {
                    $column->withFields('send_copy_to|12')
                        ->withFields('compta_email|12')
                        ->withFields('compta_ik|3', 'compta_peage|3', 'compta_train|3', 'compta_hotel|3')
                        ->withFields('compta_taxi|3', 'compta_restauration|3', 'compta_divers|3', 'compta_tvadeductible|3')
                        ->withFields('compta_carburant0recup|3', 'compta_carburant80recup|3', 'compta_carburant100recup|3')
                        ->withFields('compta_compteperso|3', 'compta_compteprocb|3', 'compta_compteproesp|3');
                });
            //'compta_carburant60recup|3',
        });
        $this->addSection('Configuration de la TVA', function (ShowLayoutSection $section) {
            $section->addColumn(12, function (ShowLayoutColumn $column) {
                $column->withSingleField("tvalabel");
            });

            $section->addColumn(12, function (ShowLayoutColumn $column) {
                $column
                    ->withFields('compta_tva_tx1|3', 'compta_tva_tx2|3', 'compta_tva_tx3|3', 'compta_tva_tx4|3');
            });
        });
    }

    /**
     * @throws \Code16\Sharp\Exceptions\SharpException
     */
    function buildShowConfig()
    {
    }

    function findSingle(): array
    {
        $u = $this->setCustomTransformer(
            'tvalabel',
            function ($value, $user) {
                return "Vous pouvez modifier les taux de TVA utilisés par DoliSCAN pour les adapter à vos spécificités locales. Note: Les modifications seront appliquées aux prochains frais scannés, cette modification n'est PAS rétroactive.";
            }
        )->transform(User::findOrFail(auth()->id()));
        Log::debug("DefaultAccountsSettingsSharpForm : findSingle " . json_encode($u));
        // Log::debug("User::findSingle " . json_encode($u));
        return $this->transform($u);
    }
}
