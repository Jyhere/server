<?php

namespace App\Sharp;

use App\TagsFrais;
use Code16\Sharp\EntityList\Containers\EntityListDataContainer;
use Code16\Sharp\EntityList\EntityListQueryParams;
use Code16\Sharp\EntityList\SharpEntityList;
use App\Sharp\Filters\UsersEntrepriseFilter;
use Code16\Sharp\Http\WithSharpContext;
use App\Sharp\Commands\UserShowNdfCommand;
use Illuminate\Support\Facades\Log;
use App\Entreprise;

class TagsFraisSharpList extends SharpEntityList
{
    /**
     * Build list containers using ->addDataContainer()
     *
     * @return void
     */
    public function buildListDataContainers()
    {
        $this->addDataContainer(
            EntityListDataContainer::make('code')
                ->setLabel('Code')
                ->setSortable()
                ->setHtml()
        )->addDataContainer(
            EntityListDataContainer::make('label')
                ->setLabel('Titre')
                ->setSortable()
                ->setHtml()
        )->addDataContainer(
            EntityListDataContainer::make('created_at')
                ->setLabel('Date de création')
                ->setSortable()
                ->setHtml()
        )->addDataContainer(
            EntityListDataContainer::make('start')
                ->setLabel('Valable du')
                ->setSortable()
                ->setHtml()
        )->addDataContainer(
            EntityListDataContainer::make('end')
                ->setLabel('Au')
                ->setSortable()
                ->setHtml()
        )->addDataContainer(
            EntityListDataContainer::make('user')
                ->setLabel('Utilisateur')
                ->setHtml()
        )->addDataContainer(
            EntityListDataContainer::make('entreprise')
                ->setLabel('Partagé avec')
                ->setHtml()
        );
    }

    /**
     * Build list layout using ->addColumn()
     *
     * @return void
     */

    public function buildListLayout()
    {
        $this->addColumn('code', 2);
        $this->addColumn('label', 4);
        $this->addColumn('created_at', 2);
        $this->addColumn('user', 2);
        $this->addColumn('entreprise', 2);
    }

    /**
     * Build list config
     *
     * @return void
     */
    public function buildListConfig()
    {
        $this->setInstanceIdAttribute('id')
            ->setSearchable()
            ->setDefaultSort('label', 'asc')
            ->setPaginated()

            ->addFilter("entreprise", UsersEntrepriseFilter::class)
            ->addInstanceCommand("show_ndf", UserShowNdfCommand::class);
    }

    /**
     * Retrieve all rows data as array.
     *
     * @param EntityListQueryParams $params
     * @return array
     */
    public function getListData(EntityListQueryParams $params)
    {
        $tags  = sharp_user()->getMyTags($params->sortedBy(), $params->sortedDir(), false);


        collect($params->searchWords())
            ->each(function ($word) use ($tags) {
                $tags->where(function ($query) use ($word) {
                    $query->orWhere('label', 'like', $word);
                });
            });

        if ($params->filterFor("entreprise")) {
            Log::debug("on filtre sur l'entreprise " . $params->filterFor("entreprise"));
            $tags->where("entreprise_id", $params->filterFor("entreprise"));
        }

        // Log::debug(" =================================== ");
        // Log::debug($tags->toSql());
        // Log::debug(" =================================== ");


        return $this
            ->setCustomTransformer("created_at", function ($a, $b) {
                // Log::debug(" =================================== ");
                // Log::debug(" ici ");
                // Log::debug(json_encode($a));
                // Log::debug(json_encode($b));
                // Log::debug(" =================================== ");
                if ($b->created_at != null) {
                    return $b->created_at->format("Y-m-d");
                } else {
                    return "-";
                }
            })
            ->setCustomTransformer("user", function ($a, $b) {
                return $b->user->full_name;
            })
            ->setCustomTransformer("entreprise", function ($a, $b) {
                if ($b->entreprise != null)
                    return $b->entreprise->name;
                else
                    return "-";
            })
            ->transform(
                $tags->paginate(30)
            );
    }
}
