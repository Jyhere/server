<?php
/*
 * LdeFraisSharpFormatter.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Sharp\Formatters;

use Code16\Sharp\Form\Fields\Formatters\SharpFieldFormatter;
use Code16\Sharp\Form\Fields\SharpFormField;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Config;

class LdeFraisSharpFormatter extends SharpFieldFormatter
{
    private $_tabTVAUtilisee = array();

    /**
     * @param SharpFormField $field
     * @param $value
     * @return mixed
     */
    function toFront(SharpFormField $field, $value)
    {
        // Log::debug("toFront " . serialize($field) . " : value=" . serialize($value));
        //On ajoute du texte
        $txt = "";
        //Si jamais on a plusieurs fois la même valeur on passe alors aux données par défaut du compte
        if (in_array($value, $this->_tabTVAUtilisee)) {
            $j = count($this->_tabTVAUtilisee);
            $defaultUserTvaTx = "compta_tva_tx" . $j;
            $value = sharp_user()->{$defaultUserTvaTx};
        }
        array_push($this->_tabTVAUtilisee, $value);
        if ($value !== null) {
            $txt = "Montant de la TVA à $value % : ";
        } else {
            // $txt = "";
            $k = $field->key;
            //On initialise les taux aux valeurs actuelles voir le fichier config/constants.php
            //Note: Le système récupérèra tout seul les taux lors du fromFront ...
            // note 2021 -> on bascule sur les taux configurables par les utilisateurs
            if ($k == "tvaTx1") {
                // $txt = "Montant de la TVA à " . Config::get('constants.tva.tvaTx1') . " % :";
                $txt = "Montant de la TVA à " . sharp_user()->compta_tva_tx1 . " % :";
            } elseif ($k == "tvaTx2") {
                // $txt = "Montant de la TVA à " . Config::get('constants.tva.tvaTx2') . " % :";
                $txt = "Montant de la TVA à " . sharp_user()->compta_tva_tx2 . " % :";
            } elseif ($k == "tvaTx3") {
                // $txt = "Montant de la TVA à " . Config::get('constants.tva.tvaTx3') . " % :";
                $txt = "Montant de la TVA à " . sharp_user()->compta_tva_tx3 . " % :";
            } elseif ($k == "tvaTx4") {
                // $txt = "Montant de la TVA à " . Config::get('constants.tva.tvaTx4') . " % :";
                $txt = "Montant de la TVA à " . sharp_user()->compta_tva_tx4 . " % :";
            }
        }
        return $txt;
    }

    /**
     * @param SharpFormField $field
     * @param string $attribute
     * @param $value
     * @return mixed
     */
    function fromFront(SharpFormField $field, string $attribute, $value)
    {
        //On supprime le texte autour de la valeur
        $txt = preg_replace("/Montant de la TVA à (.*) % :/", "$1", $value);
        return $txt;
    }
}
