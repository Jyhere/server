<?php
/*
 * LdeFraisTagsSharpFormatter.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Sharp\Formatters;

use App\TagsFrais;
use Code16\Sharp\Form\Fields\Formatters\SharpFieldFormatter;
use Code16\Sharp\Form\Fields\SharpFormField;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Config;

class LdeFraisTagsSharpFormatter extends SharpFieldFormatter
{

    /**
     * @param SharpFormField $field
     * @param $value
     * @return mixed
     */
    function toFront(SharpFormField $field, $value)
    {
        // Log::debug("LdeFraisTagsSharpFormatter toFront " . serialize($field) . " : value=" . serialize($value));
        return $value;
    }

    /**
     * @param SharpFormField $field
     * @param string $attribute
     * @param $value
     * @return mixed
     */
    function fromFront(SharpFormField $field, string $attribute, $value)
    {
        // Log::debug("LdeFraisTagsSharpFormatter fromFront value=" . json_encode($value));
        $tabR = array();
        //[{"id":null,"label":"Nouvelle"}]
        foreach ($value as $val) {
            $id = $val['id'];
            //Creation d'une nouvelle étiquette
            if (is_null($id)) {
                $t = TagsFrais::create($val);
                $val['id'] = $t->id;
                // Log::debug("LdeFraisTagsSharpFormatter creation d'un nouveau tag id=" . $t->id);
                // $value[$val['id']] = $t->id;
            }
            $tabR[] = $val;
        }
        // Log::debug("LdeFraisTagsSharpFormatter fromFront value=" . json_encode($tabR));
        //creation des tags a la vollée
        return $tabR;
    }
}
