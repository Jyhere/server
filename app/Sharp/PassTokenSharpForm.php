<?php
/*
 * PassTokenSharpForm.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Sharp;

use App\Passport\PassToken;
use App\Passport\PassClient;
use Illuminate\Validation\Rule;
use Code16\Sharp\Form\SharpForm;
use Illuminate\Support\Facades\Validator;
use Code16\Sharp\Form\Layout\FormLayoutColumn;
use Code16\Sharp\Form\Fields\SharpFormListField;
use Code16\Sharp\Form\Fields\SharpFormTextField;
use App\Sharp\Formatters\TimestampSharpFormatter;
use Code16\Sharp\Form\Fields\SharpFormCheckField;
use Code16\Sharp\Form\Fields\SharpFormSelectField;
use Code16\Sharp\Form\Fields\SharpFormTextareaField;
use Code16\Sharp\Form\Eloquent\WithSharpFormEloquentUpdater;
use Code16\Sharp\Form\Fields\SharpFormAutocompleteField;
use Code16\Sharp\Http\WithSharpContext;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Ramsey\Uuid\Uuid;
use Illuminate\Http\Client\Request;
use Carbon\Carbon;

class PassTokenSharpForm extends SharpForm
{
    use WithSharpFormEloquentUpdater, WithSharpContext;

    /**
     * Retrieve a Model for the form and pack all its data as JSON.
     *
     * @param $id
     * @return array
     */
    public function find($id): array
    {
        Log::debug("******* PassTokenSharpForm find $id ************");
        $token = PassToken::findOrFail($id);
        $client = PassClient::findOrFail($token->client_id);
        $token->client = $client->name;

        //Uniquement si on en a pas déjà un ...
        // if (!empty($id)) {
        //     $guzzle = new \GuzzleHttp\Client;
        //     $response = $guzzle->post('http://localhost/oauth/token', [
        //         'form_params' => [
        //             'grant_type' => 'client_credentials',
        //             'client_id' => $client->id,
        //             'client_secret' => $client->secret,
        //             'scope' => '',
        //         ],
        //     ]);
        //     $token->cle = json_decode((string) $response->getBody(), true)['access_token'];
        //     Log::debug($response->getBody());
        // }
        return $this->transform(
            $token
        );
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed the instance id
     */
    public function update($id, array $data)
    {
        Log::debug("******* PassTokenSharpForm update $id ************");
        Log::debug(\json_encode($data));
        $ignore = ['client'];
        //update est aussi en phase de creation
        $token = $id ? PassToken::findOrFail($id) : new PassToken;
        $cle = "";
        //Si on est en phase de creation c'est pas tout a fait un update
        // if ($this->context()->isCreation()) {
        if (empty($id)) {
            Log::debug("******* PassTokenSharpForm update create ************");

            $clientId = $data['client_id'];
            $client = PassClient::findOrFail($clientId);
            $guzzle = new \GuzzleHttp\Client;

            $response = $guzzle->post(route('passport.token'), [
                'form_params' => [
                    'grant_type' => 'client_credentials',
                    'client_id' => $data['client_id'],
                    'client_secret' => $client->secret,
                    'scope' => '',
                ],
                'headers' => [ 'User-Agent' => 'DoliSCAN/' . config('app.domain') ]
            ]);
            $res = json_decode((string) $response->getBody(), true);
            $cle = $res['access_token'];
            Log::debug($response->getBody());

            $token = PassToken::where('client_id', $clientId)->first();
            $token->user_id = \sharp_user()->id;
            $token->name = $data['name'];

            $data['created_at'] = Carbon::now();
            $data['updated_at'] = $token->updated_at;
            $data['expires_at'] = $token->expires_at;
        }
        // À chaque modification, on vérifie qu'il y a au maximum un token actif en lien avec l'application
        // $rules = [
        //     'revoked' => [
        //         'boolean',
        //         Rule::unique('oauth_access_tokens')->where(function ($query) use ($token) {
        //             return $query->whereClientId($token->client_id)->where('revoked', 0);
        //         })->ignore($token)
        //     ]
        // ];
        // $messages = ['revoked.unique' => "Un jeton actif existe déjà pour cette application."];
        // Validator::make($data, $rules, $messages)->validate();

        //On ignore les champs qu'on ne peut mettre à jour
        Log::debug("******* PassTokenSharpForm update end ************");
        if ($cle != "") {
            $message = "<p style='font-style:italic;'>Vous en aurez besoin et il n'y a aucun moyen pour vous le re-communiquer si besoin ... vous devrez le supprimer et en générer un nouveau le cas échéant</p>";
            $this->notify("IMPORTANT: Copiez ce Jeton et sauvegardez-le")
                ->setDetail($message . "<p>" . $cle . "</p>")
                ->setLevelSuccess()
                ->setAutoHide(false);

            Log::debug("******* token debug before saving... ************");
            Log::debug(\json_encode($token));
        }

        $this->ignore($ignore)->save($token, $data);
    }

    // function create(): array
    // {
    //     Log::debug("******* PassTokenSharpForm CREATE ************");
    // $t = new PassToken();
    // $t->code = Str::random(40);
    // Log::debug($this->transform($t));
    // return $this->transform($t);
    // }



    /**
     * @param $id
     */
    public function delete($id)
    {
        PassToken::findOrFail($id)->delete();
    }

    /**
     * Build form fields using ->addField()
     *
     * @return void
     */
    public function buildFormFields()
    {
        $timestampFormatter = new TimestampSharpFormatter;
        $this->addField(
            SharpFormTextField::make('name')
                ->setLabel('Nom')
        )->addField(
            SharpFormAutocompleteField::make("client_id", "local")
                ->setLabel("Application")
                ->setLocalSearchKeys(["name"])
                ->setLocalValues(PassClient::getMyClients('name'))
                ->setListItemInlineTemplate("{{name}}")
                ->setResultItemInlineTemplate("{{name}}")
        )->addField(
            SharpFormTextareaField::make('scopes')
                ->setLabel('Portées')
                ->setReadOnly()
                ->setRowCount(3)
        )->addField(
            SharpFormCheckField::make('revoked', 'Révoqué')
                ->setLabel('Révoqué')
        )->addField(
            SharpFormTextField::make('expires_at')
                ->setLabel('Expire le')
                ->setFormatter($timestampFormatter)
                ->setReadOnly()
        )->addField(
            SharpFormTextField::make('created_at')
                ->setLabel('Créé le')
                ->setFormatter($timestampFormatter)
                ->setReadOnly()
        )->addField(
            SharpFormTextField::make('last_connexion_at')
                ->setLabel('Dernière connexion le')
                ->setFormatter($timestampFormatter)
                ->setReadOnly()
        );
    }

    /**
     * Build form layout using ->addTab() or ->addColumn()
     *
     * @return void
     */
    public function buildFormLayout()
    {
        $this->addColumn(6, function (FormLayoutColumn $column) {
            $column->withSingleField('name');
        })->addColumn(6, function (FormLayoutColumn $column) {
            $column->withSingleField('client_id');
            // })->addColumn(6, function(FormLayoutColumn $column) {
            // $column->withSingleField('scopes');
        })->addColumn(6, function (FormLayoutColumn $column) {
            $column->withSingleField('revoked');
        })->addColumn(6, function (FormLayoutColumn $column) {
            $column->withSingleField('created_at');
        })->addColumn(6, function (FormLayoutColumn $column) {
            $column->withSingleField('expires_at');
        })->addColumn(6, function (FormLayoutColumn $column) {
            $column->withSingleField('last_connexion_at');
        });
    }
}
