<?php
/*
 * EntrepriseSharpList.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Sharp;

use App\Entreprise;
use App\User;
use Code16\Sharp\EntityList\SharpEntityList;
use Code16\Sharp\EntityList\EntityListQueryParams;
use Code16\Sharp\EntityList\Containers\EntityListDataContainer;
use Illuminate\Support\Facades\Log;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\DB;
use App\Sharp\Commands\CdeFraisExportPDF;
use App\Sharp\Commands\CdeFraisSendMail;

class EntrepriseSharpList extends SharpEntityList
{
    /**
     * Build list containers using ->addDataContainer()
     *
     * @return void
     */
    public function buildListDataContainers()
    {
        // Description du modèle
        $this->addDataContainer(
            EntityListDataContainer::make("name")
                ->setLabel("Entreprise")
                ->setSortable()
        )->addDataContainer(
            EntityListDataContainer::make("adresse")
                ->setLabel("Adresse")
                ->setSortable()
        )->addDataContainer(
            EntityListDataContainer::make("email")
                ->setLabel("Mail")
                ->setSortable()
        )->addDataContainer(
            EntityListDataContainer::make("web")
                ->setLabel("Web")
                ->setSortable()
        )->addDataContainer(
            EntityListDataContainer::make("tel")
                ->setLabel("Téléphone")
                ->setSortable()
        )->addDataContainer(
            EntityListDataContainer::make("users")
                ->setLabel("NbU")
        );
    }

    /**
     * Build list layout using ->addColumn()
     *
     * @return void
     */
    public function buildListLayout()
    {
        // Affichage
        $this->addColumn("name", 3)
            ->addColumn("adresse", 4)
            ->addColumn("email", 3)
            ->addColumn("users", 1)
            ->addColumn("actions", 1);
    }

    /**
     * Build list config
     *
     * @return void
     */
    public function buildListConfig()
    {
        //
        $this->setInstanceIdAttribute('id')
            ->setSearchable()
            ->setDefaultSort('name', 'asc')
            ->setPaginated()
            ->addInstanceCommand("export_global_ndf", CdeFraisExportPDF::class)
            ->addInstanceCommand("envoyer_mail_global_ndf", CdeFraisSendMail::class);
    }

    /**
     * Retrieve all rows data as array.
     *
     * @param EntityListQueryParams $params
     * @return array
     */
    public function getListData(EntityListQueryParams $params)
    {
        $entreprises = sharp_user()->getEntreprises($params->sortedBy(), $params->sortedDir(), false);

        collect($params->searchWords())
            ->each(function ($word) use ($entreprises) {
                $entreprises->where(function ($query) use ($word) {
                    $query->orWhere('name', 'like', $word)
                    ->orWhere('adresse', 'like', $word)
                    ->orWhere('cp', 'like', $word)
                    ->orWhere('ville', 'like', $word)
                    ->orWhere('email', 'like', $word);
                });
            });


        return $this->setCustomTransformer("users", function ($users, $entreprise) {
            return $entreprise->users()->count();
        })->setCustomTransformer("adresse", function ($adresse, $entreprise) {
            return $entreprise->adresse . "<br />" . $entreprise->cp . " " . $entreprise->ville;
        })->transform(
            $entreprises->paginate(30)
        );
    }
}
