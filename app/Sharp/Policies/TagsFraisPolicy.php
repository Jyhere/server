<?php
/**
 * TagsFraisPolicy.php
 *
 * Copyright (c) 2021 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Sharp\Policies;

use App\TagsFrais;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TagsFraisPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can access DocDummyPluralModel.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function entity(User $user)
    {
        //
        return true;
    }

    /**
     * Determine whether the user can view the DocTagsFrais.
     *
     * @param  \App\User  $user
     * @param  int $tagsFraisId
     * @return mixed
     */
    public function view(User $user, $tagsFraisId)
    {
        //
        return true;
    }

    /**
     * Determine whether the user can update the DocTagsFrais.
     *
     * @param  \App\User  $user
     * @param  int $tagsFraisId
     * @return mixed
     */
    public function update(User $user, $tagsFraisId)
    {
        //
        return true;
    }

    /**
     * Determine whether the user can delete the DocTagsFrais.
     *
     * @param  \App\User  $user
     * @param  int $tagsFraisId
     * @return mixed
     */
    public function delete(User $user, $tagsFraisId)
    {
        return true;
        //
    }

    /**
     * Determine whether the user can create DocDummyPluralModel.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
        //
    }
}
