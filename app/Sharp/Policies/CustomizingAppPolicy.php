<?php
/*
 * CustomizingAppPolicy.php
 *
 * Copyright (c) 2021 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Sharp\Policies;

use App\User;
use App\CustomizingApp;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class CustomizingAppPolicy
{

    public function entity()
    {
        //Super Admin peut tout faire :)
        if (sharp_user()->hasRole('superAdmin')) {
            return true;
        }
        if (sharp_user()->hasRole('adminRevendeur')) {
            return true;
        }
        return false;
    }

    public function view()
    {
        //Super Admin peut tout faire :)
        if (sharp_user()->hasRole('superAdmin')) {
            return true;
        }
        if (sharp_user()->hasRole('adminRevendeur')) {
            return true;
        }
        return false;
    }

    public function update()
    {
        //Super Admin peut tout faire :)
        if (sharp_user()->hasRole('superAdmin')) {
            return true;
        }
        if (sharp_user()->hasRole('adminRevendeur')) {
            return true;
        }
        return false;
    }

    public function delete()
    {
        //Super Admin peut tout faire :)
        if (sharp_user()->hasRole('superAdmin')) {
            return true;
        }
        if (sharp_user()->hasRole('adminRevendeur')) {
            return true;
        }
        return false;
    }

    public function create()
    {
        //Super Admin peut tout faire :)
        if (sharp_user()->hasRole('superAdmin')) {
            return true;
        }
        if (sharp_user()->hasRole('adminRevendeur')) {
            //Pas plus d'une customization pour le moment
            $nb = CustomizingApp::where('user_id', \sharp_user()->id)->count();
            if ($nb < 1) {
                return true;
            }
        }
        return false;
    }
}
