<?php
/*
 * EntreprisePolicy.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Sharp\Policies;

use App\User;
use App\Entreprise;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Log;

class EntreprisePolicy
{

    public function entity()
    {
        //A faire a la mano pour l'instant pour definir le super admin ...
        // sharp_user()->assignRole('superAdmin');
        return true; //sharp_user()->hasPermissionTo('show Entreprise');
    }

    public function view(User $u, $e)
    {
        //Super Admin peut tout faire :)
        if (sharp_user()->hasRole('superAdmin')) {
            return true;
        }

        if (sharp_user()->hasPermissionTo('show Entreprise')) {
            //On cherche pour voir si c'est une entreprise de l'utilisateur
            $entreprises_possibles = Entreprise::getMyEntreprises()->pluck('eid')->toArray();

            if (in_array($e, $entreprises_possibles)) {
                //Et si ce profil utilisateur a le droit
                return true;
            }
        }
        return false;
    }

    public function update(User $u, $e)
    {
        //Super Admin peut tout faire :)
        if (sharp_user()->hasRole('superAdmin')) {
            Log::debug("  PolicyUpdate :: on retourne true pour entreprise $e");
            return true;
        }

        //cas particuliers: un responsable d'entreprise a le droit de modifier son entreprise ...
        Log::debug("  PolicyUpdate :: L'utilisateur " . $u->id . " veut modifier l'entreprise " . $e);
        if (sharp_user()->hasPermissionTo('edit Entreprise')) {
            $entreprises_possibles = Entreprise::getMyEntreprises()->pluck('eid')->toArray();
            Log::debug("  PolicyUpdate :: On regarde donc dans " . \json_encode($entreprises_possibles));

            if (in_array($e, $entreprises_possibles)) {
                Log::debug("  PolicyUpdate :: bonne nouvelle, cet utilisateur a acces a cette entreprise $e");
                //de base si on a les droits on fonce
                if (sharp_user()->hasPermissionTo('edit Entreprise')) {
                    Log::debug("  PolicyUpdate :: on retourne true pour entreprise $e");
                    return true;
                }
            }
        }

        //Qui a le droit de modifier une entreprise ? surtout pas un utilisateur lambda
        if (sharp_user()->hasRole('utilisateur')) {
            Log::debug("  PolicyUpdate :: on retourne false pour entreprise $e");
            return false;
        }

        Log::debug("  PolicyUpdate :: on retourne false pour entreprise $e");
        return false;
    }

    public function delete()
    {
        return sharp_user()->hasPermissionTo('delete Entreprise');
    }

    public function create(User $u)
    {
        Log::debug("L'utilisateur " . $u->id . " veut créer une entreprise... ");
        if (sharp_user()->hasPermissionTo('create Entreprise')) {
            Log::debug("  bonne nouvelle il a le droit !");
            return true;
        }
        Log::debug("  mauvaise nouvelle il n'a le droit !");
        return false;
    }
}
