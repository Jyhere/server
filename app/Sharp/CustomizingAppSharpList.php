<?php
/*
 * CustomizingAppSharpList.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Sharp;

use App\CustomizingApp;
use App\User;
use Code16\Sharp\EntityList\SharpEntityList;
use Code16\Sharp\EntityList\EntityListQueryParams;
use Code16\Sharp\EntityList\Containers\EntityListDataContainer;
use Illuminate\Support\Facades\Log;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\DB;
use App\Sharp\Commands\CdeFraisExportPDF;
use App\Sharp\Commands\CdeFraisSendMail;

class CustomizingAppSharpList extends SharpEntityList
{
    /**
     * Build list containers using ->addDataContainer()
     *
     * @return void
     */
    public function buildListDataContainers()
    {
        // Description du modèle
        $this->addDataContainer(
            EntityListDataContainer::make("css")
                ->setLabel("css")
        )->addDataContainer(
            EntityListDataContainer::make("logo")
                ->setLabel("logo")
        );
    }

    /**
     * Build list layout using ->addColumn()
     *
     * @return void
     */
    public function buildListLayout()
    {
        // Affichage
        $this->addColumn("css", 3);
        $this->addColumn("logo", 3);
    }

    /**
     * Build list config
     *
     * @return void
     */
    public function buildListConfig()
    {
        //
        $this->setInstanceIdAttribute('id')
            ->setSearchable()
            ->setDefaultSort('css', 'asc')
            ->setPaginated();
    }

    /**
     * Retrieve all rows data as array.
     *
     * @param EntityListQueryParams $params
     * @return array
     */
    public function getListData(EntityListQueryParams $params)
    {
        $c = CustomizingApp::where('user_id', '=', auth()->id());

        if (is_null($c))
            $c = new CustomizingApp();

        return $this->transform(
            $c->paginate(30)
        );
    }
}
