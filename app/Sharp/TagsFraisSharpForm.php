<?php

namespace App\Sharp;

use App\TagsFrais;
use Code16\Sharp\Form\Eloquent\WithSharpFormEloquentUpdater;
use Code16\Sharp\Form\Fields\SharpFormTextField;
use Code16\Sharp\Form\Fields\SharpFormDateField;
use Code16\Sharp\Form\Fields\SharpFormSelectField;
use Code16\Sharp\Form\Layout\FormLayoutColumn;
use Code16\Sharp\Form\SharpForm;
use Code16\Sharp\Http\WithSharpContext;
use App\Entreprise;
use Illuminate\Support\Facades\Log;

class TagsFraisSharpForm extends SharpForm
{
    use WithSharpFormEloquentUpdater, WithSharpContext;

    /**
     * Retrieve a Model for the form and pack all its data as JSON.
     *
     * @param $id
     * @return array
     */
    public function find($id): array
    {
        return $this->transform(
            TagsFrais::findOrFail($id)
        );
    }

    /**
     * @param $id
     * @param array $data
     * @return mixed the instance id
     */
    public function update($id, array $data)
    {
        $tagsFrais = $id ? TagsFrais::findOrFail($id) : new TagsFrais;

        if ($this->context()->isCreation()) {
            $tagsFrais->user_id = sharp_user()->id;
        }


        $this->save($tagsFrais, $data);
    }

    /**
     * @param $id
     */
    public function delete($id)
    {
        TagsFrais::findOrFail($id)->find($id)->delete();
    }

    /**
     * Build form fields using ->addField()
     *
     * @return void
     */
    public function buildFormFields()
    {
        $this->addField(
            SharpFormTextField::make('code')
                ->setLabel('Code')
        )->addField(
            SharpFormTextField::make('label')
                ->setLabel('Titre')
        )->addField(
            SharpFormDateField::make('start')
                ->setLabel('Valable du')
                ->setHelpMessage("Si votre étiquette a une période de validité (par exemple pour un projet)")
        )->addField(
            SharpFormDateField::make('end')
                ->setLabel('au')
                ->setHelpMessage("Si votre étiquette a une période de validité (par exemple pour un projet)")
        )->addField(
            SharpFormSelectField::make('entreprise_id', Entreprise::getMyEntreprises()->map(function ($entreprise) {
                Log::debug("on est dans " . json_encode($entreprise));
                return [
                    'id' => $entreprise->id,
                    'label' => $entreprise->name,
                ];
            })->all())
                ->setLabel('Partager cette étiquette avec les membres de :')
                ->setClearable(true)
                ->setDisplayAsDropdown()
                ->setHelpMessage("Vous pouvez partager cette étiquette avec les autres membres de votre équipe (exemple pour une mission)")
        );
    }

    /**
     * Build form layout using ->addTab() or ->addColumn()
     *
     * @return void
     */
    public function buildFormLayout()
    {
        $this->addColumn(6, function (FormLayoutColumn $column) {
            $column->withSingleField('code');
        })->addColumn(6, function (FormLayoutColumn $column) {
            $column->withSingleField('label');
        });
        $this->addColumn(6, function (FormLayoutColumn $column) {
            $column->withSingleField('start');
        })->addColumn(6, function (FormLayoutColumn $column) {
            $column->withSingleField('end');
        });
        $this->addColumn(6, function (FormLayoutColumn $column) {
            $column->withSingleField('entreprise_id');
        });
    }
}
