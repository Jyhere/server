<?php

/**
 * UsersEntrepriseFilter.php
 *
 * Copyright (c) 2021 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Sharp\Filters;

use App\Entreprise;
use Code16\Sharp\EntityList\EntityListSelectFilter;
// use Code16\Sharp\EntityList\EntityListMultipleFilter;

class UsersEntrepriseFilter implements EntityListSelectFilter
{
    /**
     * @return array
     */
    public function values()
    {
        return Entreprise::getMyEntreprises("name")
            ->pluck("name", "id")
            ->all();
    }

    public function defaultValue()
    {
        return Entreprise::getMyEntreprises("name")->first()->id;
    }

    public function label()
    {
        return "Entreprise";
    }

    public function retainValueInSession()
    {
        return true;
    }
}
