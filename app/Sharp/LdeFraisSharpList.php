<?php
/*
 * LdeFraisSharpList.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Sharp;

use App\User;
use App\NdeFrais;
use App\LdeFrais;
use Spatie\Permission\Models\Role;
use Code16\Sharp\EntityList\SharpEntityList;
use Code16\Sharp\EntityList\EntityListQueryParams;
use Code16\Sharp\EntityList\Containers\EntityListDataContainer;
use Illuminate\Support\Facades\Log;
use Code16\Sharp\Utils\LinkToEntity;
use App\Sharp\Commands\NdeFraisExportPDFCommand;
use App\Sharp\Filters\LdeFraisPeriodFilter;
use App\Sharp\Filters\LdeFraisAmountFilter;
use App\Sharp\Filters\LdeFraisTypeFraisFilter;
use App\Sharp\Filters\LdeFraisTagsFraisFilter;
use App\Sharp\Filters\LdeFraisUserFilter;
use App\Sharp\Filters\LdeFraisMoyenPaiementFilter;
use App\Sharp\Commands\LdeFraisExportCommand;
use App\Sharp\Commands\LdeFraisExportJustifsCommand;

class LdeFraisSharpList extends SharpEntityList
{
    /**
     * Build list containers using ->addDataContainer()
     *
     * @return void
     */
    public function buildListDataContainers()
    {
        $this->addDataContainer(
            EntityListDataContainer::make('ladate')
                ->setLabel('Date')
                ->setSortable()
        )->addDataContainer(
            EntityListDataContainer::make('label')
                ->setLabel('Intitulé')
                ->setSortable()
        )->addDataContainer(
            EntityListDataContainer::make('ttc')
                ->setLabel('TTC')
                ->setSortable()
        )->addDataContainer(
            EntityListDataContainer::make('type_frais_id')
                ->setLabel('Type de frais')
                ->setSortable()
        )->addDataContainer(
            EntityListDataContainer::make('moyen_paiement_id')
                ->setLabel('Moyen de paiement')
                ->setSortable()
        );

        if (sharp_user()->hasPermissionTo('show others LdeFrais')) {
            $this->addDataContainer(
                EntityListDataContainer::make('user:FullName')
                    ->setLabel('Utilisateur')
                    ->setSortable()
            );
        }
    }

    /**
     * Build list layout using ->addColumn()
     *
     * @return void
     */

    public function buildListLayout()
    {
        $this->addColumn('ladate', 1)
            ->addColumn('label', 4)
            ->addColumn('ttc', 1)
            ->addColumn('type_frais_id', 2)
            ->addColumn('moyen_paiement_id', 2);
        if (sharp_user()->hasPermissionTo('show others LdeFrais')) {
            $this->addColumn('user:FullName', 2);
        }
    }

    /**
     * Build list config
     *
     * @return void
     */
    public function buildListConfig()
    {
        $this->setPaginated()
            ->setSearchable()
            ->setInstanceIdAttribute('id')
            ->setDefaultSort('ladate', 'desc');

        $this
            ->addFilter("createdAt", LdeFraisPeriodFilter::class)
            ->addFilter("amount", LdeFraisAmountFilter::class)
            ->addFilter("typeFrais", LdeFraisTypeFraisFilter::class)
            ->addFilter("tagsFrais", LdeFraisTagsFraisFilter::class)
            ->addFilter("moyenPaiement", LdeFraisMoyenPaiementFilter::class);

        if (sharp_user()->hasPermissionTo('show others LdeFrais')) {
            $this->addFilter("user", LdeFraisUserFilter::class);
        }
        $this->addEntityCommand("export_ldeFrais", LdeFraisExportCommand::class)
            ->addEntityCommand("export_ldeFraisJustifs", LdeFraisExportJustifsCommand::class);

        // ->addInstanceCommand("imprimer_ndf", NdeFraisExportPDFCommand::class);
    }

    /**
     * Retrieve all rows data as array.
     *
     * @param EntityListQueryParams $params
     * @return array
     */
    public function getListData(EntityListQueryParams $params)
    {
        // Log::debug("========================");
        // Log::debug($params->filterFor('ldefrais'));
        // Log::debug("========================");
        $userid = \sharp_user()->id;
        if (sharp_user()->hasPermissionTo('show others LdeFrais')) {
            if (session('usertoget') !== null) {
                $userid = session('usertoget');
            } else {
                $userid = User::getMyUsers()->pluck('id')->toArray();
            }
        }
        if (is_array($userid))
            $ldfs = LdeFrais::with("tagsFrais", "user")->whereIn('user_id', $userid);
        else
            $ldfs = LdeFrais::with("tagsFrais", "user")->where('user_id', $userid);

        if ($params->filterFor('nde_frais_id')) {
            $ldfs->where('nde_frais_id', $params->filterFor('nde_frais_id'));
        }
        $ldfs->orderBy($params->sortedBy(), $params->sortedDir());

        collect($params->searchWords())
            ->each(function ($word) use ($ldfs) {
                $ldfs->where(function ($query) use ($word) {
                    $query->orWhere('label', 'like', $word)
                        ->orWhere('ttc', 'like', $word);
                });
            });

        //Filtre sur la période
        if ($range = $params->filterFor("createdAt")) {
            $ldfs->whereBetween(
                "created_at",
                [
                    $range['start'],
                    $range['end']
                ]
            );
        }

        //Filtre sur le type de frais
        if ($params->filterFor("typeFrais") != null) {
            $ldfs->where("type_frais_id", $params->filterFor("typeFrais"));
        }

        //Filtre sur l'étiquette / le tag (multiples)
        if ($params->filterFor("tagsFrais") != null) {
            $ldfs->whereHas('tagsFrais', function ($q) use ($params) {
                $q->where('tags_frais_id', $params->filterFor("tagsFrais"));
            });
        }

        //Filtre sur le moyen de paiement
        if ($params->filterFor("moyenPaiement") != null) {
            $ldfs->where('moyen_paiement_id', $params->filterFor("moyenPaiement"));
        }

        //Filtre sur l'utilisateur
        if ($params->filterFor("user") != null) {
            Log::debug("On filtre sur l'utilisateur : " . $params->filterFor("user"));
            $ldfs->where('user_id', $params->filterFor("user"));
        }


        //Filtre sur le montant
        if ($params->filterFor("amount") != null) {
            $ldfs->where(function ($query) use ($params) {
                $listeFiltres = new LdeFraisAmountFilter();
                $tabValues = $listeFiltres->values();
                if (is_array($params->filterFor("amount"))) {
                    foreach ($params->filterFor("amount") as $rangeid) {
                        $tab = explode(' - ', $tabValues[$rangeid]);
                        Log::debug($tab);
                        $query->OrWhereBetween(
                            "ttc",
                            [
                                $tab[0],
                                $tab[1]
                            ]
                        );
                    }
                } else {
                    $rangeid = $params->filterFor("amount");
                    $tab = explode(' - ', $tabValues[$rangeid]);
                    $query->OrWhereBetween(
                        "ttc",
                        [
                            $tab[0],
                            $tab[1]
                        ]
                    );
                }
            });
        }

        // Log::debug($ldfs->toSql());
        return $this->setCustomTransformer(
            "ttc",
            function ($ttc, $ldeFrais) {
                if ($ttc == 0)
                    return "-";
                else
                    return nbFR($ttc) . "&nbsp;€";
            }
        )->setCustomTransformer(
            "label",
            function ($label, $ldeFrais) {
                return $ldeFrais->getResume();
            }
        )->setCustomTransformer(
            "type_frais_id",
            function ($label, $ldeFrais) {
                return $ldeFrais->getTypeFrais();
            }
        )->setCustomTransformer(
            "moyen_paiement_id",
            function ($label, $ldeFrais) {
                return $ldeFrais->getMoyenPaiement();
            }
        )->transform($ldfs->paginate(50));
        // ->setCustomTransformer("ndeFrais", function($ndfs, $ldfs) {
        //     return $ldfs->NdeFrais->map(function($edit) {
        //         return (new LinkToEntity($ldfs->name, "LdeFrais"))
        //             ->setTooltip("Détails de la note de frais")
        //             ->setSearch($ldfs->name)
        //             ->render();
        //     })->implode("<br>");
        // })
    }
}
