<?php
/*
 * NdeFraisSharpShow.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Sharp;

use App\NdeFrais;
use App\LdeFrais;
use App\User;
use Code16\Sharp\Show\Fields\SharpShowEntityListField;
use Code16\Sharp\Show\Fields\SharpShowTextField;
use Code16\Sharp\Show\Layout\ShowLayoutColumn;
use Code16\Sharp\Show\Layout\ShowLayoutSection;
use Code16\Sharp\Show\SharpShow;
use Code16\Sharp\Utils\Transformers\Attributes\Eloquent\SharpUploadModelThumbnailUrlTransformer;
use Code16\Sharp\Utils\Transformers\Attributes\MarkdownAttributeTransformer;
use Illuminate\Support\Facades\Log;
use Code16\Sharp\Exceptions\Form\SharpApplicativeException;

class NdeFraisSharpShow extends SharpShow
{
    function buildShowFields()
    {
        // Log::debug("NdeFraisSharpShow::buildShowFields");
        $this->addField(
            SharpShowEntityListField::make("ldefraiss", "ldeFrais")
                ->hideFilterWithValue("nde_frais_id", function ($id) {
                    return $id;
                })
                ->showCreateButton(false)
        );
    }

    /**
     * @throws \Code16\Sharp\Exceptions\SharpException
     */
    function buildShowConfig()
    {
        // Log::debug("NdeFraisSharpShow::buildShowConfig");
    }

    function buildShowLayout()
    {
        // Log::debug("NdeFraisSharpShow::buildShowLayout");
        $this->addEntityListSection("ldefraiss");
    }

    function find($id): array
    {
        $userid = -1;
        // Log::debug("  NdeFraisSharpShow::find 01 ($id)");
        //Cas particulier du service comptabilité
        if (sharp_user()->hasPermissionTo('edit others NdeFrais') && (session('usertoget') !== null)) {
            // Log::debug("NdeFraisSharpShow::find on a la perm 'edit others NdeFrais' et on affiche le détail de la NDF ref $id de user : " . session('usertoget'));
            //On vérifie si cet utilisateur est "possible"
            $users_possibles = User::getMyUsers()->pluck('id')->toArray();
            Log::debug("  NdeFraisSharpShow::find 01b : " . json_encode($users_possibles));

            if (\in_array(session('usertoget'), $users_possibles)) {
                // Log::debug("  NdeFraisSharpShow::find 02");
                $userid = session('usertoget');
            } else {
                // Log::debug("  NdeFraisSharpShow::find 02b");
                throw new SharpApplicativeException("Vous n'avez pas le droit d'accéder à cette ressource !");
            }
        } else {
            $userid = sharp_user()->id;
        }
        $ldeFrais = LdeFrais::where('user_id', $userid)->where('nde_frais_id', $id);
        // Log::debug("  NdeFraisSharpShow::find 05 : ($id / $userid) " . json_encode($this->transform($ldeFrais->get())));
        return $this->transform($ldeFrais->get());
    }
}
