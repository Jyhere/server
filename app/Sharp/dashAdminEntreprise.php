<?php
/*
 * dashAdminEntreprise.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Sharp;

use DB;
use Code16\Sharp\Dashboard\SharpDashboard;
use Code16\Sharp\Dashboard\DashboardQueryParams;
use Code16\Sharp\Dashboard\Widgets\SharpLineGraphWidget;
use Code16\Sharp\Dashboard\Widgets\SharpBarGraphWidget;
use Code16\Sharp\Dashboard\Widgets\SharpPanelWidget;

use Code16\Sharp\Dashboard\Widgets\SharpGraphWidgetDataSet;
use Code16\Sharp\Dashboard\Widgets\SharpPieGraphWidget;
use Code16\Sharp\Dashboard\Layout\DashboardLayoutRow;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Auth;

use App\User;
use App\Entreprise;
use Carbon\Carbon;
use App\TypeFrais;

class dashAdminEntreprise extends SharpDashboard
{
    private static $colors = [
        "#f06292", "#ba68c8", "#a1887f", "#ffb74d", "#9575cd", "#7986cb", "#4fc3f7", "#4dd0e1", "#4db6ac", "#91a7ff", "#42bd41", "#aed581", "#dce775", "#fff176", "#ffd54f", "#ff8a65", "#e0e0e0", "#f36c60", "#90a4ae"
    ];

    /**
     * Build dashboard's widget using ->addWidget.
     */
    protected function buildWidgets()
    {
        Log::debug("************buildWidgets*******");
        /*
        ->addWidget(
            SharpPanelWidget::make("welcome")
                ->setInlineTemplate("<h1>Bienvenue !</h1> <div style='text-align: left'><p>Votre profil est ''DSI'', vous pouvez utiliser cette interface pour gérer vos utilisateurs.</p></div>")
        )
        */
        $this->addWidget(
            SharpBarGraphWidget::make("notesDeFrais")
                ->setTitle("Montant des remboursements")
                ->setShowLegend(false)
        )->addWidget(
            SharpLineGraphWidget::make("facturettes")
                ->setTitle("Nombre de justificatifs (facturettes)")
                ->setShowLegend(false)
        )->addWidget(
            SharpPieGraphWidget::make("frais_stats")
                ->setTitle("Répartition des frais (quantité)")
        )->addWidget(
            SharpPieGraphWidget::make("frais_stats_amount")
                ->setTitle("Répartition des frais (montant)")
        );
    }

    /**
     * Build dashboard's widgets layout.
     */
    protected function buildWidgetsLayout()
    {
        Log::debug("************buildWidgetsLayout*******");
        /*
            ->addRow(function (DashboardLayoutRow $row) {
                $row->addWidget(12, "welcome");
*/
        $this
            ->addRow(function (DashboardLayoutRow $row) {
                $row->addWidget(6, "frais_stats")
                    ->addWidget(6, "frais_stats_amount");
            })->addRow(function (DashboardLayoutRow $row) {
                $row->addWidget(6, "notesDeFrais")
                    ->addWidget(6, "facturettes");
            });
    }

    /**
     * Build dashboard's widgets data, using ->addGraphDataSet and ->setPanelData
     *
     * @param DashboardQueryParams $params
     */
    protected function buildWidgetsData(DashboardQueryParams $params)
    {
        Log::debug("**************buildWidgetsData*****");

        $this->setGraphStatsDataSet();
        $this->setGraphStatsDataSetAmount();

        // $this->setPanelData(
        //     "welcome",
        //     ["count" => 10]
        // );

        // Log::debug("**************buildWidgetsData*****");
        //SQLite ne sait pas faire des group by comme on le souhaite (ou je ne sais pas le faire)
        //Alors j'ai bricolé un truc qui génère des données (mais pas les bonnes) pour pouvoir avancer sachant que la
        //prod est sur un MariaDB :)
        if (config('database.default') === 'sqlite') {
            $queryNDF = Auth::user()->NdeFrais()->select(DB::raw("strftime('%Y-%m',created_at) as label, montant as value"));
            $queryLDF = Auth::user()->LdeFrais()->select(DB::raw("strftime('%Y-%m',created_at) as label, count(*) as value"));
            $queryNDF->groupBy(DB::raw('label'));
            $queryLDF->groupBy(DB::raw('label'));
        } else {
            $queryNDF = Auth::user()->NdeFrais()->select(DB::raw("DATE_FORMAT(created_at,'%Y-%m') as label, montant as value"))->groupBy('montant');
            $queryLDF = Auth::user()->LdeFrais()->select(DB::raw("DATE_FORMAT(created_at,'%Y-%m') as label, count(*) as value"));

            $queryNDF->groupBy(DB::raw("DATE_FORMAT(created_at,'%Y-%m')"));
            $queryLDF->groupBy(DB::raw("DATE_FORMAT(created_at,'%Y-%m')"));
        }

        $dataNDF = $queryNDF
            ->where('created_at', '>=', Carbon::now()->submonth(12))
            ->take(12)
            ->orderBy("debut")
            ->get()
            ->pluck("value", "label");

        $dataLDF = $queryLDF
            ->where('created_at', '>=', Carbon::now()->submonth(12))
            ->take(12)
            ->orderBy("created_at")
            ->get()
            ->pluck("value", "label");

        $this->addGraphDataSet(
            "notesDeFrais",
            SharpGraphWidgetDataSet::make($dataNDF)
                ->setLabel("")
                ->setColor("green")
        );

        $this->addGraphDataSet(
            "facturettes",
            SharpGraphWidgetDataSet::make($dataLDF)
                ->setLabel("")
                ->setColor("grey")
        );
    }


    public function setGraphStatsDataSet(): void
    {
        $u = new User();

        $counts = DB::table('lde_frais')
            ->select(DB::raw('type_frais_id, count(*) as count'))
            ->whereIn('user_id', $u->getMyUsers()->pluck('id'))
            ->groupBy('type_frais_id')
            ->get();

        TypeFrais::whereIn("id", $counts->pluck("type_frais_id"))
            ->each(function (TypeFrais $type) use ($counts) {
                $this->addGraphDataSet(
                    "frais_stats",
                    SharpGraphWidgetDataSet::make([
                        $counts->where("type_frais_id", $type->id)->first()->count
                        // 5
                    ])
                        ->setLabel($type->label)
                        ->setColor(static::chooseColor($type->id))
                );
            });
    }

    public function setGraphStatsDataSetAmount(): void
    {
        $u = new User();
        $counts = DB::table('lde_frais')
            ->select(DB::raw('type_frais_id, SUM(ttc) as count'))
            ->whereIn('user_id', $u->getMyUsers()->pluck('id'))
            ->groupBy('type_frais_id')
            ->get();

        TypeFrais::whereIn("id", $counts->pluck("type_frais_id"))
            ->each(function (TypeFrais $type) use ($counts) {
                $this->addGraphDataSet(
                    "frais_stats_amount",
                    SharpGraphWidgetDataSet::make([
                        $counts->where("type_frais_id", $type->id)->first()->count
                    ])
                        ->setLabel($type->label)
                        ->setColor(static::chooseColor($type->id))
                );
            });
    }

    private static function chooseColor($nb): string
    {
        if ($nb >= sizeof(static::$colors)) {
            $nb = 0;
        }

        return static::$colors[$nb];
    }
}
