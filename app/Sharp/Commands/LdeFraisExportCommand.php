<?php

/**
 * LdeFraisExportCommand.php
 *
 * Copyright (c) 2021 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Sharp\Commands;

use App\Entreprise;
use App\NdeFrais;
use App\CdeFrais;
use App\LdeFrais;
use App\User;
use App\Sharp\Filters\LdeFraisAmountFilter;
use BaconQrCode\Writer;
use Illuminate\Support\Facades\Storage;
use Code16\Sharp\EntityList\Commands\EntityCommand;
use Code16\Sharp\EntityList\EntityListQueryParams;
use Illuminate\Support\Facades\Log;
use Code16\Sharp\Form\Fields\SharpFormCheckField;
use Code16\Sharp\Http\WithSharpContext;
use Code16\Sharp\Http\SharpContext;
use Modules\ExportToXLS\Http\Controllers\ExportToXLSController;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ExportLdeFrais;
use Code16\Sharp\Form\Fields\SharpFormSelectField;
use Code16\Sharp\Form\Fields\SharpFormTextField;
use Illuminate\Support\Carbon;

class LdeFraisExportCommand extends EntityCommand
{
    use WithSharpContext;

    /**
     * @return string
     */
    public function label(): string
    {
        //
        return "Exporter cette liste de frais";
    }

    public function description(): string
    {
        return "Différents formats sont proposés (ods, xls, csv)";
    }

    /**
     * @param EntityListQueryParams $params
     * @param array $data
     * @return array
     */
    public function execute(EntityListQueryParams $params, array $data = []): array
    {
        Log::debug("=============== LdeFraisExportCommand::execute avec params = " . json_encode($params) . " et data = " . json_encode($data));
        Log::debug("contexte : " . json_encode($this));
        $carbon = Carbon::now();
        $ladate = $carbon->format('Ymd-hi');

        $e = new ExportLdeFrais(
            $params->filterFor("ndeFrais"),
            $params->filterFor("typeFrais"),
            $params->searchWords(),
            $params->filterFor("createdAt"),
            $params->filterFor("tagsFrais"),
            $params->filterFor("amount"),
            $params->filterFor("moyenPaiement"),
            $params->filterFor("user")
        );
        $filename = "doliscan-analytics_export_$ladate." . strtolower($data['format']);
        $e->store($filename, 'local', $data['format'], [$headings = true]);

        return $this->download($filename, $filename, "local");
    }

    public function buildFormFields()
    {
        $this->addField(
            SharpFormSelectField::make(
                "format",
                [
                    ["id" => \Maatwebsite\Excel\Excel::ODS, "label" => "LibreOffice (ods)"],
                    ["id" => \Maatwebsite\Excel\Excel::XLSX, "label" => "Excel (xls)"],
                    ["id" => \Maatwebsite\Excel\Excel::CSV, "label" => "CSV (csv)"],
                ]
            )
                ->setLabel("Sous quel format voulez-vous télécharger votre export ?")
                ->setDisplayAsDropdown()
        );
    }
}
