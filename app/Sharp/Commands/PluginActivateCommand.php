<?php

/**
 * PluginActivateCommand.php
 *
 * Copyright (c) 2021 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Sharp\Commands;

use Code16\Sharp\EntityList\Commands\InstanceCommand;
use Code16\Sharp\Form\Fields\SharpFormTextField;
use Code16\Sharp\Form\Layout\FormLayoutColumn;
use Illuminate\Support\Facades\Log;
use App\User;
use App\Plugin;
use Illuminate\Support\Facades\Auth;
use Code16\Sharp\Form\Fields\SharpFormTextareaField;
use Code16\Sharp\Form\Fields\SharpFormCheckField;

class PluginActivateCommand extends InstanceCommand
{
    /**
     * @return string
     */
    public function label(): string
    {
        //
        return "Activer ou désactiver ce module";
    }

    public function description(): string
    {
        return "";
    }

    /**
     * @param string $instanceId
     * @param array $data
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    public function execute($instanceId, array $data = []): array
    {
        // $p = Plugin::findOrFail($instanceId);
        Log::debug("PluginActivateCommand::execute $instanceId + " . json_encode($data));
        $u = Auth::user();
        $msg = "";
        if ($data['status']) {
            $u->setPluginEnable($instanceId, 'on');
            $msg = "Module activé";
        } else {
            $u->setPluginEnable($instanceId, 'off');
            $msg = "Module désactivé";
        }
        // return $this->info($msg);
        return $this->reload();
        // sharp_user()->impersonate($u);
        // return $this->link("/admindoli");
    }

    function buildFormFields()
    {
        $this->addField(
            SharpFormCheckField::make("status", "Activer le module")
                ->setLabel("État du module")
                ->setHelpMessage("Cochez la case pour activer ce module...")
        );
    }

    protected function initialData($instanceId): array
    {
        $v = 0;
        $u = Auth::user();
        if ($u->isPluginEnabled($instanceId) == "on") {
            $v = 1;
        }
        Log::debug("PluginActivateCommand::initialData $v ==");

        return [
            "status" => $v
        ];
    }
}
