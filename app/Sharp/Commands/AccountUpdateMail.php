<?php
/*
 * AccountUpdateMail.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Sharp\Commands;

use App\User;
use Illuminate\Support\Facades\Auth;
use Code16\Sharp\EntityList\Commands\SingleInstanceCommand;
use Code16\Sharp\Form\Fields\SharpFormTextField;
use Code16\Sharp\Form\Fields\SharpFormHtmlField;

class AccountUpdateMail extends SingleInstanceCommand
{

    /**
     * @return string
     */
    public function label(): string
    {
        return "Changer d'adresse mail";
    }

    /**
     * @param array $data
     * @return array
     * @throws \Illuminate\Validation\ValidationException
     */
    public function executeSingle(array $data = []): array
    {
        $this->validate(
            $data,
            [
                "email" => "required|email|unique:users"
            ],
            [
                "L'adresse mail indiquée est soit invalide soit déjà prise ..."
            ]
        );

        $u = User::findOrFail(auth()->id());
        if ($u->changeEmail($data['email'])) {
            Auth::logout();
            return $this->info("Modification faite, merci de vous ré-authentifier avec votre nouvelle adresse ...");
        } else {
            return $this->info("Erreur de modification de l'adresse mail, veuillez contacter le SAV : sav@doliscan.fr");
        }
    }

    function buildFormFields()
    {
        $this->addField(
            SharpFormHtmlField::make("message")
                ->setInlineTemplate("<h2>Modification d'adresse mail</h2><br /><p>Après avoir modifié votre adresse mail pensez bien à vous déconnecter puis vous reconnecter.</p>")
        )->addField(
            SharpFormTextField::make("email")
                ->setLabel("Nouvelle adresse mail")
        );
    }

    /**
     * @return array
     */
    protected function initialSingleData(): array
    {
        return $this->transform(User::findOrFail(auth()->id()));
    }

    /**
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }
}
