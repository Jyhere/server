<?php
/*
 * PluginSharpList.php
 *
 * Copyright (c) 2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Sharp;

use App\User;
use App\Entreprise;
use App\Events\EventsPlugins;
use App\Plugin;
use Spatie\Permission\Models\Role;
use Code16\Sharp\EntityList\SharpEntityList;
use Code16\Sharp\EntityList\EntityListQueryParams;
use Code16\Sharp\EntityList\Containers\EntityListDataContainer;
use Illuminate\Support\Facades\Log;
use App\Sharp\Commands\PluginActivateCommand;
use App\PluginUserConfiguration;
use Illuminate\Support\Facades\Auth;

class PluginSharpList extends SharpEntityList
{
    /**
     * Build list containers using ->addDataContainer()
     *
     * @return void
     */
    public function buildListDataContainers()
    {
        $this->addDataContainer(
            EntityListDataContainer::make('name')
                ->setLabel('Extension')
                ->setSortable()
                ->setHtml()
        )->addDataContainer(
            EntityListDataContainer::make('description')
                ->setLabel('Description')
                ->setSortable()
                ->setHtml()
        )->addDataContainer(
            EntityListDataContainer::make('status')
                ->setLabel('Etat')
        )->addDataContainer(
            EntityListDataContainer::make('commandes')
                ->setLabel('Actions')
        );
    }

    /**
     * Build list layout using ->addColumn()
     *
     * @return void
     */

    public function buildListLayout()
    {
        $this->addColumn('name', 2)
            ->addColumn('description', 8)
            ->addColumn('status', 2);
        // ->addColumn('commandes', 2);
    }

    /**
     * Build list config
     *
     * @return void
     */
    public function buildListConfig()
    {
        $this->setPaginated()
            ->setSearchable()
            ->setInstanceIdAttribute('id')
            ->setDefaultSort('name')
            ->addInstanceCommand("activate", PluginActivateCommand::class);
        // ->addInstanceCommand("envoyer_mail_ndf_du_mois", UserEnvoyerMailCurrentNdfCommand::class)
    }

    /**
     * Retrieve all rows data as array.
     *
     * @param EntityListQueryParams $params
     * @return array
     */
    public function getListData(EntityListQueryParams $params)
    {
        // Log::debug("******************* sort by " . $params->sortedDir());

        //Le superadmin -> on demande un refresh des plugins
        if (sharp_user()->hasRole('superAdmin')) {
            // Log::debug("On lance un event ::List");
            event(new EventsPlugins("List"));
        }

        $plugins = Plugin::all();

        collect($params->searchWords())
            ->each(function ($word) use ($plugins) {
                $plugins->where(function ($query) use ($word) {
                    $query->orWhere('name', 'like', $word);
                });
            });

        return $this->setCustomTransformer("status", function ($status, $plugin) {
            // Log::debug(" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ");
            // Log::debug($plugin->id . " et " . Auth::user()->id);
            $txt = "";
            $p = PluginUserConfiguration::where('plugin_id', $plugin->id)->where('user_id', Auth::user()->id)->first();
            if ($p) {
                // Log::debug($p->config_value['status']);
                $txt = $p->status();
            } else {
                if($plugin->name == "ExportToQuadratus") {
                    $txt = "default";
                }
            }
            // Log::debug(" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ ");
            // Log::debug(json_encode($p->plugin->name));

            // return sharp_user()->getPluginStatus($plugin->id);
            return $txt;
        })
            ->transform($plugins); //->paginate(30);
    }
}
