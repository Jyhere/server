<?php
/*
 * CustomizingAppSharpForm.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Sharp;

use App\User;
use App\CustomizingApp;
use Code16\Sharp\Form\Layout\FormLayoutColumn;
use Code16\Sharp\Form\Layout\FormLayoutFieldset;
use Code16\Sharp\Form\Layout\FormLayoutTab;
use Code16\Sharp\Form\Fields\SharpFormNumberField;
use Code16\Sharp\Form\Fields\SharpFormHtmlField;
use Code16\Sharp\Form\Fields\SharpFormTextareaField;
use Code16\Sharp\Form\Fields\SharpFormCheckField;
use Code16\Sharp\Form\Fields\SharpFormTextField;
use Code16\Sharp\Form\Fields\SharpFormListField;
use Code16\Sharp\Form\Fields\SharpFormAutocompleteField;
use Code16\Sharp\Form\Eloquent\WithSharpFormEloquentUpdater;
use Code16\Sharp\Utils\Transformers\Attributes\Eloquent\SharpUploadModelThumbnailUrlTransformer;
use Code16\Sharp\Form\Fields\SharpFormWysiwygField;
use Code16\Sharp\Form\Fields\SharpFormDateField;
use Code16\Sharp\Form\SharpForm;
use Code16\Sharp\Form\Fields\SharpFormUploadField;
use Code16\Sharp\Http\WithSharpContext;
use Illuminate\Support\Facades\Log;


class CustomizingAppSharpForm extends SharpForm
{
    use WithSharpFormEloquentUpdater, WithSharpContext;

    function find($id): array
    {

        Log::debug("CustomizingApp : find " . $id);
        return $this
            ->setCustomTransformer("picture", new SharpUploadModelThumbnailUrlTransformer(140))
            ->setCustomTransformer(
                "logo",
                function ($value, $c, $attribute) {
                    $fullFileName = storage_path() . "/data/CustomizingApp/" . $c->id . "/logo.png";
                    if (\file_exists($fullFileName)) {
                        return [
                            "name" => "logo.png",
                            "thumbnail" => $c->thumbnail(),
                            "size" => filesize($fullFileName),
                        ];
                    } else {
                        return;
                    }
                }
            )
            ->transform(
                CustomizingApp::findOrFail($id)
            );
    }


    /**
     * @param array $data
     * @return mixed
     */
    function update($id, array $data)
    {
        Log::debug("CustomizingApp : update " . $id);
        $instance = $id ? CustomizingApp::findOrFail($id) : new CustomizingApp();

        $data["user_id"] = auth()->id();

        Log::debug("sauvegarde ==================");

        $this->ignore(["logo"])->save($instance, $data);

        //On change le nom du fichier pour avoir un nom fixe  logo.png
        // Log::debug(json_encode($data['logo']));
        if (isset($data['logo']) && isset($data['logo']['file_name'])) {
            $fullFileName = storage_path() . "/" . $data['logo']['file_name'];
            $logoFileName = storage_path() . "/data/CustomizingApp/" . $id . "/logo.png";
            if (\file_exists($fullFileName)) {
                Log::debug("rename du fichier $fullFileName -> $logoFileName");
                if (rename($fullFileName, $logoFileName)) {
                    Log::debug("rename success");
                } else {
                    Log::debug("rename ERROR");
                }
            } else {
                Log::debug("Le fichier $fullFileName n'existe pas ...");
            }
        }

        return $instance->id;
        // // Log::debug(json_encode($test));
        // Log::debug("sauvegarde ==================");

        // if (is_null($test)) {
        //     $test = new CustomizingApp($data);
        //     return $test->save($data);
        // } else {
        //     return $this->save(CustomizingApp::where('user_id', auth()->id())->first(), $data);
        // }
    }

    function delete($id)
    {
        Log::debug("CustomizingApp : delete " . $id);
        CustomizingApp::findOrFail($id)->delete();
    }

    function create(): array
    {
        parent::create();

        Log::debug("CustomizingApp : create ");
        $c = new CustomizingApp();
        Log::debug($this->transform($c));
        return $this->transform($c);
    }


    /**
     * Build form fields using ->addField()
     *
     * @return void
     */
    function buildFormFields()
    {
        // Log::debug("Contexte:");
        // Log::debug(\json_encode($this));

        $this
            ->addField(
                SharpFormTextField::make("user_id")
                    ->setLabel("Auteur (id)")
                    ->setReadOnly(true)
            )->addField(
                SharpFormUploadField::make("logo")
                    ->setLabel("Choisissez le logo que vous voulez:")
                    ->setHelpMessage("PNG, Résolution : 35 x 135 ou + (conservez le même ratio)")
                    ->setStorageDisk("local")
                    ->setCompactThumbnail(false)
                    ->shouldOptimizeImage(false)
                    ->setFileFilter("png")
                    ->setMaxFileSize(0.5)
                    ->setStorageBasePath("data/CustomizingApp/{id}")
            )
            ->addField(
                SharpFormTextareaField::make("css")
                    ->setLabel("Modifiez la feuille de style CSS :")
                    ->setHelpMessage("Vous pouvez vous aider du site-maquette mis à disposition sur https://css.doliscan.org/")
                    ->setRowCount(20)
            )
            ->addField(
                SharpFormCheckField::make("status", "Publier ce thème sur tout le parc d'utilisateurs")
                    ->setLabel("État")
                    ->setHelpMessage("Cochez cette case si vous voulez déployer ce thème sur l'ensemble du parc. Dans le cas contraire seuls les utilisateurs de votre société seront concernés.")
            )
            ->addField(
                SharpFormTextareaField::make("message")
                    ->setLabel("Message")
                    ->setRowCount(20)
                    ->setHelpMessage("Sera affiché à vos utilisateurs lors du lancement de l'application (prochaine version)")
            )
            ->addField(
                SharpFormWysiwygField::make("apropos")
                    ->setLabel("Personnalisation de la boite à propos : ")
                    ->showToolbar()
                    ->setToolbar([
                        SharpFormWysiwygField::B,
                        SharpFormWysiwygField::I,
                        SharpFormWysiwygField::SEPARATOR,
                        SharpFormWysiwygField::UL,
                        SharpFormWysiwygField::OL,
                        SharpFormWysiwygField::INCREASE_NESTING,
                        SharpFormWysiwygField::DECREASE_NESTING,
                        SharpFormWysiwygField::SEPARATOR,
                        SharpFormWysiwygField::H1,
                        SharpFormWysiwygField::SEPARATOR,
                        SharpFormWysiwygField::A,
                        SharpFormWysiwygField::SEPARATOR,
                        SharpFormWysiwygField::CODE,
                        SharpFormWysiwygField::QUOTE,
                        SharpFormWysiwygField::SEPARATOR,
                        SharpFormWysiwygField::UNDO,
                        SharpFormWysiwygField::REDO,
                    ])
            );
    }

    /**
     * Build form layout using ->addTab() or ->addColumn()
     *
     * @return void
     */
    function buildFormLayout()
    {
        $this->addColumn(12, function (FormLayoutColumn $column) {
            $column->withFields('status|8');
            $column->withFields('css|8', 'message|4');
            $column->withFields('apropos|8', 'logo|4');
        });
    }
}
