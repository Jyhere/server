<?php
/*
 * ResetPassword.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Notifications;

use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Auth\Notifications\ResetPassword as ResetPasswordNotification;

class ResetPassword extends ResetPasswordNotification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $u = User::where('email', $notifiable->email)->first();

        $token = app('auth.password.broker')->createToken($u);
        DB::table(config('auth.passwords.users.table'))->insert([
            'email' => $u->email,
            'token' => $token,
            'created_at' => Carbon::now()
        ]);

        $resetUrl = url(config('app.url') . route('password.reset', $token, false));

        return (new MailMessage)
            ->subject("[" . config('app.name') . "] - Changement de mot de passe")
            ->line("Une demande de changement de mot de passe viens d'être faite pour votre compte DoliSCAN.")
            ->action("Modifier votre mot de passe", $resetUrl)
            ->line('Ce lien expirera dans ' . (config('auth.passwords.' . config('auth.defaults.passwords') . '.expire') / 60) . ' heures')
            ->line("Si vous ne voulez pas modifier votre mot de passe ignorez tout simplement ce courriel");
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
