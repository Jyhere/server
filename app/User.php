<?php
/*
 * User.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App;

use DB;
use Schema;
use Mail;
use Swift_Encoding;
use App\LdeFrais;
use App\NdeFrais;
use DatabaseSeeder;
use App\Jobs\ProcessCreateAccount;
use App\Jobs\ProcessSendEmail;
use Symfony\Component\Process\Process;
use Spatie\Permission\Models\Role;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Str;
use Illuminate\Support\Carbon;
use Spatie\Permission\Traits\HasRoles;
use App\Mail\UserMailInvitation;
use Spatie\Activitylog\Traits\LogsActivity;
use App\Notifications\ResetPassword;
use Lab404\Impersonate\Models\Impersonate;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\Crypt;
use App\Mail\UserMailFullBackup;
use App\Mail\UserMailVerification;
use QrCode;
use App\Encryption;
use App\PluginUserConfiguration;

class User extends Authenticatable
{
    use LogsActivity;
    use Impersonate;
    use Notifiable;
    use HasRoles;
    use SoftDeletes;
    use HasApiTokens;

    protected static $logName = 'User';
    protected static $logAttributes = ['*'];
    protected static $logAttributesToIgnore = ['password', 'api_token', 'updated_at', 'remember_token'];
    protected static $logOnlyDirty = true;
    protected static $submitEmptyLogs = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // public $nomComplet = "Gaston Lagafe";

    protected $fillable = [
        'firstname', 'name', 'email', 'password', 'adresse', 'cp', 'ville', 'pays', 'send_copy_to',
        'compta_email', 'compta_ik', 'compta_peage', 'compta_hotel', 'compta_train',
        'compta_carburant0recup', 'compta_carburant60recup', 'compta_carburant80recup', 'compta_carburant100recup',
        'compta_taxi', 'compta_restauration', 'compta_divers',
        'compta_compteperso', 'compta_compteprocb', 'compta_compteproesp',
        'compta_tvadeductible', 'main_role', 'mode_simple', 'corrector_id', 'creator_id',
        'compta_tva_tx1', 'compta_tva_tx2', 'compta_tva_tx3', 'compta_tva_tx4',
        'security_code'
    ];
    protected $dates = ['created_at', 'deleted_at'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
        'password',
    ];

    protected $guarded = ['id'];

    public function __construct(array $attributes = array())
    {
        $this->setRawAttributes(array(
            'password'  => "xxxxxx",
            'api_token' => $this->generateToken()
        ), true);
        parent::__construct($attributes);
    }

    public function pluginUserConfiguration()
    {
        return $this->hasOne(PluginUserConfiguration::class);
    }

    public function delete()
    {
        // Le softDeletes laisse ce compte dans la base de données, on suffixe son mail pour eviter le refus de re-créer un compte plus tard
        // avec cette même adresse mail
        $this->email .= "-deleted-" . time();
        $this->save();
        parent::delete();
    }


    /**
     * setMainRole : affecte le role principal de l'utilisateur
     *
     * @param  mixed $roleName
     * @return void
     */
    public function setMainRole($roleName)
    {
        Log::debug("User::setMainRole $roleName");
        $role = Role::findByName($roleName, 'web');
        $this->main_role = $role->id;
        $this->assignRole($role);
        $this->save();
        return $role->id;
    }

    /**
     * mainRole : retourne le role principal de l'utilisateur
     *
     * @return mixed $roleName
     */
    public function mainRole()
    {
        $role = Role::findById($this->main_role, 'web');
        return $role->name;
    }

    public function getMainRoleAttribute($value)
    {
        if (is_null($value)) {
            $role = Role::findByName('utilisateur', 'web');
            return $role->id;
        } else {
            return $value;
        }
    }

    // public function getAuthIdentifierName()
    // {
    //     //On passe au parent
    //     parent::getAuthIdentifierName();

    //     $this->nomComplet = $this->firstname . " " . $this->name;
    //     return $this->nomComplet;
    // }
    public function vehicules()
    {
        return $this->hasMany(Vehicule::class);
    }

    public function smartphoneApps()
    {
        return $this->hasMany(SmartphoneApp::class);
    }

    public function entreprises()
    {
        return $this->belongsToMany(Entreprise::class)->withPivot('role_id'); //, 'entreprise_user', 'user_id', 'role_id');
    }

    public function NdeFrais()
    {
        return $this->hasMany('App\NdeFrais');
    }

    public function LdeFrais()
    {
        return $this->hasMany('App\LdeFrais');
    }

    //Un utilisateur peut créer des tags pour ses frais
    public function TagsFrais()
    {
        return $this->hasMany(TagsFrais::class);
    }


    public function CalculDistanceAnnuelle($year)
    {
        // TODO SELECT SUM(distance) FROM `lde_frais` WHERE user_id=2 AND YEAR(ladate)='2018'
        $t = DB::table('lde_frais')
            ->select(DB::raw('SUM(distance) as total'))
            ->whereYear('ladate', '=', $year)
            ->value('total');
        // Log::debug('======== getTotalKMfor ===========');
        // Log::debug($t);
        return $t;
    }

    public function generateToken()
    {
        $this->api_token = Str::random(60);
        if ($this->id) {
            $this->save();
        }
        return $this->api_token;
    }

    public function getToken()
    {
        return $this->api_token;
    }

    //Retourne les initiales du compte eventuellement limitee aux $size premieres lettres
    //pour eviter des intiiales du genre JTCCD
    public function initials($size = 0)
    {
        $ret = '';
        // foreach (explode(' ', preg_replace("/[^a-zA-Z ]+/", "", ($this->name))) as $word)
        //     $ret .= strtoupper($word[0]);
        $ret .= strtoupper(Str::slug($this->firstname)[0]) . strtoupper(Str::slug($this->name)[0]);

        if ($size > 0) {
            return substr($ret, 0, $size);
        }
        return $ret;
    }

    //retourne les codes comptables de l'utilisateur
    /*
        'compta_email', 'compta_ik', 'compta_peage', 'compta_hotel', 'compta_train',
        'compta_carburant0recup', 'compta_carburant60recup', 'compta_carburant80recup', 'compta_carburant100recup',
        'compta_taxi', 'compta_restauration', 'compta_divers',
        'compta_compteperso', 'compta_compteprocb', 'compta_compteproesp',
        'compta_tvadeductible',
        */
    public function getComptaCode($slug)
    {
        //Si slug commence par compta_ on ne touche pas sinon on prefixe de compta
        if (Str::startsWith($slug, 'compta_')) {
            $slug_complet = $slug;
        } else {
            $slug_complet = "compta_$slug";
        }
        $val = $this->$slug_complet;
        // Log::debug("getComptaCode pour $slug : $slug_complet soit $val");
        return $val;
    }

    //Retourne un tableau avec toutes les valeurs des comptes comptables (pratique pour créer un nouveau compte)
    public function getComptaConfig()
    {
        $r = array();
        foreach ($this->fillable as $compte) {
            if (Str::startsWith($compte, "compta_")) {
                // Log::debug("On cherche pour $compte");
                $r[$compte] = $this->$compte;
            }
        }
        return $r;
    }

    public function ajouteDataDemo()
    {
        ProcessCreateAccount::dispatch($this);
    }

    /**
     * Expédition d'un mail d'invitation pour un nouvel utilisateur
     *
     * @param   int  $relance     à 1 si c'est un mail de "relance" (on pourra moduler le contenu le cas échéant)
     *
     * @return  [type]            [return description]
     */
    public function envoyerMailInvitation($relance = 0)
    {
        Log::debug("User::envoyerMailInvitation : relance = $relance");

        //Allez il faut envoyer un mail de bienvenue ...
        //Si on a affaire à un revendeur

        //Si on a affaire à un utilisateur
        // if ($this->hasRole("utilisateur") || $this->hasRole("adminRevendeur") || $this->hasRole("adminEntreprise") || $this->hasRole("responsableEntreprise") ) {

        $details = array(
            'to' => $this->email,
            'bcc' => config('mail.notifications'),
            'subject' => "[" . config('app.name') . "] Invitation : votre assistant note de frais",
            'objectMail' => new UserMailInvitation($this, $relance),
        );

        //Il faudrait envoyer en copie le mail au revendeur associé...
        $message = "envoyerMailInvitation to " . $this->email;
        if ($this->creator_id > 1) {
            $details['cc'] = User::where('id', $this->creator_id)->first()->email;
            $message .= " Cc: " . $details['cc'];
        }

        activity('Mail')
            ->by(\sharp_user())
            ->performedOn($this)
            ->withProperty('email', $this->email)
            ->log($message);

        ProcessSendEmail::dispatch($details);
        return "Mail envoyé";
        // } else {
        // return "Aucun mail type n'est encore prêt pour ce type de profil ...";
        // }
    }

    /**
     * Expédition d'un mail de vérification de connexion via l'API si le compte existe déjà
     *
     * @return  [type]            [return description]
     */
    public function envoyerMailVerificationRegisterAPI($relance = 0)
    {
        Log::debug("User::envoyerMailVerificationRegisterAPI");

        $details = array(
            'to' => $this->email,
            'bcc' => config('mail.securitycheck'),
            'subject' => "[" . config('app.name') . "] Sécurité : Vérification de votre compte",
            'objectMail' => new UserMailVerification($this),
        );
        $message = "envoyerMailVerificationRegisterAPI to " . $this->email;

        activity('Mail')
            ->by(\sharp_user())
            ->performedOn($this)
            ->withProperty('email', $this->email)
            ->log($message);

        ProcessSendEmail::dispatch($details);
        return "Mail envoyé";
    }


    /**
     * Recupere la liste de mes tags + ceux qui sont partagés au sein de ma société
     * question: si je suis dirigeant j'ai aussi accès aux tags des salariés ?
     *
     * @param   [type] $orderBy   [$orderBy description]
     * @param   label  $orderDir  [$orderDir description]
     * @param   asc    $as_get    [$as_get description]
     * @param   true              [ description]
     *
     * @return  [type]            [return description]
     */
    public function getMyTags($orderBy = 'label', $orderDir = 'asc', $as_get = true)
    {
        Log::debug("User::getMyTags");
        $tags = null;
        $tags = TagsFrais::where(function ($query) {
            $e = $this->getEntreprises()->pluck('id');
            $query->where('user_id', sharp_user()->id)
                ->orWhereIn('entreprise_id', $e);
        })->orderBy($orderBy, $orderDir);

        $r = null;
        if ($as_get) {
            $r = $tags->get();
        } else {
            $r = $tags;
        }
        // Log::debug("User::getMyTags result :: " . json_encode($r));
        return $r;
    }


    //Recupere la listes des utilisateurs auxquels j'ai accès
    static function getMyUsers($orderBy = 'name', $orderDir = 'asc', $as_get = true)
    {
        Log::debug("User::getMyUsers");
        $users = null;
        $u = User::findOrFail(sharp_user()->id); //->entreprises();

        switch (sharp_user()->mainRole()) {
            case 'superAdmin':
                Log::debug("User::getMyUsers superadmin");
                $users = User::orderBy($orderBy, $orderDir);
                break;
            case 'adminRevendeur':
                Log::debug("User::getMyUsers adminRevendeur");
                //+ tous les utilisateurs de "mes" entreprises
                //1 les entreprises
                $entreprises = $u->getEntreprises()->pluck('id');
                // Log::debug("retour des entreprises : " . json_encode($entreprises));

                //On recupere tous les utilisateurs créés par cet utilisateur
                //+ myself mais dont le profil est < à moi (pour pas avoir d'escalation de privileges)


                $collection = collect();
                $usersID = $collection
                    ->concat(DB::table('users')
                        ->whereRaw(DB::raw("((creator_id='" . $u->id . "' OR id='" . $u->id . "') AND main_role <= '" . $u->main_role . "')"))
                        ->pluck('id'))
                    ->concat(DB::table('entreprise_user')
                        ->select('user_id AS id')
                        ->where('role_id', '<=', $u->main_role)
                        ->whereIN('entreprise_id', $entreprises)
                        ->pluck('id'))
                    ->unique()
                    ->values();

                Log::debug("getMyUsers : retour des userid 2 : " . json_encode($usersID->all()));
                $users = User::whereIN('id', $usersID->all())
                    ->orderBy($orderBy, $orderDir);
                break;
            case 'responsableEntreprise':
            case 'adminEntreprise':
                //Tous les utilisateurs qui sont sous la responsabilité de ce responsable d'entreprise ...
                // autrement dit tous les comptes utilisateurs rattachés à l'entreprise dont il est responsable :)
                Log::debug("User::getMyUsers responsableEntreprise ou adminEntreprise");
                //+ tous les utilisateurs de "mes" entreprises
                //1 les entreprises
                $entreprises = $u->getEntreprises()->pluck('id');
                // Log::debug("retour des entreprises : " . json_encode($entreprises));

                //On recupere tous les utilisateurs créés par cet utilisateur
                //+ myself
                $collection = collect();
                $usersID = $collection
                    ->concat(DB::table('users')
                        ->whereRaw(DB::raw("((creator_id='" . $u->id . "' OR id='" . $u->id . "') AND main_role <= '" . $u->main_role . "')"))
                        ->pluck('id'))
                    ->concat(DB::table('entreprise_user')
                        ->select('user_id AS id')
                        ->where('role_id', '<=', $u->main_role)
                        ->whereIN('entreprise_id', $entreprises)
                        ->pluck('id'))
                    ->unique()
                    ->values();

                Log::debug("getMyUsers : retour des userid 2bis : " . json_encode($usersID->all()));
                $users = User::whereIN('id', $usersID->all())
                    ->orderBy($orderBy, $orderDir);
                break;
            case 'serviceComptabilite':
                //le comptable d'entreprise ... un cas un peu plus particulier, il n'a pas créé les collègues mais doit avoir accès
                Log::debug("User::getMyUsers serviceComptabilite");
                //+ tous les utilisateurs de "mes" entreprises
                //1 les entreprises
                $entreprises = $u->getEntreprises()->pluck('id');
                // Log::debug("retour des entreprises : " . json_encode($entreprises));
                $maxroleid = Role::findByName('adminEntreprise', 'web')->id;

                //On recupere tous les utilisateurs sauf les superadmin
                //+ myself
                $collection = collect();
                $usersID = $collection
                    ->concat(DB::table('users')
                        ->whereRaw(DB::raw("((creator_id='" . $u->id . "' OR id='" . $u->id . "') AND main_role < '" . $maxroleid . "')"))
                        ->pluck('id'))
                    ->concat(DB::table('entreprise_user')
                        ->select('user_id AS id')
                        ->where('role_id', '<', $maxroleid)
                        ->whereIN('entreprise_id', $entreprises)
                        ->pluck('id'))
                    ->unique()
                    ->values();

                Log::debug("getMyUsers : retour des userid 2bis : " . json_encode($usersID->all()));
                $users = User::whereIN('id', $usersID->all())
                    ->orderBy($orderBy, $orderDir);
                break;
            case 'correcteur':
                Log::debug("User::getMyUsers correcteur");
                // $users = $u;
                $users = User::where('id', sharp_user()->id);
                break;
            case 'utilisateur':
                Log::debug("User::getMyUsers utilisateur");
                // $users = $u;
                $users = User::where('id', sharp_user()->id);
                break;
            default:
                Log::debug("User::getMyUsers Erreur, ce cas n'est pas géré !");
        }

        // Log::debug(json_encode($users));
        // if ($users == null) {
        //     $users = new user();
        // }
        $r = null;
        if ($as_get) {
            // Log::debug("  getMyUsers : " . json_encode($users));
            $r = $users->get();
        } else {
            $r = $users;
        }
        return $r;
    }

    //Tellement genial, tellement simple ... pour avoir le nom complet directement on utilisera
    //$user->full_name ... la magie de Laravel, cf
    //https://medium.com/@petehouston/laravel-fact-make-computed-attributes-for-eloquent-models-fc78fe5f1aa4
    public function getFullNameAttribute()
    {
        return $this->firstname . ' ' . $this->name;
    }

    /**
     * Accesseur qui "corrige" la mise en forme du prénom
     *
     * @param   [type]  $value  [$value description]
     *
     * @return  [type]          [return description]
     */
    public function getFirstnameAttribute($value)
    {
        return (mb_convert_case($value, MB_CASE_TITLE, 'UTF-8'));
    }

    /**
     * Accesseur qui "corrige" la mise en forme du nom
     *
     * @param   [type]  $value  [$value description]
     *
     * @return  [type]          [return description]
     */
    public function getNameAttribute($value)
    {
        return (Str::upper($value));
    }

    // public function getComptaTvaTx1Attribute($value)
    // {
    //     Log::debug("User::getTvaTx1Attribute");
    //     return "2.1";
    // }
    // public function getComptaTvaTx2Attribute($value)
    // {
    //     Log::debug("User::getTvaTx2Attribute $value");
    //     return "5.5";
    // }
    // public function getComptaTvaTx3Attribute($value)
    // {
    //     Log::debug("User::getTvaTx3Attribute $value");
    //     return "10";
    // }
    // public function getComptaTvaTx4Attribute($value)
    // {
    //     Log::debug("User::getTvaTx4Attribute $value");
    //     return "20";
    // }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        activity('Mail')
            ->causedBy(\sharp_user())
            ->performedOn($this)
            ->withProperty('email', $this->email)
            ->log('sendPasswordResetNotification to ' . $this->email);

        $this->notify(new ResetPassword($token));
    }

    //L'adresse mail de l'utilisateur
    public function getMail()
    {
        return $this->email;
    }

    //L'adresse mail du comptable à qui il faut envoyer les NDF
    public function getMailCompta()
    {
        return $this->compta_email;
    }

    //L'adresse mail de la personne à mettre en copie
    public function getMailCopyTo()
    {
        return $this->send_copy_to;
    }

    //L'entreprise a laquelle cet utilisateur est liée ... posera probablement un soucis pour les personnes
    //qui ont plusieurs rôles sur plusieurs entreprises (ex. revendeurs)
    //TODO: interdire aux comptes revendeurs et + (relecteurs ?) de faire des notes de frais avec leur compte rvd
    public function getEntreprise()
    {
        $uidtmp = $this->id;
        $societe = Entreprise::whereHas('users', function ($q) use ($uidtmp) {
            $q->where('user_id', $uidtmp);
        })->first();
        return $societe;
    }

    /**
     * Add a user's role on a Entreprise object
     *
     * @param Role|string|int $role
     * @param Entreprise|string|int $entreprise
     *
     * @return true on success
     */
    public function addRoleOnEntreprise($role, $entreprise)
    {
        $rid = 0;
        $eid = 0;
        if (is_int($role)) {
            $rid = $role;
        } elseif (is_string($role)) {
            $r = Role::findByName($role, 'web');
            $rid = $r->id;
        } elseif (is_object($role)) {
            $rid = $role->id;
        }

        if (is_int($entreprise)) {
            $eid = $entreprise;
        } elseif (is_string($entreprise)) {
            $e = Entreprise::where('name', $entreprise)->first();
            $eid = $e->id;
        } elseif (is_object($entreprise)) {
            $eid = $entreprise->id;
        }

        //On evite les doublons (pourquoi n'y a t il pas d'index ?)
        //-> pour eviter les messages d'erreurs de type "can't insert"
        //de l'époque où on ne savait pas que updateOrInsert existait !
        if ($rid > 0 && $eid > 0) {
            DB::table("entreprise_user")->updateOrInsert([
                'entreprise_id' => $eid,
                'role_id' => $rid,
                'user_id' => $this->id,
            ]);
            return true;
        }
        return false;
    }


    //La liste des entreprises auxquelles j'ai accès
    public function getEntreprises($orderBy = 'name', $orderDir = 'asc', $as_get = true)
    {
        Log::debug("User::getEntreprises");
        $entreprises = null;

        //Note: On ne liste que les utilisateurs pour lesquels on a des droits ...
        //Si on est superAdmin -> tout

        switch (sharp_user()->mainRole()) {
            case 'superAdmin':
                Log::debug("  getEntreprises : superAdmin");
                $entreprises = Entreprise::with('users')->where('id', '>', 0);
                break;
            case 'adminRevendeur':
                //Toutes les entreprises pour lesquelles cet utilisateur a les droits adminRevendeur
                Log::debug("  getEntreprises : adminRevendeur");
                $roleId = Role::findByName('adminRevendeur', 'web')->id;
                $entreprises = Entreprise::getMyEntreprises('name', $this->id, false, false);
                // $entreprises = User::find(sharp_user()->id)->entreprises();
                // Log::debug("  ===================== getEntreprises : " . get_class($entreprises));
                // $entreprises = User::find(sharp_user()->id)->entreprises();
                break;
            case 'responsableEntreprise':
            case 'adminEntreprise':
                //Un DSI ... sa boite
                Log::debug("  getEntreprises : adminEntreprise ou responsableEntreprise");
                // $roleId = Role::findByName('adminEntreprise', 'web')->id;
                $entreprises = User::find(sharp_user()->id)->entreprises(); //->where('role_id', $roleId);
                break;
            case 'serviceComptabilite':
            case 'correcteur':
                //Le service comptabilité ... son entreprise
                Log::debug("  getEntreprises : serviceComptabilite");
                $roleId = Role::findByName('serviceComptabilite', 'web')->id;
                $entreprises = User::find(sharp_user()->id)->entreprises(); //->where('role_id', $roleId);
                break;
            case 'utilisateur':
                //Un utilisateur lambda ... sa boite s'il en a une !
                Log::debug("  getEntreprises : utilisateur");
                $roleId = Role::findByName('utilisateur', 'web')->id;
                $entreprises = User::find(sharp_user()->id)->entreprises(); //->where('role_id', $roleId);
                break;
            default:
                Log::debug("  getEntreprises : Erreur : ce cas n'est pas géré !");
        }

        //Only during debug time
        // Log::debug("  getEntreprises : retour ");
        // Log::debug(json_encode($entreprises->get()));

        if ($entreprises) {
            $entreprises->orderBy($orderBy, $orderDir);
        }

        if ($as_get) {
            return $entreprises->get();
        } else {
            return $entreprises;
        }
    }

    public function canImpersonate(User $targetUser = null)
    {
        $email = "";
        if ($targetUser != null) {
            $email = $targetUser->email;
        }
        Log::debug("User::canImpersonate pour $email");

        //Qui a le droit de changer d'utilisateur -> superadmin
        if (sharp_user()->hasRole('superAdmin')) {
            return true;
        }
        //Si on est un revendeur on autorise uniquement l'usurpation d'identité de l'admin d'une entreprise pour pouvoir
        //gérer les clés d'API
        elseif (sharp_user()->hasRole('adminRevendeur')) {
            if ($targetUser->hasRole('adminEntreprise')) {
                return true;
            }
        }

        //Et sinon
        return false;
    }

    public function canBeImpersonated(User $targetUser = null)
    {
        if (sharp_user()->hasRole('superAdmin')) {
            return false;
        } else {
            return true;
        }
        // return $this->can_be_impersonated == 1;
    }

    /**
     * seedDemoData : injecte des données de démonstration pour ce compte utilisateur (utile pour les serveurs de démo)
     *
     * @return void
     */
    public function seedDemoData()
    {
        Log::debug("User::seedDemoData");
        //Quand on est en mode déploiement initial on peut être dans la situation où on injecte un jeu de données de base...
        if (App::isDownForMaintenance()) {
            Log::debug(" mode maintenance on ne fait rien");
            return;
        }
        if (config('app.env') != 'prod') {
            $faker = \Faker\Factory::create('fr_FR');
            $faker->seed(142);
            $faker->addProvider(new \App\Faker\DoliSCAN($faker));
            $faker->addProvider(new \Faker\Provider\fr_FR\Person($faker));

            $s = new DatabaseSeeder();
            $s->seedDataForUser($this->email, $faker, [], 3);
        }
    }

    /**
     * canHaveNDF : retourne true si cet utilisateur peut avoir des notes de frais, pour résumer, les comptes suivants ne peuvent
     *              pas avoir de notes de frais: superAdmin, adminRevendeur, adminEntreprise, inactif
     *
     * @return void
     */
    public function canHaveNDF()
    {
        $excludeArray = ['superAdmin', 'adminRevendeur', 'adminEntreprise', 'inactif'];
        $role = Role::findById($this->main_role, 'web');
        if (\in_array($role->name, $excludeArray)) {
            return false;
        }
        return true;
    }

    /**
     * qrCodeForApp : retourne un qrCode pour autoconfiguration de l'application
     *                contenu du qrCode: serverURI;login;api_token;
     *                l'utilisateur n'aura plus qu'a entrer son code PIN pour confirmer
     * @return void
     */
    public function qrCodeForApp($format = 'png', $size = 240, $pincode = "1234")
    {
        $userInfo = config('app.domain') . ";" . $this->email . ";" . $this->api_token . ";";

        $Encryption = new Encryption();
        $encrypted = $Encryption->encrypt($userInfo, $pincode);

        return QrCode::format($format)->size($size)->generate($encrypted);
    }

    public function qrCodeImageURLForApp($forceRefreshImage = 0)
    {
        return "/user/qrCodeForApp/" . $this->email;
    }

    //Exporte la majeure partie de l'objet en texte lisible ... pratique pour envoyer une notification
    public function toText()
    {
        $txt = "";
        foreach ($this->fillable as $f) {
            if (!\in_array($f, $this->hidden))
                $txt .= " - $f : " . $this->$f . "\n";
        }
        return $txt;
    }

    //Pour ameliorer le champ "description" des logs
    public function getDescriptionForEvent(string $eventName): string
    {
        $m = "";
        if (null !== Auth::user()) {
            $m = $this->email . " by " . Auth::user()->email;
        }
        return "{$eventName} $m";
    }


    /**
     * User change mail address
     *
     * @param   [type]  $newmail  [$newmail description]
     *
     * @return  [type]            [return description]
     */
    public function changeEmail($newmail)
    {
        Log::debug('======== User::changeEmail -> ' . $newmail);
        activity('User')
            ->causedBy(\sharp_user())
            ->performedOn($this)
            ->withProperty('email', $this->email)
            ->log("Try to change mail to $newmail");

        $retour = false;
        $errors = 0;
        //Changer les répertoires et tout et tout ...
        $directoryLDE    = storage_path() . "/LdeFrais/" . $this->email;
        $newDirectoryLDE = storage_path() . "/LdeFrais/" . $newmail;

        $directoryNDE = storage_path() . "/NdeFrais/" . $this->email;
        $newDirectoryNDE = storage_path() . "/NdeFrais/" . $newmail;

        $directoryUser = storage_path() . "/Users/" . $this->email;
        $newDirectoryUser = storage_path() . "/Users/" . $newmail;

        //Vérifications initiales : les répertoires de destination existent déjà
        if (is_dir($newDirectoryLDE)) {
            Log::debug('======== User::changeEmail -> new LdeFrais already exists ERROR');
            $errors++;
        }
        if (is_dir($newDirectoryNDE)) {
            Log::debug('======== User::changeEmail -> new NdeFrais already exists ERROR');
            $errors++;
        }
        if (is_dir($newDirectoryUser)) {
            Log::debug('======== User::changeEmail -> new LdeFrais already exists ERROR');
            $errors++;
        }

        //Vérifications initiales : les répertoires de départ n'existent pas ... on les créé
        if (!is_dir($directoryLDE)) {
            if (!mkdir($directoryLDE, 0777, true)) {
                Log::debug('======== User::changeEmail -> can\'t create LdeFrais directory ERROR');
                $errors++;
            }
        }
        if (!is_dir($directoryNDE)) {
            Log::debug('======== User::changeEmail -> new NdeFrais already exists ERROR');
            if (!mkdir($directoryNDE, 0777, true)) {
                Log::debug('======== User::changeEmail -> can\'t create NdeFrais directory ERROR');
                $errors++;
            }
        }
        if (!is_dir($directoryUser)) {
            Log::debug('======== User::changeEmail -> new LdeFrais already exists ERROR');
            if (!mkdir($directoryUser, 0777, true)) {
                Log::debug('======== User::changeEmail -> can\'t create User directory ERROR');
                $errors++;
            }
        }

        //Pas la peine d'aller plus loin
        if ($errors == 0) {

            if (rename($directoryLDE, $newDirectoryLDE)) {
                Log::debug('======== User::changeEmail -> move LdeFrais dir ok');
                if (rename($directoryNDE, $newDirectoryNDE)) {
                    Log::debug('======== User::changeEmail -> move NdeFrais dir ok');
                    if (rename($directoryUser, $newDirectoryUser)) {
                        Log::debug('======== User::changeEmail -> move User dir ok');
                        $this->update([
                            "email" => $newmail
                        ]);
                        $retour = true;
                    } else {
                        Log::debug('======== User::changeEmail -> move User dir ERROR');
                        //On annule les 2 étapes précédentes
                        if (rename($newDirectoryNDE, $directoryNDE)) {
                            Log::debug('======== User::changeEmail -> rollback Nde OK');
                        } else {
                            Log::debug('======== User::changeEmail -> rollback Nde ERROR GRAVE');
                            //TODO envoyer une alerte
                        }
                        if (rename($newDirectoryLDE, $directoryLDE)) {
                            Log::debug('======== User::changeEmail -> rollback Lde OK');
                        } else {
                            Log::debug('======== User::changeEmail -> rollback Lde ERROR GRAVE');
                            //TODO envoyer une alerte
                        }
                    }
                } else {
                    Log::debug('======== User::changeEmail -> move NdeFrais dir ERROR');
                    //On annule l'étape précédente
                    if (rename($newDirectoryLDE, $directoryLDE)) {
                        Log::debug('======== User::changeEmail -> rollback Lde OK');
                    } else {
                        Log::debug('======== User::changeEmail -> rollback Lde ERROR GRAVE');
                        //TODO envoyer une alerte
                    }
                }
            } else {
                Log::debug('======== User::changeEmail -> move LdeFrais dir ERROR');
                //Rien à faire
            }
        }
        return $retour;
    }

    /**
     * Create a zip file with all user data
     *
     * @return  [type]  [return description]
     */
    public function makeFullTarball()
    {
        Log::debug('======== User::makeFullTarball');
        $message = "";
        if ($this->email != "") {
            //On supprime toute l'arborescence de données de l'utilisateur
            $alldirs = array(
                storage_path() . "/LdeFrais/" . $this->email,
                storage_path() . "/NdeFrais/" . $this->email,
                storage_path() . "/Users/" . $this->email
            );

            $cwd = storage_path() . "/Backups";
            $pathZip = $cwd . "/" . $this->email;
            if (!is_dir($pathZip)) {
                mkdir($pathZip, 0777, true);
            }

            $createZip = true;
            foreach ($alldirs as $d) {
                $cmd = "/usr/bin/zip -rf $pathZip/fullBackup-" . $this->email . ".zip .";
                Log::debug("======= Onlance : " . \json_encode($cmd) . " avec cwd " . $d);
                // cf https://github.com/symfony/symfony/issues/36801
                $process = Process::fromShellCommandline($cmd, $d);
                // $process = new Process($cmd, $this->_justifsPath);
                $process->setWorkingDirectory($d);
                $process->run();

                /*accept zip exit codes as success : 0      normal; no errors or warnings detected.  12     zip has nothing to do */
                if ($createZip == true && $process->getExitCode() != 0 && $process->getExitCode() != 12) {
                    $createZip = false;
                    Log::debug("  ====> Erreur pour  : " . \json_encode($cmd) . ", exit code : " . $process->getExitCode());
                }
            }

            if ($createZip) {
                $message = "Sauvegarde réalisée, le lien de téléchargement a été envoyé par mail à cet utilisateur.";
            } else {
                $message = "Erreur de creation du fichier ZIP, veuillez contacter votre adminsitrateur ...";
            }
        }

        $details = array(
            'to' => $this->email,
            'bcc' => config('mail.notifications'),
            'subject' => "[" . config('app.name') . "] votre sauvegarde complète ...",
            'objectMail' => new UserMailFullBackup($this),
        );

        activity('Mail')
            ->causedBy(\sharp_user())
            ->performedOn($this)
            ->withProperty('email', $this->email)
            ->log($message);

        ProcessSendEmail::dispatch($details);


        return $message;
    }

    /**
     * Close user account: delete account, remove all data (but make a zip and send download link by email)
     *
     * @param   [type] $make_tarball  [$make_tarball description]
     * @param   false                 [ description]
     *
     * @return  [type]                [return description]
     */
    public function closeAccount($make_tarball = false)
    {
        Log::debug('======== User::closeAccount');

        if ($make_tarball)
            $this->makeFullTarball();

        //On supprime toutes les ndf et ldf associées
        $LdeFrais = LdeFrais::where('user_id', Auth::id())->get();
        foreach ($LdeFrais as $l) {
            $l->forceDelete();
        }

        $NdeFrais = NdeFrais::where('user_id', Auth::id())->get();
        foreach ($NdeFrais as $n) {
            $n->forceDelete();
        }

        if ($this->email != "") {
            //On supprime toute l'arborescence de données de l'utilisateur
            $directoryLDE  = storage_path() . "/LdeFrais/" . $this->email;
            $directoryNDE  = storage_path() . "/NdeFrais/" . $this->email;
            $directoryUser = storage_path() . "/Users/" . $this->email;

            if (is_dir($directoryLDE))
                $this->rmdir_recurse($directoryLDE);
            if (is_dir($directoryNDE))
                $this->rmdir_recurse($directoryNDE);
            if (is_dir($directoryUser))
                $this->rmdir_recurse($directoryUser);
        }

        $this->delete();
    }

    function rmdir_recurse($path)
    {
        $path = rtrim($path, '/') . '/';
        $handle = opendir($path);
        while (false !== ($file = readdir($handle))) {
            if ($file != '.' and $file != '..') {
                $fullpath = $path . $file;
                if (is_dir($fullpath)) $this->rmdir_recurse($fullpath);
                else unlink($fullpath);
            }
        }
        closedir($handle);
        rmdir($path);
    }

    public function setPluginEnable($pluginID, $onoff)
    {
        Log::debug("User::pluginEnable $pluginID ============");

        $p = Plugin::findOrFail($pluginID);
        $puc = PluginUserConfiguration::where('plugin_id', $pluginID)->where('user_id', Auth::user()->id)->first();
        if (!$puc) {
            Log::debug("User::pluginEnable n'existe pas");
            $puc = new PluginUserConfiguration;
            $puc->user_id = Auth::user()->id;
            $puc->plugin_id = $pluginID;
        }

        $puc->setStatus($onoff);
        return $puc->save();
    }

    /**
     * retourne on (actif) / off (désactivé) ou "false" (jamais activé)
     *
     * @param   [type]  $pluginID  [$pluginID description]
     *
     * @return  [type]             [return description]
     */
    public function ispluginEnabled($pluginID)
    {
        Log::debug("User::ispluginEnabled $pluginID ============");

        $p = Plugin::findOrFail($pluginID);
        $puc = PluginUserConfiguration::where('plugin_id', $pluginID)->where('user_id', Auth::user()->id)->first();
        if ($puc) {
            return $puc->status();
        }
        return false;
    }

    private function functionName()
    {
        throw new Exception('Method not implemented');
    }
}
