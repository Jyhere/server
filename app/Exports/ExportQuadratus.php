<?php
/*
 * ExportQuadratus.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Exports;

use Illuminate\Support\Facades\Log;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Illuminate\Support\Str;
use Illuminate\Support\Carbon;
use App\Jobs\ProcessSendEmail;
use App\LdeFrais;

class ExportQuadratus extends Exports
{
    private $_justifsPath;

    public function __construct($user, $filename, $directory, $endOfMonth, $filenameNDF)
    {
        parent::__construct($user, $filename, $directory, $endOfMonth, $filenameNDF);
        $this->_justifsPath = storage_path() . "/NdeFrais/" . $user->email . "/" . $endOfMonth->format("Ymd") . "-quadratus/";
        $this->_zipFileName = $endOfMonth->format("Ymd") . "-" . Str::slug($user->email,'_') . "-quadratus.zip";
    }

    //Creation du fichier ZIP qui propose tout d'un coup avec les justificatifs
    private function buildZIP()
    {
        Log::debug("  ExportQuadratus : buildZIP vers *" . $this->_justifsPath . "*");
        $dir = $this->_justifsPath;
        if ($dir != "" && is_dir($dir)) {
            //Deux cas particulier : le fichier quadratus.txt
            copy($this->_directory . "/" . $this->_filename, $this->_justifsPath . "/quadratus.TXT");
            //TODO Et le fichier de Note de frais a joindre pour les frais perso
            copy($this->_directory . "/" . $this->_filenameNDF, $this->_justifsPath . "/D0000NDF.PDF");

            $cmd = "/usr/bin/zip " . $this->_zipFileName . " *.TXT *.PDF";
            Log::debug("======= Onlance : " . \json_encode($cmd) . " avec cwd " . $this->_justifsPath);
            // cf https://github.com/symfony/symfony/issues/36801
            $process = Process::fromShellCommandline($cmd, $this->_justifsPath);
            // $process = new Process($cmd, $this->_justifsPath);
            $process->setWorkingDirectory($this->_justifsPath);
            $process->run();
            if (!$process->isSuccessful()) {
                Log::debug("  ExportQuadratus::buildZIP erreur : " . $process->getWorkingDirectory());
                Log::debug("  ExportQuadratus erreur : " . $process->getErrorOutput());
                Log::debug("  ExportQuadratus message : " . $process->getOutput());
            }


            if (\file_exists($this->_justifsPath . "/" . $this->_zipFileName)) {
                return true;
            } else {
                Log::debug("  ExportQuadratus : buildZIP erreur de creation :" . \json_encode($cmd));
                return false;
            }
        } else {
            Log::debug("  ExportQuadratus : buildZIP erreur, _justifsPath incorrect 1");
            return false;
        }
    }

    /* exporte le fichier Quadratus */
    public function export($filename = "", $forceUpdate = false)
    {
        Log::debug("ExportQuadratus::export $filename / $forceUpdate | Nb lines : " . $this->_nbLines);

        $verif = ($this->_totalCredit - $this->_totalDebit) / 100;
        Log::debug("  ExportQuadratus::export::verif " . $this->_totalCredit . " - " . $this->_totalDebit . " = $verif");
        if ($verif != 0) {
            Log::debug("  ExportQuadratus::export on corrige");
            if ($verif > 0) {
                $this->addLine($this->_lastEcritNum + 1, 658000, "ECART ARRONDI", "", "", "ECART D'ARRONDI", abs($verif), 0, "", "", false);
            } else {
                $this->addLine($this->_lastEcritNum + 1, 758000, "ECART ARRONDI", "", "", "ECART D'ARRONDI", 0, abs($verif), "", "", false);
            }
            $verif2 = $this->_totalCredit - $this->_totalDebit;
            Log::debug("  ExportQuadratus::export::verif2 $verif2");
        }

        $test = parent::export($filename, $forceUpdate);
        //Pour créer le zip il faut avoir le fichier TXT donc on appelle le code général AVANT le buildZIP
        return $this->buildZIP();
    }

    /* Une version minimale de l'ajout d'écritures au format Quadratrus */
    /*
M445710  VE000011119 FAC00000006 - grosseC+000000011148                                            FAC00000   VE                                                                     00000001.PDF
M419100  VE000011119 FAC00000006 - grosseC+000000111484                                            FAC00000   VE                                                                     00000001.PDF
M9GROSS  VE000011119 FAC00000006 - grosseD+000000122632                                            FAC00000   VE                                                                     00000001.PDF

plus facile à lire avec des séparateurs

M;445710  ;VE;000;011119; ;FAC00000006 - grosse;C;+000000011148;        ;      ;  ;   ;     ;          ;          ;FAC00000;   ;VE ; ; ; ;                              ;  ;          ;          ;             ;00000001.PDF;          ;          ;    ;              ;
M;419100  ;VE;000;011119; ;FAC00000006 - grosse;C;+000000111484;        ;      ;  ;   ;     ;          ;          ;FAC00000;   ;VE ; ; ; ;                              ;  ;          ;          ;             ;00000001.PDF;          ;          ;    ;              ;
M;9GROSS  ;VE;000;011119; ;FAC00000006 - grosse;D;+000000122632;        ;      ;  ;   ;     ;          ;          ;FAC00000;   ;VE ; ; ; ;                              ;  ;          ;          ;             ;00000001.PDF;          ;          ;    ;              ;
    */
    public function addLine($ecritureNum, $compteNum, $compteLib, $compAuxNum, $compAuxLib, $ecritureLib, $debit, $credit, $documentJustif = "", $ladate = "", $isPro = true)
    {
        // Log::debug(" ***************  Quadratus :: addLine pour $compteLib et $compteNum");

        $u = $this->_user;

        $finDuMois = $this->_endOfMonth;
        if ($ladate == "") {
            $ladate = $finDuMois;
        }

        //Si la date avant le 1er du mois en cours et qu'on est sur des frais perso
        if ($isPro == false) {
            $finDuMois    = Carbon::parse($this->_endOfMonth);
            $debutDuMois  = Carbon::parse($this->_endOfMonth)->startOfMonth();
            $ladateCarbon = Carbon::parse($ladate);
            if ($ladateCarbon->lt($debutDuMois)) {
                $ecritureLib .= " " . $ladateCarbon->format("d/m/y");
                $ladate = $debutDuMois;
            }
        }

        //Montant en centimes signe (position 43 = signe)
        $montant = 0;

        $documentPDF = "";
        //Gestion du document justificatif :)
        $ledossier   = substr($documentJustif, 0, 6);
        $ldeFicSrc  = storage_path() . "/LdeFrais/" . $u->email . "/" . $ledossier . "/" . $documentJustif;

        //On créé un fichier 8+3 a partir du numero de l'écriture...
        $pdfFileName = "D" . str_pad($ecritureNum, 7, "0", STR_PAD_LEFT) . ".PDF";
        $this->_justifsPath = storage_path() . "/NdeFrais/" . $u->email . "/" . $finDuMois->format("Ymd") . "-quadratus/";
        $ldePDFdst = $this->_justifsPath . $pdfFileName;

        if (!is_dir($this->_justifsPath)) {
            mkdir($this->_justifsPath, 0770, true);
        }

        Log::debug("exportQUADRA ... src $ldeFicSrc et PDF : $ldePDFdst");

        //Attention si on a archive probante, le PDF existe déjà !
        $ldfFicSrcPDF = str_replace(["jpeg","jpg"], "pdf", $ldeFicSrc);

        if ($documentJustif == "D0000NDF.PDF") {
            $documentPDF = $documentJustif;
        } else if (file_exists($ldfFicSrcPDF) && is_file($ldfFicSrcPDF)) {
            copy($ldfFicSrcPDF, $ldePDFdst);
            $documentPDF = $pdfFileName;
        } else {
            //Quadra permet d'importer un pdf pour chaque ligne pour avoir les justificatifs ça serait con de ne pas le faire
            if (file_exists($ldeFicSrc) && is_file($ldeFicSrc)) {
                //Transformation en PDF, changement de nom et stockage dans le meme dossier que le fichier Quadra...
                //Creation du PDF
                if (file_exists($ldePDFdst)) {
                    // Log::debug("======= Fichier déjà existant" . $ldePDFdst);
                    $documentPDF = $pdfFileName;
                } else {
                    Log::debug("exportQUADRA ... on cherche la ligne de frais de " . basename($ldeFicSrc));
                    $l = LdeFrais::where('fileName', basename($ldeFicSrc))->firstOrFail();
                    if ($l) {
                        $l->makePDF($ldeFicSrc, $ldePDFdst, 1);
                    }

                    $documentPDF = $pdfFileName;
                }
            }
        }

        //Le montant en centimes
        if ($debit > 0)
            $montant = $debit * 100;
        else
            $montant = $credit * 100;

        $l = "M";
        //Num de compte
        $l .= $this->str_sub_pad($compteNum, 8);
        //Code journal 2 char
        $l .= $this->str_sub_pad("NDF", 2); //Oui ça coupera a 2
        //Folio
        $l .= $this->str_sub_pad("000", 3);
        //Date
        $l .= $this->str_sub_pad($ladate->format("dmy"), 6);
        //Code libelle
        $l .= $this->str_sub_pad("", 1);
        // Libelle libre
        $l .= $this->str_sub_pad(preg_replace('!\s+!', ' ', "NDF-" . $this->_initials . " " . $compteLib), 20);
        //Debit ou Credit
        if ($debit > 0) {
            $l .= $this->str_sub_pad("D", 1);
            $this->_totalDebit += round($debit * 100, 0);
        } else {
            $l .= $this->str_sub_pad("C", 1);
            $this->_totalCredit += round($credit * 100, 0);
        }
        //Montant en centimes signé
        $l .= $this->str_sub_pad("+" . str_pad(str_replace('.', ',', $montant), 12, "0", STR_PAD_LEFT), 13);
        //Compte de contrepartie
        $l .= $this->str_sub_pad("", 8);
        $l .= $this->str_sub_pad("", 6);
        $l .= $this->str_sub_pad("", 2);
        //
        $l .= $this->str_sub_pad("", 3);
        $l .= $this->str_sub_pad("", 5);
        $l .= $this->str_sub_pad("", 10);
        $l .= $this->str_sub_pad("", 10);
        $l .= $this->str_sub_pad("", 8);
        $l .= $this->str_sub_pad("", 3);
        //Code journal sur 3 lettres
        $l .= $this->str_sub_pad("NDF", 3);
        $l .= $this->str_sub_pad("", 1);
        $l .= $this->str_sub_pad("", 1);
        $l .= $this->str_sub_pad("", 1);

        //Libellé écriture: si on a TVA alors on ajoute le libelle
        if (Str::contains($compteLib, "TVA")) {
            //update  avril 2020 on prefere (plus facile à lire, ça regroupe les ecritures visuellement)
            // BLABLABLA TVA 10% plutot que
            // TVA 10 % BLABLABL
            //calcul de la longueur dispo pour le blablabla
            $long = 30 - strlen("NDF-" . $this->_initials . " " . $compteLib . " ");
            $libelle = substr($ecritureLib, 0, $long);
            $l .= $this->str_sub_pad(preg_replace('!\s+!', ' ', "NDF-" . $this->_initials . " " . $libelle . " " . $compteLib), 30);
        } else {
            $l .= $this->str_sub_pad(preg_replace('!\s+!', ' ', "NDF-" . $this->_initials . " " . $ecritureLib), 30);
        }
        //Code TVA
        $l .= $this->str_sub_pad("", 2);
        //Num de piece
        $l .= $this->str_sub_pad_left($ecritureNum, 10);
        //Réservé
        $l .= $this->str_sub_pad("", 10);
        //Montant dans la devise
        $l .= $this->str_sub_pad("", 13);
        //Piece jointe (en 8.3 yahoo)
        $l .= $this->str_sub_pad($documentPDF, 12);
        //Quantité2
        $l .= $this->str_sub_pad("", 10);
        //NumUniq
        $l .= $this->str_sub_pad("", 10);
        //Code operateur
        $l .= $this->str_sub_pad("", 4);
        //Date systeme
        $l .= $this->str_sub_pad("", 14);
        $this->_content .= "$l\r\n";
        $this->_nbLines++;
        $this->_lastEcritNum = $ecritureNum;
    }

    public function getFullFileName()
    {
        Log::debug("ExportQuadratus::getFullFileName :: " . $this->_justifsPath . "/" . $this->_zipFileName);
        return $this->_justifsPath . "/" . $this->_zipFileName;
    }

    /**
     * checkBalanced : vérifie si le fichier est équilibré
     *
     * @return true/false
     */
    public function checkBalanced()
    {
        Log::debug("Exports::Quadratus::checkBalanced");

        //La longueur des colonnes du format quadratus ...
        $quadraCols = array(1, 8, 2, 3, 6, 1, 20, 1, 13, 8, 6, 2, 3, 5, 10, 10, 8, 3, 3, 1, 1, 1, 30, 2, 10, 10, 13, 12, 10, 10, 4, 14);

        $totalCredit = 0;
        $totalDebit = 0;
        $fp = fopen($this->_directory . "/" . $this->_filename, 'r');
        $tab = [];
        try {
            if ($fp) {
                while (($ligne = fgets($fp, 4096)) !== false) {
                    $ligneTab = [];
                    for ($i = 0, $pos = 0; $i < count($quadraCols); $i++) {
                        $long = $quadraCols[$i];
                        $ligneTab[$i] = substr($ligne, $pos, $long);
                        $pos += $long;
                    }

                    Log::debug("Exports::Quadratus::checkBalanced : " . $ligneTab[8]);
                    if ($ligneTab[7] == "C") {
                        $totalCredit += $ligneTab[8];
                    }
                    if ($ligneTab[7] == "D") {
                        $totalDebit += $ligneTab[8];
                    }
                }
            }
        } catch (Exception $e) {
            Log::debug("Exports::Quadratus::checkBalanced exception reçue : " .  $e->getMessage());
        }
        Log::debug("Exports::Quadratus::checkBalanced Total Débit : $totalDebit || Total Crédit : $totalCredit");
        if ($totalDebit == $totalCredit) {
            return true;
        }
        //Alerte par mail ?
        else {
            Log::debug("Exports::Quadratus::checkBalanced non équilibré, on envoie un mail d'urgence au SAV ...");
            $details = array(
                'to' => config('mail.sav'),
                'subject' => "[" . config('app.name') . "] Erreur, export des comptes déséquilibré",
                'message' => "À vérifier de toute urgence: Exports::Quadratus::checkBalanced\n\n" .
                    "Total débit  : $totalDebit\n" .
                    "Total crédit : $totalCredit\n" .
                    "User : " . $this->_user->firstname . " " . $this->_user->lastname . "\n" .
                    "Initiales : " . $this->_initials . "\n" .
                    "FileName : " . $this->_filename . "\n" .
                    "FileNameNDF : " . $this->_filenameNDF . "\n" .
                    "Directory : " . $this->_directory . "\n" .
                    "EndOfMonth : " . $this->_endOfMonth . "\n" .
                    "NbLines : " . $this->_nbLines . "\n" .
                    "Content : \n\n" . $this->_content . "\n" .
                    "\n\n--\n" . config('app.url')
            );

            ProcessSendEmail::dispatch($details);
            return false;
        }
        //Ramasse miette (ne devrait jamais être atteint)
        return true;
    }
}
