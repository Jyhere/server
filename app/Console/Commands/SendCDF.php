<?php
/*
 * SendCDF.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Http\Controllers\NdeFraisController;
use App\Http\Controllers\CdeFraisController;
use Illuminate\Support\Facades\Log;
use App\LdeFrais;
use App\User;
use App\Entreprise;
use App\Epref;
use Illuminate\Support\Carbon;

class SendCDF extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'doliscan:sendCdf
                            {--entrepriseID= : id de l\'entreprise}
                            {--ladate= : date de fin de période des NDF au format yyyy-mm-jj}
                            {--email= : pour forcer le destinataire du mail, si vide pas de mail}
                            {--auto : pour envoyer tous les classeurs à toutes les entreprises qui sont configurées pour}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send all last NDF of a company grouped in one big file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //TODO
        $retour = 0;

        $erreur = "";
        if ($this->option('auto')) {
            $nb = 1;
            //On est en mode pilote automatique on cherche donc toutes les entreprises qui ont l'option CDF
            $entreprises = Epref::all();
            // print_r($entreprises);
            $cdf = new CdeFraisController();
            foreach ($entreprises as $entreprise) {
                $eid = $entreprise->entreprise_id;
                $mail = $entreprise->compta_global_ndf_target;
                $endofmonth = new Carbon('last day of last month');
                $ladate = $endofmonth->format("Y-m-d");
                Log::debug("Cron::SendCDF: on cherche pour entreprise $eid du mois de $ladate et on envoie à $mail");
                echo "$nb : Cron::SendCDF: on cherche pour entreprise $eid du mois de $ladate et on envoie à $mail\n";
                $cdf->SendCdeFrais($eid, $ladate, $mail);
                $nb++;
            }
        } else {

            if ($this->option('ladate') == "") {
                $erreur .= "  * Il faut donner une date ! (--ladate=)\n";
            }
            if ($this->option('entrepriseID') == "") {
                $erreur .= "  * Il faut donner l'id de l'entreprise pour laquelle vous voulez le ClasseurDeFrais ! (--entrepriseID=)\n";
            }
            if ($this->option('email') == "") {
                $erreur .= "  * Il faut donner l'adresse mail à laquelle vous voulez recevoir le classeur ! (--email=)\n";
            }
            if ($erreur != "") {
                Log::debug("Cron::SendCDF: ERREUR : $erreur");
                echo "Erreur:\n";
                echo "=======\n";
                echo $erreur;
                exit - 1;
            }
            $cdf = new CdeFraisController();
            // $e = Entreprise::with('eprefs')->findOrFail($this->option('entrepriseID'));
            // Log::debug($e);
            $cdf->SendCdeFrais(
                $this->option('entrepriseID'),
                $this->option('ladate'),
                $this->option('email')
            );
        }
        return $retour;
    }
}
