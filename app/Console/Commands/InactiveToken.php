<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class InactiveToken extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'passport:inactive';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove all inactive tokens';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $date = Carbon::now()->subDays(config('passport.inactive_token_lifetime')); // On calcule la date limite d'inactivité à partir d'aujourd'hui
        Log::debug(" Purge des tokens inactifs depuis le " . $date->format('d/m/Y h:i:s'));
        DB::table('oauth_access_tokens')->where('last_connexion_at', '<', $date)->delete();
    }
}
