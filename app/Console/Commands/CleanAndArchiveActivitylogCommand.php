<?php
/*
 * CleanAndArchiveActivitylogCommand.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Storage;
use Spatie\Activitylog\ActivitylogServiceProvider;

class CleanAndArchiveActivitylogCommand extends Command
{
    protected $signature = 'activitylog:cleanAndArchive
                            {log? : (optional) The log name that will be cleaned.}
                            {--days= : (optional) Records older than this number of days will be cleaned.}';

    protected $description = 'Dump old logs to activity.log then clean up from the database.';

    public function handle()
    {
        $this->comment('Cleaning activity log...');

        $log = $this->argument('log');

        $maxAgeInDays = $this->option('days') ?? config('activitylog.delete_records_older_than_days');

        $cutOffDate = Carbon::now()->subDays($maxAgeInDays)->format('Y-m-d H:i:s');

        $activity = ActivitylogServiceProvider::getActivityModelInstance();

        $toDelete = $activity::where('created_at', '<', $cutOffDate)
            ->when($log !== null, function (Builder $query) use ($log) {
                $query->inLog($log);
            })->get();

        $endofmonth = new Carbon(Carbon::now()->subDay($maxAgeInDays)->lastOfMonth());
        $ladate = $endofmonth->format("Y-m-d");
        $logFile = "logs/archives-activity-$ladate.log";
        $this->comment('Store old activity log to ' . $logFile);

        $amountDeleted = 0;
        $bigtext = "";
        foreach ($toDelete as $a) {
            // $this->info("Pivot on {$a} record...");
            $ip = "127.0.0.1";
            if (isset($a->properties['ip']))
                $ip = $a->properties['ip'];
            $bigtext .= $a->created_at . " $ip " . $a->log_name . " $a";

            $amountDeleted++;
            if ($amountDeleted % 100) {
                $this->output->write('.', false);
            } else {
                $this->output->write(" [$amountDeleted]", true);
            }
        }

        Storage::append("logs/archives-activity-$ladate.log", $bigtext);
        $this->output->write('', true);
        $this->info("Need to deleted {$amountDeleted} record(s) from the activity log...");

        //do it in one sql request cf original code
        $amountDeleted = $activity::where('created_at', '<', $cutOffDate)
            ->when($log !== null, function (Builder $query) use ($log) {
                $query->inLog($log);
            })
            ->delete();

        $this->info("Deleted {$amountDeleted} record(s) from the activity log.");

        $this->comment('All done!');
    }
}
