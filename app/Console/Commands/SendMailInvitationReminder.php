<?php
/*
 * sendMailInvitationReminder.php
 *
 * Copyright (c) 2019-2021 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Console\Commands;

use App\Http\Controllers\Controller;
use Illuminate\Console\Command;
use App\User;
use App\NdeFrais;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class sendMailInvitationReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'doliscan:sendMailInvitationReminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send a mail reminder for new users (invitation not yet activated)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $retour = 0;
        Log::debug("Cron::sendMailInvitationReminder");

        //Modification du principe de fonctionnement : on regarde quels sont les utilisateurs qui ont le mot de passe à xxxxxx
        //et ensuite on vérifie si une procédure de demande de nouveau mot de passe n'est pas déjà en cours
        $us = User::where('password', '=', 'xxxxxx')->get();
        foreach ($us as $u) {
            $msg = "";
            $msg .= " sendMailInvitationReminder vérification pour " . $u->email . " ";
            $verif = DB::table(config('auth.passwords.users.table'))->select(['email'])
                ->where('email', '=', $u->email)
                ->where('created_at', '>', Carbon::now()->subHours(24)->toDateTimeString())
                ->distinct()->get();
            if ($verif->count() == 0) {
                //Aucune procédure n'est engagée alors on lance un rappel ... peut-être pas si c'est un compte qui n'a pas cliqué sur un lien depuis
                //5 relances ?
                $verifSec = DB::table(config('auth.passwords.users.table'))->select(['email'])
                    ->where('email', '=', $u->email)
                    ->distinct()->get();
                if ($verifSec->count() == 0) {
                    $msg .= "(" . $u->created_at . ") a envoyer: ";
                    $msg .= $u->envoyerMailInvitation(1);
                    $msg .= "\n";
                } else {
                    $msg .= "désactivé : vieux compte qui a déjà reçu " . $verifSec->count() . " invitations !";
                }
            } else {
                $msg .= " pas nécessaire : invitation envoyée il y a moins de 24h";
            }
            echo $msg . "\n";
        }

        //Ancienne idée
        //On regarde ce qu'on a dans la table password_resets et on limite a -7 jours
        // $comptes = DB::table(config('auth.passwords.users.table'))->select(['email'])
        //     ->whereBetween(
        //         'created_at',
        //         [Carbon::now()->subDays(7)->toDateTimeString(), Carbon::now()->subHours(24)->toDateTimeString()]
        //     )
        //     ->distinct()->get();

        // foreach ($comptes as $compte) {
        //     Log::debug("Cron::sendMailInvitationReminder: verif sendMailInvitationReminder pour " . $compte->email);
        //     //On verifie donc si le compte est récent, ie créé il y a moins de 7 jours et dont l'adresse mail n'a pas été vérifée
        //     $u = User::where('email', '=', $compte->email)
        //         ->where('created_at', '>', Carbon::now()->subDays(7)->toDateTimeString())
        //         ->whereNull('email_verified_at')
        //         ->where('password', '=', 'xxxxxx')
        //         ->first();
        //     if ($u) {
        //         echo " sendMailInvitationReminder pour " . $compte->email . "(" . $compte->created_at . ") a envoyer: \n";
        //         echo $u->envoyerMailInvitation(1);
        //         echo "\n";
        //     } else {
        //         echo " sendMailInvitationReminder pour " . $compte->email . " pas nécessaire\n";
        //     }
        // }

        return $retour;
    }
}
