<?php
/*
 * RebuildCDF.php
 *
 * Copyright (c) 2019-2021 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Http\Controllers\NdeFraisController;
use App\Http\Controllers\CdeFraisController;
use Illuminate\Support\Facades\Log;
use App\LdeFrais;
use App\User;
use App\Entreprise;
use App\Epref;
use Illuminate\Support\Carbon;

class RebuildCDF extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'doliscan:rebuildCDF
                            {--entrepriseID= : id de l\'entreprise}
                            {--ladate= : date de fin de période des NDF au format yyyy-mm-jj}
                            {--auto : pour reconstruire tous les classeurs de toutes les entreprises qui sont configurées pour}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Rebuild ZIP for last NDF of a company grouped in one big file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //TODO
        $retour = 0;

        $erreur = "";
        if ($this->option('auto')) {
            $nb = 1;
            //On est en mode pilote automatique on cherche donc toutes les entreprises qui ont l'option CDF
            $entreprises = Epref::all();
            // print_r($entreprises);
            $cdf = new CdeFraisController();
            foreach ($entreprises as $entreprise) {
                $eid = $entreprise->entreprise_id;
                $endofmonth = new Carbon('last day of last month');
                $ladate = $endofmonth->format("Y-m-d");
                Log::debug("Cron::RebuildCDF: on cherche pour entreprise $eid du mois de $ladate ");
                echo "$nb : Cron::RebuildCDF: on cherche pour entreprise $eid du mois de $ladate \n";
                $cdf->BuildCdeFrais($eid, $ladate);
                $nb++;
            }
        } else {
            if ($this->option('ladate') == "") {
                $erreur .= "  * Il faut donner une date ! (--ladate=)\n";
            }
            if ($this->option('entrepriseID') == "") {
                $erreur .= "  * Il faut donner l'id de l'entreprise pour laquelle vous voulez le ClasseurDeFrais ! (--entrepriseID=)\n";
            }
            if ($erreur != "") {
                Log::debug("Cron::RebuildCDF: ERREUR : $erreur");
                echo "Erreur:\n";
                echo "=======\n";
                echo $erreur;
                exit - 1;
            }
            $cdf = new CdeFraisController();
            $cdf->BuildCdeFrais(
                $this->option('entrepriseID'),
                $this->option('ladate')
            );
        }
        return $retour;
    }
}
