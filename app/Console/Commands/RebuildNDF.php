<?php
/*
 * RebuildNDF.php
 *
 * Copyright (c) 2019-2021 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Console\Commands;

use App\Http\Controllers\Controller;
use Illuminate\Console\Command;
use App\User;
use App\NdeFrais;
use App\Http\Controllers\NdeFraisController;
use App\LdeFrais;
use Illuminate\Support\Facades\Log;

class RebuildNDF extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'doliscan:rebuildNDF
                            {id? : (optional) The ndf id to rebuild.}
                            {--force : (optional) Force action.}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Rebuild PDF for all Closed NDF for this month (month ended)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //TODO
        $retour = 0;

        $ndfId = $this->argument('id') ?? 0;
        $force = $this->option('force') ?? false;

        if ($ndfId > 0) {
            Log::debug("Cron::RebuildNDF: Rebuild de la Note de Frais #$ndfId ($force)");
            echo "Rebuild de la Note de Frais #" . $ndfId . " ($force)\n";
            $ndf = NdeFrais::findOrFail($ndfId);
            if ($ndf) {
                $ndfC = new NdeFraisController();
                $ndfC->webBuildPDF($ndf->id, "", "", $force);
                $ndfC->webBuildPDFJustificatifs($ndf->id, "", "", $force);
            }
            return $retour;
        }

        //
        $users = User::all();
        $nb = 1;
        foreach ($users as $user) {
            // echo $user;
            $ndf = $user->NdeFrais->sortBy('fin')->where('status', NdeFrais::STATUS_CLOSED)->last();
            if ($ndf) {
                Log::debug("======================================================================================================");
                Log::debug("=                                                                                                    =");
                Log::debug("=                                                                                                    =");
                Log::debug("=     RebuildNDF::handle - Rebuild PDF de la Note de Frais #" . $ndf->id . " de " . $user->email);
                Log::debug("=                                                                                                    =");
                echo "$nb : Rebuild du PDF de la Note de Frais #" . $ndf->id . " de " . $user->email . "\n";
                $ndfC = new NdeFraisController();
                $ndfC->webBuildPDF($ndf->id, "", "", $force);
                $ndfC->webBuildPDFJustificatifs($ndf->id, "", "", $force);
                $nb++;
            }
        }
        return $retour;
    }
}
