<?php
/*
 * SendMailNewVersion.php
 *
 * Copyright (c) 2019-2021 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Console\Commands;

use App\Http\Controllers\Controller;
use Illuminate\Console\Command;
use App\User;
use App\NdeFrais;
use Illuminate\Support\Facades\Mail;
use App\Mail\MailNewVersion;
use Illuminate\Support\Facades\Log;

class SendMailNewVersion extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'doliscan:SendMailNewVersion
                            {--mail=/tmp/mail.txt : le fichier texte du mail à envoyer}';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send a mail to announce a new version';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //TODO
        $retour = 0;
        Log::debug("Cron::SendMailNewVersion");

        //
        $users = User::all();
        foreach ($users as $user) {
            Log::debug("Cron::SendMailNewVersion: SendMailNewVersion pour " . $user->email);
            echo " SendMailNewVersion pour " . $user->email . ": ";
            Mail::to($user->email)
                ->bcc(config('mail.notifications'))
                ->send(new MailNewVersion());
            activity('Mail')->log("Info new version notification for " . $user->email);
            echo "sleep for mail server rate limit\n";
            sleep(config('mail.sleep'));

            // $ndf = $user->NdeFrais->sortBy('fin')->where('status', NdeFrais::STATUS_OPEN)->last();
            // // Une NDF en cours -> un mail d'information sinon
            // if ($ndf) {
            //     Log::debug("Cron::SendMailNewVersion: expédition de " . $ndf->label);
            //     echo "expédition de " . $ndf->label . " (" . $this->option('etape') . ")\n";
            //     Mail::to($user->email)
            //         ->bcc(config('mail.notifications'))
            //         ->send(new MailReminder($this->option('etape')));
            //     echo "sleep for mail server rate limit\n";

            //     activity('Mail')->log("Cron: Notification pour " . $user->email);

            //     sleep(config('mail.sleep'));
            // }
            // else {
            //     Log::debug("Cron::SendMailNewVersion: pas de NDF pour " . $user->email) . " donc pas de mail :)";
            // }
        }
        return $retour;
    }
}
