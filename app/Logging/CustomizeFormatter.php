<?php

namespace App\Logging;

use Monolog\Formatter\LineFormatter;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;

class CustomizeFormatter
{
    /**
     * Customize the given logger instance.
     *
     * @param  \Illuminate\Log\Logger  $logger
     * @return void
     */
    public function __invoke($logger)
    {
        //ajouter peut être  | contexte: %context% | extra : %extra% a la fin du formatter
        $h = Request::getClientIp();
        $u = Auth::user() ? Auth::user()->email : '-';
        $dateFormat = "Y-m-d H:i:s";
        foreach ($logger->getHandlers() as $handler) {
            $handler->setFormatter(new LineFormatter(
                "$h $u [%datetime%] %channel%.%level_name%: %message%\n"
            ,$dateFormat));
        }
    }
}
