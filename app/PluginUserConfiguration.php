<?php
/*
 * PluginUserConfiguration.php
 *
 * Copyright (c) 2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App;

use Illuminate\Database\Eloquent\Model;
use Propaganistas\LaravelFakeId\RoutesWithFakeIds;
use Spatie\Activitylog\Traits\LogsActivity;
use App\User;
use App\Plugin;
use Illuminate\Support\Facades\Log;
use stdClass;

class PluginUserConfiguration extends Model
{
    use RoutesWithFakeIds;

    use LogsActivity;
    protected static $logName = 'PluginUserConfiguration';
    protected static $logAttributes = ['*'];
    protected static $logAttributesToIgnore = ['updated_at'];
    protected static $logOnlyDirty = true;
    protected static $submitEmptyLogs = false;

    //Tout le contenu de la configuration est laissé au plugin, nous on a uniquement un json
    protected $fillable = ['user_id', 'plugin_id', 'config_value'];
    protected $dates = ['created_at', 'deleted_at'];
    // protected $hidden = ['id'];
    // protected $guarded = ['id'];
    protected $configJson;

    protected $casts = [
        'config_value' => 'array',
    ];

    public function __construct(array $attributes = array())
    {
        $this->configJson = new stdClass;
        parent::__construct($attributes);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function plugin()
    {
        return $this->belongsTo(Plugin::class);
    }

    /**
     * retourne le status du plugin pour cet utilisateur
     *
     * @return  [type]  [return description]
     */
    public function status()
    {
        $config = $this->config_value;
        return $config['status'];
    }

    /**
     * (de)active le module
     *
     * @param   [type]  $onOff  [$onOff description]
     *
     * @return  [type]          [return description]
     */
    public function setStatus($onOff) {
        Log::debug("PluginUserConfiguration::setStatus $onOff...");
        $config = $this->config_value;
        $config['status'] = $onOff;
        $this->config_value = $config;
        return true;
    }

    public function save(array $options = array())
    {
        Log::debug("PluginUserConfiguration: On sauvegarde...");
        // Log::debug($this->configJson);
        // Log::debug("PluginUserConfiguration: On sauvegarde...end");
        // $this->config_value = 
        return parent::save();
    }
}
