<?php
/*
 * Helpers.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

if (!function_exists('nbFR')) {
    /**
     * return french number format
     *
     * @param  float $nb Number
     * @return string
     */
    function nbFR($nb)
    {
        return number_format($nb,2,',','.');
    }
}

if (!function_exists('dateFR')) {
    /**
     * return french date from mysql format
     *
     * @param  string $ladate Date
     * @return string
     */
    function dateFR($ladate)
    {
        return date("d/m/Y ", strtotime($ladate));
    }
}
