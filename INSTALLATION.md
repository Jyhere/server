# INSTALLATION


Préparation à l'installation d'un serveur DoliSCAN


## Pré-requis


Un système type AMP est nécessaire, en quelques lignes depuis une distribution debian :

```
PHPVER=7.4
apt install apache2 apache2-bin apache2-data apache2-utils libapache2-{mod-evasive,mod-geoip,mod-rpaf,mod-xsendfile}
apt install mariadb-client mariadb-common mariadb-server
apt install libapache2-mod-php${PHPVER} php-{bz2,curl,gd,intl,json,mbstring,mysql,xml,zip,imagick} php${PHPVER}-{apcu,apcu-bc,bcmath,bz2,cli,common,curl,gd,gmp,imagick,intl,json,mbstring,mysql,opcache,readline,xml,zip}
apt install jpegoptim optipng pngquant gifsicle webp
apt install npm composer git
```

## Récupérez les sources


```
cd /srv/webs
git clone https://framagit.org/doliscan/server.git doliscan.example.com -b prod
cd doliscan.example.com
touch storage/oauth-public.key #generation du vrai fichier de cle plus tard
composer install --no-interaction --prefer-dist
npm install
npm run prod
npm update
```

## Apache 2 - ou Serveur Interne


Laravel propose une implémentation de serveur interne que vous pourrez lancer à la fin à l'aide de la commande

```
php artisan serve
```

Mais si vous voulez utiliser apache (pour une installation en prod par exemple), c'est du super classique, rien de particulier à noter, vous trouverez ci-dessous un exemple de virtualhost fonctionnel:

```
<VirtualHost *:80>
     ServerName doliscan.example.com
     ServerAdmin contact@example.com

     DocumentRoot /srv/webs/doliscan.example.com/public
     LogLevel warn
     ErrorLog /var/log/apache2/doliscan.example.com-error.log
     CustomLog /var/log/apache2/doliscan.example.com-access.log combined
     <Directory /srv/webs/doliscan.example.com/public/>
       AllowOverride All
       Options Indexes FollowSymLinks MultiViews
       Require all granted
    </Directory>
    #largement conseillé en prod ...
    #Redirect / https://doliscan.example.com/
</VirtualHost>

```

### Apache 2 - modules

```
for module in alias autoindex mime rewrite headers env setenvif
do
  a2enmod ${module}
done
```


## MariaDB / MySQL


Configurez votre serveur de base de données comme d'habitude, notez bien vos identifiants & mots de passes...


## Configurez le fichier .env


```
cp .env.example .env
```

Editez le fichier .env et renseignez les différents champs, en particulier ceux-ci:


```

.../...

APP_DEBUG="true"
APP_URL="http://localhost"
APP_DOMAIN="doliscan.alpha.devtemp.fr"

ACTIVITY_LOGGER_ENABLED="true"
LOG_CHANNEL="single"

NODE_ENV="development"

#configuration mysql
DB_CONNECTION="mysql"
DB_HOST="127.0.0.1"
DB_PORT="3306"
DB_DATABASE="doliscan"
DB_USERNAME="xxxxxxxxx"
DB_PASSWORD="xxxxxxxxx"

#ou sqlite ()
#DB_CONNECTION="sqlite"
#DB_DATABASE="doliscan.sqlite"

# pour capter tous les mails sortants et les envoyer au développeur (mettre la ligne en commentaire en production)
MAIL_DEVMODE_TO="moi@devtemp.fr"

# adresse mail qui reçoit les notifications de creation de compte etc.
MAIL_NOTIFICATIONS="suivi@devtemp.fr"
MAIL_DRIVER="smtp"
MAIL_HOST="xxxxxxxxxxxxx"
MAIL_PORT="587"
MAIL_USERNAME="xxxxxxxxxxxxxxxx"
MAIL_PASSWORD="xxxxxxxxxxxxxxxx"
MAIL_ENCRYPTION="tls"
MAIL_FROM_ADDRESS="xxxxxxxxxxx"

.../...

```


## Finalisation de l\'installation


```
mkdir storage/framework/{sessions,views}

#une clé d'application (APP_KEY du fichier .env)
php artisan key:generate

#si sqlite
#touch database/doliscan.sqlite

php artisan migrate --force

#une clé de crypto pour passport (cf le touch storage/oauth-public.key)
php artisan passport:keys --force

php artisan optimize
php artisan config:clear

#si apache:
#chown www-data:www-data storage -R
```

Et c'est normalement tout ...

## Configuration sécurité pour fail2ban

Si vous trouvez des scan de failles dans vos fichiers logs comme par exemple:


```
.../... "GET /.env HTTP/1.1" .../...
.../... "GET /bag2 HTTP/1.1" .../...
.../... "GET /ReportServer HTTP/1.1" .../...
.../... "POST /mifs/.;/services/LogService HTTP/1.1" .../...
.../... "GET /wp-content/plugins/wp-file-manager/readme.txt HTTP/1.1" .../...
.../... "POST /vendor/phpunit/phpunit/src/Util/PHP/eval-stdin.php HTTP/1.1" .../...
```

Et que vous pouvez mettre en place fail2ban sur votre serveur (voir même s'il tourne déjà), tout est prêt: DoliSCAN Serveur apporte
des règles de sécurité fail2ban prêtes à l'emploi !

Copiez les fichiers fail2ban/ dans votre arborescence fail2ban, modifiez le chemin du fichier log, changez éventuellement la durée
du ban, relancez fail2ban et ça devrait marcher tout seul !

## The end ...


Indiquez nous ce qui manque et ce qui reste à écrire: sav _at_ doliscan.fr

__Note:__ voir la doc web https://doliscan.fr/docs/

