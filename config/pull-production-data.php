<?php

return [
    'deploy_server' => env('DEPLOY_SERVER'),
    'deploy_path' => env('DEPLOY_PATH'),

    'timeout' => 900, // Set default to 15 minutes for larger database or storage size

    'database_connection' => 'mysql',
    // 'database_connection' => config('database.default'),

    'paths' => [
        'mysql' => '/usr/bin/mysql', // Path to mysql binary on your local machine
        'mysqldump' => '/usr/bin/mysqldump', // Path to mysqldump binary on remote server
        'env' => 'doliscan/', // Path to the .env file relative to the SSH user's home directory
        //note super importante eric: on est donc contraint de faire un symlink ~/doliscan qui pointe vers le
        //répertoire de l'appli !
    ],
];
