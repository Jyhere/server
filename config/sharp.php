<?php
/*
 * sharp.php
 *
 * Copyright (c) 2019-2020 Eric Seigne <eric.seigne@cap-rel.fr>
 *
 *This program is free software: you can redistribute it and/or modify
 *it under the terms of the GNU Affero General Public License as
 *published by the Free Software Foundation, either version 3 of the
 *License, or (at your option) any later version.
 *
 *This program is distributed in the hope that it will be useful,
 *but WITHOUT ANY WARRANTY; without even the implied warranty of
 *MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *GNU Affero General Public License for more details.
 *
 *You should have received a copy of the GNU Affero General Public License
 *along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

return [

    // Required. The name of your app, as it will be displayed in Sharp.
    "name" => "Administration de DoliSCAN",
    "extensions" => [
        "assets" => [
            "strategy" => "mix",
            "head"     => [
                "/css/app.css",
            ],
        ],
        "activate_custom_fields" => env("SHARP_CUSTOM_FORM_FIELDS", true),
    ],

    // Optional. You can here customize the URL segment in which Sharp will live. Default in "sharp".
    "custom_url_segment" => "admindoli",

    // Optional. You can prevent Sharp version to be displayed in the page title. Default is true.
    "display_sharp_version_in_title" => false,

    // Required. Your entities list; each one must define a "list",
    // and can define "form", "validator", "policy" and "authorizations".
    "entities" => [
        "entreprise" => [
            "list" => \App\Sharp\EntrepriseSharpList::class,
            "form" => \App\Sharp\EntrepriseSharpForm::class,
            "validator" => \App\Sharp\EntrepriseSharpValidator::class,
            "policy" => \App\Sharp\Policies\EntreprisePolicy::class,
        ],
        "user" => [
            "list" => \App\Sharp\UserSharpList::class,
            "form" => \App\Sharp\UserSharpForm::class,
            "validator" => \App\Sharp\UserSharpValidator::class,
            "policy" => \App\Sharp\Policies\UserPolicy::class,
        ],
        //oAuth
        "client" => [
            "list" => \App\Sharp\PassClientSharpList::class,
            "form" => \App\Sharp\PassClientSharpForm::class,
            "validator" => \App\Sharp\PassClientSharpValidator::class,
            // "policy" => \App\Sharp\Policies\ClientPolicy::class,
        ],
        "token" => [
            "list" => \App\Sharp\PassTokenSharpList::class,
            "form" => \App\Sharp\PassTokenSharpForm::class,
            "validator" => \App\Sharp\PassTokenSharpValidator::class,
            // "validator" => \App\Sharp\PassTokenSharpValidator::class,
            // "policy" => \App\Sharp\Policies\PassTokenPolicy::class,
        ],
        //Notes de frais
        "ndeFrais" => [
            "list" => \App\Sharp\NdeFraisSharpList::class,
            "show" => \App\Sharp\NdeFraisSharpShow::class,
            // "form" => \App\Sharp\NdeFraisSharpForm::class,
            // "validator" => \App\Sharp\NdeFraisSharpValidator::class,
            "policy" => \App\Sharp\Policies\NdeFraisPolicy::class,
        ],
        //Lignes de frais
        "ldeFrais" => [
            "list" => \App\Sharp\LdeFraisSharpList::class,
            "form" => \App\Sharp\LdeFraisSharpForm::class,
            // "validator" => \App\Sharp\NdeFraisSharpValidator::class,
            "policy" => \App\Sharp\Policies\LdeFraisPolicy::class,
        ],
        //Mon compte
        "account" => [
            "show" => \App\Sharp\AccountSharpShow::class,
            "form" => \App\Sharp\AccountSharpForm::class,
        ],
        //Customization de l'application
        "customizingApp" => [
            "list" => \App\Sharp\CustomizingAppSharpList::class,
            "policy" => \App\Sharp\Policies\CustomizingAppPolicy::class,
            "form" => \App\Sharp\CustomizingAppSharpForm::class,
        ],
        "defaultAccountsSettings" => [
            "show" => \App\Sharp\DefaultAccountsSettingsSharpShow::class,
            "policy" => \App\Sharp\Policies\UserPolicy::class,
            "form" => \App\Sharp\DefaultAccountsSettingsSharpForm::class,
            "validator" => \App\Sharp\DefaultAccountsSettingsValidator::class,
        ],
        //Logs
        "activity" => [
            "list" => \App\Sharp\ActivitySharpList::class,
            "form" => \App\Sharp\ActivitySharpForm::class,
            // "validator" => \App\Sharp\ActivitySharpValidator::class,
            "policy" => \App\Sharp\Policies\ActivityPolicy::class,
        ],
        "plugin" => [
            "list" => \App\Sharp\PluginSharpList::class,
            "form" => \App\Sharp\PluginSharpForm::class,
            // "validator" => \App\Sharp\PluginSharpValidator::class,
            "policy" => \App\Sharp\Policies\PluginPolicy::class,
        ],
        "billing" => [
            "list" => \App\Sharp\BillingSharpList::class,
            "form" => \App\Sharp\BillingSharpForm::class,
            "validator" => \App\Sharp\BillingSharpValidator::class,
            "policy" => \App\Sharp\Policies\BillingPolicy::class,
        ],
        "vehicule" => [
            "list" => \App\Sharp\VehiculeSharpList::class,
            "form" => \App\Sharp\VehiculeSharpForm::class,
            "validator" => \App\Sharp\VehiculeSharpValidator::class,
            "policy" => \App\Sharp\Policies\VehiculePolicy::class,
        ],
        "tagsFrais" => [
            "list" => \App\Sharp\TagsFraisSharpList::class,
            "form" => \App\Sharp\TagsFraisSharpForm::class,
            //"validator" => \App\Sharp\TagsFraisSharpValidator::class,
            "policy" => \App\Sharp\Policies\TagsFraisPolicy::class,
        ],
    ],

    // Optional. Your dashboards list; each one must define a "view", and can define "policy".
    "dashboards" => [
        "dashSuperAdmin" => [
            "view" => \App\Sharp\dashSuperAdmin::class,
            "policy" => \App\Sharp\Policies\dashSuperAdminPolicy::class,
        ],
        "dashRevendeur" => [
            "view" => \App\Sharp\dashRevendeur::class,
            "policy" => \App\Sharp\Policies\dashRevendeurPolicy::class,
        ],
        "dashResponsableEntreprise" => [
            "view" => \App\Sharp\dashResponsableEntreprise::class,
            "policy" => \App\Sharp\Policies\dashResponsableEntreprisePolicy::class,
        ],
        "dashAdminEntreprise" => [
            "view" => \App\Sharp\dashAdminEntreprise::class,
            "policy" => \App\Sharp\Policies\dashAdminEntreprisePolicy::class,
        ],
        "dashServiceComptabilite" => [
            "view" => \App\Sharp\dashServiceComptabilite::class,
            "policy" => \App\Sharp\Policies\dashServiceComptabilitePolicy::class,
        ],
        "dashUtilisateur" => [
            "view" => \App\Sharp\dashUtilisateur::class,
            "policy" => \App\Sharp\Policies\dashUtilisateurPolicy::class,
        ],
    ],

    // Optional. Your global filters list, which will be displayed in the main menu.
    "global_filters" => [
        //        "my_global_filter" => \App\Sharp\Filters\MyGlobalFilter::class
    ],

    // Required. The main menu (left bar), which may contain links to entities, dashboards
    // or external URLs, grouped in categories.
    "menu" => [
        [
            "label" => "Tableau de bord",
            "icon" => "fa-chart-line",
            "dashboard" => "dashSuperAdmin"
        ],
        [
            "label" => "Tableau de bord",
            "icon" => "fa-chart-line",
            "dashboard" => "dashRevendeur"
        ],
        [
            "label" => "Tableau de bord",
            "icon" => "fa-chart-line",
            "dashboard" => "dashResponsableEntreprise"
        ],
        [
            "label" => "Tableau de bord",
            "icon" => "fa-chart-line",
            "dashboard" => "dashAdminEntreprise"
        ],
        [
            "label" => "Tableau de bord",
            "icon" => "fa-chart-line",
            "dashboard" => "dashServiceComptabilite"
        ],
        [
            "label" => "Tableau de bord",
            "icon" => "fa-chart-line",
            "dashboard" => "dashUtilisateur"
        ],
        [
            "label" => "Configuration",
            "entities" => [
                [
                    "label" => "Mon compte",
                    "icon" => "fa-user",
                    "entity" => "account",
                    "single" => true
                ],
                [
                    "label" => "Étiquettes",
                    "icon" => "fa-tag",
                    "entity" => "tagsFrais"
                ],
                [
                    "label" => "Comptabilité",
                    "icon" => "fa-balance-scale",
                    "entity" => "defaultAccountsSettings",
                    "single" => true
                ],
                [
                    "separator" => true,
                    "label" => "Utilitaires",
                ],        
                [
                    "label" => "Extensions",
                    "icon" => "fa-magic",
                    "entity" => "plugin"
                ],
                [
                    "label" => "API - Applications",
                    "icon" => "fa-mobile-alt",
                    "entity" => "client"
                ],
                [
                    "label" => "API - Jetons",
                    "icon" => "fa-link",
                    "entity" => "token"
                ],
                [
                    "label" => "Éditeur de thème",
                    "icon" => "fa-paint-brush",
                    "entity" => "customizingApp",
                ],
                // [
                //     "label" => "Nouveau jeton",
                //     "icon" => "fa-plus-circle",
                //     "url" => "/token"
                // ],
                // [
                //     "label" => "Rafraichir un jeton",
                //     "icon" => "el-icon-refresh",
                //     "url" => "/refresh"
                // ],
                // [
                //     "label" => "Ajouter (PA)",
                //     "icon" => "fa-globe",
                //     "url" => "/personal-access"
                // ]
            ],
        ],
        // [
        //     "label" => "Mon entreprise",
        //     "icon" => "fa-building",
        //     "entity" => "entreprise",
        //     "single" => true
        // ],
        [
            "label" => "Journaux (logs)",
            "icon" => "fa-stream",
            "entity" => "activity",
        ],
        [
            "label" => "Entreprises",
            "icon" => "fa-building",
            "entity" => "entreprise"
        ],
        [
            "label" => "Utilisateurs",
            "icon" => "fa-users",
            "entity" => "user"
        ],
        [
            "label" => "Notes de frais",
            "icon" => "fa-file-alt",
            "entity" => "ndeFrais"
        ],
        [
            "label" => "Analytique",
            "icon" => "fa-list",
            "entity" => "ldeFrais"
        ],
        [
            "label" => "Vehicules",
            "icon" => "fa-car",
            "entity" => "vehicule"
        ],
        [
            "label" => "Facturation",
            "icon" => "fa-money",
            "entity" => "billing"
        ],

        //     ]
        // ],
        [
            "label" => "Retour",
            "icon" => "fa-home",
            "url" => "/home"
        ],
        [
            "label" => "Documentation",
            "icon" => "fa-globe",
            "url" => "/docs"
        ],
        // [
        //     "label" => "CAP-REL",
        //     "icon" => "fa-globe",
        //     "url" => "https://cap-rel.fr/"
        // ]

    ],
    // Optional. Your file upload configuration.
    "uploads" => [
        // Tmp directory used for file upload.
        "tmp_dir" => env("SHARP_UPLOADS_TMP_DIR", "tmp"),

        // These two configs are used for thumbnail generation inside Sharp.
        "thumbnails_disk" => env("SHARP_UPLOADS_THUMBS_DISK", "public"),
        "thumbnails_dir" => env("SHARP_UPLOADS_THUMBS_DIR", "thumbnails"),
    ],

    // Optional. Auth related configuration.
    "auth" => [
        // Name of the login and password attributes of the User Model.
        "login_attribute" => "email",
        "password_attribute" => "password",

        // Name of the attribute used to display the current user in the UI.
        "display_attribute" => "full_name",
        "display_sharp_version_in_title" => false,

        // Optional additional auth check.
        // "check_handler" => \App\Sharp\Auth\MySharpCheckHandler::class,

        // Optional custom guard -> comme ça on espere pouvoir capter l'authentification pour activity logs
        // "guard" => "",
    ],
    //    "login_page_message_blade_path" => env("SHARP_LOGIN_PAGE_MESSAGE_BLADE_PATH", "sharp/_login-page-message"),
];
